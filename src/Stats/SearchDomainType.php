<?php

namespace Project\Stats;

use Project\Check\Validator;
use Project\Stats\Search\DomainType\ByDomain;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Поиск по статистике
 */
abstract class SearchDomainType extends Search
{
    /** индикаторы */
    const INDICATORS = '
                COALESCE(SUM(rs), 0) as rs,
                COALESCE(SUM(rb), 0) as rb,
                COALESCE(SUM(rbq), 0) as rbq,
                COALESCE(SUM(rbqu), 0) as rbqu,
                COALESCE(SUM(usd), 0) as usd,
                COALESCE(SUM(scv), 0) as scv,
                COALESCE(SUM(tcv), 0) as tcv,
                COALESCE(SUM(prelv), 0) as prelv,
                COALESCE(SUM(lead), 0) as lead,
                COALESCE(SUM(lead_amount), 0) as lead_amount,
                COALESCE(SUM(accept), 0) as accept,
                COALESCE(SUM(accept_amount), 0) as accept_amount,
                COALESCE(SUM(decline), 0) as decline,
                COALESCE(SUM(decline_amount), 0) as decline_amount,
                COALESCE(SUM(invalid), 0) as invalid,
                COALESCE(SUM(invalid_amount), 0) as invalid_amount,
                COALESCE(round(SUM(accept_amount)::numeric / NULLIF(SUM(rs)::numeric, 0) * 1000, 2), 0) as cpm,
                COALESCE(round(SUM(scv)::numeric / NULLIF(SUM(rs)::numeric, 0) * 100, 2), 0) as ctr,
                COALESCE(round(SUM(accept_amount)::numeric / NULLIF(SUM(rb)::numeric, 0) * 1000, 2), 0) as cpmb,
                COALESCE(round(SUM(scv)::numeric / NULLIF(SUM(rb)::numeric, 0) * 100, 2), 0) as ctrb
    ';

    /** список свойств фильтов */
    const WHERE = [
        'dateFrom' => [
            'bind' => 'date_from',
            'stmt' => 'AND s.date >= :date_from ',
        ],
        'dateTo' => [
            'bind' => 'date_to',
            'stmt' => 'AND s.date <= :date_to ',
        ],
        'domain' => [
            'bind' => 'domain',
            'stmt' => 'AND s.domain LIKE :domain ',
            'replace' => '%<VALUE>%'
        ],
        'adType' => [
            'bind' => 'adtype',
            'stmt' => 'AND s.adtype = :adtype ',
        ],
    ];

    /**
     * фильтры
     */

    /** @var \DateTime */
    protected $dateFrom;

    /** @var \DateTime */
    protected $dateTo;

    /** @var string */
    protected $domain;

    /** @var  string */
    protected $adType;
    
    /**
     * Маппинг группировок
     */
    const GROUP_KEY_MAP = [
        'domaintype' =>ByDomain::class,
    ];

    /**
     * Сетеры фильтров
     */

    /**
     * @param \DateTime $dateFrom
     * @return $this
     */
    public function setDateFrom(\DateTime $dateFrom)
    {
        $this->dateFrom = $dateFrom;
        return $this;
    }

    /**
     * @param \DateTime $dateTo
     * @return $this
     */
    public function setDateTo(\DateTime $dateTo)
    {
        $this->dateTo = $dateTo;
        return $this;
    }

    /**
     * @param string $domain
     * @return $this
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;
        return $this;
    }

    /**
     * @return string
     */
    public function getAdType()
    {
        return $this->adType;
    }

    /**
     * @param string $adType
     * @return $this
     */
    public function setAdType($adType)
    {
        $this->adType = $adType;
        return $this;
    }
}
