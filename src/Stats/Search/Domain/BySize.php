<?php

namespace Project\Stats\Search\Domain;

use Project\Stats\SearchDomain;

/**
 * Группировка по размерам
 */
class BySize extends SearchDomain
{
    /**
     * @return string
     */
    protected function getSQLTpl()
    {
        return '
            SELECT
                s.width,
                s.height,
                ' . static::PH_INDICATORS . '
            FROM stats_domain as s
            WHERE ' . static::PH_WHERE . '
            GROUP BY 1, 2' .
        static::PH_ORDER .
        static::PH_LIMIT;
    }

    /**
     * @return string
     */
    protected function prepareOrderDefault()
    {
        return ' ORDER BY 1 DESC , 2 DESC';
    }
}
