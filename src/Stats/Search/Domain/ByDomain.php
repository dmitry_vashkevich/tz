<?php

namespace Project\Stats\Search\Domain;

use Project\Stats\SearchDomain;

/**
 * Группировка по доменам
 */
class ByDomain extends SearchDomain
{
    const KEY = 'domain';

    /**
     * @return string
     */
    protected function getSQLTpl()
    {
        return '
            SELECT
                s.' . static::KEY . ',
                ' . static::PH_INDICATORS . '
            FROM stats_domain as s
            WHERE ' . static::PH_WHERE . '
            GROUP BY ' . static::KEY .
        static::PH_ORDER .
        static::PH_LIMIT;
    }

    /**
     * @return string
     */
    protected function prepareOrderDefault()
    {
        return ' ORDER BY ' . static::KEY . ' DESC';
    }
}
