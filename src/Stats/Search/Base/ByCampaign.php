<?php

namespace Project\Stats\Search\Base;

/**
 * Группировка по кампании
 */
class ByCampaign extends OneField
{
    const KEY = 'campaign_id';

    /**
     * @inheritdoc
     */
    protected function getSQLTpl()
    {
        return '
            SELECT
                s.' . static::KEY . ',
                c.name,
                ' . static::PH_INDICATORS . '
            FROM stats_base as s INNER JOIN campaigns AS c ON s.campaign_id = c.id 
            WHERE ' . static::PH_WHERE . '
            GROUP BY ' . static::KEY . ', c.name' .
            static::PH_ORDER .
            static::PH_LIMIT;
    }
}
