<?php

namespace Project\Stats\Search\Base;

/**
 * Группировка по дате
 */
class ByDate extends OneField
{
    const KEY = 'date';
}
