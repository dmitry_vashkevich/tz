<?php

namespace Project\Stats\Search\Base;

/**
 * Группировка по TJBlock
 */
class ByTJBlock extends OneField
{
    const KEY = 'tj_block_id';

    /**
     * @inheritdoc
     */
    protected function getSQLTpl()
    {
        return '
            SELECT
                s.' . static::KEY . ',
                b.name,
                ' . static::PH_INDICATORS . '
            FROM stats_base as s INNER JOIN tj_blocks AS b ON s.tj_block_id = b.id 
            WHERE ' . static::PH_WHERE . '
            GROUP BY ' . static::KEY . ', b.name' .
        static::PH_ORDER .
        static::PH_LIMIT;
    }
}
