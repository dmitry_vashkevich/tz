<?php

namespace Project\Stats\Search\Base;

/**
 * Группировка по TJSite
 */
class ByTJSite extends OneField
{
    const KEY = 'tj_site_id';

    /**
     * @inheritdoc
     */
    protected function getSQLTpl()
    {
        return '
            SELECT
                s.' . static::KEY . ',
                b.name,
                ' . static::PH_INDICATORS . '
            FROM stats_base as s INNER JOIN tj_sites AS b ON s.tj_site_id = b.id 
            WHERE ' . static::PH_WHERE . '
            GROUP BY ' . static::KEY . ', b.name' .
        static::PH_ORDER .
        static::PH_LIMIT;
    }
}
