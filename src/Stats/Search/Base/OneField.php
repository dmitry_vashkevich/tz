<?php

namespace Project\Stats\Search\Base;

use Project\Stats\SearchBase;

/**
 * Стандартная группировка по 1-му ключу
 */
abstract class OneField extends SearchBase
{
    const KEY = 'abstract';
    
    /**
     * @inheritdoc
     */
    protected function getSQLTpl()
    {
        return '
            SELECT
                s.' . static::KEY . ',
                ' . static::PH_INDICATORS . '
            FROM stats_base as s
            WHERE ' . static::PH_WHERE . '
            GROUP BY ' . static::KEY .
            static::PH_ORDER .
            static::PH_LIMIT;
    }

    /**
     * @inheritdoc
     */
    protected function prepareOrderDefault()
    {
        return ' ORDER BY ' . static::KEY . ' DESC';
    }
}
