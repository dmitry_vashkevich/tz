<?php

namespace Project\Stats\Search\Base;

/**
 * Группировка по офферу
 */
class ByOffer extends OneField
{
    const KEY = 'offer_id';

    /**
     * @inheritdoc
     */
    protected function getSQLTpl()
    {
        return '
            SELECT
                s.' . static::KEY . ',
                o.name,
                ' . static::PH_INDICATORS . '
            FROM stats_base as s INNER JOIN offers AS o ON s.offer_id = o.id 
            WHERE ' . static::PH_WHERE . '
            GROUP BY ' . static::KEY . ', o.name' .
            static::PH_ORDER .
            static::PH_LIMIT;
    }
}
