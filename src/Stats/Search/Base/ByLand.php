<?php

namespace Project\Stats\Search\Base;

/**
 * Группировка по ленду
 */
class ByLand extends OneField
{
    const KEY = 'land_id';
}
