<?php

namespace Project\Stats\Search\Base;

/**
 * Группировка по стране
 */
class ByCountry extends OneField
{
    const KEY = 'country';
}
