<?php

namespace Project\Stats\Search\Base;

/**
 * Группировка по adtype
 */
class ByAdType extends OneField
{
    const KEY = 'adtype';
}
