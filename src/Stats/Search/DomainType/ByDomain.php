<?php

namespace Project\Stats\Search\DomainType;

use Project\Stats\SearchDomainType;

/**
 * Группировка по доменам
 */
class ByDomain extends SearchDomainType
{
    const KEY = 'domain';

    /**
     * @return string
     */
    protected function getSQLTpl()
    {
        return '
            SELECT
                s.' . static::KEY . ',
                ' . static::PH_INDICATORS . '
            FROM stats_domain_type as s
            WHERE ' . static::PH_WHERE . '
            GROUP BY ' . static::KEY .
        static::PH_ORDER .
        static::PH_LIMIT;
    }

    /**
     * @return string
     */
    protected function prepareOrderDefault()
    {
        return ' ORDER BY ' . static::KEY . ' DESC';
    }
}
