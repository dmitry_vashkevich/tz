<?php

namespace Project\Stats\Exception;

use Project\Exception\RestException\ServerError\InternalServerErrorException;

/**
 * Если свойство не подохдит под предполагаемую типизацию блока WHERE
 */
class UnknownWherePropertyType extends InternalServerErrorException
{

}
