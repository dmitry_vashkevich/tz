<?php

namespace Project\Stats\Exception;

use Project\Exception\RestException\ServerError\InternalServerErrorException;

/**
 * Если указан неизвестный ключ группировки статистики
 */
class UnknownGroupKey extends InternalServerErrorException
{

}
