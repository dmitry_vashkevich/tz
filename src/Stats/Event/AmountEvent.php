<?php

namespace Project\Stats\Event;

use Project\Stats\Event;

/**
 * Абстрактное событие статистики с суммой
 */
abstract class AmountEvent extends Event
{
    /**
     * Cумма
     *
     * @var float
     */
    protected $amount;

    /**
     * Конструктор
     *
     * @param array $statData
     * @param string $time
     * @param null $uniq
     * @param $amount
     */
    public function __construct(
        $statData,
        $time = null,
        $uniq = null,
        $amount
    )
    {
        parent::__construct($statData, $time, $uniq);
        $this->amount = $amount;
    }

    /**
     * @inheritdoc
     */
    public function serialize()
    {
        return json_encode([
            'class' => static::class,
            'statData' => $this->statData,
            'uniq' => $this->uniq,
            'time' => $this->time,
            'amount' => $this->amount,
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function unserialize($string)
    {
        $params = json_decode($string, true);

        return new $params['class'](
            $params['statData'],
            $params['time'],
            $params['uniq'],
            $params['amount']
        );
    }
}
