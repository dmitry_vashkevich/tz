<?php

namespace Project\Stats\Event;

use Project\Stats\Event;

/**
 * Событие перехода по ссылке кампании по версии системы
 */
class ClickSV extends Event
{
    /**
     * @inheritdoc
     */
    public function getIncrements($statTable = null)
    {
        return ['scv' => 1];
    }
}
