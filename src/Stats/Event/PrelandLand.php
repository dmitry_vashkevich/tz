<?php

namespace Project\Stats\Event;

use Project\Stats\Event;

/**
 * Переходы с преленда на ленд, в случае если есть преленд
 */
class PrelandLand extends Event
{
    /**
     * @inheritdoc
     */
    public function getIncrements($statTable = null)
    {
        return ['prelv' => 1];
    }
}
