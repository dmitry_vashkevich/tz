<?php

namespace Project\Stats\Event;

use Project\Stats\Event;

/**
 * Событие показа рекламного материала
 */
class Show extends Event
{
    /**
     * @inheritdoc
     */
    public function getIncrements($statTable = null)
    {
        return ['rs' => 1];
    }
}
