<?php

namespace Project\Stats\Event;

/**
 * Заказ
 */
class Lead extends AmountEvent
{
    /**
     * @inheritdoc
     */
    public function getIncrements($statTable = null)
    {
        return [
            'lead' => 1,
            'lead_amount' => $this->amount,
        ];
    }
}
