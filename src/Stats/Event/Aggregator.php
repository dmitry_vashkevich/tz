<?php

namespace Project\Stats\Event;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception\ConnectionException;
use Project\Entity\Ad;
use Project\Entity\Stats;
use Project\Entity\StatsBase;
use Project\Entity\StatsDomain;
use Project\Entity\StatsDomainType;
use Project\Proxy;
use Project\Stats\Event;
use Psr\Log\LoggerInterface;

/**
 * Агрегатор событий
 */
class Aggregator
{
    /** General error: 7 number of parameters must be between 0 and 65535 */
    const INSERT_ROW_LIMIT = 1000;

    /**
     * Статистики
     */
    const statClasses = [
        StatsBase::class,
        StatsDomain::class,
        StatsDomainType::class,
    ];

    const DEFAULT_EVENT_ROUTE = 'default';

    protected static $eventRouting = [
        Ad::ADTYPE_TEASER => [
            StatsBase::class,
            StatsDomainType::class,
            StatsDomain::class,
        ],
        self::DEFAULT_EVENT_ROUTE => [
            StatsBase::class,
            StatsDomainType::class,
        ],
    ];

    /**
     * @var Connection
     */
    protected $db;

    /** @var LoggerInterface */
    protected $logger;

    /** @var int */
    protected $curMin;

    /** @var bool */
    protected $saveByTime;

    /**
     * Конструктор
     * @param bool $saveByTime
     */
    public function __construct($saveByTime = true)
    {
        $this->db = Proxy::init()->getEntityManager()->getConnection();
        $this->logger = Proxy::init()->getLogger();
        $this->curMin = date('i');
        $this->saveByTime = $saveByTime;
    }

    /**
     * @var array
     */
    protected $agEvents = [];

    /**
     * Агрегирование
     * @param Event $event
     */
    public function process(Event $event)
    {
        $statTablesData = $this->prepareStatTablesData($event);

        foreach ($statTablesData as $statTable => $statTableData) {
            $increments = $event->getIncrements($statTable);
            $uniq = $statTableData['uniq'];
            $this->agEvents[$statTable][$uniq]['statData'] = $statTableData;
            foreach ($increments as $k => $v) {
                if (!isset($this->agEvents[$statTable][$uniq]['increments'][$k])) {
                    $this->agEvents[$statTable][$uniq]['increments'][$k] = 0;
                }

                $this->agEvents[$statTable][$uniq]['increments'][$k] += $v;
            }
        }

        $this->checkAndSave();
    }

    /**
     * Проверка возможности и необходимости сохранения и вызов сохранения
     */
    protected function checkAndSave()
    {
        $min = date('i');
        if ($this->curMin == $min || !$this->saveByTime) {
            return;
        }

        $this->curMin = $min;
        try {
            if ($this->db->ping() === false) {
                $this->db->close();
                $this->db->connect();
            }
        } catch (ConnectionException $e) {
            return;
        }
        
        $this->saveStats();
    }

    /**
     *  сохраняем стату в БД
     */
    public function saveStats()
    {
        $startTime = microtime(true);;

        /** @var Stats $class */
        foreach (self::statClasses as $class) {
            if (!isset($this->agEvents[$class::TABLE]) || count($this->agEvents[$class::TABLE]) <= 0) {
                continue;
            }

            $this->upsertStats($class);
        }

        $this->agEvents = [];

        $this->logTime('aggregator saving finish', $startTime);
    }

    /**
     * @param $class
     * @throws \Doctrine\DBAL\DBALException
     */
    protected function upsertStats($class)
    {
        /** @var Stats $class */
        $table = $class::TABLE;
        $keys = implode(', ', $class::AGGREGATION_KEYS) . ', ' . implode(', ', $class::INCREMENTS);
        $update = [];
        foreach ($class::INCREMENTS as $incKey) {
            $update[] = "$incKey = EXCLUDED.$incKey + t.$incKey";
        }

        $update = implode(',', $update);
        ksort($this->agEvents[$class::TABLE]);

        $stmtPH = [];
        $stmtValues = [];
        $i = 0;
        foreach ($this->agEvents[$class::TABLE] as $uniq => $saveData) {
            $i++;
            $stmtPH[] = ":$i" . implode(", :$i", $class::AGGREGATION_KEYS) . ", :$i" . implode(", :$i", $class::INCREMENTS);
            $statData = $saveData['statData'];
            $increments = $saveData['increments'];
            foreach ($class::AGGREGATION_KEYS as $agKey) {
                $stmtValues[] = [
                    'key' => ":$i$agKey",
                    'value' => $statData[$agKey],
                ];
            }

            foreach ($class::INCREMENTS as $incKey) {
                $stmtValues[] = [
                    'key' => ":$i$incKey",
                    'value' => isset($increments[$incKey]) ? $increments[$incKey] : 0,
                ];
            }

            if (count($stmtPH) >= self::INSERT_ROW_LIMIT) {
                $this->executeQuery($table, $keys, $stmtPH, $stmtValues, $update);
                $stmtPH = [];
                $stmtValues = [];
                $i = 0;
            }
        }

        if (count($stmtPH) > 0) {
            $this->executeQuery($table, $keys, $stmtPH, $stmtValues, $update);
        }
    }

    /**
     * @param $table
     * @param $keys
     * @param $stmtPH
     * @param $stmtValues
     * @param $update
     * @throws \Doctrine\DBAL\DBALException
     */
    private function executeQuery($table, $keys, $stmtPH, $stmtValues, $update)
    {
        $values = '(' . implode('), (', $stmtPH) . ')';
        $sql = /** @lang text */
            "INSERT INTO $table as t ($keys) VALUES $values ON CONFLICT (uniq) DO UPDATE SET $update";

        $stmt = $this->db->prepare($sql);

        foreach ($stmtValues as $stmtValue) {
            $stmt->bindValue($stmtValue['key'], $stmtValue['value']);
        }

        try {
            $startTime = microtime(true);
            $stmt->execute();
            $this->logTime('sql execute time', $startTime);
        } catch (\Exception $e) {
            $this->logSqlException($e, $sql);
        }
    }

    /**
     * @param Event $event
     * @return array
     */
    protected function prepareStatTablesData(Event $event)
    {
        $statClasses = $this->getStatClasses($event);

        $statData = $event->getStatData();
        $statData['date'] = date('Y-m-d', strtotime($event->getTime()));

        $result = [];
        /** @var Stats $class */
        foreach ($statClasses as $class) {
            $tableStatData = $this->getTableStatData($statData, $class::AGGREGATION_KEYS);
            $tableStatData['uniq'] = $class::calcUniq($tableStatData);
            $result[$class::TABLE] = $tableStatData;
        }

        return $result;
    }

    /**
     * получение списка классов статистики для записи события
     *
     * @param Event $event
     * @return mixed
     */
    private function getStatClasses(Event $event)
    {
        $adType = $event->getStatData()['adtype'];
        if (isset(self::$eventRouting[$adType])) {
            return self::$eventRouting[$adType];
        }

        return self::$eventRouting[self::DEFAULT_EVENT_ROUTE];
    }

    /**
     * @param $statData
     * @param $keys
     * @return array
     */
    private function getTableStatData($statData, $keys)
    {
        $result = [];
        foreach ($keys as $key) {
            $result[$key] = isset($statData[$key]) ? $statData[$key] : null;
        }

        return $result;
    }

    /**
     * @param string $message
     * @param int $startTime
     */
    private function logTime($message, $startTime)
    {
        $this->logger->debug(
            $message,
            [
                'time' => microtime(true) - $startTime,
                'debug_timer' => true,
            ]
        );
    }

    /**
     * @param \Exception $e
     * @param string $sql
     */
    private function logSqlException(\Exception $e, $sql)
    {
        $msg = sprintf(
            'saveStats exception %s: %s at %s line %s with sql = %s',
            get_class($e),
            $e->getMessage(),
            $e->getFile(),
            $e->getLine(),
            $sql
        );

        $this->logger->error(
            $msg,
            [
                'group' => 'exception',
                'logger' => static::class,
            ]
        );
    }
}
