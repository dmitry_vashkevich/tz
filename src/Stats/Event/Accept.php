<?php

namespace Project\Stats\Event;

/**
 * Подтвержденная конверсия
 */
class Accept extends AmountEvent
{
    /**
     * @inheritdoc
     */
    public function getIncrements($statTable = null)
    {
        return [
            'accept' => 1,
            'accept_amount' => $this->amount,
        ];
    }
}
