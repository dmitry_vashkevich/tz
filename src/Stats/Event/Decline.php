<?php

namespace Project\Stats\Event;

/**
 * Отказ
 */
class Decline extends AmountEvent
{
    /**
     * @inheritdoc
     */
    public function getIncrements($statTable = null)
    {
        return [
            'decline' => 1,
            'decline_amount' => $this->amount,
        ];
    }
}
