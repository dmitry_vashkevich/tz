<?php

namespace Project\Stats\Event;

/**
 * Ошибка при оформлении заказа
 */
class Invalid extends AmountEvent
{
    /**
     * @inheritdoc
     */
    public function getIncrements($statTable = null)
    {
        return [
            'invalid' => 1,
            'invalid_amount' => $this->amount,
        ];
    }
}
