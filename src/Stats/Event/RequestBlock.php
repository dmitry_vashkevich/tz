<?php

namespace Project\Stats\Event;

use Project\Entity\StatsDomain;
use Project\Proxy;
use Project\Stats\Event;

/**
 * Запрос контента
 */
class RequestBlock extends Event
{
    /**
     * @inheritdoc
     */
    public function getIncrements($statTable = null)
    {
        $increments = [
            'rb' => 1,
        ];

        if ($this->isUnique($statTable)) {
            $increments['usd'] = 1;
        }

        return $increments;
    }

    /**
     * Проверка на уникальность события в контексте statClass
     *
     * @param $statTable
     * @return bool
     */
    protected function isUnique($statTable)
    {
        if ($statTable != StatsDomain::TABLE) {
            return false;
        }

        if (!isset($this->statData['domain'])) {
            return false;
        }

        $key = static::class . '|' . $this->getUniq() . '|' . $this->statData['domain'];

        return $this->getMemcache()->add($key, '1', 86400);
    }

    /**
     * @return \Memcached
     */
    protected function getMemcache()
    {
        return Proxy::init()->getMemcache();
    }
}
