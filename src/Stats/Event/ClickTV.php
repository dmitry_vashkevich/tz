<?php

namespace Project\Stats\Event;

use Project\Stats\Event;

/**
 * Событие перехода по ссылке кампании по версии трекера
 */
class ClickTV extends Event
{
    /**
     * @inheritdoc
     */
    public function getIncrements($statTable = null)
    {
        return ['tcv' => 1];
    }
}
