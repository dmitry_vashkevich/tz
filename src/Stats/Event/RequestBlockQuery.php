<?php

namespace Project\Stats\Event;

use Project\Proxy;
use Project\Stats\Event;

/**
 * Запрос контента
 */
class RequestBlockQuery extends Event
{
    /**
     * текущие инкременты для этого события вычисляются единожды)
     *
     * @var array | null
     */
    protected $increments;

    /**
     * @inheritdoc
     */
    public function getIncrements($statTable = null)
    {
        if ($this->increments === null) {
            $this->increments = [
                'rbq' => 1,
            ];

            if ($this->isUnique()) {
                $this->increments['rbqu'] = 1;
            }
        }

        return $this->increments;
    }

    /**
     * Проверка на уникальность
     *
     * @return bool
     */
    protected function isUnique()
    {
        $key = static::class . '|' . $this->getStatData()['adtype'] . '|' . $this->getUniq();

        return $this->getMemcache()->add($key, '1', 86400);
    }

    /**
     * @return \Memcached
     */
    protected function getMemcache()
    {
        return Proxy::init()->getMemcache();
    }
}
