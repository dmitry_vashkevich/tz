<?php

namespace Project\Stats\Event;
use Project\Stats\Event;

/**
 * Интерфейс сериализации событий
 */
interface ISerializable
{
    /**
     * Метод упаковки события в строковое представление
     *
     * @return string
     */
    public function serialize();

    /**
     * Фабричный метод распаковки строки в объект события
     *
     * @param string $string
     * @return Event
     */
    public static function unserialize($string);
}