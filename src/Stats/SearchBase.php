<?php

namespace Project\Stats;

use Project\Check\Validator;
use Project\Entity\Campaign;
use Project\Entity\Offer;
use Project\Entity\SiteUser;
use Project\Entity\TJBlock;
use Project\Entity\TJSite;
use Project\Entity\TJUser;
use Project\Stats\Search\Base\ByAd;
use Project\Stats\Search\Base\ByAdType;
use Project\Stats\Search\Base\ByCampaign;
use Project\Stats\Search\Base\ByCountry;
use Project\Stats\Search\Base\ByDate;
use Project\Stats\Search\Base\ByLand;
use Project\Stats\Search\Base\ByOffer;
use Project\Stats\Search\Base\ByTJBlock;
use Project\Stats\Search\Base\ByTJSite;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Поиск по статистике
 */
abstract class SearchBase extends Search
{
    /** индикаторы */
    const INDICATORS = '
                COALESCE(SUM(rs), 0) as rs,
                COALESCE(SUM(rb), 0) as rb,
                COALESCE(SUM(rbq), 0) as rbq,
                COALESCE(SUM(rbqu), 0) as rbqu,
                COALESCE(SUM(scv), 0) as scv,
                COALESCE(SUM(tcv), 0) as tcv,
                COALESCE(SUM(prelv), 0) as prelv,
                COALESCE(SUM(lead), 0) as lead,
                COALESCE(SUM(lead_amount), 0) as lead_amount,
                COALESCE(SUM(accept), 0) as accept,
                COALESCE(SUM(accept_amount), 0) as accept_amount,
                COALESCE(SUM(decline), 0) as decline,
                COALESCE(SUM(decline_amount), 0) as decline_amount,
                COALESCE(SUM(invalid), 0) as invalid,
                COALESCE(SUM(invalid_amount), 0) as invalid_amount,
                COALESCE(round(SUM(accept_amount)::numeric / NULLIF(SUM(rs)::numeric, 0) * 1000, 2), 0) as cpm,
                COALESCE(round(SUM(scv)::numeric / NULLIF(SUM(rs)::numeric, 0) * 100, 2), 0) as ctr,
                COALESCE(round(SUM(accept_amount)::numeric / NULLIF(SUM(rb)::numeric, 0) * 1000, 2), 0) as cpmb,
                COALESCE(round(SUM(scv)::numeric / NULLIF(SUM(rb)::numeric, 0) * 100, 2), 0) as ctrb
    ';

    /** список свойств фильтов */
    const WHERE = [
        'user' => [
            'bind' => 'user',
            'stmt' => 'AND s.site_user_id = :user ',
        ],
        'campaign' => [
            'bind' => 'campaign_id',
            'stmt' => 'AND s.campaign_id = :campaign_id ',
        ],
        'offer' => [
            'bind' => 'offer_id',
            'stmt' => 'AND s.offer_id = :offer_id ',
        ],
        'adType' => [
            'bind' => 'ad_type',
            'stmt' => 'AND s.adtype = :ad_type ',
        ],
        'campaignIds' => [
            'stmt' => 'AND s.campaign_id IN (<campaignIds>) ',
        ],
        'adIds' => [
            'stmt' => 'AND s.ad_id IN (<adIds>) ',
        ],
        'landIds' => [
            'stmt' => 'AND s.land_id IN (<landIds>) ',
        ],
        'browser' => [
            'bind' => 'browser',
            'stmt' => 'AND s.browser = :browser ',
        ],
        'platform' => [
            'bind' => 'platform',
            'stmt' => 'AND s.platform = :platform ',
        ],
        'dateFrom' => [
            'bind' => 'date_from',
            'stmt' => 'AND s.date >= :date_from ',
        ],
        'dateTo' => [
            'bind' => 'date_to',
            'stmt' => 'AND s.date <= :date_to ',
        ],
        'TJUser' => [
            'bind' => 'tj_user_id',
            'stmt' => 'AND s.tj_user_id = :tj_user_id ',
        ],
        'refUser' => [
            'bind' => 'ref_user_id',
            'stmt' => 'AND s.ref_user_id = :ref_user_id ',
        ],
        'TJSite' => [
            'bind' => 'tj_site_id',
            'stmt' => 'AND s.tj_site_id = :tj_site_id ',
        ],
        'TJBlock' => [
            'bind' => 'tj_block_id',
            'stmt' => 'AND s.tj_block_id = :tj_block_id ',
        ],
    ];

    /**
     * фильтры
     */

    /** @var SiteUser */
    protected $user;

    /** @var TJUser */
    protected $TJUser;

    /** @var TJUser */
    protected $refUser;
    
    /** @var TJSite */
    protected $TJSite;

    /** @var TJBlock */
    protected $TJBlock;

    /** @var \DateTime */
    protected $dateFrom;

    /** @var \DateTime */
    protected $dateTo;

    /** @var  string */
    protected $browser;

    /** @var  string */
    protected $platform;

    /** @var  string */
    protected $adType;

    /** @var  array */
    protected $campaignIds;

    /** @var  array */
    protected $adIds;

    /** @var  array */
    protected $landIds;
    
    /** @var  Campaign */
    protected $campaign;

    /** @var  Offer */
    protected $offer;

    /**
     * Маппинг группировок
     */
    const GROUP_KEY_MAP = [
        'campaign' => ByCampaign::class,
        'land' => ByLand::class,
        'ad' => ByAd::class,
        'adtype' => ByAdType::class,
        'country' => ByCountry::class,
        'offer' => ByOffer::class,
        'date' => ByDate::class,
        'tjblock' => ByTJBlock::class,
        'tjsite' => ByTJSite::class,
    ];

    /**
     * Сетеры фильтров
     */

    /**
     * @param string $browser
     * @return $this
     */
    public function setBrowser($browser)
    {
        $this->browser = $browser;
        return $this;
    }

    /**
     * @param string $adType
     * @return $this
     */
    public function setAdType($adType)
    {
        $this->adType = $adType;
        return $this;
    }

    /**
     * @param string $platform
     * @return $this
     */
    public function setPlatform($platform)
    {
        $this->platform = $platform;
        return $this;
    }

    /**
     * @param \DateTime $dateFrom
     * @return $this
     */
    public function setDateFrom(\DateTime $dateFrom)
    {
        $this->dateFrom = $dateFrom;
        return $this;
    }

    /**
     * @param \DateTime $dateTo
     * @return $this
     */
    public function setDateTo(\DateTime $dateTo)
    {
        $this->dateTo = $dateTo;
        return $this;
    }

    /**
     * @param int $offset
     * @return $this
     */
    public function setOffset($offset)
    {
        $this->offset = $offset;
        return $this;
    }

    /**
     * @param int $limit
     * @return $this
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
        return $this;
    }

    /**
     * @param SiteUser $user
     * @return $this
     */
    public function setUser(SiteUser $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @param TJUser $user
     * @return $this
     */
    public function setTJUser(TJUser $user)
    {
        $this->TJUser = $user;
        return $this;
    }

    /**
     * @param TJBlock $block
     * @return $this
     */
    public function setTJBlock(TJBlock $block)
    {
        $this->TJBlock = $block;
        return $this;
    }

    /**
     * @param TJSite $site
     * @return $this
     */
    public function setTJSite(TJSite $site)
    {
        $this->TJSite = $site;
        return $this;
    }
    
    /**
     * @param array $campaignIds
     * @return $this
     */
    public function setCampaignIds(array $campaignIds)
    {
        $this->campaignIds = $campaignIds;
        return $this;
    }

    /**
     * @param array $adIds
     * @return $this
     */
    public function setAdIds(array $adIds)
    {
        $this->adIds = $adIds;
        return $this;
    }

    /**
     * @param array $landIds
     * @return $this
     */
    public function setLandIds(array $landIds)
    {
        $this->landIds = $landIds;
        return $this;
    }

    /**
     * @param Campaign $campaign
     * @return $this
     */
    public function setCampaign(Campaign $campaign)
    {
        $this->campaign = $campaign;
        return $this;
    }

    /**
     * @param Offer $offer
     * @return $this
     */
    public function setOffer(Offer $offer)
    {
        $this->offer = $offer;
        return $this;
    }

    /**
     * @param TJUser $refUser
     * @return $this
     */
    public function setRefUser(TJUser $refUser)
    {
        $this->refUser = $refUser;
        return $this;
    }
}
