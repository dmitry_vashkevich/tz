<?php

namespace Project\Stats;

use Project\Proxy;

/**
 * Очередь событий статистики
 */
class Queue
{
    /**
     * @var \GearmanClient
     */
    protected $client;

    /**
     * Конструктор
     */
    function __construct()
    {
        $this->client = $this->getGearmanClient();
    }

    /**
     * Добавить событие в очередь
     *
     * @param Event $event
     * @return $this
     */
    public function addEvent(Event $event)
    {
        $this->getGearmanClient()
            ->doBackground(
                $event->getEventHandler(),
                $event->serialize()
            );

        return $this;
    }

    /**
     * @return \GearmanClient
     */
    protected function getGearmanClient()
    {
        return Proxy::init()->getGearmanClient();
    }
}
