<?php

namespace Project\Stats\Queue;

use Project\Proxy;
use Project\Stats\Event;

/**
 * Обработчик очереди статистики
 */
class Consumer
{
    /**
     * @var Event\Aggregator
     */
    protected $aggregator;

    /**
     * @var \GearmanWorker
     */
    protected $worker;

    /**
     * Конструктор
     */
    public function __construct()
    {
        $this->worker = $this->getGearmanWorker();
        $this->prepareWorker();
    }

    /**
     * Подготовка воркера к работе
     */
    protected function prepareWorker()
    {
        $this->worker->addFunction(
            Event::HANDLER,
            function (\GearmanJob $job) {
                $this
                    ->getEventAggregator()
                    ->process(
                        Event::unserializeAbstract($job->workload())
                    );
            }
        );
    }

    /**
     * процесс обслуживания
     */
    public function process()
    {
        while ($this->worker->work());
    }

    /**
     * @return Event\Aggregator
     */
    protected function getEventAggregator()
    {
        if ($this->aggregator === null) {
            $this->aggregator = new Event\Aggregator();
        }

        return $this->aggregator;
    }

    /**
     * @return \GearmanWorker
     */
    protected function getGearmanWorker()
    {
        return Proxy::init()->getGearmanWorker();
    }
}

