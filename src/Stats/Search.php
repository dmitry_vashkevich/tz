<?php

namespace Project\Stats;

use Project\Check\Validator;
use Project\Entity\Schema\Base;
use Project\Proxy;
use Project\Stats\Exception\UnknownGroupKey;
use Project\Stats\Exception\UnknownWherePropertyType;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Поиск по статистике
 */
abstract class Search
{
    /** плейсхолдеры */
    const PH_WHERE = '<WHERE>';
    const PH_LIMIT = '<LIMIT>';
    const PH_INDICATORS = '<INDICATORS>';
    const PH_ORDER = '<ORDER>';

    /**
     * Маппинг группировок
     * @var Search[]
     */
    const GROUP_KEY_MAP = [];

    /** индикаторы */
    const INDICATORS = 'abstract';

    /** список свойств фильтов */
    const WHERE = [];
    
    /** @var  int */
    protected $offset = 0;

    /** @var  int */
    protected $limit = 0;

    /** @var  string */
    protected $order;

    /** @var  string */
    protected $direction = 'asc';

    /** @var  int */
    protected $count;

    /**
     * Получить объект статистики с нужной группировкой
     *
     * @param string $key
     * @return static
     * @throws UnknownGroupKey
     */
    public static function getStats($key)
    {
        if (!array_key_exists($key, static::GROUP_KEY_MAP)) {
            throw new UnknownGroupKey("Unknown key [$key]");
        }

        $class = static::GROUP_KEY_MAP[$key];
        return new $class();
    }

    /**
     * Готовим sql и бинды
     *
     * @return array
     */
    protected function prepareSql()
    {
        $where = $this->prepareWhere();
        $limit = $this->prepareLimit();
        $order = $this->prepareOrder();

        return [
            'stmt' => str_replace(
                [static::PH_INDICATORS, static::PH_WHERE, static::PH_LIMIT, static::PH_ORDER],
                [static::INDICATORS, $where['stmt'], $limit, $order],
                $this->getSQLTpl()
            ),
            'stmtCount' => str_replace(
                [static::PH_INDICATORS, static::PH_WHERE, static::PH_LIMIT, static::PH_ORDER],
                [static::INDICATORS, $where['stmt'], '', $order],
                $this->getSQLCountTpl()
            ),
            'binds' => $where['binds']
        ];
    }

    /**
     * @return string
     */
    private function getSQLCountTpl()
    {
       return
           'SELECT count(*) FROM (' .
           $this->getSQLTpl() .
           ') as bstmt';
    }

    /**
     * @return string
     */
    private function prepareOrder()
    {
        if ($this->order === null) {
            return $this->prepareOrderDefault();
        }

        return ' ORDER BY ' . $this->order . ' ' . $this->direction;
    }

    /**
     * @return string
     */
    private function prepareLimit()
    {
        if ($this->limit == 0) {
            return '';
        } else {
            return ' LIMIT ' . $this->limit . ' OFFSET ' . $this->offset;
        }
    }

    /**
     * Готовим блок Where запроса
     *
     * @return array
     * @throws UnknownWherePropertyType
     */
    private function prepareWhere()
    {
        $whereStr = "'1' ";
        $binds = [];

        foreach (static::WHERE as $prop => $params) {
            if ($this->$prop === null) {
                continue;
            }

            if (is_array($this->$prop)) {
                $params['stmt'] = str_replace("<$prop>", implode(', ', $this->$prop), $params['stmt']);
            } else {
                $value = $this->getWhereFormatValue($prop);
                if (isset($params['replace'])) {
                    $value = str_replace('<VALUE>', $value, $params['replace']);
                }

                $binds[$params['bind']] = $value;
            }

            $whereStr .= $params['stmt'];
        }

        return [
            'stmt' => $whereStr,
            'binds' => $binds,
        ];
    }

    /**
     * Возвращает значение свойства адаптированное для запроса
     * @param $prop
     * @return mixed
     * @throws UnknownWherePropertyType
     */
    private function getWhereFormatValue($prop)
    {
        if ($this->$prop instanceof Base) {
            return $this->$prop->getId();
        } elseif ($this->$prop instanceof \DateTime) {
            return $this->$prop->format('Y-m-d');
        } elseif(is_array($this->$prop)) {
            return implode(',', $this->$prop);
        } else {
            return $this->$prop;
        }
    }

    /**
     * Получить все элементы выборки
     *
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    /**
     * @inheritdoc
     */
    public function fetchAll()
    {
        $db = Proxy::init()->getEntityManager()->getConnection();
        $sql = $this->prepareSql();
        $stmt = $db->prepare($sql['stmt']);

        foreach ($sql['binds'] as $param => $value) {
            $stmt->bindValue($param, $value);
        }

        $stmt->execute();

        $result = $stmt->fetchAll();

        $stmt = $db->prepare($sql['stmtCount']);

        foreach ($sql['binds'] as $param => $value) {
            $stmt->bindValue($param, $value);
        }

        $stmt->execute();
        $this->count = $stmt->fetchColumn();

        return
            $result;
    }

    /**
     * @param string $baseStmt
     * @return string
     */
    protected function prepareSqlCount($baseStmt)
    {
        return "SELECT count(*) from ($baseStmt) as bstmt";
    }

    /**
     * Получить шаблон запроса
     *
     * @return string
     */
    abstract protected function getSQLTpl();

    /**
     * Получить сортировку по умолчанию
     *
     * @return string
     */
    abstract protected function prepareOrderDefault();

    /**
     * @param int $offset
     * @return $this
     */
    public function setOffset($offset)
    {
        $this->offset = $offset;
        return $this;
    }

    /**
     * @param int $limit
     * @return $this
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
        return $this;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param string $order
     * @return $this
     */
    public function setOrder($order)
    {
        $this->order = $order;
        return $this;
    }

    /**
     * @param string $direction
     * @return $this
     */
    public function setDirection($direction)
    {
        $this->direction = $direction;
        return $this;
    }
}
