<?php

namespace Project\Stats;

use Project\Stats\Event\ISerializable;

/**
 * Абстрактное событие статистики
 */
abstract class Event implements ISerializable
{
    /**
     * Связь с воркером-обработчиком события
     */
    const HANDLER = 'handlerEvent';

    /**
     * массив параметров статистики
     *
     * @var array
     */
    protected $statData;

    /**
     * mId или ip
     * @var string
     */
    protected $uniq;

    /**
     * время строкой в формате RFC3339
     * @var string
     */
    protected $time;

    /**
     * Конструктор
     *
     * @param array $statData
     * @param string $uniq
     * @param string $time
     */
    public function __construct(
        $statData,
        $time = null,
        $uniq = null
    )
    {
        $this->statData = $statData;
        $this->time = ($time === null) ? (new \DateTime())->format(\DateTime::RFC3339) : $time;
        $this->uniq = $uniq;
    }

    /**
     * @inheritdoc
     */
    public function serialize()
    {
        return json_encode([
            'class' => static::class,
            'statData' => $this->statData,
            'uniq' => $this->uniq,
            'time' => $this->time
        ]);
    }

    /**
     * Десериализация события
     *
     * @param $string
     * @return mixed
     */
    public static function unserializeAbstract($string)
    {
        $params = json_decode($string, true);
        /** @var Event $eventClass */
        $eventClass = $params['class'];
        return $eventClass::unserialize($string);
    }

    /**
     * @inheritdoc
     */
    public static function unserialize($string)
    {
        $params = json_decode($string, true);

        return new $params['class'](
            $params['statData'],
            $params['time'],
            $params['uniq']
        );
    }

    /**
     * Получить обработчик события
     *
     * @return string
     */
    public function getEventHandler()
    {
        return static::HANDLER;
    }

    /**
     * @return string
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @return string
     */
    public function getUniq()
    {
        return $this->uniq;
    }

    /**
     * @return array
     */
    public function getStatData()
    {
        return $this->statData;
    }

    /**
     * вычисление ключей и значений инкрементов
     *
     * @param string | null $statTable
     * @return array
     */
    abstract public function getIncrements($statTable = null);
}
