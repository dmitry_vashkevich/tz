<?php

namespace Project\Stats;

use Project\Cache\EntityCache;
use Project\Entity\Ad;

/**
 * Методы работы и сборки для StatData
 */
class StatData
{
    /** @var array */
    protected $requestData;

    /** @var array */
    protected $blockData;

    /** @var array */
    protected $statData;

    /**
     * Конструктор
     *
     * @param array $requestData
     * @param array | null $blockData
     */
    public function __construct(array $requestData, array $blockData = null)
    {
        $this->requestData = $requestData;
        $this->blockData = $blockData;
        $this->prepareAdType();
        $this->prepareTJBlockId();
        $this->prepareTJBlockSize();
        $this->prepareTJUserId();
        $this->prepareTJSiteId();
        $this->prepareDomain();
        $this->prepareNetId();
        $this->prepareCountry();
        $this->preparePlatformAndBrowser();
    }

    /**
     * @return array
     */
    public function getAsArray()
    {
        return $this->statData;
    }

    /**
     * Домен
     */
    protected function prepareDomain()
    {
        if (!isset($_SERVER['HTTP_REFERER'])) {
            $this->statData['domain'] = '';
            return;
        }

        $domain = parse_url($_SERVER['HTTP_REFERER'])['host'];
        //если надо отрезать www.
        if (substr($domain, 0, 4) == 'www.') {
            $domain = substr($domain, 4, strlen($domain) - 4);
        }

        $this->statData['domain'] = $domain;
    }

    /**
     * Страна
     */
    protected function prepareCountry()
    {
        $this->statData['country'] = geoip_country_code_by_name($_SERVER['REMOTE_ADDR']);
        //грязный хак для отладки
        if (isset($_GET['test123'])) {
            $this->statData['country'] = 'RU';
        }
    }

    /**
     * Платформа и браузер
     */
    protected function preparePlatformAndBrowser()
    {
        $uaInfo = parse_user_agent(isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '');
        $this->statData['platform'] = $uaInfo['platform'];
        $this->statData['browser'] = $uaInfo['browser'];
    }

    /**
     * ID блока
     */
    protected function prepareTJBlockId()
    {
        if (isset($this->requestData['block_id'])) {
            $this->statData['tj_block_id'] = $this->requestData['block_id'];
        }
    }

    /**
     * AdType
     */
    protected function prepareAdType()
    {
        $this->statData['adtype'] = $this->requestData['ad_type'];
    }

    /**
     * netId
     */
    protected function prepareNetId()
    {
        if (isset($this->requestData['net_id'])) {
            $this->statData['net_id'] = $this->requestData['net_id'];
        }
    }

    /**
     * Размеры блока
     */
    protected function prepareTJBlockSize()
    {
        if (isset($this->blockData['height'])) {
            $this->statData['height'] = $this->blockData['height'];
        } elseif (isset($this->requestData['height'])) {
            $this->statData['height'] = $this->requestData['height'];
        }

        if (isset($this->blockData['width'])) {
            $this->statData['width'] = $this->blockData['width'];
        } elseif (isset($this->requestData['width'])) {
            $this->statData['width'] = $this->requestData['width'];
        }
    }

    /**
     * TJUserId
     */
    protected function prepareTJUserId()
    {
        if (isset($this->blockData['user']['id'])) {
            $this->statData['tj_user_id'] = $this->blockData['user']['id'];
        } elseif($this->requestData['ad_type'] == Ad::ADTYPE_CASHBACK) {
            //HARDCODE!!!
            $this->statData['tj_user_id'] = 9;
        }
    }

    /**
     * TJUserId
     */
    protected function prepareTJSiteId()
    {
        if (isset($this->blockData['site']['id'])) {
            $this->statData['tj_site_id'] = $this->blockData['site']['id'];
        }
    }
}