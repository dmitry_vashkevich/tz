<?php

namespace Project\Service;

use Doctrine\Common\Collections\Criteria;
use Project\Ad\Search;
use Project\Check\Validator;
use Project\Entity\AdminUser;
use Project\Entity\Campaign;
use Project\Entity\Schema\Base;
use Project\Service;
use Project\Stats\SearchBase;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Сервис для работы с рекламным контентом
 */
class Ad extends UserService
{
    /**
     * Поиск
     *
     * @param array $requestData
     * @return array
     */
    public function search(array $requestData)
    {
        $campaign = Campaign::prepareById($requestData['campaign_id']);

        if (!($this->user instanceof AdminUser)) {
            Validator::validateValue(
                $campaign->getSiteUserId(),
                new Assert\EqualTo(
                    $this->user->getId()
                ),
                'campaign user != session user'
            );
        }

        $order = isset($requestData['order']) ? $requestData['order'] : 'id';
        $direction = isset($requestData['direction']) ? $requestData['direction'] : Criteria::DESC;

        $search = new Search();
        $searchResult = $search->process(
            $campaign,
            $order,
            $direction
        );


        if (count($searchResult) == 0) {
            return [];
        }

        return $searchResult;
    }

    /**
     * @param Base $entity
     * @return array
     */
    protected function getEntityData(Base $entity)
    {
        /** @var \Project\Entity\Ad $entity */
        return [
            'id' => $entity->getId(),
            'type' => $entity->getType(),
            'data' => $entity->getData(),
            'images' => $entity->getImages(),
        ];
    }

    /**
     * @param array $entityIds
     * @return array
     */
    protected function getStatsData(array $entityIds)
    {
        return SearchBase::getStats('ad')
            ->setAdIds($entityIds)
            ->fetchAll();
    }

    /**
     * @return string
     */
    protected function getEntityStatIdParam()
    {
        return 'ad_id';
    }
}
