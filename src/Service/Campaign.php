<?php

namespace Project\Service;

use Doctrine\Common\Collections\Criteria;
use Project\Campaign\Active;
use Project\Campaign\Content;
use Project\Campaign\Create;
use Project\Campaign\Update;
use Project\Campaign\Flow;
use Project\Campaign\Targeting;
use Project\Campaign\Search;
use Project\Check\Exception\ValidationException;
use Project\Check\Validator;
use Project\Entity\AdminUser;
use Project\Entity\Offer;
use Project\Entity\Schema\Base;
use Project\Entity\SiteUser;
use Project\Service;
use Project\Stats\SearchBase;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Сервис для работы с кампаниями
 */
class Campaign extends UserService
{

    /** search default */
    const SEARCH_CAMPAIGN_DEFAULT_OFFSET = 0;
    const SEARCH_CAMPAIGN_DEFAULT_LIMIT = 100;

    /**
     * Загрузка контента в кампанию
     *
     * @param $campaignId
     * @param $data
     * @param $files
     * @return array
     */
    public function putContent($campaignId, $data, $files)
    {
        $ad = (new Content($this->user, \Project\Entity\Campaign::prepareById($campaignId)))
            ->process($data, $files);

        return [
            'id' => $ad->getId(),
            'type' => $ad->getType(),
            'campaign_id' => $ad->getCampaign()->getId(),
            'images' => $ad->getImages(),
            'data' => $ad->getData(),
        ];
    }

    /**
     * Вкл / выкл кампании
     *
     * @param $campaignId
     * @param $active
     * @return array
     * @throws \Exception
     */
    public function putActive($campaignId, $active)
    {
        $campaign = (new Active($this->user, \Project\Entity\Campaign::prepareById($campaignId)))
            ->process($active == 'true' ? true : false);

        return $this->getEntityData($campaign);
    }

    /**
     * Создание таргетинга кампании
     *
     * @param $campaignId
     * @param array $requestData
     * @return array
     */
    public function putTargeting($campaignId, array $requestData)
    {
        $this->validateUser();

        $countries = null;
        if (isset($requestData['countries'])) {
            $countries = json_decode($requestData['countries'], true);
            if (!is_array($countries) || count($countries) == 0) {
                $countries = null;
            }
        }

        $browsers = null;
        if (isset($requestData['browsers'])) {
            $browsers = json_decode($requestData['browsers'], true);
            if (!is_array($browsers) || count($browsers) == 0) {
                $browsers = null;
            }
        }

        $platforms = null;
        if (isset($requestData['platforms'])) {
            $platforms = json_decode($requestData['platforms'], true);
            if (!is_array($platforms) || count($platforms) == 0) {
                $platforms = null;
            }
        }

        $campaign = (new Targeting($this->user, \Project\Entity\Campaign::prepareById($campaignId)))
            ->process(
                $countries,
                $browsers,
                $platforms
            );

        return $this->getEntityData($campaign);
    }

    /**
     * @throws ValidationException
     */
    protected function validateUser()
    {
        Validator::validateValue(
            $this->user instanceof SiteUser && $this->user->isExternal(),
            new Assert\EqualTo(false),
            'User have to be internal for this operation'
        );
    }

    /**
     * Апдейт внутренних зависимостей кампании при
     * редактировании включенной кампании
     *
     * @param \Project\Entity\Campaign $campaign
     */
    private function updateIfActve(\Project\Entity\Campaign $campaign)
    {
        if (!$campaign->isActive()) {
            return;
        }

        (new Active($this->user, $campaign))
            ->process(true);
    }

    /**
     * Создание сетки лендов для кампании
     *
     * @param $campaignId
     * @param $flow
     * @return array
     */
    public function putFlow($campaignId, $flow)
    {
        $campaign = (new Flow($this->user, \Project\Entity\Campaign::prepareById($campaignId)))
            ->process($flow);

        $this->updateIfActve($campaign);
        
        return $this->getEntityData($campaign);
    }

    /**
     * Создание кампании
     *
     * @param array $requestData
     * @return array
     * @throws \Exception
     */
    public function create(array $requestData)
    {
        $name = $requestData['name'];
        $offer = isset($requestData['offer_id']) ? Offer::prepareById($requestData['offer_id']) : null;
        $url = isset($requestData['url']) ? $requestData['url'] : null;
        $adType = isset($requestData['ad_type']) ? $requestData['ad_type'] : null;
        $cpm = isset($requestData['cpm']) ? (int)$requestData['cpm'] : null;

        $script = isset($requestData['script']) ? $requestData['script'] : null;
        
        $limitUserShow = isset($requestData['limit_user_show']) ? $requestData['limit_user_show'] : null;
        $limitAmount = isset($requestData['limit_amount']) ? $requestData['limit_amount'] : null;
        
        $blockWidthFrom = isset($requestData['b_width_from']) ? $requestData['b_width_from'] : null;
        $blockWidthTo = isset($requestData['b_width_to']) ? $requestData['b_width_to'] : null;
        $blockHeightFrom = isset($requestData['b_height_from']) ? $requestData['b_height_from'] : null;
        $blockHeightTo = isset($requestData['b_height_to']) ? $requestData['b_height_to'] : null;

        $archive = isset($requestData['archive']) && $requestData['archive'] == 'true' ? true : false;

        if (!($this->user instanceof SiteUser)) {
            throw new \Exception("invalid user type");
        }
        
        $campaign = (new Create($this->user, $adType, $name))
            ->process(
                $offer,
                $cpm,
                $url,
                $script,
                $limitUserShow,
                $limitAmount,
                $blockWidthFrom,
                $blockWidthTo,
                $blockHeightFrom,
                $blockHeightTo,
                $archive
            );

        return $this->getEntityData($campaign);
    }

    /**
     * Редактирование кампании
     *
     * @param array $requestData
     * @param int $campaignId
     * @return array
     */
    public function update(array $requestData, $campaignId)
    {
        $campaign = \Project\Entity\Campaign::prepareById($campaignId);
        $name = isset($requestData['name']) ? $requestData['name'] : null;
        $offer = isset($requestData['offer_id']) ? Offer::prepareById($requestData['offer_id']) : null;
        $url = isset($requestData['url']) ? $requestData['url'] : null;
        $cpm = isset($requestData['cpm']) ? (int)$requestData['cpm'] : null;

        $script = isset($requestData['script']) ? $requestData['script'] : null;

        $limitUserShow = isset($requestData['limit_user_show']) ? $requestData['limit_user_show'] : null;
        $limitAmount = isset($requestData['limit_amount']) ? $requestData['limit_amount'] : null;

        $blockWidthFrom = isset($requestData['b_width_from']) ? $requestData['b_width_from'] : null;
        $blockWidthTo = isset($requestData['b_width_to']) ? $requestData['b_width_to'] : null;
        $blockHeightFrom = isset($requestData['b_height_from']) ? $requestData['b_height_from'] : null;
        $blockHeightTo = isset($requestData['b_height_to']) ? $requestData['b_height_to'] : null;

        $archive = isset($requestData['archive']) ? ($requestData['archive'] == 'true' ? true : false) : null;

        $campaign = (new Update($this->user, $campaign))
            ->process(
                $name,
                $offer,
                $cpm,
                $url,
                $script,
                $limitUserShow,
                $limitAmount,
                $blockWidthFrom,
                $blockWidthTo,
                $blockHeightFrom,
                $blockHeightTo,
                $archive
            );

        return $this->getEntityData($campaign);
    }

    /**
     * Поиск
     *
     * @param array $requestData
     * @return array
     */
    public function search(array $requestData)
    {
        $campaign = isset($requestData['campaign_id']) ? \Project\Entity\Campaign::prepareById($requestData['campaign_id']) : null;
        $offer = isset($requestData['offer_id']) ? Offer::prepareById($requestData['offer_id']) : null;
        $name = isset($requestData['name']) ? $requestData['name'] : null;
        $adType = isset($requestData['ad_type']) ? $requestData['ad_type'] : null;
        $active = null;
        if (isset($requestData['active'])) {
            $active = $requestData['active'] == 'true' ? true : false;
        }

        $external = null;
        if (isset($requestData['external'])) {
            $external = $requestData['external'] == 'true' ? true : false;
        }

        $order = isset($requestData['order']) ? $requestData['order'] : 'id';
        $direction = isset($requestData['direction']) ? $requestData['direction'] : Criteria::DESC;
        $offset = isset($requestData['offset']) ? $requestData['offset'] : self::SEARCH_CAMPAIGN_DEFAULT_OFFSET;
        $limit = isset($requestData['limit']) ? $requestData['limit'] : self::SEARCH_CAMPAIGN_DEFAULT_LIMIT;
        $archive = isset($requestData['archive']) ? ($requestData['archive'] == 'true' ? true : false) : null;
        
        $user = null;
        if ($this->user instanceof AdminUser) {
            if (isset($requestData['site_user_id'])) {
                $user = SiteUser::prepareById($requestData['site_user_id']);
            }
        } else {
            $user = $this->user;
        }
        
        $search = new Search();
        $searchResult = $search->process(
            $user,
            $campaign,
            $offer,
            $adType,
            $active,
            $external,
            $name,
            $archive,
            $order,
            $direction,
            $offset,
            $limit
        );


        if (count($searchResult) == 0) {
            return [];
        }

        return [
            'count' => $search->getCount(),
            'data' => $searchResult,
        ];
    }

    /**
     * @param Base $entity
     * @return array
     */
    protected function getEntityData(Base $entity)
    {
        /** @var \Project\Entity\Campaign $entity */
        return [
            'id' => $entity->getId(),
            'name' => $entity->getName(),
            'offer_id' => $entity->getOfferId(),
            'offer_name' => $entity->getOfferId() !== null ? $entity->getOffer()->getName() : null,
            'ad_type' => $entity->getAdType(),
            'external' => $entity->isExternal(),
            'cpm' => $entity->getCpm(),
            'url' => $entity->getUrl(),
            'flow' => $entity->getFlow(),
            'targetBrowsers' => $entity->getTargetBrowsers(),
            'targetCountries' => $entity->getTargetCountries(),
            'targetPlatforms' => $entity->getTargetPlatforms(),
            'targetDomains' => $entity->getTargetDomains(),
            'limitAmount' => $entity->getLimitAmount(),
            'limitUserShow' => $entity->getLimitUserShow(),
            'active' => $entity->isActive(),
            'ad_count' => $entity->getAds()->count(),
            'archive' => $entity->isArchive(),
            'script' => $entity->getScript(),
        ];
    }

    /**
     * @param array $entityIds
     * @return array
     */
    protected function getStatsData(array $entityIds)
    {
        return SearchBase::getStats('campaign')
            ->setCampaignIds($entityIds)
            ->fetchAll();
    }

    /**
     * @return string
     */
    protected function getEntityStatIdParam()
    {
        return 'campaign_id';
    }
}
