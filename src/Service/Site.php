<?php

namespace Project\Service;

use Doctrine\Common\Collections\Criteria;
use Project\Entity\TJBlock;
use Project\Entity\TJSite;
use Project\Entity\TJUser;
use Project\Service;
use Project\TJSite\Search;
use Project\Check\Validator;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Сервис для работы с площадками
 */
abstract class Site extends Service\UserService
{
    /**
     * @param $siteId
     * @return array
     */
    public function delete($siteId)
    {
        $site = TJSite::prepareByParams(
            [
                'id' => $siteId,
                'deleted' => false,
            ]
        );
        
        $this->validateSiteUser($site);

        $site->setDeleted(true);
        $site->save();

        return ['true'];
    }

    /**
     * @param TJSite $site
     */
    abstract protected function validateSiteUser(TJSite $site);
    
    /**
     * @param array $requestData
     * @return array
     */
    public function search(array $requestData)
    {
        $active = null;
        if (isset($requestData['active'])) {
            $active = $requestData['active'] == 'true' ? true : false;
        }

        $offset = isset($requestData['offset']) ? $requestData['offset'] : null;
        $limit = isset($requestData['limit']) ? $requestData['limit'] : null;

        $search = new Search();
        
        $user = $this->getSearchUser($requestData);
        
        $sites = $search->process($user, $active, $offset, $limit);

        $result = [];

        foreach ($sites as $site) {
            $result[] = $this->format($site);
        }

        return [
            'count' => $search->getCount(),
            'data' => $result,
        ];
    }

    /**
     * @param array $requestData
     * @return null|TJUser
     */
    abstract protected function getSearchUser(array $requestData);

    /**
     * @param $siteId
     * @param array $requestData
     * @return array
     */
    public function update($siteId, array $requestData)
    {
        $site = TJSite::prepareById($siteId);

        $this->validateSiteUser($site);

        if (isset($requestData['name'])) {
            $site->setName($requestData['name']);
        }
        
        if (isset($requestData['url'])) {
            $site->setUrl($requestData['url']);
        }
        
        if (isset($requestData['alias'])) {
            $site->setAlias($requestData['alias']);
        }

        if (isset($requestData['themes'])) {
            $site->setThemes($requestData['themes']);
        }

        if (isset($requestData['statlink'])) {
            $site->setStatlink($requestData['statlink']);
        }

        if (isset($requestData['params'])) {
            $site->setParams($requestData['params']);
        }

        if (isset($requestData['active'])) {
            $site->setActive($requestData['active'] == 'true' ? true : false);
        }
        
        $site->save();

        return $this->format($site);
    }

    /**
     * @param TJSite $site
     * @return array
     */
    public function format(TJSite $site)
    {
        $blockCount = TJBlock::repo()->createQueryBuilder('base')
            ->select("count(base.id)")
            ->addCriteria(
                (new Criteria())
                    ->where(
                        Criteria::expr()->eq('site', $site)
                    )
                    ->andWhere(
                        Criteria::expr()->eq('deleted', false)
                    )
            )
            ->getQuery()
            ->getSingleScalarResult();

        return [
            'id' => $site->getId(),
            'name' => $site->getName(),
            'url' => $site->getUrl(),
            'alias' => $site->getAlias(),
            'themes' => $site->getThemes(),
            'statlink' => $site->getStatlink(),
            'params' => $site->getParams(),
            'status' => $site->getStatus(),
            'active' => $site->isActive(),
            'blockCount' => $blockCount,
        ];
    }
}
