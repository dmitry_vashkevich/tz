<?php

namespace Project\Service;

use Project\Ad\View;
use Project\Entity\TJBlock;
use Project\Entity\TJSite;
use Project\Entity\TJUser;
use Project\Service;
use Project\TJBlock\Search;
use Project\Check\Validator;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Сервис для работы с блоками
 */
abstract class Block extends Service\UserService
{
    /**
     * @param $blockId
     * @return array
     */
    public function delete($blockId)
    {
        $block = TJBlock::prepareByParams(
            [
                'id' => $blockId,
                'deleted' => false,
            ]
        );

        $this->validateBlockUser($block);

        $block->setDeleted(true);
        $block->save();

        return ['true'];
    }

    /**
     * @param TJBlock $block
     */
    abstract protected function validateBlockUser(TJBlock $block);
    
    /**
     * @param array $requestData
     * @return array
     */
    public function search(array $requestData)
    {
        $site = null;
        if (isset($requestData['site_id'])) {
            $site = TJSite::prepareById($requestData['site_id']);
        }

        $user = $this->getSearchUser($requestData);

        $blocks = (new Search())->process($user, $site);

        $result = [];

        foreach ($blocks as $block) {
            $result[] = $this->format($block);
        }

        return $result;
    }

    /**
     * @param array $requestData
     * @return null|TJUser
     */
    abstract protected function getSearchUser(array $requestData);

    /**
     * @param array $requestData
     * @return array
     */
    public function preview(array $requestData)
    {
        $config = json_decode($requestData['config'], true);
        $params = json_decode($requestData['params'], true);
        $testAd = [
            'url' => 'http://google.com',
            'data' => json_encode([
                'captionTop' => 'captionTop',
                'captionBottom' => 'captionBottom',
            ]),
            'images' => json_encode([
                'example.jpeg'
            ]),
        ];

        $ads = array_fill(0, $config['row'] * $config['col'], $testAd);

        return (new View\Teaser([
            'width' => $requestData['width'],
            'height' => $requestData['height'],
            'row' => $config['row'],
            'col' => $config['col'],
            'cell_width' => $config['cell_width'],
            'cell_height' => $config['cell_height'],
            'config' => $config,
            'ads' => $ads,
            'params' => $params,
        ]))
            ->generate();

    }

    /**
     * @param $blockId
     * @param array $requestData
     * @return array
     */
    public function update($blockId, array $requestData)
    {
        $block = TJBlock::prepareById($blockId);

        $this->validateBlockUser($block);

        if (isset($requestData['name'])) {
            $block->setName($requestData['name']);
        }

        if (isset($requestData['custom'])) {
            $block->setCustom($requestData['custom'] == 'true' ? true : false);
        }

        if ($block->isCustom()) {
            if (isset($requestData['params'])) {
                $block->setParams($requestData['params']);
            }

            if (isset($requestData['config'])) {
                $block->setConfig($requestData['config']);
            }
        } else {
            $block->setParams(null);
            $block->setConfig(null);
        }

        if (isset($requestData['active'])) {
            $block->setActive($requestData['active'] == 'true' ? true : false);
        }

        $block->save();

        return $this->format($block);
    }

    /**
     * @param TJBlock $block
     * @return array
     */
    protected function format(TJBlock $block)
    {
        return [
            'id' => $block->getId(),
            'adtype' => $block->getAdType(),
            'name' => $block->getName(),
            'custom' => $block->isCustom(),
            'params' => $block->getParams(),
            'config' => $block->getConfig(),
            'width' => $block->getWidth(),
            'height' => $block->getHeight(),
            'active' => $block->isActive(),
            'site' => $this->getSiteService()->format($block->getSite()),
        ];
    }

    /**
     * @return Site
     */
    abstract protected function getSiteService();
}
