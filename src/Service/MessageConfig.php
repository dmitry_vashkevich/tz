<?php

namespace Project\Service;

use Project\MessageConfig\Search;
use Project\Service;

/**
 * Сервис для работы с конфигами месседжей
 */
class MessageConfig extends Service
{
    /**
     * Поиск
     *
     * @param array $requestData
     * @return array
     */
    public function search(array $requestData)
    {
        return (new Search())->process();
    }


    /**
     * Создание
     *
     * @param array $requestData
     * @return array
     */
    public function create(array $requestData)
    {
        $config = (new \Project\Entity\MessageConfig())
            ->setData($requestData['data'])
            ->save();

        return [
            'id' => $config->getId(),
        ];
    }

    /**
     * Обновление
     *
     * @param $configId
     * @param array $requestData
     * @return array
     */
    public function update($configId, array $requestData)
    {
        $tConf = \Project\Entity\MessageConfig::prepareById($configId);
        $tConf
            ->setActive($requestData['active'] == 'true' ? true : false)
            ->save();

        return ['result' => true];
    }
}
