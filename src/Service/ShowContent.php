<?php

namespace Project\Service;

use Project\Ad\Action;
use Project\Cache\AdCache;
use Project\Ad\Index;
use Project\Cache\EntityCache\TJBlock;
use Project\Campaign\Limit;
use Project\Stats\StatData;
use Project\Ad\View;
use Project\Check\Validator;
use Project\Entity\Ad;
use Project\Service;
use Project\Stats\Queue;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Публичный сервис отдачи контента
 */
class ShowContent extends Service
{
    /**
     * Список включенных AdType
     * @var array
     */
    protected static $enabledAdTypes = [
        Ad::ADTYPE_TEASER,
        Ad::ADTYPE_SLIDER_BANNER,
        Ad::ADTYPE_CLICKUNDER,
    //    Ad::ADTYPE_CASHBACK,
    ];
    
    /**
     * Отдача контента
     *
     * @param $requestData
     * @return string
     */
    public function show($requestData)
    {
        $adType = $requestData['ad_type'];
        $blockData = $this->getBlockData($requestData);
        $statData = $this->getStatData($requestData, $blockData);

        /** @var Action\ShowContent $action */
        $action = Action::getAction(Action::TYPE_CONTENT, $adType, $statData);
        $eventUniq = $this->prepareUniq($requestData);
        $action->requestBlockQueryStart($blockData, $eventUniq);
        
        $this->checkAdTypeEnabled($adType);

        $limit = new Limit();
        
        $cacheAds = (new Index(
            (new AdCache())->get($adType)
        ))->search(
            $statData['country'],
            $statData['platform'],
            $statData['browser'],
            $statData['domain'],
            $limit
        );
        
        $content = $action->prepareContent(
            $cacheAds, $requestData, $blockData
        );

        $action->requestBlockQueryFinish($blockData, $eventUniq, $limit);

        return $content;
    }

    /**
     * @param array $requestData
     * @return array | null
     */
    protected function getBlockData(array $requestData)
    {
        $blockData = null;
        if (isset($requestData['block_id'])) {
            $blockData = (new TJBlock())->get($requestData['block_id']);
            Validator::validateValue(
                $blockData['ad_type'],
                new Assert\EqualTo($requestData['ad_type']),
                "blocked AdType must be equal to requested AdType"
            );
        }
        
        return $blockData;
    }

    /**
     * @param $requestData
     * @param $blockData
     * @return array
     */
    protected function getStatData(array $requestData, array $blockData = null)
    {
        return (new StatData($requestData, $blockData))->getAsArray();
    }
    
    /**
     * Проверка что adType включен
     *
     * @param $adType
     */
    protected function checkAdTypeEnabled($adType)
    {
        //грязный хак для отладки
        if (isset($_GET['dima2'])) {
            return;
        }

        Validator::validateValue(
            $adType,
            new Assert\Choice(self::$enabledAdTypes),
            "$adType disabled"
        );
    }
    
    /**
     * Готовим $eventUniq, который используется для проверки уникальности событий
     *
     * @param array $requestData
     * @return string
     */
    protected function prepareUniq(array $requestData)
    {
        return isset($requestData['mId']) ? $requestData['mId'] : $_SERVER['REMOTE_ADDR'];
    }
}
