<?php

namespace Project\Service;

use Project\Service;
use Project\OfferCat\Search;

/**
 * Сервис для работы со странами
 */
class OfferCat extends Service
{
    /**
     * Поиск стран
     *
     * @return array
     */
    public function search()
    {
        $cats = (new Search())->process();

        $result = [];

        foreach ($cats as $cat) {
            $result[] = [
                'id' => $cat->getId(),
                'name' => $cat->getName(),
            ];
        }

        return $result;
    }
}
