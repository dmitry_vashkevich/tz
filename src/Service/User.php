<?php

namespace Project\Service;

use Project\Service;
use Project\User\Auth;
use Project\User\Logout;
use Project\User\Password;
use Project\User\Registration;
use Project\User\Session;

/**
 * Сервис для работы с юзерами
 */
abstract class User extends Service
{
    /**
     * Пользователь
     *
     * @var \Project\Entity\User
     */
    protected $user;

    /**
     * Сессия
     *
     * @var Session
     */
    protected $session;

    /**
     * Конструктор
     *
     * @param string | null $sessionId
     */
    public function __construct($sessionId = null)
    {
        if ($sessionId !== null) {
            $this->session = $this->getSession($sessionId);
            $this->user = $this->session->getUser();
        }
    }

    /**
     * Регистрация пользователя
     *
     * @param array $requestData
     * @return array
     */
    public function register(array $requestData)
    {
        $this->getRegistration($requestData['login'], $requestData['password'])->process();

        return [
            'success' => true,
        ];
    }

    /**
     * Авторизация пользователя
     *
     * @param array $requestData
     * @return array
     * @throws \Exception
     */
    public function auth(array $requestData)
    {
        $protection = (new Auth\Protection())->check();

        try {
            $session = $this->getAuth($requestData['login'], $requestData['password'])->process();
        } catch (\Exception $e) {
            $protection->addAttempt();
            throw $e;
        }

        return [
            'session_id' => $session->getId(),
        ];
    }

    /**
     * Логаут пользователя
     *
     * @return array
     */
    public function logout()
    {
        (new Logout($this->session))->process();

        return [
            'success' => true,
        ];
    }

    /**
     * Смена пароля пользователя
     *
     * @param array $requestData
     * @return array
     */
    public function changePassword(array $requestData)
    {
        $password = new Password($this->user);
        $password->verify($requestData['old_password']);
        $this->user
            ->setPassword($password->createHash($requestData['password']))
            ->save();

        return [
            'success' => true,
        ];
    }

    /**
     * Получить юзера по сессии
     *
     * @return \Project\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param string $login
     * @param string $password
     *
     * @return Auth
     */
    abstract protected function getAuth($login, $password);

    /**
     * @param string $login
     * @param string $password
     *
     * @return Registration
     */
    abstract protected function getRegistration($login, $password);

    /**
     * @param $sessionId
     * @return Session
     */
    abstract protected function getSession($sessionId);
}
