<?php

namespace Project\Service;

use Project\Entity\User as UserEntity;
use Project\Service;

/**
 * Юзер-ориентированный сервис
 */
abstract class UserService extends Service
{
    /**
     * Пользователь
     *
     * @var UserEntity
     */
    protected $user;

    /**
     * Конструктор
     *
     * @param UserEntity $user
     */
    public function __construct(UserEntity $user)
    {
        $this->user = $user;
    }
}
