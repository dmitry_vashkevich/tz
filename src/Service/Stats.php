<?php

namespace Project\Service;

use Project\Entity\Campaign;
use Project\Entity\Offer;
use Project\Entity\SiteUser as ESiteUser;
use Project\Entity\SiteUser;
use Project\Entity\TJBlock;
use Project\Entity\TJSite;
use Project\Entity\TJUser;
use Project\Entity\AdminUser;
use Project\Service;
use Project\Stats\Exception\UnknownGroupKey;
use Project\Stats\Search;
use Project\Stats\SearchBase;
use Project\Stats\SearchDomain;
use Project\Stats\SearchDomainType;

/**
 * Сервис для работы со статистикой
 */
class Stats extends UserService
{
    /** @var array  */
    protected $rateFields = [
        'accept_amount',
        'decline_amount',
        'invalid_amount',
        'lead_amount',
    ];

    /**
     * Поиск статистичесих данных
     *
     * @param array $requestData
     * @return array
     */
    public function search(array $requestData)
    {
        $stats = $this->searchStats($requestData);

        if (isset($requestData['offset'])) {
            $stats->setOffset(
                $requestData['offset']
            );
        }

        if (isset($requestData['limit'])) {
            $stats->setLimit(
                $requestData['limit']
            );
        }

        if (isset($requestData['order'])) {
            $stats->setOrder(
                $requestData['order']
            );
        }

        if (isset($requestData['direction'])) {
            $stats->setDirection(
                $requestData['direction']
            );
        }

        return $this->format($stats);
    }

    /**
     * @param array $requestData
     * @return Search
     * @throws UnknownGroupKey
     * @throws \Exception
     */
    protected function searchBase(array $requestData)
    {
        $stats = SearchBase::getStats($requestData['key']);
        /**
         * Формируем фильтры
         */

        if ($this->user instanceof TJUser) {
            if (isset($requestData['refs'])) {
                $stats->setRefUser($this->user);
            } else {
                $stats->setTJUser($this->user);
            }
        } elseif ($this->user instanceof ESiteUser) {
            $stats->setUser($this->user);
        } elseif ($this->user instanceof AdminUser) {
            //ограничения не накладываются в общем случае
            //но есть спец-фильтры по юзерам
            if (isset($requestData['site_user_id'])) {
                $stats->setUser(
                    SiteUser::prepareById($requestData['site_user_id'])
                );
            }

            if (isset($requestData['tj_user_id'])) {
                $stats->setTJUser(
                    TJUser::prepareById($requestData['tj_user_id'])
                );
            }
        } else {
            throw new \Exception('undefined user type');
        }

        if (isset($requestData['ad_type'])) {
            $stats->setAdType($requestData['ad_type']);
        }

        if (isset($requestData['browser'])) {
            $stats->setBrowser($requestData['browser']);
        }

        if (isset($requestData['platform'])) {
            $stats->setPlatform($requestData['platform']);
        }

        if (isset($requestData['tj_block_id'])) {
            $stats->setTJBlock(
                TJBlock::prepareById($requestData['tj_block_id'])
            );
        }

        if (isset($requestData['tj_site_id'])) {
            $stats->setTJSite(
                TJSite::prepareById($requestData['tj_site_id'])
            );
        }

        if (isset($requestData['offer_id'])) {
            $stats->setOffer(
                Offer::prepareById($requestData['offer_id'])
            );
        }

        if (isset($requestData['campaign_id'])) {
            $stats->setCampaign(
                Campaign::prepareById($requestData['campaign_id'])
            );
        }

        if (isset($requestData['date_from'])) {
            $stats->setDateFrom(
                new \DateTime($requestData['date_from'])
            );
        }

        if (isset($requestData['date_to'])) {
            $stats->setDateTo(
                new \DateTime($requestData['date_to'])
            );
        }

        return $stats;
    }

    /**
     * @param array $requestData
     * @return Search
     * @throws UnknownGroupKey
     */
    protected function searchDomain(array $requestData)
    {
        $stats = SearchDomain::getStats($requestData['key']);
        if (isset($requestData['domain'])) {
            $stats->setDomain(
                $requestData['domain']
            );
        }

        if (isset($requestData['date_from'])) {
            $stats->setDateFrom(
                new \DateTime($requestData['date_from'])
            );
        }

        if (isset($requestData['date_to'])) {
            $stats->setDateTo(
                new \DateTime($requestData['date_to'])
            );
        }

        if (isset($requestData['width_from'])) {
            $stats->setWidthFrom(
                $requestData['width_from']
            );
        }

        if (isset($requestData['width_to'])) {
            $stats->setWidthTo(
                $requestData['width_to']
            );
        }

        if (isset($requestData['height_from'])) {
            $stats->setHeightFrom(
                $requestData['height_from']
            );
        }

        if (isset($requestData['height_to'])) {
            $stats->setHeightTo(
                $requestData['height_to']
            );
        }

        return $stats;
    }

    /**
     * @param array $requestData
     * @return Search
     * @throws UnknownGroupKey
     */
    protected function searchDomainType(array $requestData)
    {
        $stats = SearchDomainType::getStats($requestData['key']);
        if (isset($requestData['domain'])) {
            $stats->setDomain(
                $requestData['domain']
            );
        }

        if (isset($requestData['date_from'])) {
            $stats->setDateFrom(
                new \DateTime($requestData['date_from'])
            );
        }

        if (isset($requestData['date_to'])) {
            $stats->setDateTo(
                new \DateTime($requestData['date_to'])
            );
        }

        if (isset($requestData['ad_type'])) {
            $stats->setAdType($requestData['ad_type']);
        }

        return $stats;
    }

    /**
     * @param Search $stats
     * @return array
     */
    protected function format(Search $stats)
    {
        /** получаем результат с учетом фильров и группировок */
        return [
            'data' => $this->updateAmounts(
                $stats->fetchAll()
            ),
            'count' => $stats->getCount(),
        ];
    }

    protected function updateAmounts(array $data)
    {
        if (count($data) == 0) {
            return $data;
        }

        if (!$this->user instanceof TJUser) {
            return $data;
        }

        foreach ($data as &$row) {
            foreach ($this->rateFields as $rateField) {
                $row[$rateField] = $this->user->getRate() * $row[$rateField] / 100;
            }
        }

        return $data;
    }

    /**
     * @param array $requestData
     * @return Search
     * @throws UnknownGroupKey
     */
    protected function searchStats(array $requestData)
    {
        $key = $requestData['key'];
        if (array_key_exists($key, SearchBase::GROUP_KEY_MAP)) {
            return $this->searchBase($requestData);
        } elseif (array_key_exists($key, SearchDomain::GROUP_KEY_MAP)) {
            $this->checkAdminUser();
            return $this->searchDomain($requestData);
        } elseif (array_key_exists($key, SearchDomainType::GROUP_KEY_MAP)) {
            $this->checkAdminUser();
            return $this->searchDomainType($requestData);
        } else {
            throw new UnknownGroupKey("Unknown key [$key]");
        }
    }

    protected function checkAdminUser()
    {
        if (!$this->user instanceof AdminUser) {
            throw new \Exception('adminUser access control failed');
        }
    }
}
