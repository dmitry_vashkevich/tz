<?php

namespace Project\Service;

use Project\Entity\Wallet;
use Project\Payment\Create;
use Project\Payment\Search;
use Project\Service;

/**
 * Сервис для работы с выплатами
 */
class Payment extends UserService
{
    /**
     * Запрос выплаты
     *
     * @param array $requestData
     * @return array
     */
    public function request(array $requestData)
    {
        $wallet = Wallet::prepareById($requestData['wallet_id']);
        
        $payment = (new Create($this->user, $wallet))
            ->process(
                $requestData['amount'],
                isset($requestData['comment']) ? $requestData['comment'] : null
            );

        return $this->format($payment);
    }

    /**
     * Поиск выплат
     *
     * @param array $requestData
     * @return array
     */
    public function search(array $requestData)
    {
        if (isset($requestData['date_from'])) {
            $dateFrom = new \DateTime($requestData['date_from']);
        } else {
            $dateFrom = (new \DateTime())->sub(new \DateInterval('P1M'));
        }

        if (isset($requestData['date_to'])) {
            $dateTo = new \DateTime($requestData['date_to']);
        } else {
            $dateTo = new \DateTime();
        }

        $offset = isset($requestData['offset']) ? $requestData['offset'] : 0;
        $limit = isset($requestData['limit']) ? $requestData['limit'] : 100;
        
        $search = new Search($this->user);
        $payments = $search->process(
            $dateFrom,
            $dateTo,
            $offset,
            $limit
        );

        $result = [];

        if (!$payments->isEmpty()) {
            foreach ($payments as $payment) {
                $result[] = $this->format($payment);
            }

        }
        
        return [
            'count' => $search->getCount(),
            'data' => $result,
        ];
    }
    
    protected function format(\Project\Entity\Payment $payment) {
        return [
            'changed' => $payment->getChanged()->format(\DateTime::RFC3339),
            'wallet_type_name' => $payment->getWallet()->getType()->getName(),
            'wallet_number' => $payment->getWallet()->getNumber(),
            'amount' => $payment->getAmount(),
            'comment' => $payment->getComment(),
            'status' => $payment->getStatus(),
        ];
    }
}
