<?php

namespace Project\Service;

use Project\Check\Validator;
use Project\Entity\TJUser;
use Project\Entity\WalletType;
use Project\Service;
use Project\Wallet\Create;
use Project\Wallet\Search;
use Project\Wallet\Update;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Сервис для работы с кошельками
 */
class Wallet extends UserService
{
    /** @var  TJUser */
    protected $user;

    /**
     * Поиск кошельков
     * - в текущей реализации отдает список целиком
     *
     * @return array
     */
    public function search()
    {
        $wallets = (new Search($this->user))->process();

        $result = [];

        if ($wallets->isEmpty()) {
            return $result;
        }

        foreach ($wallets as $wallet) {
            $result[] = $this->format($wallet);
        }

        return $result;
    }

    /**
     * Создание кошелька
     *
     * @param array $requestData
     * @return array
     */
    public function create(array $requestData)
    {
        $walletType = WalletType::prepareById($requestData['wallet_type_id']);
        $wallet = (new Create($this->user, $walletType))
            ->process(
                $requestData['number'],
                isset($requestData['comment']) ? $requestData['comment'] : null
            );

        return $this->format($wallet);
    }

    /**
     * Обновление данных кошелька
     *
     * @param int $walletId
     * @param array $requestData
     * @return array
     */
    public function update($walletId, array $requestData)
    {
        $wallet = \Project\Entity\Wallet::prepareById($walletId);
        (new Update($this->user, $wallet))->process(
            isset($requestData['active'])
                ? filter_var($requestData['active'], FILTER_VALIDATE_BOOLEAN)
                : $wallet->isActive()
        );

        return $this->format($wallet);
    }

    /**
     * @param $walletId
     * @return array
     */
    public function delete($walletId)
    {
        $wallet = \Project\Entity\Wallet::prepareByParams(
            [
                'id' => $walletId,
                'deleted' => false,
            ]
        );

        Validator::validateValue(
            $this->user->getId(),
            new Assert\EqualTo(
                $wallet->getUser()->getId()
            ),
            'Wallet User must be equal to Session User'
        );

        $wallet->setDeleted(true);
        $wallet->save();

        return ['true'];
    }
    
    /**
     * @param \Project\Entity\Wallet $wallet
     * @return array
     */
    protected function format(\Project\Entity\Wallet $wallet)
    {
        return [
            'id' => $wallet->getId(),
            'number' => $wallet->getNumber(),
            'comment' => $wallet->getComment(),
            'active' => $wallet->isActive(),
            'status' => $wallet->getStatus(),
            'wallet_type' => [
                'id' => $wallet->getType()->getId(),
                'name' => $wallet->getType()->getName(),
            ],
        ];
    }
}
