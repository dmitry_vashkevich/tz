<?php

namespace Project\Service\Admin;

use Project\Ad\View;
use Project\Entity\TJBlock;
use Project\Entity\TJUser as AdminUserEntity;
use Project\Entity\TJUser;
use Project\Service;
use Project\Check\Validator;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Сервис для работы с блоками
 */
class Block extends Service\Block
{
    /**
     * Пользователь
     *
     * @var AdminUserEntity
     */
    protected $user;

    /**
     * @inheritdoc
     */
    protected function validateBlockUser(TJBlock $block)
    {
        //ограничений для админа нет
    }

    /**
     * @inheritdoc
     */
    protected function getSearchUser(array $requestData)
    {
        $user = null;
        if (isset($requestData['tj_user_id'])) {
            $user = TJUser::prepareById($requestData['tj_user_id']);
        }
        
        return $user;
    }

    /**
     * @inheritdoc
     */
    protected function getSiteService()
    {
        return new Site($this->user);
    }
}
