<?php

namespace Project\Service\Admin;

use Project\Check\Validator;
use Project\Entity\AdminUser;
use Project\Service;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Сервис для работы с лендами от Admin
 */
class Land extends Service\Land
{
    /**
     * Пользователь
     *
     * @var AdminUser
     */
    protected $user;
    
    /**
     * @inheritdoc
     */
    protected function validateUser()
    {
        //админу можно все
    }
}
