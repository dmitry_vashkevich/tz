<?php

namespace Project\Service\Admin;

use Project\Service;
use Project\AdminUser\Auth;
use Project\AdminUser\Registration;
use Project\AdminUser\Session;

/**
 * Сервис для работы с admin-юзерами
 */
class User extends Service\User
{
    /**
     * @var \Project\Entity\TJUser
     */
    protected $user;

    /**
     * Регистрация пользователя
     *
     * @param array $requestData
     * @return array
     * @throws \Exception
     */
    public function register(array $requestData)
    {
        throw new \Exception('admin user registration disabled');
        return parent::register($requestData);
    }

    /**
     * @inheritdoc
     */
    protected function getAuth($login, $password)
    {
        return new Auth($login, $password);
    }

    /**
     * @inheritdoc
     */
    protected function getRegistration($login, $password)
    {
        return new Registration($login, $password);
    }

    /**
     * @inheritdoc
     */
    protected function getSession($sessionId)
    {
        return new Session($sessionId);
    }
}
