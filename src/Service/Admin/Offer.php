<?php

namespace Project\Service\Admin;

use Project\Check\Validator;
use Project\Entity\AdminUser;
use Project\Service;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Сервис для работы с офферами от Admin
 */
class Offer extends Service\Offer
{
    /**
     * Пользователь
     *
     * @var AdminUser
     */
    protected $user;
    
    /**
     * @inheritdoc
     */
    protected function validateUser()
    {
        //админу можно все
    }
}
