<?php

namespace Project\Service\Admin;

use Project\Entity\TJSite;
use Project\Entity\TJUser;
use Project\Service;
use Project\Check\Validator;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Сервис для работы с площадками
 */
class Site extends Service\Site
{
    /**
     * Пользователь
     *
     * @var TJUser
     */
    protected $user;

    protected function validateSiteUser(TJSite $site)
    {
        //у админа нет
    }

    /**
     * @inheritdoc
     */
    protected function getSearchUser(array $requestData)
    {
        $user = null;
        if (isset($requestData['tj_user_id'])) {
            $user = TJUser::prepareById($requestData['tj_user_id']);
        }

        return $user;
    }
}
