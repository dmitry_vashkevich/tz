<?php

namespace Project\Service\TJ;

use Project\Entity\TJUser;
use Project\Service;
use Project\TJUser\Auth;
use Project\TJUser\Registration;
use Project\TJUser\Search;
use Project\TJUser\Session;

/**
 * Сервис для работы с tj-юзерами
 */
class User extends Service\User
{
    /**
     * @var \Project\Entity\TJUser
     */
    protected $user;

    /**
     * Регистрация пользователя
     *
     * @param array $requestData
     * @return array
     */
    public function register(array $requestData)
    {
        $reg = $this->getRegistration($requestData['login'], $requestData['password']);
        $reg->setSkype(isset($requestData['skype']) ? $requestData['skype'] : null);
        $reg->setIcq($requestData['icq']);
        $reg->setJabber($requestData['jabber']);
        $reg->setSiteList($requestData['site_list']);
        if (isset($requestData['ref_id'])) {
            $reg->setRefId($requestData['ref_id']);
        }
        $reg->process();

        return [
            'success' => true,
        ];
    }

    /**
     * Обновление данных пользователя
     *
     * @param array $requestData
     * @return array
     */
    public function update(array $requestData)
    {
        if (isset($requestData['first_name'])) {
            $this->user->setFirstName($requestData['first_name']);
        }

        if (isset($requestData['second_name'])) {
            $this->user->setSecondName($requestData['second_name']);
        }

        if (isset($requestData['email'])) {
            $this->user->setEmail($requestData['email']);
        }
        
        if (isset($requestData['skype'])) {
            $this->user->setSkype($requestData['skype']);
        }

        if (isset($requestData['icq'])) {
            $this->user->setIcq($requestData['icq']);
        }

        if (isset($requestData['jabber'])) {
            $this->user->setJabber($requestData['jabber']);
        }

        if (isset($requestData['auto_pay'])) {
            $this->user->setAutoPay($requestData['auto_pay']);
        }
        
        $this->user->save();

        return $this->format($this->user);
    }

    /**
     * Получение данных пользователя
     *
     * @return array
     */
    public function info()
    {
        return $this->format($this->user);
    }

    /**
     * Получение списка пользователей
     *
     * @param array $requestData
     * @return array
     */
    public function search(array $requestData)
    {
        $search = new Search();
        $users = $search->process(
            isset($requestData['login']) ? $requestData['login'] : null,
            isset($requestData['offset']) ? $requestData['offset'] : null,
            isset($requestData['limit']) ? $requestData['limit'] : null
        );

        if ($users->isEmpty()) {
            return [];
        }

        $result = [];
        /** @var \Project\Entity\SiteUser $user */
        foreach ($users as $user) {
            $result[] = [
                'id' => $user->getId(),
                'login' => $user->getLogin(),
            ];
        }

        return [
            'count' => $search->getCount(),
            'data' => $result,
        ];
    }
    
    /**
     * @param TJUser $user
     * @return array
     */
    protected function format(TJUser $user)
    {
        return [
            'firstName' => $user->getFirstName(),
            'secondName' => $user->getSecondName(),
            'email' => $user->getEmail(),
            'skype' => $user->getSkype(),
            'icq' => $user->getIcq(),
            'jabber' => $user->getJabber(),
            'autoPay' => $user->isAutoPay(),
            'balance' => $user->getBalance(),
            'hold' => $user->getHold(),
        ];
    }

    /**
     * @inheritdoc
     */
    protected function getAuth($login, $password)
    {
        return new Auth($login, $password);
    }

    /**
     * @inheritdoc
     */
    protected function getRegistration($login, $password)
    {
        return new Registration($login, $password);
    }

    /**
     * @inheritdoc
     */
    protected function getSession($sessionId)
    {
        return new Session($sessionId);
    }
}
