<?php

namespace Project\Service\TJ;

use Project\Entity\TJSite;
use Project\Entity\TJUser as TJUserEntity;
use Project\Service;
use Project\TJSite\Search;
use Project\Check\Validator;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Сервис для работы с площадками
 */
class Site extends Service\Site
{
    /**
     * Пользователь
     *
     * @var TJUserEntity
     */
    protected $user;

    protected function validateSiteUser(TJSite $site)
    {
        Validator::validateValue(
            $this->user->getId(),
            new Assert\EqualTo(
                $site->getUser()->getId()
            ),
            'TJ Site User must be equal to Session User'
        );
    }

    /**
     * @param array $requestData
     * @return array
     */
    public function create(array $requestData)
    {
        $site = (new TJSite())
            ->setName($requestData['name'])
            ->setUrl($requestData['url'])
            ->setThemes($requestData['themes'])
            ->setStatlink(isset($requestData['statlink']) ? $requestData['statlink'] : null)
            ->setParams($requestData['params'])
            ->setActive(isset($requestData['active']) && $requestData['active'] == 'true' ? true : false)
            ->setUser($this->user)
            ->save();

        if (isset($requestData['alias'])) {
            $site->setAlias($requestData['alias']);
        }

        return $this->format(
            $site
        );
    }

    /**
     * @param array $requestData
     * @return array
     */
    public function activeAll(array $requestData)
    {
        $sites = (new Search($this->user))->process();
        foreach ($sites as $site) {
            $site
                ->setActive($requestData['active'] == 'true' ? true : false)
                ->save();
        }

        return ['result' => true];
    }

    /**
     * @param array $requestData
     * @return null|TJUserEntity
     */
    protected function getSearchUser(array $requestData)
    {
        return $this->user;
    }
}
