<?php

namespace Project\Service\TJ;

use Project\Ad\View;
use Project\Entity\TJBlock;
use Project\Entity\TJSite;
use Project\Entity\TJUser as TJUserEntity;
use Project\Service;
use Project\Check\Validator;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Сервис для работы с блоками
 */
class Block extends Service\Block
{
    /**
     * Пользователь
     *
     * @var TJUserEntity
     */
    protected $user;

    /**
     * @inheritdoc
     */
    protected function validateBlockUser(TJBlock $block)
    {
        Validator::validateValue(
            $this->user->getId(),
            new Assert\EqualTo(
                $block->getUser()->getId()
            ),
            'TJ Block User must be equal to Session User'
        );
    }
    
    /**
     * @inheritdoc
     */
    protected function getSearchUser(array $requestData)
    {
        return $this->user;
    }

    /**
     * @param int $siteId
     * @param array $requestData
     * @return array
     */
    public function create($siteId, array $requestData)
    {
        $site = TJSite::prepareById($siteId);
        Validator::validateValue(
            $this->user->getId(),
            new Assert\EqualTo(
                $site->getUser()->getId()
            ),
            'TJ Site User must be equal to Session User'
        );

        $block = new TJBlock();
        $block->setAdType($requestData['ad_type']);
        $block->setName($requestData['name']);
        $block->setCustom($requestData['custom'] == 'true' ? true : false);
        $block->setSite($site);
        $block->setUser($this->user);

        if ($block->isCustom()) {
            if (isset($requestData['params'])) {
                $block->setParams($requestData['params']);
            }

            if (isset($requestData['config'])) {
                $block->setConfig($requestData['config']);
            }
        }

        if (isset($requestData['active'])) {
            $block->setActive($requestData['active'] == 'true' ? true : false);
        }

        $block->save();

        return $this->format($block);
    }

    /**
     * @inheritdoc
     */
    protected function getSiteService()
    {
        return new Site($this->user);
    }
}
