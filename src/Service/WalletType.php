<?php

namespace Project\Service;

use Project\Service;
use Project\WalletType\Search;

/**
 * Сервис для работы с типами кошельков
 */
class WalletType extends UserService
{
    /**
     * Поиск типов кошельков
     * - в текущей реализации отдает список целиком
     *
     * @return array
     */
    public function search()
    {
        $types = (new Search())->process();

        $result = [];

        foreach ($types as $type) {
            $result[] = [
                'id' => $type->getId(),
                'name' => $type->getName(),
            ];
        }

        return $result;
    }
}
