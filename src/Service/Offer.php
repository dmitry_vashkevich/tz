<?php

namespace Project\Service;

use Project\Check\Exception\ValidationException;
use Project\Service;
use Project\Offer\Search;

/**
 * Сервис для работы с офферами
 */
abstract class Offer extends UserService
{
    /**
     * Поиск офферов
     *
     * @param array $requestData
     * @return array
     */
    public function search(array $requestData)
    {
        $this->validateUser();
        $offers = (new Search($this->user))->process(
            isset($requestData['name']) ? $requestData['name'] : null
        );

        $result = [];

        foreach ($offers as $offer) {
            $result[] = [
                'id' => $offer->getId(),
                'name' => $offer->getName(),
                'description' => $offer->getDescription(),
                'model' => $offer->getModel(),
                'imageSmallUrl' => $offer->getImageSmallUrl(),
                'imageUrl' => $offer->getImageUrl(),
            ];
        }

        return $result;
    }

    /**
     * @throws ValidationException
     */
    abstract protected function validateUser();
}
