<?php

namespace Project\Service;

use Doctrine\Common\Collections\Criteria;
use Project\Check\Exception\ValidationException;
use Project\Check\Validator;
use Project\Entity\Offer;
use Project\Land\Search;
use Project\Service;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Сервис для работы с лендами
 */
abstract class Land extends UserService
{
    /**
     * Поиск
     *
     * @param array $requestData
     * @return array
     */
    public function search(array $requestData)
    {
        $this->validateUser();

        $offer = null;
        if (isset($requestData['offer_id'])) {
            $offer = Offer::prepareById($requestData['offer_id']);
        }

        $ids = null;
        if (isset($requestData['land_ids'])) {
            $ids = json_decode($requestData['land_ids'], true);
        }

        $order = isset($requestData['order']) ? $requestData['order'] : 'id';
        $direction = isset($requestData['direction']) ? $requestData['direction'] : Criteria::DESC;

        $search = new Search();
        
        $searchResult = $search->process(
            $offer,
            $ids,
            true,
            $order,
            $direction
        );

        if (count($searchResult) == 0) {
            return [];
        }

        return $searchResult;
    }

    /**
     * @throws ValidationException
     */
    abstract protected function validateUser();
}
