<?php

namespace Project\Service;

use Project\Service;
use Project\SiteUser\Auth;
use Project\SiteUser\Registration;
use Project\SiteUser\Search;
use Project\SiteUser\Session;

/**
 * Сервис для работы с сайт-юзерами
 */
class SiteUser extends User
{
    /**
     * @var \Project\Entity\SiteUser
     */
    protected $user;

    /**
     * Получение данных пользователя
     *
     * @return array
     */
    public function info()
    {
        return [
            'key' => $this->user->getKey(),
            'external' => $this->user->isExternal(),
        ];
    }

    /**
     * Получение списка пользователей
     *
     * @param array $requestData
     * @return array
     */
    public function search(array $requestData)
    {
        $search = new Search();
        $users = $search->process(
            isset($requestData['login']) ? $requestData['login'] : null,
            isset($requestData['offset']) ? $requestData['offset'] : null,
            isset($requestData['limit']) ? $requestData['limit'] : null
        );
        
        if ($users->isEmpty()) {
            return [];
        }
        
        $result = [];
        /** @var \Project\Entity\SiteUser $user */
        foreach ($users as $user) {
            $result[] = [
                'id' => $user->getId(),
                'login' => $user->getLogin(),
            ];
        }
        
        return [
            'count' => $search->getCount(),
            'data' => $result,
        ];
    }
    
    /**
     * @inheritdoc
     */
    protected function getAuth($login, $password)
    {
        return new Auth($login, $password);
    }

    /**
     * @inheritdoc
     */
    protected function getRegistration($login, $password)
    {
        return new Registration($login, $password);
    }

    /**
     * @inheritdoc
     */
    protected function getSession($sessionId)
    {
        return new Session($sessionId);
    }
}
