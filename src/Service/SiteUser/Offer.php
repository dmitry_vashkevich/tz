<?php

namespace Project\Service\SiteUser;

use Project\Check\Validator;
use Project\Entity\SiteUser;
use Project\Service;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Сервис для работы с офферами от SiteUser
 */
class Offer extends Service\Offer
{

    /**
     * Пользователь
     *
     * @var SiteUser
     */
    protected $user;

    /**
     * @inheritdoc
     */
    protected function validateUser()
    {
        Validator::validateValue(
            $this->user->isExternal(),
            new Assert\EqualTo(false),
            'User have to be internal for this operation'
        );
    }
}
