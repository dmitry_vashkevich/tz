<?php

namespace Project\Service;

use Project\Service;
use Project\TeaserConfig\Search;
use Project\TeaserConfig\SearchByBlock;

/**
 * Сервис для работы с конфигами тизеров
 */
class TeaserConfig extends Service
{
    /**
     * Поиск
     *
     * @param array $requestData
     * @return array
     */
    public function searchByBlock(array $requestData)
    {
        return (new SearchByBlock\DB($requestData['width'], $requestData['height']))
            ->process();
    }

    /**
     * Поиск
     *
     * @param array $requestData
     * @return array
     */
    public function search(array $requestData)
    {
        $configs = (new Search())->process(
            isset($requestData['b_width_from']) ? $requestData['b_width_from'] : null,
            isset($requestData['b_width_to']) ? $requestData['b_width_to'] : null,
            isset($requestData['b_height_from']) ? $requestData['b_height_from'] : null,
            isset($requestData['b_height_to']) ? $requestData['b_height_to'] : null
        );

        $result = [];
        /** @var \Project\Entity\TeaserConfig $config */
        foreach ($configs as $config) {
            $result[] = [
                'id' => $config->getId(),
                'data' => $config->getData(),
                'b_width_from' => $config->getBlockWidthFrom(),
                'b_width_to' => $config->getBlockWidthTo(),
                'b_height_from' => $config->getBlockHeightFrom(),
                'b_height_to' => $config->getBlockHeightTo(),
            ];
        }

        return $result;
    }


    /**
     * Создание
     *
     * @param array $requestData
     * @return array
     */
    public function create(array $requestData)
    {
        $teaserConfig = (new \Project\Entity\TeaserConfig())
            ->setData($requestData['data'])
            ->setBlockWidthFrom($requestData['b_width_from'])
            ->setBlockWidthTo($requestData['b_width_to'])
            ->setBlockHeightFrom($requestData['b_height_from'])
            ->setBlockHeightTo($requestData['b_height_to'])
            ->save();
        return [
            'id' => $teaserConfig->getId(),
        ];
    }
}
