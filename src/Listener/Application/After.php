<?php

namespace Project\Listener\Application;

use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Project\Listener\Listener;
use Project\Logger;

/**
 * Слушатель события application:after
 *
 * Вызывается, когда ответ уже готов, но еще не отправлен
 */
class After extends Listener
{
    /**
     * @var \Symfony\Component\HttpFoundation\Request
     */
    private $request;

    /**
     * @var \Symfony\Component\HttpFoundation\Response
     */
    private $response;


    /**
     * Конструктор
     *
     * @param Request $request
     * @param Response $response
     */
    public function __construct(Request $request, Response $response)
    {
        $this->request = $request;
        $this->response = $response;
    }

    /**
     * Запуск обработчика
     *
     * @return mixed|void
     */
    public function run()
    {
        if ($this->response instanceof JsonResponse) {
            $this->response->setEncodingOptions(
                JSON_PRETTY_PRINT |
                JSON_UNESCAPED_SLASHES |
                JSON_UNESCAPED_UNICODE
            );
        }
    }
}
