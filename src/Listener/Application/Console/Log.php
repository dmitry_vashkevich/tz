<?php

namespace Project\Listener\Application\Console;

use Psr\Log\LoggerInterface;
use Symfony\Component\Console\ConsoleEvents;
use Symfony\Component\Console\Event\ConsoleCommandEvent;
use Symfony\Component\Console\Event\ConsoleExceptionEvent;
use Symfony\Component\Console\Event\ConsoleTerminateEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Project\Logger;

/**
 * Слушатель консольных событий
 *
 * Логгирует события связанные с запуском консольных скриптов
 */

class Log implements EventSubscriberInterface
{
    /**
     * Логгер
     *
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Лог в случае запуска консольной команды
     *
     * @param ConsoleCommandEvent $event
     */
    public function logCommand(ConsoleCommandEvent $event)
    {
        $logger = new Logger\ConsoleCommandEvent($event);
        $this->logger->info($logger->getMessage(), $logger->getContext());
    }

    /**
     * Лог в случае exception выброшенного из консольного скрипта
     *
     * @param ConsoleExceptionEvent $event
     */
    public function logException(ConsoleExceptionEvent $event)
    {
        $logger = new Logger\ConsoleExceptionEvent($event);
        $this->logger->error($logger->getMessage(), $logger->getContext());
    }

    /**
     * Лог в случае окончания работы консольного скрипта
     *
     * @param ConsoleTerminateEvent $event
     */
    public function logTerminate(ConsoleTerminateEvent $event)
    {
        $logger = new Logger\ConsoleTerminateEvent($event);
        $statusCode = $event->getExitCode();
        $message = $logger->getMessage();
        $context = $logger->getContext();

        if ($statusCode == 0) {
            $this->logger->info($message, $context);
        } else {
            $this->logger->warning($message, $context);
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            ConsoleEvents::COMMAND      => 'logCommand',
            ConsoleEvents::EXCEPTION    => 'logException',
            ConsoleEvents::TERMINATE    => 'logTerminate',
        ];
    }
}
