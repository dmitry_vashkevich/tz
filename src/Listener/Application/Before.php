<?php

namespace Project\Listener\Application;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Project\Listener\Listener;
use Project\Logger;

/**
 * Слушатель события application:before
 *
 * Вызывается как только пришел запрос но еще даже не определен контроллер
 */
class Before extends Listener
{
    /**
     * @var \Symfony\Component\HttpFoundation\Request
     */
    private $request;

    /**
     * Конструктор
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Запуск обработчика
     *
     * @return mixed|void
     */
    public function run()
    {
        //do something
    }
}
