<?php

namespace Project\Listener\Application;

use Silex\Application;
use Silex\EventListener\LogListener;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Project\Logger;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Слушатель системных событий
 *
 * Логгирует входыщие запросы и ответы
 */
class Kernel extends LogListener
{

    /**
     * Залоггировать запрос
     *
     * @param Request $request
     */
    protected function logRequest(Request $request)
    {
        $logger = new Logger\Request($request);
        $this->logger->info($logger->getMessage(), $logger->getContext());
    }

    /**
     * Залоггировать ответ
     *
     * @param Response $response
     */
    protected function logResponse(Response $response)
    {
        /*
        $logger = new Logger\Response($response);
        $this->logger->info($logger->getMessage(), $logger->getContext());
        */
    }

    /**
     * Логгируем исключение
     *
     * @param \Exception $e
     */
    public function logException(\Exception $e)
    {
        $exceptionLogger = new Logger\Exception($e);

        $message = $exceptionLogger->getMessage('uncaught exception');

        $context = $exceptionLogger->getContext();

        if ($e instanceof HttpExceptionInterface && $e->getStatusCode() < 500) {
            $this->logger->error($message, $context);
        } else {
            $this->logger->critical($message, $context);
        }
    }

    /**
     * Получить список событий на которые подписаны
     *
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::EXCEPTION => ['onKernelException', -4],
            KernelEvents::REQUEST => ['onKernelRequest', Application::EARLY_EVENT],
            KernelEvents::RESPONSE => ['onKernelResponse', Application::LATE_EVENT],
        ];
    }
}
