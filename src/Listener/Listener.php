<?php

namespace Project\Listener;

/**
 * Абстрактный класс слушателя
 */
abstract class Listener
{
    /**
     * Вызвать слушателя
     *
     * @return mixed
     */
    abstract public function run();
}
