<?php

namespace Project\Listener;

use Project\Callback\Action\AccountBalanceUpdatePayment;
use Project\Callback\Action\AccountBalanceUpdateStats;
use Project\Listener\Entity\Action\PostPersist;
use Project\Listener\Entity\Action\PreUpdate;
use Project\Proxy;

/**
 * Слушатель событий Doctrine
 */
class Entity extends Listener
{
    /**
     * Запуск слушателя за событиями Doctrine
     */
    public function run()
    {
        /**
        $evm = Proxy::init()->getEntityManager()->getEventManager();

        $preUpdateListener = new PreUpdate([
            new AccountBalanceUpdateStats(),
            new AccountBalanceUpdatePayment(),
        ]);

        $postPersistListener = new PostPersist([
            new AccountBalanceUpdateStats(),
//            new AccountBalanceUpdatePayment(),
        ]);

        $evm->addEventSubscriber($preUpdateListener);
        $evm->addEventSubscriber($postPersistListener);
         */
    }
}
