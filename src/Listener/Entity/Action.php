<?php

namespace Project\Listener\Entity;

use Doctrine\Common\EventSubscriber;

/**
 * Действия по разным событиям Doctrine
 */
abstract class Action implements EventSubscriber
{
    /**
     * @var \Project\Callback\Action[]
     */
    private $actions = [];

    /**
     * @param \Project\Callback\Action[] $actions
     */
    public function __construct(array $actions)
    {
        $this->actions = $actions;
    }

    /**
     * @return \Project\Callback\Action[]
     */
    public function getActions()
    {
        return $this->actions;
    }
}
