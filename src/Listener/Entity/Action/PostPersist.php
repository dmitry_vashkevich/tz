<?php

namespace Project\Listener\Entity\Action;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Project\Listener\Entity\Action;

/**
 * Слушает события PostPersist
 *
 * Сохранение новых данных в базу
 */
class PostPersist extends Action
{
    /**
     * Следим за событием вставки новой строки в БД
     *
     * @param LifecycleEventArgs $args
     */
    public function postPersist(LifecycleEventArgs $args)
    {
        foreach ($this->getActions() as $listener) {
            if ($listener->forEntity($args->getEntity())) {
                $listener->onInsert(
                    $args->getEntity()
                );
            }
        }
    }

    /**
     * Подписываемся на все собятия вставки в БД
     */
    public function getSubscribedEvents()
    {
        return [Events::postPersist];
    }
}
