<?php

namespace Project\Listener\Entity\Action;

use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;
use Project\Listener\Entity\Action;

/**
 * Слушает события PreUpdate
 *
 * Самый навороченный слушатель, который описан в документации Doctrine
 * Позволяет отслеживать все вызовы метода flush
 */
class PreUpdate extends Action
{
    /**
     * Слушает все события обновления строчек в БД
     *
     * @param PreUpdateEventArgs $args
     */
    public function preUpdate(PreUpdateEventArgs $args)
    {
        /**
         * @var \Project\Callback\Action $listener
         */

        foreach ($this->getActions() as $listener) {
            if ($listener->forEntity($args->getEntity())) {
                $listener->onChange(
                    $args->getEntity(),
                    $args->getEntityChangeSet()
                );
            }
        }
    }

    /**
     * Подписываемся на все события обновления данных в БД
     *
     * @return array
     */
    public function getSubscribedEvents()
    {
        return array(Events::preUpdate);
    }
}
