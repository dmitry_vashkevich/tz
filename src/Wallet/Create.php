<?php

namespace Project\Wallet;

use Project\Entity\TJUser;
use Project\Entity\Wallet;
use Project\Entity\WalletType;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Создание кошелька
 */
class Create
{
    /**
     * Пользователь
     *
     * @var TJUser
     */
    protected $user;

    /**
     * Кошелек
     *
     * @var WalletType
     */
    protected $walletType;

    /**
     * Конструктор
     *
     * @param TJUser $user
     * @param WalletType $walletType
     */
    public function __construct(TJUser $user, WalletType $walletType)
    {
        $this->user = $user;
        $this->walletType = $walletType;
    }

    /**
     * Создание кошелька
     *
     * @param $number
     * @param string | null $comment
     * @return Wallet
     */
    public function process($number, $comment = null)
    {
        return (new Wallet())
            ->setUser($this->user)
            ->setType($this->walletType)
            ->setNumber($number)
            ->setComment($comment)
            ->save();
    }
}
