<?php

namespace Project\Wallet;

use Doctrine\Common\Collections\Criteria;
use Project\Entity\Search\SearchInUser;
use Project\Entity\TJUser;
use Project\Entity\Wallet;

/**
 * Поиск кошельков
 */
class Search extends SearchInUser
{
    /**
     * Юзер
     *
     * @var TJUser
     */
    protected $user;

    /**
     * Поиск кошельков
     *
     * @param null $notId
     * @return \Doctrine\Common\Collections\Collection|\Project\Entity\Wallet[]
     */
    public function process($notId = null)
    {
        $criteria = (new Criteria())->where(
            Criteria::expr()->andX(
                Criteria::expr()->eq('user', $this->user),
                Criteria::expr()->eq('deleted', false)
            )
        );
        
        if (isset($notId)) {
            $criteria->andWhere(
                Criteria::expr()->neq('id', $notId)
            );
        }

        return Wallet::prepareList($criteria);
    }
}
