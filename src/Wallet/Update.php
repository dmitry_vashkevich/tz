<?php

namespace Project\Wallet;

use Project\Check\Validator;
use Project\Entity\TJUser;
use Project\Entity\Wallet;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Обновление кошелька
 */
class Update
{
    /**
     * Пользователь
     *
     * @var TJUser
     */
    protected $user;

    /**
     * Кошелек
     *
     * @var Wallet
     */
    protected $wallet;

    /**
     * Конструктор
     *
     * @param TJUser $user
     * @param Wallet $wallet
     */
    public function __construct(TJUser $user, Wallet $wallet)
    {
        $this->user = $user;
        $this->wallet = $wallet;

        $this->validate();
    }

    /**
     * Валидация
     *
     * @throws \Project\Check\Exception\ValidationException
     */
    protected function validate()
    {
        Validator::validateValue(
            $this->user->getId(),
            new Assert\EqualTo(
                $this->wallet->getUser()->getId()
            )
        );
    }

    /**
     * Обновление размещения
     *
     * @param bool $active
     * @return Wallet
     * @throws \Project\Entity\Exception\IllegalValue
     */
    public function process($active)
    {
        $this->wallet
            ->setActive($active)
            ->save();

        if ($this->wallet->isActive()) {
            (new Search($this->user))->process($this->wallet->getId())
                ->map(
                    function ($wallet) {
                        /** @var Wallet $wallet */
                        $wallet
                            ->setActive(false)
                            ->save();
                    }
                );
        }
    }
}
