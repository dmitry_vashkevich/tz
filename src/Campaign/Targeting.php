<?php

namespace Project\Campaign;

use Project\Check\Validator;
use Project\Entity\Campaign;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Создание таргетинга кампании
 */
class Targeting extends UserAction
{


    /**
     * Сборка Flow
     *
     * @param $countries
     * @param $browsers
     * @param $platforms
     * @return Campaign
     */
    public function process(array $countries = null, array $browsers = null, array $platforms = null)
    {
        return $this->campaign
            ->setTargetCountries($countries !== null ? json_encode($countries) : null)
            ->setTargetBrowsers($browsers !== null ? json_encode($browsers) : null)
            ->setTargetPlatforms($platforms !== null ? json_encode($platforms) : null)
            ->save();
    }
}
