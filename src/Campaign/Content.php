<?php

namespace Project\Campaign;

use Project\Check\Validator;
use Project\Entity\Ad;
use Project\Proxy;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Загрузка контента
 */
class Content extends UserAction
{
    /**
     * @param $data
     * @param $files
     * @return Ad
     */
    public function process($data, $files)
    {
        $files = $this->processFiles($files);
        return (new Ad())
            ->setData($data)
            ->setType($this->campaign->getAdType())
            ->setImages(json_encode($files))
            ->setCampaign($this->campaign)
            ->save();
    }

    /**
     * Обработка файлов
     * @param $files
     * @return array|void
     */
    protected function processFiles($files)
    {
        if (count($files) <= 0) {
            return null;
        }

        $dirName = $this->prepareDirName();
        if (!is_dir($dirName['wsPath'])) {
            mkdir($dirName['wsPath'], 0777, true);
        }
        $newFiles = [];

        /** @var UploadedFile $file */
        foreach ($files as $file) {
            $fileName = uniqid() . '.' . $file->guessClientExtension();
            $file->move($dirName['wsPath'], $fileName);
            $newFiles[] = 'storage/' . $dirName['wosPath'] . '/' . $fileName;
        }

        return $newFiles;
    }

    /**
     * @return array
     */
    protected function prepareDirName()
    {
        $wosPath = $this->user->getId() . '/' . $this->campaign->getId();
        $wsPath = Proxy::init()->getConfig()['storage'] . '/' . $wosPath;
        return [
            'wosPath' =>$wosPath,
            'wsPath' =>$wsPath,
        ];
    }
}
