<?php

namespace Project\Campaign\Flow\Exception;

use Project\Exception\RestException\ServerError\InternalServerErrorException;

/**
 * Когда задан пустой flow
 */
class EmptyFlowException extends InternalServerErrorException
{

}
