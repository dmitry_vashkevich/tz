<?php

namespace Project\Campaign;

use Project\Campaign\Limit\Exception\CheckLimitException;
use Project\Proxy;

/**
 * Модуль ограничений для кампаний
 */
class Limit
{
    /** куки */
    const COOKIE_NAME_SHOW = 'cslimit';

    /** @var array */
    protected $userShowLimit;

    /**
     * Конструктор
     */
    public function __construct()
    {
        $this->userShowLimit = $this->getUserShowLimit(
            Proxy::init()->getRequest()->cookies->all()
        );
    }

    /**
     * @param array $cookies
     * @return string | null
     */
    protected function getUserShowLimit(array $cookies)
    {
        if (isset($cookies[self::COOKIE_NAME_SHOW])) {
            $res = unserialize($cookies[self::COOKIE_NAME_SHOW]);
            if (is_array($res) && count($res) > 0) {
                return $res;
            }
        }

        return null;
    }

    /**
     * @param $campaignId
     * @param $campaignUserShowLimit
     * @throws CheckLimitException
     */
    public function checkUserShowLimit($campaignId, $campaignUserShowLimit = null)
    {
        if ($campaignUserShowLimit === null) {
            return;
        }

        if (isset($this->userShowLimit[$campaignId]) && $this->userShowLimit[$campaignId] >= $campaignUserShowLimit) {
            throw  new CheckLimitException("campaign[$campaignId] userShowLimit exceeded");
        }
    }

    /**
     * Устанавливаем лимиты показа кампаний
     *
     * @param array $cacheAds
     */
    public function setUserShowLimit(array $cacheAds)
    {
        foreach ($cacheAds as $ad) {
            if ($ad['limitUserShow'] !== null) {
                if (!isset($this->userShowLimit[$ad['campaignId']])) {
                    $this->userShowLimit[$ad['campaignId']] = 0;
                }

                $this->userShowLimit[$ad['campaignId']]++;
            }
        }

        if ($this->userShowLimit !== null) {
            setcookie(self::COOKIE_NAME_SHOW, serialize($this->userShowLimit), time() + 86400 * 365, '/');
        }
    }
}
