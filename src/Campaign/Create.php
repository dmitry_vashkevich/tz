<?php

namespace Project\Campaign;

use Project\Campaign\Exception\IncorrectParamsException;
use Project\Check\Validator;
use Project\Entity\Campaign;
use Project\Entity\Offer;
use Project\Entity\SiteUser;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Создание кампании
 */
class Create
{
    /**
     * Пользователь
     *
     * @var SiteUser
     */
    protected $user;

    /**
     * @var string
     */
    protected $adType;

    /**
     * @var string
     */
    protected $name;

    /**
     * Конструктор
     *
     * @param SiteUser $user
     * @param string $adType
     * @param $name
     */
    public function __construct(SiteUser $user, $adType, $name)
    {
        $this->user = $user;
        $this->adType = $adType;
        $this->name = $name;
    }

    /**
     * Валидация
     *
     * @param Offer $offer
     * @param $cpm
     * @param $url
     * @param $script
     * @throws IncorrectParamsException
     */
    protected function validate(Offer $offer = null, $cpm, $url, $script)
    {
        if ($offer !== null && $this->user->isExternal()) {
            throw new IncorrectParamsException(
                'User have to be "internal" for creating campaign with offer'
            );
        }

        if (
            ($offer === null && ($cpm === null || ($url === null && $script === null)))
            ||
            ($offer !== null && ($cpm !== null || $url !== null || $script !== null))
        ) {
            throw new IncorrectParamsException(
                'cpm and url / script must be existed for external campaign, otherwise offer have to be existed'
            );
        }
    }

    /**
     * Создание кампании
     *
     * @param Offer $offer
     * @param $cpm
     * @param $url
     * @param $script
     * @param $limitUserShow
     * @param $limitAmount
     * @param int $blockWidthFrom
     * @param int $blockWidthTo
     * @param int $blockHeightFrom
     * @param int $blockHeightTo
     * @param bool $archive
     * @return Campaign
     * @throws IncorrectParamsException
     */
    public function process(Offer $offer = null, $cpm = null, $url = null, $script = null, $limitUserShow = null, $limitAmount = null,
        $blockWidthFrom = null, $blockWidthTo = null, $blockHeightFrom = null, $blockHeightTo = null, $archive = false)
    {
        $this->validate($offer, $cpm, $url, $script);

        $external = true;
        if ($offer !== null) {
            $external = false;
        }

        return (new Campaign())
            ->setName($this->name)
            ->setSiteUser($this->user)
            ->setAdType($this->adType)
            ->setExternal($external)
            ->setOffer($offer)
            ->setCpm($cpm)
            ->setUrl($url)
            ->setScript($script)
            ->setLimitAmount($limitAmount)
            ->setLimitUserShow($limitUserShow)
            ->setBlockWidthFrom($blockWidthFrom)
            ->setBlockWidthTo($blockWidthTo)
            ->setBlockHeightFrom($blockHeightFrom)
            ->setBlockHeightTo($blockHeightTo)
            ->setArchive($archive)
            ->save();
    }
}
