<?php

namespace Project\Campaign\Limit\Exception;

use Project\Exception\RestException\ClientError\BadRequestException;

/**
 * При выполнении проверки на лимиты для кампании
 */
class CheckLimitException extends BadRequestException
{

}
