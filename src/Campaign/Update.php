<?php

namespace Project\Campaign;

use Project\Campaign\Exception\IncorrectParamsException;
use Project\Check\Validator;
use Project\Entity\Campaign;
use Project\Entity\Offer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Обновление кампании
 */
class Update extends UserAction
{
    /**
     * Валидация измененных данных перед сохранением
     *
     * @throws IncorrectParamsException
     */
    protected function beforeSaveValidate()
    {
        $offer = $this->campaign->getOffer();
        $cpm = $this->campaign->getCpm();
        $url = $this->campaign->getUrl();
        $script = $this->campaign->getScript();

        if (
            (!$this->campaign->isExternal() && $offer === null && ($cpm === null || ($url === null && $script === null)))
            ||
            ($this->campaign->isExternal() && $offer !== null && ($cpm !== null || $url !== null || $script !== null))
        ) {
            throw new IncorrectParamsException(
                'cpm and url / script must be existed for external campaign, otherwise offer have to be existed'
            );
        }
    }

    /**
     * Создание кампании
     *
     * @param $name
     * @param Offer $offer
     * @param $cpm
     * @param $url
     * @param $script
     * @param $limitUserShow
     * @param $limitAmount
     * @param $blockWidthFrom
     * @param $blockWidthTo
     * @param $blockHeightFrom
     * @param $blockHeightTo
     * @param $archive
     * @return Campaign
     * @throws IncorrectParamsException
     */
    public function process($name = null, Offer $offer = null, $cpm = null, $url = null, $script = null, $limitUserShow = null, $limitAmount = null,
                            $blockWidthFrom = null, $blockWidthTo = null, $blockHeightFrom = null, $blockHeightTo = null, $archive = null)
    {
        if ($name !== null) {
            $this->campaign->setName($name);
        }

        if ($offer !== null) {
            $this->campaign->setOffer($offer);
        }

        if ($cpm !== null) {
            $this->campaign->setCpm($cpm);
        }

        if ($url !== null) {
            $this->campaign->setUrl($url);
        }

        if ($script !== null) {
            $this->campaign->setScript($script);
        }

        if ($limitAmount !== null) {
            $this->campaign->setLimitAmount($limitAmount);
        }

        if ($limitUserShow !== null) {
            $this->campaign->setLimitUserShow($limitUserShow);
        }


        if ($blockWidthFrom !== null) {
            $this->campaign->setBlockWidthFrom($blockWidthFrom);
        }
        
        if ($blockWidthTo !== null) {
            $this->campaign->setBlockWidthTo($blockWidthTo);
        }

        if ($blockHeightFrom !== null) {
            $this->campaign->setBlockHeightFrom($blockHeightFrom);
        }

        if ($blockHeightTo !== null) {
            $this->campaign->setBlockHeightTo($blockHeightTo);
        }

        if ($archive !== null) {
            $this->campaign->setArchive($archive);
        }
        
        $this->beforeSaveValidate();

        return $this->campaign->save();
    }
}
