<?php

namespace Project\Campaign;

use Project\Entity\AdminUser;
use Project\Entity\Campaign;
use Project\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;
use Project\Check\Validator;

/**
 * Пользовательское действие с кампанией
 */
abstract class UserAction
{
    /**
     * Пользователь
     *
     * @var User
     */
    protected $user;

    /**
     * @var Campaign
     */
    protected $campaign;

    /**
     * Конструктор
     *
     * @param User $user
     * @param Campaign $campaign
     */
    public function __construct(User $user, Campaign $campaign)
    {
        $this->user = $user;
        $this->campaign = $campaign;
        $this->validate();
    }

    /**
     * Валидация
     */
    protected function validate()
    {
        //если юзер не админ
        if (!($this->user instanceof AdminUser)) {
            //кампания должна принадлежать пользователю
            Validator::validateValue(
                $this->campaign->getSiteUserId(),
                new Assert\EqualTo(
                    $this->user->getId()
                )
            );
        }
    }
}
