<?php

namespace Project\Campaign;

use Project\Campaign\Exception\IncorrectParamsException;
use Project\Check\Validator;
use Project\Entity\Campaign;
use Project\Tracker\Tracker;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Вкл / выкл кампании
 */
class Active extends UserAction
{
    /**
     * Валидация
     *
     * @throws IncorrectParamsException
     * @internal param $amount
     */
    protected function validate()
    {
        parent::validate();

        if ($this->campaign->isExternal()) {
            //у внешней компании должен быть задан url / script_url
            Validator::validateValue(
                $this->campaign->getUrl() !== null || $this->campaign->getScript() !== null,
                new Assert\EqualTo(true),
                'for external campaign url \ script must be existed before activation'
            );
        } else {
            // а у внутренней flow
            Validator::validateValue(
                $this->campaign->getFlow(),
                new Assert\NotNull(),
                'for internal campaign flow must be existed before activation'
            );
        }
    }

    /**
     * Вкл / выкл
     *
     * @param bool $active
     * @param null|array $config
     * @return Campaign
     */
    public function process($active, $config = null)
    {
        //выключить кампанию просто
        if (!$active) {
            return $this->campaign
                ->setActive(false)
                ->save();
        }

        //если кампания внутренняя. то формируем УРЛ
        //создавая кампанию на трекере
        if (!$this->campaign->isExternal()) {
            $res = (new Tracker($config))
                ->addCampaign(
                    json_decode($this->campaign->getFlow())
                );

            $this->campaign
                ->setExtCampaignId($res['campaignId'])
                ->setUrl($res['campaignUrl']);
        }

        return $this->campaign
            ->setActive(true)
            ->save();
    }
}
