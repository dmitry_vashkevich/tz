<?php

namespace Project\Campaign;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Query\Expr;
use Project\Entity\Ad;
use Project\Entity\Campaign;
use Project\Entity\Offer;
use Project\Entity\SiteUser;
use Project\Proxy;

/**
 * Поиск кампаний c подгрузкой статистики
 */
class Search
{
    /**
     * сортировки
     * @var array
     */
    public static $orders = [
        'id',
        'added',
        'cpm',
        'name',
        'ad_type',
        'active',
        'archive',
        'external',
        'rs',
        'rb',
        'rbq',
        'rbqu',
        'scv',
        'tcv',
        'prelv',
        'lead',
        'lead_amount',
        'accept',
        'accept_amount',
        'decline',
        'decline_amount',
        'invalid',
        'invalid_amount',
        'ctr',
        'cpmb',
        'ctrb'
    ];

    /**
     * Кол-во кампаний в выборке, до применения операции limit
     *
     * @var int
     */
    protected $count;

    /**
     * вычисляемые поля, требующие округления,
     * т.к. dql postgres не поддерживает round
     *
     * @var array
     */
    protected $roundCalcFields = [
        'cpm',
        'ctr',
        'cpmb',
        'ctrb',
    ];

    /**
     * Поиск
     *
     * @param SiteUser $user
     * @param Campaign $campaign
     * @param Offer $offer
     * @param string $adType
     * @param bool $active
     * @param bool $external
     * @param string $name
     * @param bool $archive
     * @param string $order
     * @param string $direction
     * @param int $offset
     * @param int $limit
     * @param null|array $campaignIds
     * @return array
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function process(
        SiteUser $user = null,
        Campaign $campaign = null,
        Offer $offer = null,
        $adType = null,
        $active = null,
        $external = null,
        $name = null,
        $archive = null,
        $order = 'id',
        $direction = Criteria::DESC,
        $offset = null,
        $limit = null,
        $campaignIds = null
    )
    {
        $criteria = new Criteria();
        if ($user !== null) {
            $criteria->where(
                Criteria::expr()->andX(
                    Criteria::expr()->eq('site_user', $user)
                )
            );
        }

        if ($campaign !== null) {
            $criteria->andWhere(
                Criteria::expr()->eq('id', $campaign->getId())
            );
        }

        if ($campaignIds !== null) {
            $criteria->andWhere(
                Criteria::expr()->in('id', $campaignIds)
            );
        }

        if ($offer !== null) {
            $criteria->andWhere(
                Criteria::expr()->eq('offer', $offer)
            );
        }

        if ($adType !== null) {
            $criteria->andWhere(
                Criteria::expr()->eq('adType', $adType)
            );
        }

        if ($active !== null) {
            $criteria->andWhere(
                Criteria::expr()->eq('active', $active)
            );
        }

        if ($archive !== null) {
            $criteria->andWhere(
                Criteria::expr()->eq('archive', $archive)
            );
        }

        if ($external !== null) {
            $criteria->andWhere(
                Criteria::expr()->eq('external', $external)
            );
        }

        if ($name !== null) {
            $criteria->andWhere(
                Criteria::expr()->contains('name', $name)
            );
        }

        $this->count = Campaign::repo()->createQueryBuilder('base')
            ->select("count(base.id)")
            ->addCriteria($criteria)
            ->getQuery()
            ->getSingleScalarResult();

        $qb = Proxy::init()->getEntityManager()->createQueryBuilder();
        $qb->select(
            "(CASE WHEN c.external!=true THEN COALESCE(SUM(s.accept_amount) / NULLIF(SUM(s.rs), 0) * 1000, 0) ELSE c.cpm END) as cpm",
            'c.id as id',
            'c.name as name',
            'c.offer_id',
            'o.name as offer_name',
            'c.adType as ad_type',
            'c.external as external',
            'c.url',
            'c.flow',
            'c.targetBrowsers',
            'c.targetCountries',
            'c.targetPlatforms',
            'c.targetDomains',
            'c.limitAmount',
            'c.limitUserShow',
            'c.active as active',
            'c.archive as archive',
            'c.script',
            'COALESCE(SUM(s.rs), 0) as rs',
            'COALESCE(SUM(s.rb), 0) as rb',
            'COALESCE(SUM(s.rbq), 0) as rbq',
            'COALESCE(SUM(s.rbqu), 0) as rbqu',
            'COALESCE(SUM(s.scv), 0) as scv',
            'COALESCE(SUM(s.tcv), 0) as tcv',
            'COALESCE(SUM(s.prelv), 0) as prelv',
            'COALESCE(SUM(s.lead), 0) as lead',
            'COALESCE(SUM(s.lead_amount), 0) as lead_amount',
            'COALESCE(SUM(s.accept), 0) as accept',
            'COALESCE(SUM(s.accept_amount), 0) as accept_amount',
            'COALESCE(SUM(s.decline), 0) as decline',
            'COALESCE(SUM(s.decline_amount), 0) as decline_amount',
            'COALESCE(SUM(s.invalid), 0) as invalid',
            'COALESCE(SUM(s.invalid_amount), 0) as invalid_amount',
            'COALESCE(SUM(s.scv) / NULLIF(SUM(s.rs), 0) * 100, 0) as ctr',
            'COALESCE(SUM(s.accept_amount) / NULLIF(SUM(s.rb), 0) * 1000, 0) as cpmb',
            'COALESCE(SUM(s.scv) / NULLIF(SUM(s.rb), 0) * 100, 0) as ctrb'
        )
            ->from('\Project\Entity\Campaign', 'c')
            ->leftJoin('\Project\Entity\StatsBase', 's', Expr\Join::WITH, 'c.id = s.campaign_id')
            ->leftJoin('\Project\Entity\Offer', 'o', Expr\Join::WITH, 'c.offer_id = o.id')
            ->groupBy('c.id', 'o.name')
            ->addCriteria($criteria)
            ->orderBy($order, $direction);

        if ($offset !== null) {
            $qb->setFirstResult($offset);
        }

        if ($limit !== null) {
            $qb->setMaxResults($limit);
        }

        $campaigns = $qb
            ->getQuery()
            ->getArrayResult();

        return $this->prepareSearchResult($campaigns);
    }

    /**
     * @param array $searchResult
     * @return array
     * @throws \Doctrine\ORM\Query\QueryException
     */
    protected function prepareSearchResult(array $searchResult)
    {
        if (count($searchResult) <= 0) {
            return $searchResult;
        }

        foreach ($searchResult as &$result) {
            $result['ad_count'] = Ad::repo()->createQueryBuilder('ad')
                ->select("count(ad.id)")
                ->addCriteria(
                    (new Criteria())
                        ->where(
                            Criteria::expr()->eq(
                                'ad.campaign_id', $result['id']
                            )
                        )
                )
                ->getQuery()
                ->getSingleScalarResult();
            foreach ($this->roundCalcFields as $roundCalcField) {
                $result[$roundCalcField] = round($result[$roundCalcField], 2);
            }
        }

        return $searchResult;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return (int)$this->count;
    }
}
