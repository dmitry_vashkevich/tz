<?php

namespace Project\Campaign;

use Project\Campaign\Exception\IncorrectParamsException;
use Project\Campaign\Exception\ExpectedInternalCampaignException;
use Project\Campaign\Flow\Exception\EmptyFlowException;
use Project\Check\Exception\EmptyEntityException;
use Project\Check\Validator;
use Project\Entity\Campaign;
use Project\Entity\Land;
use Project\Entity\Preland;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Сборка flow кампании
 */
class Flow extends UserAction
{
    /**
     * Валидация
     *
     * @throws IncorrectParamsException
     * @internal param $amount
     */
    protected function validate()
    {
        parent::validate();
        //кампания должна быть внутренней
        Validator::validateValue(
            $this->campaign->isExternal(),
            new Assert\EqualTo(false),
            null,
            ExpectedInternalCampaignException::class
        );
    }

    /**
     * Сборка Flow
     *
     * @param $flow
     * @return Campaign
     * @throws EmptyFlowException
     */
    public function process($flow)
    {
        $flow = json_decode($flow, true);

        $trackerFlow = new \Project\Tracker\Command\AddCampaign\Flow();

        if (!is_array($flow) || count($flow) == 0) {
            throw new EmptyFlowException('flow = ' . print_r($flow, true));
        }

        foreach ($flow as $data) {
            if (!isset($data['land_id'])) {
                continue;
            }

            try {
                $land = Land::prepareById($data['land_id']);
                if ($land->getOffer()->getId() != $this->campaign->getOffer()->getId()) {
                    continue;
                }

                $preland = null;
                if (isset($data['preland_id'])) {
                    $preland = Preland::prepareById($data['preland_id']);
                }

                $trackerFlow->addPath($land, $preland);

            } catch (EmptyEntityException $e) {
                continue;
            }
        }

        return $this->campaign
            ->setFlow(json_encode($trackerFlow->prepare()))
            ->save();
    }
}
