<?php

namespace Project\Campaign\Exception;

use Project\Exception\RestException\ClientError\BadRequestException;

/**
 * Указаны некорректные параметры для создания кампании
 */
class IncorrectParamsException extends BadRequestException
{

}
