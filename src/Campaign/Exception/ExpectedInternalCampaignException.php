<?php

namespace Project\Campaign\Exception;

use Project\Exception\RestException\ServerError\InternalServerErrorException;

/**
 * Когда ожидалась внутренняя кампания
 */
class ExpectedInternalCampaignException extends InternalServerErrorException
{

}
