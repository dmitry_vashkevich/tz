<?php

namespace Project\AdminUser;

use Project\Entity\AdminUser;

/**
 * Регистрация
 */
class Registration extends \Project\User\Registration
{
    /**
     * @inheritdoc
     */
    protected function prepareUser()
    {
        return new AdminUser();
    }
}
