<?php

namespace Project\AdminUser;

use Project\Entity\AdminUser;

/**
 * Сессия
 */
class Session extends \Project\User\Session
{
    const PREFIX = 'admin_user';

    /**
     * @inheritdoc
     */
    protected function getPrefix()
    {
        return self::PREFIX;
    }

    /**
     * @inheritdoc
     */
    protected function prepareUserById($id)
    {
        return AdminUser::prepareById($id);
    }
}
