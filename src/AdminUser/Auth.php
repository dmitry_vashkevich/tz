<?php

namespace Project\AdminUser;

use Project\Entity\AdminUser;

/**
 * Регистрация
 */
class Auth extends \Project\User\Auth
{
    /**
     * @inheritdoc
     */
    protected function prepareUser()
    {
        return AdminUser::prepareByParams([
            'login' => $this->login
        ]);
    }

    /**
     * @inheritdoc
     */
    protected function getSession()
    {
        return new Session();
    }
}
