<?php

namespace Project\Cache\Exception;

use Project\Exception\RestException\ServerError\InternalServerErrorException;

/**
 * При ошибке получения данных из кеша
 */
class GetCacheException extends InternalServerErrorException
{

}
