<?php

namespace Project\Cache\EntityCache;

use Project\Cache\EntityCache;
use Project\Entity\Schema\Model;
use Project\TeaserConfig\Search;

/**
 * Кеш сущности
 */
class TeaserConfig extends EntityCache
{
    const PREFIX = 'TeaserConfig';

    /**
     * @inheritdoc
     */
    protected function getEntities()
    {
        return (new Search())->process();
    }

    /**
     * @inheritdoc
     */
    protected function format(Model $entity)
    {
        /** @var \Project\Entity\TeaserConfig $entity */
        return [
            'id' => $entity->getId(),
            'data' => $entity->getData(),
            'b_width_from' => $entity->getBlockWidthFrom(),
            'b_width_to' => $entity->getBlockWidthTo(),
            'b_height_from' => $entity->getBlockHeightFrom(),
            'b_height_to' => $entity->getBlockHeightTo(),
        ];
    }
}
