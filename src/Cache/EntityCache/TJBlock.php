<?php

namespace Project\Cache\EntityCache;

use Project\Cache\EntityCache;
use Project\Entity\Schema\Model;
use Project\Proxy;

/**
 * Кеш сущности
 *
 * TJBlock кешируем вместе с данными площадки
 */
class TJBlock extends EntityCache
{
    const PREFIX = 'TJBlock';

    /**
     * @inheritdoc
     */
    protected function getEntities()
    {
        $sql = /** @lang sql */
            "
            SELECT
                b.id as blockid,
                b.adtype as ad_type,
                b.custom,
                b.config as blockconfig,
                b.params as blockparams,
                b.clickinterval as clickinterval,
                b.closeinterval as closeinterval,
                b.width as width,
                b.height as height,
                b.tj_site_id as tj_site_id,
                b.tj_user_id as tj_user_id,
                s.themes as sitethemes,
                s.params as siteparams
            FROM
                tj_blocks as b JOIN tj_sites as s ON b.tj_site_id=s.id
            WHERE       
                b.deleted = false AND
                b.active = true AND
                s.deleted=false AND
                s.active=true AND
                s.status='approved'
            ";

        $db = Proxy::init()->getEntityManager()->getConnection();
        $stmt = $db->prepare($sql);

        $stmt->execute();

        return $stmt->fetchAll();
    }

    /**
     * @inheritdoc
     */
    public function update()
    {
        $rows = $this->getEntities();

        if (count($rows) <= 0) {
            return;
        }

        foreach ($rows as $row) {
            $this->set(
                $row['blockid'],
                $this->formatRow($row),
                static::CACHE_EXPIRE_TIME
            );
        }
    }

    /**
     * @param array $row
     * @return array
     */
    private function formatRow(array $row)
    {
        return [
            'id' => $row['blockid'],
            'ad_type' => $row['ad_type'],
            'custom' => $row['custom'],
            'params' => $row['blockparams'],
            'clickinterval' => $row['clickinterval'],
            'closeinterval' => $row['closeinterval'],
            'width' => $row['width'],
            'height' => $row['height'],
            'config' => $row['blockconfig'],
            'site' => [
                'id' => $row['tj_site_id'],
                'themes' => $row['sitethemes'],
                'params' => $row['siteparams'],
            ],
            'user' => [
                'id' => $row['tj_user_id'],
            ],
        ];
    }

    /**
     * @param $id
     * @return string
     */
    protected function prepareKey($id)
    {
        return static::PREFIX . '|id|' . $id;
    }

    /**
     * @inheritdoc
     */
    public function getAll()
    {
        throw new \Exception('not implemented');
    }

    /**
     * @inheritdoc
     * @see $this->formatRow()
     */
    protected function format(Model $entity)
    {
        throw new \Exception('not implemented');
    }
}
