<?php

namespace Project\Cache\EntityCache;

use Project\Cache\EntityCache;
use Project\Entity\Schema\Model;
use Project\MessageConfig\Search;

/**
 * Кеш сущности
 */
class MessageConfig extends EntityCache
{
    const PREFIX = 'MessageConfig';

    /**
     * @inheritdoc
     */
    protected function getEntities()
    {
        return (new Search())->process();
    }

    /**
     * @inheritdoc
     */
    protected function format(Model $entity)
    {
        /** @var \Project\Entity\MessageConfig $entity */
        return [
            'id' => $entity->getId(),
            'data' => $entity->getData(),
        ];
    }
}
