<?php

namespace Project\Cache\EntityCache;

use Project\Cache\EntityCache;
use Project\Entity\Schema\Model;
use Project\SiteUser\Search;

/**
 * Кеш сущности
 */
class SiteUser extends EntityCache
{
    const PREFIX = 'SiteUser';

    /**
     * @inheritdoc
     */
    protected function getEntities()
    {
        return (new Search())->process();
    }

    /**
     * @inheritdoc
     */
    protected function format(Model $entity)
    {
        /** @var \Project\Entity\SiteUser $entity */
        return [
            'id' => $entity->getId(),
            'key' => $entity->getKey(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function update()
    {
        $entities = $this->getEntities();
        if (!$entities->isEmpty()) {
            /** @var \Project\Entity\SiteUser $entity */
            foreach ($entities as $entity) {
                $this->set($entity->getKey(), $this->format($entity), static::CACHE_EXPIRE_TIME);
            }
        }
    }

    /**
     * @param $key
     * @return string
     */
    protected function prepareKey($key)
    {
        return static::PREFIX . '|key|' . $key;
    }
}
