<?php

namespace Project\Cache;

use Project\Check\Validator;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Кеш объявлений
 */
class AdCache extends Cache
{
    const PREFIX = 'adcache';

    /**
     * Валидация полученного кеша
     *
     * @param $cache
     */
    protected function validate($cache)
    {
        Validator::validateValue(
            is_array($cache) && count($cache) > 0,
            new Assert\EqualTo(true),
            "empty adCache"
        );
    }
}
