<?php

namespace Project\Cache;

use Doctrine\Common\Collections\Collection;
use Project\Check\Validator;
use Project\Entity\Schema\Model;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Кеш сущности БД
 */
abstract class EntityCache extends Cache
{
    const PREFIX = 'abstractEntity';

    const CACHE_EXPIRE_TIME = 600;//10 минут
    
    /**
     * Валидация полученного кеша
     *
     * @param $cache
     */
    protected function validate($cache)
    {
        Validator::validateValue(
            empty($cache),
            new Assert\EqualTo(false),
            "empty " . static::PREFIX . " cache data"
        );
    }

    /**
     * Обновление кеша
     */
    public function update()
    {
        $entities = $this->getEntities();
        if (!$entities->isEmpty()) {
            $result = [];
            foreach ($entities as $entity) {
                $result[] = $this->format($entity);
            }

            $this->set(static::PREFIX, $result, static::CACHE_EXPIRE_TIME);
        }
    }

    /**
     * Получить весь кеш
     *
     * @return array
     */
    public function getAll()
    {
        return $this->get(static::PREFIX);
    }
    
    /**
     * Получить список сущностей для кеширования
     * @return Model | Collection
     */
    abstract protected function getEntities();

    /**
     * Формат записи
     *
     * @param Model $entity
     * @return array
     */
    abstract protected function format(Model $entity);
}