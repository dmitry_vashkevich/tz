<?php

namespace Project\Cache;

use Project\Cache\Exception\GetCacheException;
use Project\Proxy;

/**
 * Абстрактный кеш
 */
abstract class Cache
{
    const PREFIX = 'abstract';

    /**
     * @param $key
     * @return mixed
     */
    public function get($key)
    {
        $cache = $this->getMemcache()->get($this->prepareKey($key));
        $this->checkResult();
        $cache = unserialize($cache);
        $this->validate($cache);

        return $cache;
    }


    /**
     * @throws GetCacheException
     */
    protected function checkResult()
    {
        if ($this->getMemcache()->getResultCode() == \Memcached::RES_NOTFOUND) {
            throw new GetCacheException('Unable to get data from Cache::' . static::PREFIX);
        }
    }
    
    /**
     * @param string $key
     * @param string $data
     * @param int | null $expiration
     * @return bool
     */
    public function set($key, $data, $expiration = null)
    {
        return $this->getMemcache()->set(
            $this->prepareKey($key),
            serialize($data),
            $expiration
        );
    }
    
    /**
     * Валидация полученного кеша
     *
     * @param $cache
     */
    abstract protected function validate($cache);

    /**
     * Сборка ключа
     *
     * @param $key
     * @return string
     */
    protected function prepareKey($key)
    {
        return static::PREFIX . '|' . $key;
    }
    
    protected function getMemcache()
    {
        return Proxy::init()->getMemcache();
    }
}