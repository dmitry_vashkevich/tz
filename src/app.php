<?php

require_once __DIR__.'/../vendor/autoload.php';

$app = new \Silex\Application();

\Project\Proxy::init($app);

require_once __DIR__.'/registers.php';
require_once __DIR__.'/controllers.php';

return $app;
