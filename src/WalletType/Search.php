<?php

namespace Project\WalletType;

use Project\Entity\WalletType;

/**
 * Поиск типов кошельков
 */
class Search
{
    /**
     * Поиск типов кошельков
     *
     * @return \Doctrine\Common\Collections\Collection | WalletType[]
     */
    public function process()
    {
        return WalletType::prepareList();
    }
}
