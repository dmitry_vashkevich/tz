<?php

namespace Project\Console;

use Project\Stats\Queue\Consumer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Консольный обработчик событий системы
 */
class StatsEventHandler extends Command
{
    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this
            ->setName('stats:event-handler')
            ->setDescription('prepare and push events to aggregator');
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        (new Consumer())
            ->process();
    }
}
