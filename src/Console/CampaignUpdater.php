<?php

namespace Project\Console;

use Doctrine\Common\Collections\Criteria;
use Project\Entity\Campaign;
use Project\Stats\Event;
use Project\Stats\Queue;
use Project\Stats\SearchBase;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Обновление статистических данных кампании
 */
class CampaignUpdater extends DBConcurrencyCommand
{
    /**
     * Интервал между операциями
     */
    const DELAY = 10; //sec

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this
            ->setName('campaign:updater')
            ->setDescription('update campaigns');

        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return mixed|void
     */
    protected function concurrencyDBExecute(InputInterface $input, OutputInterface $output)
    {
        $this->setActiveByLimitAmount();
    }

    /**
     * Отключение кампаний по лимиту сумм
     */
    protected function setActiveByLimitAmount()
    {
        $campaigns = Campaign::prepareList(
            (new Criteria())
                ->where(Criteria::expr()->eq('active', true))
                ->andWhere(Criteria::expr()->eq('external', true))
                ->andWhere(Criteria::expr()->neq('limitAmount', null))
        );

        if ($campaigns->isEmpty()) {
            return;
        }

        $campaignByIds = [];
        /** @var Campaign $campaign */
        foreach ($campaigns as $campaign) {
            $campaignByIds[$campaign->getId()] = $campaign;
        }

        $stats = SearchBase::getStats('campaign')
            ->setCampaignIds(array_keys($campaignByIds))
            ->fetchAll();

        foreach ($stats as $stat) {
            $campaign = $campaignByIds[$stat['campaign_id']];
            if ($stat['rb'] / 1000 * $campaign->getCpm() >= $campaign->getLimitAmount()) {
                $campaign
                    ->setActive(false)
                    ->save();
            }
        }
    }
}
