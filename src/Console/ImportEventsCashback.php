<?php

namespace Project\Console;

use Project\Entity\TrackerEventCashback;
use Project\Proxy;
use Project\Stats\Event;
use Project\Tracker\Tracker;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Импорт событий с трекера из проекта cashback
 */
class ImportEventsCashback extends ImportEvents
{
    /** @var  Tracker */
    protected $tracker;

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        parent::configure();
        
        $this
            ->setName('import:tracker:events:cashback')
            ->setDescription('import cashback events');
    }

    /**
     * @inheritdoc
     */
    public function __construct($name = null)
    {
        parent::__construct($name);
        $this->tracker = new Tracker(
            Proxy::init()->getConfig()['tracker_cashback']
        );
    }

    /**
     * @inheritdoc
     */
    protected function concurrencyDBExecute(InputInterface $input, OutputInterface $output)
    {
        $lastId = $this->getLastId();
        $this->import($lastId);
    }

    /**
     * @inheritdoc
     */
    protected function checkTrackerEvent($id)
    {
        TrackerEventCashback::prepareById($id);
    }

    /**
     * @inheritdoc
     */
    protected function saveTrackerEvent(array $row)
    {
        return (new TrackerEventCashback())
            ->setId($row['id'])
            ->setData(json_encode($row))
            ->save();
    }

    /**
     * @inheritdoc
     */
    protected function getLastId()
    {
        $sql = /** @lang sql */
            "select max(id) from tracker_events_cashback";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        return (int)$stmt->fetchColumn();
    }

    /**
     * @inheritdoc
     */
    protected function prepareEvent(array $row)
    {
        $row = $this->fixRow($row);
        return parent::prepareEvent($row);
    }

    /**
     * TODO Костыль, убрать через месяц
     */
    private function fixRow($row)
    {
        $subId1 = $row['subId1'];
        $statData = json_decode(base64_decode($row['subId1']), true);

        if (is_array($statData)) {
            return $row;
        }


        $pos = strrpos($subId1, 'http');
        $subId1 = substr($subId1, 0, $pos);

        $row['subId1'] = $subId1;

        $statData = json_decode(base64_decode($row['subId1']), true);

        if (is_array($statData)) {
            return $row;
        }

        $pos = strrpos($subId1, 'http');
        $subId1 = substr($subId1, 0, $pos);

        $row['subId1'] = $subId1;
        
        return $row;
    }
}
