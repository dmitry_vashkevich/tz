<?php

namespace Project\Console\Exception;

use Project\Exception\RestException\ClientError\BadRequestException;

/**
 * В случае передачи неправильного параметра в консольный скрипт
 */
class InvalidOptionException extends BadRequestException
{

}
