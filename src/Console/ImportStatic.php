<?php

namespace Project\Console;

use Project\Console\ImportStatic\CBLands;
use Project\Console\ImportStatic\CBOfferCats;
use Project\Console\ImportStatic\CBOffers;
use Project\Console\ImportStatic\Lands;
use Project\Console\ImportStatic\OfferCats;
use Project\Console\ImportStatic\Offers;
use Project\Console\ImportStatic\Prelands;
use Project\Proxy;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Импорт статических даных с трекера:
 * - продукты и их категории
 * - ленды и преленды
 * - аналогичные сущности кешбек проекта
 */
class ImportStatic extends DBConcurrencyCommand
{
    /**
     * Интервал между операциями
     */
    const DELAY = 300; //sec

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this
            ->setName('import:tracker:static')
            ->setDescription('Update tracker entities');

        parent::configure();
    }

    /**
     * @inheritdoc
     */
    protected function concurrencyDBExecute(InputInterface $input, OutputInterface $output)
    {
        $entityManager = Proxy::init()->getEntityManager();
        $entityManager->beginTransaction();

        try {
            (new OfferCats())->import();
            (new CBOfferCats())->import();
            (new CBOffers())->import();
            (new Offers())->import();
            (new Lands())->import();
            (new Prelands())->import();
            (new CBLands())->import();
            $entityManager->commit();
        } catch (\Exception $e) {
            $entityManager->rollback();
            throw $e;
        }
    }
}
