<?php

namespace Project\Console;

use Project\Proxy;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Конкурентный демон, который единовременно выполняется на 1-м из
 * доступных воркеров
 */
abstract class ConcurrencyCommand extends Command
{
    /**
     * статусы
     */
    const STATUS_CYCLE_START = 'cycle start';
    const STATUS_CYCLE_FINISH = 'cycle finish';

    /**
     * Интервал между операциями
     */
    const DELAY = 10; //sec

    /**
     * Настройки конкуррентного запуска,
     * в текущей реализации поддерживается 1 рабочий процесс
     */
    const CONCURRENCY_KEY_EXPIRATION = 300; //5 min

    /**
     * @var \Memcached
     */
    protected $memcached;

    /** @var LoggerInterface */
    protected $logger;

    /** @var  string */
    protected $cycleId;

    /** @var  string */
    protected $hostName;
    
    /**
     * Конструктор
     * @param null $name
     */
    public function __construct($name = null)
    {
        $this->memcached = Proxy::init()->getMemcache();
        $this->logger = Proxy::init()->getLogger();
        $this->hostName = gethostname();
        parent::__construct($name);
    }


    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setDefinition(
            [
                new InputOption(
                    'test',
                    null,
                    null,
                    'run only 1 cycle without concurrency check than stop and output timer,
                    DONT RUN IN PRODUCTION'
                ),
            ]
        );
    }

    /**
     * Проверка при конкурентном запуске
     *
     * @return bool
     */
    protected function concurrencyCheckFailed()
    {
        $this->memcached->add(static::class, $this->hostName, static::CONCURRENCY_KEY_EXPIRATION);
        if ($this->memcached->get(static::class) == $this->hostName) {
            return false;
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        while (true) {
            $start = microtime(true);
            if (!$input->getOption('test')) {
                sleep(static::DELAY);
            }

            if ($this->concurrencyCheckFailed()) {
                continue;
            }

            $this->cycleId = uniqid('cycle');

            $this->log(self::STATUS_CYCLE_START);

            $this->concurrencyExecute($input, $output);

            $this->log(self::STATUS_CYCLE_FINISH);

            if ($input->getOption('test')) {
                $output->writeln('execution time = ' . (string)(microtime(true) - $start));
                exit;
            }
        }
    }

    /**
     * Логирование внутренних циклов
     * @param $status
     */
    protected function log($status)
    {
        $this->logger->info($this->getName() . ' :: ' . $status,
            [
                'group'  => $status,
                'logger' => static::class,
                'cycle_id' => $this->cycleId,
                'hostname' => $this->hostName,
            ]
        );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return mixed
     */
    abstract protected function concurrencyExecute(InputInterface $input, OutputInterface $output);
}
