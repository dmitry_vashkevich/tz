<?php

namespace Project\Console;

use Project\Cache\EntityCache;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Кешер сущностей
 */
class EntityCacher extends DBConcurrencyCommand
{
    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this
            ->setName('entity:cacher')
            ->setDescription('cache entities');

        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return mixed|void
     */
    protected function concurrencyDBExecute(InputInterface $input, OutputInterface $output)
    {
        (new EntityCache\TeaserConfig())->update();
        (new EntityCache\MessageConfig())->update();
        (new EntityCache\SiteUser())->update();
        (new EntityCache\TJBlock())->update();
    }
}
