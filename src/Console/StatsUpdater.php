<?php

namespace Project\Console;

use Project\Proxy;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Обновление статистических данных
 */
class StatsUpdater extends DBConcurrencyCommand
{
    /**
     * Интервал между операциями
     */
    const DELAY = 100; //sec

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this
            ->setName('stats:updater')
            ->setDescription('update stats (external campaigns accept_amount)');

        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return mixed|void
     */
    protected function concurrencyDBExecute(InputInterface $input, OutputInterface $output)
    {
        $db = Proxy::init()->getEntityManager()->getConnection();
        $sql = /** @lang text */
            'UPDATE stats_base as s SET accept_amount = c.cpm * rs / 1000 FROM campaigns as c WHERE c.external = true AND s.campaign_id = c.id';
        $db
            ->prepare($sql)
            ->execute();
    }
}
