<?php

namespace Project\Console;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception\ConnectionException;
use Project\Proxy;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Потомок конкуретного демона с
 * автоматическим обновлением соединения БД
 */
abstract class DBConcurrencyCommand extends ConcurrencyCommand
{
    const STATUS_CATCH_CONNECTION_ERROR = 'catch connection error';

    /**
     * @var Connection
     */
    protected $db;
    
    /**
     * Конструктор
     * @param null $name
     */
    public function __construct($name = null)
    {
        parent::__construct($name);
        $this->db = Proxy::init()->getEntityManager()->getConnection();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function concurrencyExecute(InputInterface $input, OutputInterface $output)
    {
        try {
            if ($this->db->ping() === false) {
                $this->db->close();
                $this->db->connect();
            }
        } catch (ConnectionException $e) {
            $this->log(self::STATUS_CATCH_CONNECTION_ERROR);
            return;
        }
        
        $this->concurrencyDBExecute($input, $output);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return mixed
     */
    abstract protected function concurrencyDBExecute(InputInterface $input, OutputInterface $output);
}
