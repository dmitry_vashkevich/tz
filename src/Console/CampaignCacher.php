<?php

namespace Project\Console;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Project\Ad\Weight;
use Project\Cache\AdCache;
use Project\Ad\Index;
use Project\Campaign\Search;
use Project\Entity\Ad;
use Project\Entity\Campaign;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Кешер кампаний
 * - сборка файловых индексов для отдачи контента
 */
class CampaignCacher extends DBConcurrencyCommand
{
    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this
            ->setName('campaign:cacher')
            ->setDescription('cache campaigns');

        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return mixed|void
     */
    protected function concurrencyDBExecute(InputInterface $input, OutputInterface $output)
    {
        $index = new Index();
        $criteria = new Criteria();
        $criteria->where(
            Criteria::expr()->eq('active', true)
        );

        $campaigns = Campaign::prepareList($criteria);
        if ($campaigns->isEmpty()) {
            return;
        }

        $stats = $this->getCampaignsStats($campaigns);
        $weight = new Weight($stats);
        /** @var Campaign $campaign */
        foreach ($campaigns as $campaign) {
            $countryIndexAr = $this->getTargetIndexAr($campaign, 'countries');
            $platformIndexAr = $this->getTargetIndexAr($campaign, 'platforms');
            $browserIndexAr = $this->getTargetIndexAr($campaign, 'browsers');
            $domainIndexAr = $this->getTargetIndexAr($campaign, 'domains');

            $ads = Ad::prepareList(
                (new Criteria())->where(
                    Criteria::expr()->eq('campaign', $campaign)
                )
            );

            $statData = [
                'adtype' => $campaign->getAdType(),
                'campaign_id' => $campaign->getId(),
                'site_user_id' => $campaign->getSiteUserId(),
                'offer_id' => $campaign->getOfferId(),
            ];

            $campaignData = [
                'type' => $campaign->getAdType(),
                'campaignId' => $campaign->getId(),
                'campaignUrl' => $campaign->getUrl(),
                'campaignScript' => $campaign->getScript(),
                'limitUserShow' => $campaign->getLimitUserShow(),
                'blockWidthFrom' => $campaign->getBlockWidthFrom(),
                'blockWidthTo' => $campaign->getBlockWidthTo(),
                'blockHeightFrom' => $campaign->getBlockHeightFrom(),
                'blockHeightTo' => $campaign->getBlockHeightTo(),
                'weight' => $weight->prepare($campaign),
                'statData' => $statData,
            ];

            foreach ($countryIndexAr as $countryIndex) {
                foreach ($platformIndexAr as $platformIndex) {
                    foreach ($browserIndexAr as $browserIndex) {
                        foreach ($domainIndexAr as $domainIndex) {
                            if ($ads->isEmpty()) {
                                $index->addData($campaign->getAdType(), $countryIndex, $platformIndex, $browserIndex, $domainIndex,
                                    $campaignData
                                );
                            } else {
                                /** @var Ad $ad */
                                foreach ($ads as $ad) {
                                    $campaignData['statData']['ad_id'] = $ad->getId();
                                    $index->addData($campaign->getAdType(), $countryIndex, $platformIndex, $browserIndex, $domainIndex,
                                        $campaignData + [
                                            'adId' => $ad->getId(),
                                            'images' => $ad->getImages(),
                                            'data' => $ad->getData(),
                                        ]
                                    );
                                }
                            }
                        }
                    }
                }
            }
        }

        $this->cacheIndex(
            $index
        );
    }

    /**
     * @param Campaign $campaign
     * @param $targetName
     * @return array|mixed
     */
    protected function getTargetIndexAr(Campaign $campaign, $targetName)
    {
        $funcs = [
            'countries' => 'getTargetCountries',
            'platforms' => 'getTargetPlatforms',
            'browsers' => 'getTargetBrowsers',
            'domains' => 'getTargetDomains',
        ];

        $func = $funcs[$targetName];

        $targetList = $campaign->$func();

        $indexAr = [];
        if ($targetList === null) {
            $indexAr[] = Index::ANY_INDEX;
        } else {
            $indexAr = json_decode($targetList, true);
        }

        return $indexAr;
    }

    /**
     * Кешируем индекс
     *
     * @param Index $index
     */
    protected function cacheIndex(Index $index)
    {
        $indexData = $index->getData();
        $adCache = new AdCache();
        foreach ($indexData as $adtype => $adtypeData) {
            $adCache->set($adtype, $adtypeData);
        }
    }

    /**
     * @param Collection $campaigns
     * @return array
     */
    protected function getCampaignsStats(Collection $campaigns)
    {
        $campaignIds = [];
        /** @var Campaign $campaign */
        foreach ($campaigns as $campaign) {
            $campaignIds[] = $campaign->getId();
        }

        $result = (new Search())
            ->process(
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                'id',
                Criteria::DESC,
                null,
                null,
                $campaignIds
            );
        
        $tmp = [];
        foreach ($result as $row){
            $tmp[$row['id']] = $row;
        }
        
        return $tmp;
    }
}
