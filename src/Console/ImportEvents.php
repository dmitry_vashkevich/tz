<?php

namespace Project\Console;

use Project\Check\Exception\EmptyEntityException;
use Project\Console\ImportEvents\Exception\UndefinedStatDataException;
use Project\Console\ImportEvents\Exception\UnknownTrackerEventTypeException;
use Project\Entity\TrackerEvent;
use Project\Stats\Event;
use Project\Tracker\Tracker;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Импорт событий с трекера:
 */
class ImportEvents extends DBConcurrencyCommand
{
    /**
     * Кол-во выгружаемых событий за раз
     */
    const EVENT_LIMIT = 10000;

    /**
     * денежные события
     */
    const AMOUNT_EVENTS = [
        Event\Lead::class,
        Event\Accept::class,
        Event\Decline::class,
        Event\Invalid::class,
    ];

    /**
     * Карта событий трекера
     *
     * @var array
     */
    protected $map = [
        1 => Event\ClickTV::class,
        2 => Event\PrelandLand::class,
        3 => Event\Lead::class,
        4 => Event\Accept::class,
        5 => Event\Decline::class,
        6 => Event\Invalid::class,
    ];

    /** @var  Tracker */
    protected $tracker;

    /** @var  Event\Aggregator */
    protected $aggregator;

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->setName('import:tracker:events')
            ->setDescription('import events');
    }

    /**
     * Конструктор
     * @param null $name
     */
    public function __construct($name = null)
    {
        parent::__construct($name);
        $this->tracker = new Tracker();
        $this->aggregator = new Event\Aggregator(false);
    }

    /**
     * @inheritdoc
     */
    protected function concurrencyDBExecute(InputInterface $input, OutputInterface $output)
    {
        $lastId = $this->getLastId();
        $this->import($lastId);
    }

    /**
     * @param int $lastId
     * @param bool $confirm
     * @throws \Doctrine\DBAL\ConnectionException
     */
    protected function import($lastId, $confirm = true)
    {
        $eventData = $this->tracker->getEventList($lastId, self::EVENT_LIMIT);
        if (count($eventData) > 0) {
            $this->db->beginTransaction();
            foreach ($eventData as $row) {
                try {
                    //если вдруг событие уже импортировано
                    $this->checkTrackerEvent($row['id']);
                    echo 'test';die;
                    continue;
                } catch (EmptyEntityException $e) {
                }

                try {
                    $event = $this->prepareEvent($row);
                } catch (\Exception $e) {
                    $this->logError($e);
                    continue;
                }

                $this->aggregator->process($event);

                $this->saveTrackerEvent($row);
            }

            $this->aggregator->saveStats();
            $this->db->commit();

            if ($confirm) {
                $lastId = $this->getLastId();
                $this->tracker->confirmEventList($lastId);
            }
        }
    }

    /**
     * @param $id
     * @throws EmptyEntityException
     */
    protected function checkTrackerEvent($id)
    {
        TrackerEvent::prepareById($id);
    }

    /**
     * @param array $row
     * @return Event
     * @throws UndefinedStatDataException
     * @throws UnknownTrackerEventTypeException
     */
    protected function prepareEvent(array $row)
    {
        if (!array_key_exists($row['type'], $this->map)) {
            throw new UnknownTrackerEventTypeException('with type = [' . $row['type'] . ']');
        }

        $statData = json_decode(base64_decode($row['subId1']), true);

        if (!is_array($statData)) {
            throw new UndefinedStatDataException('row = [' . print_r($row, true) . ']');
        }

        if (isset($row['landId']) && $row['landId'] > 0) {
            $statData['land_id'] = $row['landId'];
        }

        if (isset($row['prelandId']) && $row['prelandId'] > 0) {
            $statData['preland_id'] = $row['prelandId'];
        }

        /** @var Event $className */
        $className = $this->map[$row['type']];

        if (in_array($className, self::AMOUNT_EVENTS)) {
            return new $className(
                $statData,
                $row['time'],
                null,
                $row['amountRub']
            );
        } else {
            return new $className(
                $statData,
                $row['time']
            );
        }
    }

    /**
     * @param array $row
     * @return static
     */
    protected function saveTrackerEvent(array $row)
    {
        return (new TrackerEvent())
            ->setId($row['id'])
            ->setData(json_encode($row))
            ->save();
    }

    /**
     * @return int
     * @throws \Doctrine\DBAL\DBALException
     */
    protected function getLastId()
    {
        $sql = /** @lang sql */
            "select max(id) from tracker_events";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        return (int)$stmt->fetchColumn();
    }

    /**
     * @param \Exception $e
     * @return mixed
     */
    private function logError(\Exception $e)
    {
        $message = sprintf(
            '$ %s | exception %s: %s at %s line %s',
            $this->getName(),
            get_class($e),
            $e->getMessage(),
            $e->getFile(),
            $e->getLine()
        );

        $this->logger->error($message,
            [
                'group' => 'exception',
                'logger' => static::class,
                'hostname' => gethostname(),
            ]
        );
    }
}
