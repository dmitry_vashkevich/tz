<?php

namespace Project\Console;

use Project\Stats\Event;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Реимпорт событий с трекера
 * (
 *  ЗАПУСКАТЬ ОБАЗАТЕЛЬНО С КЛЮЧЕМ --test
 * )
 */
class ReImportEvents extends ImportEvents
{
    /**
     * @inheritdoc
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->setName('reimport:tracker:events')
            ->setDescription('re-Import tracker Events which not contains in table');
    }

    /**
     * @inheritdoc
     */
    protected function concurrencyExecute(InputInterface $input, OutputInterface $output)
    {
        if (!$input->getOption('test')) {
            die("Эта команда требует нежного обращения и опции --test \n");
        }

        $lastId = $this->getLastId();
        for ($i = 0; $i <= $lastId + self::EVENT_LIMIT; $i+=self::EVENT_LIMIT) {
            $this->import($i, false);
        }
    }
}
