<?php

namespace Project\Console\ImportEvents\Exception;

use Project\Exception\RestException\ServerError\InternalServerErrorException;

/**
 * Если трекер прислал неизвестный тип события
 */
class UnknownTrackerEventTypeException extends InternalServerErrorException
{

}
