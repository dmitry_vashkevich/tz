<?php

namespace Project\Console\ImportEvents\Exception;

use Project\Exception\RestException\ServerError\InternalServerErrorException;

/**
 * Если трекер прислал событие без стат данных
 */
class UndefinedStatDataException extends InternalServerErrorException
{

}
