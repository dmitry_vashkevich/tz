<?php

namespace Project\Console;

use Doctrine\Common\Collections\Criteria;
use Project\Campaign\Active;
use Project\Campaign\Create;
use Project\Campaign\Flow;
use Project\Entity\Ad;
use Project\Entity\Campaign;
use Project\Entity\Land;
use Project\Entity\Offer;
use Project\Entity\SiteUser;
use Project\Proxy;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Создание и обновление кешбек кампаний
 */
class CampaignCashback extends DBConcurrencyCommand
{
    const CB_USER_ID = -1;
    /**
     * Интервал между операциями
     */
    const DELAY = 300;//sec

    /** @var  SiteUser */
    protected $cashbackUser;

    /**
     * Конструктор
     * @param null $name
     */
    public function __construct($name = null)
    {
        parent::__construct($name);
        $this->cashbackUser = SiteUser::prepareById(self::CB_USER_ID);
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this
            ->setName('campaign:cashback')
            ->setDescription('import cashback data & update campaigns');

        parent::configure();
    }

    /**
     * @inheritdoc
     */
    protected function concurrencyDBExecute(InputInterface $input, OutputInterface $output)
    {
        $entityManager = Proxy::init()->getEntityManager();
        $lands = Land::prepareList(
            (new Criteria())->where(
                Criteria::expr()->eq('cashback', true)
            )->andWhere(
                Criteria::expr()->eq('active', true)
            )->andWhere(
                    Criteria::expr()->eq('offer_id', 139)
                )
        );

        if ($lands->isEmpty()) {
            return;
        }

        try {
            $entityManager->beginTransaction();
            foreach ($lands as $land) {
                $offer = $land->getOffer();

                $campaigns = Campaign::prepareList(
                    (new Criteria())->where(
                        Criteria::expr()->eq('offer', $offer)
                    )->andWhere(
                        Criteria::expr()->eq('site_user', $this->cashbackUser)
                    )->andWhere(
                        Criteria::expr()->eq('active', true)
                    )
                );
            
                if ($campaigns->isEmpty()) {
                    $campaign = $this->createCampaign($offer, $land);
                } else {
                    $campaign = $campaigns->first();
                }

                $this->updateCampaign($campaign, $offer);

                Proxy::init()->getEntityManager()->detach($campaign);
            }

            $entityManager->commit();

        } catch (\Exception $e) {
            $entityManager->rollback();
            throw $e;
        }
    }


    /**
     * @param Offer $offer
     * @param Land $land
     * @return Campaign
     * @throws Flow\Exception\EmptyFlowException
     */
    protected function createCampaign(Offer $offer, Land $land)
    {
        $cashbackUser = $this->getCashbackUser();
        $campaign = (new Create(
            $cashbackUser,
            Ad::ADTYPE_CASHBACK,
            $offer->getName()
        ))->process($offer);

        $flow = json_encode([
            ['land_id' => $land->getId()]
        ]);

        return (new Flow($cashbackUser, $campaign))
            ->process($flow);
    }

    /**
     * @param Campaign $campaign
     * @param Offer $offer
     * @return Campaign
     */
    protected function updateCampaign(Campaign $campaign, Offer $offer)
    {
        $cashbackUser = $this->getCashbackUser();
        $campaign->setName($offer->getName());
        $campaign->setTargetDomains(json_encode([$offer->getName()]));
        $campaign->save();

        return (new Active($cashbackUser, $campaign))
            ->process(
                $offer->isActive(),
                Proxy::init()->getConfig()['tracker_cashback']
            );
    }

    /**
     * @return SiteUser
     */
    protected function getCashbackUser()
    {
        return SiteUser::prepareById(self::CB_USER_ID);
    }
}
