<?php

namespace Project\Console\ImportStatic;

use Project\Proxy;
use Project\Tracker\Tracker;

/**
 * Импорт кешбек продуктов (домены в поле name)
 */
class CBOffers extends Offers
{
    /**
     * @inheritdoc
     */
    protected function getEntityData()
    {
        return (new Tracker(
            Proxy::init()->getConfig()['tracker_cashback']
        ))->getProductList();
    }
}
