<?php

namespace Project\Console\ImportStatic;

use Project\Entity\Offer;
use Project\Entity\Preland;
use Project\Entity\Schema\EntityWithDeterminedId;
use Project\Tracker\Tracker;

/**
 * Импорт прелендингов
 */
class Prelands extends AbstractImport
{
    /**
     * @inheritdoc
     */
    protected function getEntityClassName()
    {
        return Preland::class;
    }

    /**
     * @inheritdoc
     */
    protected function getEntityData()
    {
        return (new Tracker())->getPreLandList();
    }

    /**
     * @inheritdoc
     */
    protected function updateEntity(EntityWithDeterminedId $entity, array $row)
    {
        /** @var Preland $entity */
        return $entity
            ->setName($row['name'])
            ->setOffer($row['productId'] !== null ? Offer::prepareById($row['productId']) : null)
            ->setActive($row['enabled'])
            ->setUrl($row['url'])
            ->save();
    }
}
