<?php

namespace Project\Console\ImportStatic;

use Project\Proxy;
use Project\Tracker\Tracker;

/**
 * Импорт категорий кешбек продуктов
 */
class CBOfferCats extends OfferCats
{
    /**
     * @inheritdoc
     */
    protected function getEntityData()
    {
        return (new Tracker(
            Proxy::init()->getConfig()['tracker_cashback']
        ))->getCategoryList();
    }
}
