<?php

namespace Project\Console\ImportStatic;

use Doctrine\Common\Collections\Criteria;
use Project\Check\Exception\EmptyEntityException;
use Project\Entity\Schema\EntityWithDeterminedId;
use Project\Proxy;

/**
 * Абстрактный импорт статической сущности с трекера
 */
abstract class AbstractImport
{
    /**
     * Импорт
     */
    public function import()
    {
        $data = $this->getEntityData();

        if (is_array($data) && count($data) > 0) {
            $ids = [];
            foreach ($data as $row) {
                $entity = $this->prepareEntity($row);
                $ids[] = $entity->getId();
                $this->updateEntity(
                    $entity,
                    $row
                );
                
                Proxy::init()->getEntityManager()->detach($entity);
            }

            $this->updateInverse($ids);
        }
    }

    /**
     * Обновление сущностей не вошедших в список:
     * - active = false
     *
     * @param array $ids
     */
    protected function updateInverse(array $ids)
    {
        Proxy::init()->getEntityManager()->createQueryBuilder()
            ->update($this->getEntityClassName(), 'e')
            ->set('e.active', ':active')
            ->setParameter('active', false)
            ->addCriteria(
                (new Criteria())->where(
                    Criteria::expr()->notIn('id', $ids)
                )
            )
            ->getQuery()
            ->execute();
    }

    /**
     * @param array $row
     * @return EntityWithDeterminedId
     */
    protected function prepareEntity(array $row)
    {
        $className = $this->getEntityClassName();

        /** @var EntityWithDeterminedId $className */
        try {
            $entity = $className::prepareById($row['id']);
        } catch (EmptyEntityException $e) {
            $entity = (new $className())
                ->setId($row['id']);
        }

        return $entity;
    }

    /**
     * Получить класс импортируемой сущности
     *
     * @return string of EntityWithDeterminedId class
     */
    abstract protected function getEntityClassName();

    /**
     * Получить импортируемые данные
     *
     * @return array
     */
    abstract protected function getEntityData();

    /**
     * Обновить сущность
     *
     * @param EntityWithDeterminedId $entity
     * @param array $row
     * @return EntityWithDeterminedId
     */
    abstract protected function updateEntity(EntityWithDeterminedId $entity, array $row);
}
