<?php

namespace Project\Console\ImportStatic;

use Project\Entity\Offer;
use Project\Entity\OfferCat;
use Project\Entity\Schema\EntityWithDeterminedId;
use Project\Tracker\Tracker;

/**
 * Импорт продуктов
 */
class Offers extends AbstractImport
{
    /**
     * @inheritdoc
     */
    protected function getEntityClassName()
    {
        return Offer::class;
    }

    /**
     * @inheritdoc
     */
    protected function getEntityData()
    {
        return (new Tracker())->getProductList();
    }

    /**
     * @inheritdoc
     */
    protected function updateEntity(EntityWithDeterminedId $entity, array $row)
    {
        /** @var Offer $entity */
        $entity
            ->setActive($row['enabled'])
            ->setName($row['name'])
            ->setDescription($row['description'])
            ->setPriceModel($row['model'])
            ->setImageUrl($row['imageUrl'])
            ->setImageSmallUrl($row['imageSmallUrl']);

        if (is_array($row['categoryList']) && count($row['categoryList']) > 0) {
            $entity->getOfferCats()->clear();
            foreach ($row['categoryList'] as $catId) {
                $entity->addOfferCat(OfferCat::prepareById($catId));
            }
        }

        return $entity->save();
    }
}
