<?php

namespace Project\Console\ImportStatic;

use Project\Entity\Land;
use Project\Entity\Offer;
use Project\Entity\Schema\EntityWithDeterminedId;
use Project\Tracker\Tracker;

/**
 * Импорт лендингов
 */
class Lands extends AbstractImport
{
    /**
     * @inheritdoc
     */
    protected function getEntityClassName()
    {
        return Land::class;
    }

    /**
     * @inheritdoc
     */
    protected function getEntityData()
    {
        return (new Tracker())->getLandList();
    }

    /**
     * @inheritdoc
     */
    protected function updateEntity(EntityWithDeterminedId $entity, array $row)
    {
        if ($row['mobile'] === null) {
            $platform = Land::PLATFORM_BOTH;
        } elseif ($row['mobile']) {
            $platform = Land::PLATFORM_MOBILE;
        } else {
            $platform = Land::PLATFORM_DESKTOP;
        }

        /** @var Land $entity */
        return $entity
            ->setName($row['name'])
            ->setOffer(Offer::prepareById($row['productId']))
            ->setActive($row['enabled'])
            ->setUrl($row['url'])
            ->setCountrySettings(json_encode($row['countryList']))
            ->setPlatform($platform)
            ->save();
    }
}
