<?php

namespace Project\Console\ImportStatic;

use Project\Entity\OfferCat;
use Project\Entity\Schema\EntityWithDeterminedId;
use Project\Tracker\Tracker;

/**
 * Импорт категорий продуктов
 */
class OfferCats extends AbstractImport
{
    /**
     * @inheritdoc
     */
    protected function getEntityClassName()
    {
        return OfferCat::class;
    }

    /**
     * @inheritdoc
     */
    protected function getEntityData()
    {
        return (new Tracker())->getCategoryList();
    }

    /**
     * @inheritdoc
     */
    protected function updateEntity(EntityWithDeterminedId $entity, array $row)
    {
        /** @var OfferCat $entity */
        return $entity
            ->setName($row['name'])
            ->save();
    }
}
