<?php

namespace Project\Console\ImportStatic;

use Project\Entity\Land;
use Project\Entity\Schema\EntityWithDeterminedId;
use Project\Proxy;
use Project\Tracker\Tracker;

/**
 * Импорт кешбек лендингов
 */
class CBLands extends Lands
{
    /**
     * @inheritdoc
     */
    protected function getEntityData()
    {
        return (new Tracker(
            Proxy::init()->getConfig()['tracker_cashback']
        ))->getLandList();
    }

    /**
     * @inheritdoc
     */
    protected function updateEntity(EntityWithDeterminedId $entity, array $row)
    {
        /** @var Land $entity */
        $entity->setCashback(true);
        return parent::updateEntity($entity, $row);
    }
}
