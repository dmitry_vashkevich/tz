<?php

namespace Project\Console\DataFilling;

/**
 * Менеджер для заполнения БД
 *
 * Умеет оперировать структурами, добавляет только те строки, которых еще нет в БД
 */
class Manager
{
    /**
     * @var Data
     */
    private $data;

    /**
     * Инициализация
     *
     * @param Data $data
     */
    public function __construct(Data $data)
    {
        $this->data = $data;
    }

    /**
     * Вставить данные фикстур в БД
     *
     * @return Result
     */
    public function insert()
    {
        $updated = $added = $missed = 0;

        $diffTool = new DiffTool($this->data->getDiffOptions());

        foreach ($this->data->getRows() as $row) {
            $found = $this->data->find($row);
            if ($found) {
                $missed++;
                continue;
            }

            $diffTool
                ->setNewRow($row)
                ->diff();

            $row->save();
            $added++;
        }

        return (new Result())
            ->setAdded($added)
            ->setMissed($missed)
            ->setUpdated($updated)
            ->setDiffTable($diffTool->getDiffResult());
    }

    /**
     * Обновить данные в БД из фикстур
     *
     * @return Result
     */
    public function update()
    {
        $updated = $added = $missed = 0;
        $diffTool = new DiffTool($this->data->getDiffOptions());

        foreach ($this->data->getRows() as $row) {
            $found = $this->data->find($row);
            if ($found) {
                $diffTool
                    ->setNewRow($row)
                    ->setOldRow($found)
                    ->diff();
                $this->data->merge($found, $row);
                $found->save();
                $updated++;
            } else {
                $diffTool
                    ->setNewRow($row)
                    ->diff();

                $row->save();
                $added++;
            }
        }

        return (new Result())
            ->setAdded($added)
            ->setMissed($missed)
            ->setUpdated($updated)
            ->setDiffTable($diffTool->getDiffResult());
    }
}
