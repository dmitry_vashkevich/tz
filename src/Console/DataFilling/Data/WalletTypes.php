<?php

namespace Project\Console\DataFilling\Data;

use Project\Console\DataFilling\Data;
use Project\Console\DataFilling\DiffOptions;
use Project\Entity\Schema\Base;
use Project\Entity\WalletType;

/**
 * Типы кошельков
 */
class WalletTypes extends Data
{
    /**
     * @inheritdoc
     */
    public function getRows()
    {
        return [
            (new WalletType())
                ->setId(1)
                ->setName('WebMoney')
        ];
    }

    /**
     * @inheritdoc
     */
    public function find(Base $row)
    {
        /** @var WalletType $row */

        return WalletType::repo()->findOneBy([
            'id' => $row->getId(),
        ]);
    }

    /**
     * @param Base $exist
     * @param Base $fixture
     */
    public function merge(Base $exist, Base $fixture)
    {
        /**
         * @var WalletType $exist
         * @var WalletType $fixture
         */
        $exist
            ->setName($fixture->getName());
    }

    /**
     * Получить список опций для DiffTool
     *
     * @return DiffOptions
     */
    public function getDiffOptions()
    {
        return new DiffOptions(
            WalletType::class,
            [
                'Id',
                'Name',
            ]
        );
    }
}
