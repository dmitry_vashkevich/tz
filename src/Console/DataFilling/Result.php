<?php

namespace Project\Console\DataFilling;

/**
 * Результат заполнения данных по одной сущности
 */
class Result
{
    /**
     * Переменные хранимые в структуре
     *
     * @var int
     */
    private $all = 0;
    private $added = 0;
    private $missed = 0;
    private $updated = 0;
    private $diffTable = [];
    /**
     * Установить количество чего-либо
     *
     * @param $count
     *
     * @return $this
     */
    public function setAll($count)
    {
        $this->all = $count;

        return $this;
    }

    /**
     * Получить количество чего-либо
     *
     * @return int
     */
    public function getAll()
    {
        return $this->all;
    }

    /**
     * Установить количество обновленных строк
     *
     * @param $count
     *
     * @return $this
     */
    public function setUpdated($count)
    {
        $this->updated = $count;

        return $this;
    }

    /**
     * Установить количество добавленных данных
     *
     * @param $count
     *
     * @return $this
     */
    public function setAdded($count)
    {
        $this->added = $count;

        return $this;
    }

    /**
     * Получить количество обновленных строк
     *
     * @return int
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Получить количество добавленных данных
     *
     * @return int
     */
    public function getAdded()
    {
        return $this->added;
    }

    /**
     * Установить количество пропусков
     *
     * @param $count
     *
     * @return $this
     */
    public function setMissed($count)
    {
        $this->missed = $count;

        return $this;
    }

    /**
     * Получить количество пропусков
     *
     * @return int
     */
    public function getMissed()
    {
        return $this->missed;
    }

    /**
     * Установить таблицу результатов сравнения
     *
     * @param array $diffTable
     *
     * @return $this
     */
    public function setDiffTable($diffTable)
    {
        $this->diffTable = $diffTable;
        return $this;
    }

    /**
     * Получить таблицу результатов сравнения
     *
     * @return array
     */
    public function getDiffTable()
    {
        return $this->diffTable;
    }
}
