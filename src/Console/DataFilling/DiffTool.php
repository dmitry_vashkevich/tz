<?php

namespace Project\Console\DataFilling;

use Project\Console\DataFilling\Exception\UndefinedDiffToolPropsException;
use Project\Console\DataFilling\Exception\UnexpectedDiffClassException;
use Project\Entity\Schema\Base;
use Symfony\Component\Console\Helper\TableSeparator;

/**
 * Класс DiffTool
 */
class DiffTool
{
    /**
     * ссылки на свойства
     *
     * @see old
     * @see new
     */
    const OLD_ROW = 'old';
    const NEW_ROW = 'new';

    /**
     * Новая запись
     *
     * @var Base
     */
    protected $new;

    /**
     * Старая запись
     *
     * @var Base
     */
    protected $old;

    /**
     * @var DiffOptions
     */
    protected $options;

    /**
     * Заголовок результата
     *
     * @var array
     */
    protected $rows = [];

    /**
     * Строки результата
     *
     * @var array
     */
    protected $header = [];

    /**
     * Конструктор
     *
     * @param DiffOptions $options
     */
    public function __construct(DiffOptions $options)
    {
        $this->options = $options;
    }

    /**
     * Установить новую запись
     *
     * @param Base $new
     *
     * @return $this
     */
    public function setNewRow(Base $new)
    {
        $this->new = $new;
        return $this;
    }

    /**
     * Установить старую запись
     *
     * @param Base $old
     *
     * @return $this
     */
    public function setOldRow(Base $old)
    {
        $this->old = $old;
        return $this;
    }

    /**
     * Записывает результат сравнения в буфер
     * возвращая результат операции
     *
     * @return bool
     */
    public function diff()
    {
        $this->check();

        $this->header = $this->options->getProperties();

        if ($this->old === null) {
            $this->addDiffRecord();
            return true;
        }

        if ($this->isDiffer()) {
            $this->addDiffRecord();
            return true;
        }

        return false;
    }

    /**
     * Получить из буфера результаты сравнений
     *
     * @return array
     */
    public function getDiffResult()
    {
        return [
            'header' => $this->header,
            'rows'   => $this->rows,
        ];
    }

    /**
     * Есть ли разница
     *
     * @return bool
     */
    protected function isDiffer()
    {
        foreach ($this->header as $property) {
            $method = $this->prepareMethod($property);
            if ((string)$this->new->$method() != (string)$this->old->$method()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Добавляет запись о сравнении
     */
    protected function addDiffRecord()
    {
        if (count($this->rows) > 0) {
            $this->rows[] = new TableSeparator();
        }

        $this->addRow(self::NEW_ROW);

        if ($this->old !== null) {
            $this->addRow(self::OLD_ROW);
        }
    }

    /**
     * Формирует строку в записи
     *
     * @param string $name
     */
    protected function addRow($name)
    {
        $row = [];
        foreach ($this->header as $property) {
            $method = $this->prepareMethod($property);
            $row[] = (string)$this->$name->$method();
        }

        $this->rows[] = $row;
    }

    /**
     * Получает имя гетера для свойства
     *
     * @param $property
     *
     * @return string
     */
    protected function prepareMethod($property)
    {
        $prefix = 'get';
        if (in_array($property, ['Active', 'Triable'])) {
            $prefix = 'is';
        }

        return $prefix . ucfirst($property);
    }

    /**
     * Проверка перед началом сравнения
     *
     * @throws Exception\UndefinedDiffToolPropsException
     */
    protected function check()
    {
        $expectedClass = $this->options->getEntityClass();

        if (!$this->new instanceof $expectedClass) {
            throw new UnexpectedDiffClassException(
                'Unexpected class for new record:' .
                ' expected = ' . $expectedClass .
                ' actual = ' . get_class($this->new)
            );
        }

        if (($this->old !== null) && (!$this->old instanceof $expectedClass)) {
            throw new UnexpectedDiffClassException(
                'Unexpected class for old record:' .
                ' expected = ' . $expectedClass .
                ' actual = ' . get_class($this->old)
            );
        }

        if (count($this->options->getProperties()) == 0) {
            throw new UndefinedDiffToolPropsException('Undefined diff properties');
        }
    }
}
