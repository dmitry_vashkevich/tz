<?php

namespace Project\Console\DataFilling\Exception;

use Project\Exception\RestException\ServerError\InternalServerErrorException;

/**
 * В случае, если не заданы опции сравнения для записей
 */
class UndefinedDiffToolPropsException extends InternalServerErrorException
{

}
