<?php

namespace Project\Console\DataFilling;

use Project\Entity\Schema\Base;

/**
 * Структура с данными
 *
 * Помимо данных содержится логика по проверке существования строки в БД
 */
abstract class Data
{
    /**
     * Получить строки для вставки в БД
     *
     * @return Base[]
     */
    abstract public function getRows();

    /**
     * Проверить не существует ли подобная строка в БД
     *
     * @param Base $row
     *
     * @return Base|null
     */
    abstract public function find(Base $row);

    /**
     * Слияние уже существующей строки и строки из фикстур
     *
     * @param Base $exist
     * @param Base $fixture
     */
    abstract public function merge(Base $exist, Base $fixture);

    /**
     * Получить список опций для DiffTool
     * @return DiffOptions
     */
    abstract public function getDiffOptions();
}
