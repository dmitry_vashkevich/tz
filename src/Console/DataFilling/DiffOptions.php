<?php

namespace Project\Console\DataFilling;

/**
 * Класс DiffOptions
 */
class DiffOptions
{
    /**
     * Класс связанной сущности
     * @var string
     */
    protected $entityClass;

    /**
     * Свойства, по которым идет сравнение
     *
     * @var array
     */
    protected $properties = [];

    /**
     * Конструктор
     *
     * @param $entityClass
     * @param array $properties
     */
    public function __construct($entityClass, array $properties)
    {
        $this->entityClass = $entityClass;
        $this->properties = $properties;
    }

    /**
     * Получить класс сущности
     *
     * @return string
     */
    public function getEntityClass()
    {
        return $this->entityClass;
    }

    /**
     * Получить список свойств
     *
     * @return array
     */
    public function getProperties()
    {
        return $this->properties;
    }
}
