<?php

namespace Project\Console;

use Project\Console\DataFilling\Data;
use Project\Console\DataFilling\Manager;
use Project\Proxy;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Наполнение БД обязательными параметрами
 */
class DataFilling extends Command
{
    /**
     * Сообщения
     */
    const INIT_MESSAGE = 'let\'s start';
    const FORCE_FINISH_MESSAGE = 'Commited!';
    const NON_FORCE_FINISH_MESSAGE = 'Rollback...';

    /**
     * Хендлеры импорта
     */
    const IMPORT_INSERT = 'insert';
    const IMPORT_UPDATE = 'update';

    /**
     * Хендлеры завершения
     */
    const FINISH_FORCE = 'forceFinish';
    const FINISH_NONFORCE = 'nonforceFinish';

    /**
     * Хендлер выполнения импорта
     *
     * @var string
     */
    private $hImport = self::IMPORT_INSERT;

    /**
     * Хендлер завершения команды
     *
     * @var string
     */
    private $hFinish = self::FINISH_NONFORCE;

    /**
     * Фикстуры
     *
     * @var Data[]
     */
    private $elements;

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this
            ->setName('db:filling')
            ->setDescription('Fills database with default data')
            ->setDefinition(
                [
                    new InputOption(
                        'update',
                        null,
                        null,
                        'Update DB with actual fixtures with saving IDs'
                    ),
                    new InputOption(
                        'force',
                        null,
                        null,
                        'Command will show differences only, without "force" option'
                    ),
                ]
            );
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(
            $this->init($input)
        );

        Proxy::init()->getEntityManager()->beginTransaction();

        foreach ($this->elements as $element) {
            $output->writeln('Fill "' . get_class($element) . '", rows info:');

            $result = $this->importData($element);

            $output->writeln(
                'all: ' . $result->getAll() .
                ', added: ' . $result->getAdded() .
                ', missed: ' . $result->getMissed() .
                ', updated: ' . $result->getUpdated(),
                OutputInterface::OUTPUT_PLAIN
            );

            if (count($result->getDiffTable()['rows']) > 0) {
                (new Table($output))
                    ->setHeaders($result->getDiffTable()['header'])
                    ->setRows($result->getDiffTable()['rows'])
                    ->render();
            }
        }

        $output->writeln(
            $this->finish()
        );
    }

    /**
     * Инициализация параметров команды
     *
     * @param InputInterface $input
     *
     * @return string
     */
    private function init(InputInterface $input)
    {
        if ($input->getOption('update')) {
            $this->hImport = self::IMPORT_UPDATE;
        }

        if ($input->getOption('force')) {
            $this->hFinish = self::FINISH_FORCE;
        }

        $this->elements = [
            new Data\WalletTypes(),
        ];

        return self::INIT_MESSAGE;
    }

    /**
     * Выполнить импорт фикстур тем или иным способом
     *
     * @param Data $element
     *
     * @return DataFilling\Result
     */
    private function importData(Data $element)
    {
        $manager = new Manager($element);

        return $manager->{$this->hImport}();
    }


    /**
     * Завершить выполнение команды
     *
     * @see forceFinish
     * @see nonforceFinish
     * @return string
     */
    private function finish()
    {
        return $this->{$this->hFinish}();
    }

    /**
     * Завершить выполнение команды с force
     *
     * @return string
     */
    private function forceFinish()
    {
        Proxy::init()->getEntityManager()->commit();
        return self::FORCE_FINISH_MESSAGE;
    }

    /**
     * Завершить выполнение команды без force
     *
     * @return string
     */
    private function nonforceFinish()
    {
        Proxy::init()->getEntityManager()->rollback();
        return self::NON_FORCE_FINISH_MESSAGE;
    }
}
