<?php

namespace Project;

use Symfony\Component\HttpFoundation\Request;

/**
 * Логгер
 */
class Logger extends \Monolog\Logger
{
    /**
     * Процесс породившый лог
     *
     * @var string
     */
    private $sapi;

    /**
     * Идентификатор сессии исполнения
     *
     * @var string
     */
    private $sessionId;

    /**
     * Вытягивает из конфигов значение PHP_SAPI
     *
     * @param string $name
     * @param array $handlers
     * @param array $processors
     */
    public function __construct($name, array $handlers = [], array $processors = [])
    {
        parent::__construct($name, $handlers, $processors);

        $this->sapi = $this->getConfig()['env']['sapi'];
        $this->sessionId = uniqid($this->getConfig()['env']['project_name'], true);
    }

    /**
     * Лог
     *
     * Дописываем информацию из конфигов в контекст, дальше пробрасываем все в родитель
     *
     * @param int $level
     * @param string $message
     * @param array $context
     * @return bool|void
     */
    public function addRecord($level, $message, array $context = [])
    {
        $context['process'] = $this->sapi;
        $context['session_id'] = $this->sessionId;

        if ($this->sapi == 'fpm') {
            $XRequestId =$this->getRequest()->headers->get('X-Request-ID');
            if ($XRequestId) {
                $context['X-Request-ID'] = $XRequestId;
            }
        }

        parent::addRecord($level, $message, $context);
    }

    /**
     * Возвращает конфиг
     *
     * @return array
     */
    protected function getConfig()
    {
        return Proxy::init()->getConfig();
    }

    /**
     * Возвращает запрос
     *
     * @return Request
     */
    protected function getRequest()
    {
        return Proxy::init()->getRequest();
    }
}
