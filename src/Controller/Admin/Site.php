<?php

namespace Project\Controller\Admin;

use Project\Controller\TJ;
use Project\Check\Validator\Digit;
use Project\Entity\User as EUser;
use Silex\ControllerCollection;
use Project\Service\Admin\Site as TJSiteService;
use Project\Service\Admin\User as AdminUser;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Контроллер работающий с TJSite от админа
 */
class Site extends TJ\Site
{
    /**
     * Установка роутинга для конкретного контроллера
     *
     * @param ControllerCollection $controllers
     */
    protected function route(ControllerCollection $controllers)
    {
        $controllers->get('/{sessionId}', $this->transaction('search'));
        $controllers->put('/{sessionId}/{siteId}', $this->transaction('update'));
        $controllers->delete('/{sessionId}/{siteId}', $this->transaction('delete'));
    }

    /**
     * @inheritdoc
     */
    public function search(Request $request, $sessionId)
    {
        $this->validateType(
            $request->query->all(),
            [
                'tj_user_id' => [
                    new Digit(),
                    new Assert\GreaterThan([
                        'value' => 0,
                    ]),
                ],
            ]
        );

        return parent::search($request, $sessionId);
    }

    /**
     * Получить сервис для работы с пользователями
     *
     * @param string $sessionId
     * @return \Project\Service\SiteUser
     */
    protected function getUserService($sessionId)
    {
        return new AdminUser($sessionId);
    }
    
    /**
     * Получить сервис для работы с TJSite
     *
     * @param EUser $user
     * @return TJSiteService
     */
    protected function getTJSiteService(EUser $user)
    {
        return new TJSiteService($user);
    }
}
