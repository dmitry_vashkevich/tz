<?php

namespace Project\Controller\Admin;

use Project\Controller\Api;
use Project\Service\Admin\User as AdminUserService;
use Silex\ControllerCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Контроллер работающий с кампаниями от админа
 */
class Campaign extends Api\Campaign
{
    /**
     * Установка роутинга для конкретного контроллера
     *
     * @param ControllerCollection $controllers
     */
    protected function route(ControllerCollection $controllers)
    {
        $controllers->get('/{sessionId}', $this->raw('search'));
        $controllers->put('/{sessionId}/{campaignId}/flow', $this->transaction('putFlow'));
        $controllers->put('/{sessionId}/{campaignId}/targeting', $this->transaction('putTargeting'));
        $controllers->put('/{sessionId}/{campaignId}/active', $this->transaction('putActive'));
        $controllers->post('/{sessionId}/{campaignId}/content', $this->transaction('putContent'));
        $controllers->put('/{sessionId}/{campaignId}', $this->transaction('update'));
    }

    /**
     * Получить сервис для работы с пользователями
     *
     * @param string $sessionId
     * @return AdminUserService
     */
    protected function getUserService($sessionId)
    {
        return new AdminUserService($sessionId);
    }
}
