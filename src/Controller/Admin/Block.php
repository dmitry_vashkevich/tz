<?php

namespace Project\Controller\Admin;

use Project\Check\Validator\Digit;
use Project\Service\Admin\User as AdminUser;
use Project\Controller\TJ;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Project\Service\Admin\Block as TJBlockService;
use Project\Entity\User as EUser;


/**
 * Контроллер работающий с блоками от админа
 */
class Block extends TJ\Block
{
    /**
     * Установка роутинга для конкретного контроллера
     *
     * @param ControllerCollection $controllers
     */
    protected function route(ControllerCollection $controllers)
    {
        $controllers->get('/{sessionId}', $this->transaction('search'));
        $controllers->post('/{sessionId}/preview', $this->transaction('preview'));
        $controllers->put('/{sessionId}/{blockId}', $this->transaction('update'));
        $controllers->delete('/{sessionId}/{blockId}', $this->transaction('delete'));
    }

    /**
     * @inheritdoc
     */
    public function search(Request $request, $sessionId)
    {
        $this->validateType(
            $request->query->all(),
            [
                'tj_user_id' => [
                    new Digit(),
                    new Assert\GreaterThan([
                        'value' => 0,
                    ]),
                ],
            ]
        );

        return parent::search($request, $sessionId);
    }
    
    /**
     * Получить сервис для работы с пользователями
     *
     * @param string $sessionId
     * @return \Project\Service\SiteUser
     */
    protected function getUserService($sessionId)
    {
        return new AdminUser($sessionId);
    }

    /**
     * Получить сервис для работы с TJBlock
     *
     * @param EUser $user
     * @return TJBlockService
     */
    protected function getTJBlockService(EUser $user)
    {
        return new TJBlockService($user);
    }
}
