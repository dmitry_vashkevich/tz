<?php

namespace Project\Controller\Admin;

use Project\Controller\Api;
use Project\Service\Admin\User as AdminUserService;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Контроллер admin статистики
 */
class Stats extends Api\Stats
{
    /**
     * Получить сервис для работы с пользователями
     *
     * @param string $sessionId
     * @return AdminUserService
     */
    protected function getUserService($sessionId)
    {
        return new AdminUserService($sessionId);
    }
}
