<?php

namespace Project\Controller\Admin;

use Project\Service\Admin\User as AdminUser;
use Project\Controller\Api;


/**
 * Контроллер работающий с рекламными материалам
 */
class Ad extends Api\Ad
{
    /**
     * Получить сервис для работы с пользователями
     *
     * @param string $sessionId
     * @return \Project\Service\SiteUser
     */
    protected function getUserService($sessionId)
    {
        return new AdminUser($sessionId);
    }
}
