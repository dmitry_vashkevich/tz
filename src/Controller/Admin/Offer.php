<?php

namespace Project\Controller\Admin;

use Project\Controller\Api;
use Project\Service\Admin\User as AdminUser;
use Project\Service\Admin\Offer as OfferService;
use Project\Entity\User as EUser;

/**
 * Контроллер работающий с офферами
 */
class Offer extends Api\Offer
{
    /**
     * @inheritdoc
     */
    protected function getUserService($sessionId)
    {
        return new AdminUser($sessionId);
    }

    /**
     * @inheritdoc
     */
    protected function getOfferService(EUser $user)
    {
        return new OfferService($user);
    }
}
