<?php

namespace Project\Controller\Admin;

use Project\Controller\Api;
use Project\Service\Admin\User as AdminUser;
use Silex\ControllerCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\Request;

/**
 * Контроллер работающий с админ-юзером
 */
class User extends Api\User
{
    /**
     * Установка роутинга для конкретного контроллера
     *
     * @param ControllerCollection $controllers
     */
    protected function route(ControllerCollection $controllers)
    {
        parent::route($controllers);
        $controllers->get('/{sessionId}', $this->raw('info'));
    }

    /**
     * Получение данных пользователя
     *
     *
     * @param Request $request
     * @param string $sessionId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function info(Request $request, $sessionId)
    {
        return $this->format(
            [
                'success' => true,
            ]
        );
    }

    /**
     * Получить сервис для работы с пользователями
     *
     * @param string | null $sessionId
     * @return \Project\Service\SiteUser
     */
    protected function getUserService($sessionId = null)
    {
        return new AdminUser($sessionId);
    }
}
