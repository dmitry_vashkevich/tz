<?php

namespace Project\Controller\Admin;

use Project\Controller\Api;
use Project\Service\Admin\User as AdminUserService;
use Symfony\Component\Validator\Constraints as Assert;
use Project\Entity\User as UserEntity;
use Project\Service\Admin\Land as LandService;

/**
 * Контроллер работающий с лендами
 */
class Land extends Api\Land
{
    /**
     * @inheritdoc
     */
    protected function getUserService($sessionId)
    {
        return new AdminUserService($sessionId);
    }

    /**
     * @inheritdoc
     */
    protected function getLandService(UserEntity $user)
    {
        return new LandService($user);
    }
}
