<?php

namespace Project\Controller\Exception;

use Project\Exception\RestException\ServerError\InternalServerErrorException;

/**
 * Возникает при неудачной попытке создания хендлера для роутера Silex
 */
class CustomCallableCreatingFailed extends InternalServerErrorException
{

}
