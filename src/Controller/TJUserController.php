<?php

namespace Project\Controller;

use Project\Controller\Api;
use Project\Service\TJ\User as TJUserService;

/**
 * TJЮзер-ориентированный контроллер
 */
abstract class TJUserController extends Api
{

    /**
     * Получить сервис для работы с пользователями
     *
     * @param string $sessionId
     * @return TJUserService
     */
    protected function getUserService($sessionId)
    {
        return new TJUserService($sessionId);
    }
}
