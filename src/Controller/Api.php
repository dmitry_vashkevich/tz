<?php

namespace Project\Controller;

use Silex\Application;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Абстрактный контроллер Api
 */
abstract class Api extends Common
{
    /**
     * Приводит к единому формату все ответы от API
     *
     * Оборачивает ответы в 'result'
     *
     * @param array $result
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    protected function format(array $result)
    {
        return $this->getApp()->json([
            'result' => $result
        ]);
    }
}
