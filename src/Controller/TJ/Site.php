<?php

namespace Project\Controller\TJ;

use Project\Check\Validator\Digit;
use Project\Entity\User as EUser;
use Project\Controller\TJUserController;
use Silex\ControllerCollection;
use Project\Service\TJ\Site as TJSiteService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Контроллер работающий с TJSite
 */
class Site extends TJUserController
{
    /**
     * Установка роутинга для конкретного контроллера
     *
     * @param ControllerCollection $controllers
     */
    protected function route(ControllerCollection $controllers)
    {
        $controllers->get('/{sessionId}', $this->transaction('search'));
        $controllers->post('/{sessionId}', $this->transaction('create'));
        $controllers->put('/{sessionId}/activeAll', $this->transaction('putActiveAll'));
        $controllers->put('/{sessionId}/{siteId}', $this->transaction('update'));
        $controllers->delete('/{sessionId}/{siteId}', $this->transaction('delete'));
    }

    /**
     * @param Request $request
     * @param $sessionId
     * @param $siteId
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function delete(Request $request, $sessionId, $siteId)
    {
        $user = $this->getUserService($sessionId)->getUser();

        return $this->format(
            $this->getTJSiteService($user)->delete($siteId)
        );
    }

    /**
     * Получить список TJSite
     *
     * @param Request $request
     * @param $sessionId
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function search(Request $request, $sessionId)
    {
        $requestData = $request->query->all();
        
        $this->validateType(
            $requestData,
            [
                'active' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                    new Assert\Choice(['true', 'false'])
                ],
                'offset' => [
                    new Digit(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                ],
                'limit' => [
                    new Digit(),
                    new Assert\GreaterThan([
                        'value' => 0,
                    ]),
                ],
            ]
        );

        $user = $this->getUserService($sessionId)->getUser();

        return $this->format(
            $this->getTJSiteService($user)->search($requestData)
        );
    }

    /**
     * @param Request $request
     * @param $sessionId
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @internal param $tconfId
     */
    public function create(Request $request, $sessionId)
    {
        $requestData = $request->request->all();

        $this->validateRequired($requestData, [
            'name',
            'url',
            'themes',
            'params',
        ]);

        $this->validateType(
            $requestData,
            [
                'name' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                ],
                'url' => [
                    new Assert\Url(),
                ],
                'alias' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                ],
                'themes' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                ],
                'statlink' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                ],
                'params' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                ],
                'active' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                    new Assert\Choice(['true', 'false'])
                ],
            ]
        );

        $user = $this->getUserService($sessionId)->getUser();

        return $this->format(
            $this->getTJSiteService($user)->create($requestData)
        );
    }

    /**
     * @param Request $request
     * @param $sessionId
     * @param $siteId
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @internal param $tconfId
     */
    public function update(Request $request, $sessionId, $siteId)
    {
        $requestData = $request->request->all();

        $this->validateType(
            $requestData,
            [
                'name' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                ],
                'url' => [
                    new Assert\Url(),
                ],
                'alias' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                ],
                'themes' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                ],
                'statlink' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                ],
                'params' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                ],
                'active' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                    new Assert\Choice(['true', 'false'])
                ],
            ]
        );

        $user = $this->getUserService($sessionId)->getUser();

        return $this->format(
            $this->getTJSiteService($user)->update($siteId, $requestData)
        );
    }
    
    /**
     * @param Request $request
     * @param $sessionId
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function putActiveAll(Request $request, $sessionId)
    {
        $requestData = $request->request->all();

        $this->validateRequired($requestData, [
            'active'
        ]);

        $this->validateType(
            $requestData,
            [
                'active' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                    new Assert\Choice(['true', 'false'])
                ],
            ]
        );

        $user = $this->getUserService($sessionId)->getUser();

        return $this->format(
            $this->getTJSiteService($user)->activeAll($requestData)
        );
    }

    /**
     * Получить сервис для работы с TJSite
     *
     * @param EUser $user
     * @return TJSiteService
     */
    protected function getTJSiteService(EUser $user)
    {
        return new TJSiteService($user);
    }
}
