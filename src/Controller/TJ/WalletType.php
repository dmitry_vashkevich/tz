<?php

namespace Project\Controller\TJ;

use Project\Controller\TJUserController;
use Project\Entity\TJUser;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Project\Service\TJ\User as TJUserService;

/**
 * Контроллер работающий с типами кошельков
 */
class WalletType extends TJUserController
{
    /**
     * Установка роутинга для конкретного контроллера
     *
     * @param ControllerCollection $controllers
     */
    protected function route(ControllerCollection $controllers)
    {
        $controllers->get('/{sessionId}', $this->transaction('search'));
    }

    /**
     * Получить список типов кошельков
     *
     * @param Request $request
     * @param $sessionId
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function search(Request $request, $sessionId)
    {
        /** @var TJUser $user */
        $user = $this->getUserService($sessionId)->getUser();

        return $this->format(
            $this->getWalletTypeService($user)->search()
        );
    }

    /**
     * Получить сервис для работы с типами кошельков
     *
     * @param TJUser $user
     * @return \Project\Service\WalletType
     */
    protected function getWalletTypeService(TJUser $user)
    {
        return new \Project\Service\WalletType($user);
    }

    /**
     * Получить сервис для работы с пользователями
     *
     * @param string $sessionId
     * @return TJUserService
     */
    protected function getUserService($sessionId)
    {
        return new TJUserService($sessionId);
    }
}
