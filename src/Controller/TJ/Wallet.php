<?php

namespace Project\Controller\TJ;

use Project\Check\Validator\Digit;
use Project\Controller\TJUserController;
use Project\Entity\TJUser;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Контроллер работающий с кошельками
 */
class Wallet extends TJUserController
{
    /**
     * Установка роутинга для конкретного контроллера
     *
     * @param ControllerCollection $controllers
     */
    protected function route(ControllerCollection $controllers)
    {
        $controllers->put('/{sessionId}/{walletId}', $this->transaction('update'));
        $controllers->post('/{sessionId}', $this->transaction('create'));
        $controllers->get('/{sessionId}', $this->transaction('search'));
        $controllers->delete('/{sessionId}/{walletId}', $this->transaction('delete'));
    }


    /**
     * Обновление кошелька пользователя
     *
     * @param Request $request
     * @param string $sessionId
     * @param $walletId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function update(Request $request, $sessionId, $walletId)
    {
        /** @var TJUser $user */
        $user = $this->getUserService($sessionId)->getUser();

        $requestData = $request->request->all();

        $this->validateRequired($requestData, [
            'active'
        ]);

        $this->validateType(
            $requestData,
            [
                'active' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                    new Assert\Choice(['true', 'false'])
                ],
            ]
        );

        return $this->format(
            $this->getWalletService($user)->update($walletId, $requestData)
        );
    }

    /**
     * @param Request $request
     * @param $sessionId
     * @param $walletId
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function delete(Request $request, $sessionId, $walletId)
    {
        /** @var TJUser $user */
        $user = $this->getUserService($sessionId)->getUser();

        return $this->format(
            $this->getWalletService($user)->delete($walletId)
        );
    }

    /**
     * Получить списка кошельков
     *
     * @param Request $request
     * @param $sessionId
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function search(Request $request, $sessionId)
    {
        /** @var TJUser $user */
        $user = $this->getUserService($sessionId)->getUser();

        return $this->format(
            $this->getWalletService($user)->search()
        );
    }

    /**
     * Добавление кошелька
     *
     * @param Request $request
     * @param string $sessionId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(Request $request, $sessionId)
    {
        $requestData = $request->request->all();

        $this->validateRequired($requestData, [
            'wallet_type_id',
            'number',
        ]);

        $this->validateType(
            $requestData,
            [
                'wallet_type_id' => [
                    new Digit(),
                    new Assert\NotEqualTo([
                        'value' => 0,
                    ]),
                ],

                'number' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank()
                ],

                'comment' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank()
                ],
            ]
        );

        /** @var TJUser $user */
        $user = $this->getUserService($sessionId)->getUser();

        return $this->format(
            $this->getWalletService($user)->create($requestData)
        );
    }


    /**
     * Получить сервис для работы с кошельками
     *
     * @param TJUser $user
     * @return \Project\Service\Wallet
     */
    protected function getWalletService(TJUser $user)
    {
        return new \Project\Service\Wallet($user);
    }
}
