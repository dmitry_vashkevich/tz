<?php

namespace Project\Controller\TJ;

use Project\Controller\Api;
use Symfony\Component\Validator\Constraints as Assert;
use Project\Service\TJ\User as TJUserService;

/**
 * Контроллер TJ статистики
 */
class Stats extends Api\Stats
{
    /**
     * Получить сервис для работы с пользователями
     *
     * @param string $sessionId
     * @return TJUserService
     */
    protected function getUserService($sessionId)
    {
        return new TJUserService($sessionId);
    }
}
