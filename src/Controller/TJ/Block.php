<?php

namespace Project\Controller\TJ;

use Project\Check\Validator;
use Project\Check\Validator\Digit;
use Project\Entity\Ad;
use Project\Entity\TJBlock;
use Project\Entity\User as EUser;
use Project\Controller\TJUserController;
use Silex\ControllerCollection;
use Project\Service\TJ\Block as TJBlockService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Контроллер работающий с TJBlock
 * 
 * todo: необходим рефакторинг валидаторов при create и update
 */
class Block extends TJUserController
{
    /**
     * Установка роутинга для конкретного контроллера
     *
     * @param ControllerCollection $controllers
     */
    protected function route(ControllerCollection $controllers)
    {
        $controllers->get('/{sessionId}', $this->transaction('search'));
        $controllers->post('/{sessionId}/preview', $this->transaction('preview'));
        $controllers->post('/{sessionId}/{siteId}', $this->transaction('create'));
        $controllers->put('/{sessionId}/{blockId}', $this->transaction('update'));
        $controllers->delete('/{sessionId}/{blockId}', $this->transaction('delete'));
    }

    /**
     * @param Request $request
     * @param $sessionId
     * @param $blockId
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function delete(Request $request, $sessionId, $blockId)
    {
        $user = $this->getUserService($sessionId)->getUser();

        return $this->format(
            $this->getTJBlockService($user)->delete($blockId)
        );
    }

    /**
     * @param Request $request
     * @param $sessionId
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function search(Request $request, $sessionId)
    {
        $requestData = $request->query->all();

        $this->validateType(
            $requestData,
            [
                'site_id' => [
                    new Digit(),
                    new Assert\GreaterThan([
                        'value' => 0,
                    ]),
                ],
            ]
        );

        $user = $this->getUserService($sessionId)->getUser();

        return $this->format(
            $this->getTJBlockService($user)->search($requestData)
        );
    }

    /**
     * @param Request $request
     * @param $sessionId
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function preview(Request $request, $sessionId)
    {
        $requestData = $request->request->all();

        $this->validateRequired($requestData, [
            'ad_type',
        ]);

        $this->validateType(
            $requestData,
            [
                'ad_type' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                    new Assert\Choice([Ad::ADTYPE_TEASER])
                ],
            ]
        );
        
        $this->validateCustomSubParams($requestData);

        $user = $this->getUserService($sessionId)->getUser();

        return $this->getTJBlockService($user)->preview($requestData);
    }
    
    /**
     * @param Request $request
     * @param $sessionId
     * @param $siteId
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function create(Request $request, $sessionId, $siteId)
    {
        $requestData = $request->request->all();

        $this->validateRequired($requestData, [
            'name',
            'ad_type',
            'custom',
        ]);

        $this->validateType(
            $requestData,
            [
                'name' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                ],
                'ad_type' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                    new Assert\Choice(Ad::$adtypes)
                ],
                'custom' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                    new Assert\Choice(['true', 'false'])
                ],
                'active' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                    new Assert\Choice(['true', 'false'])
                ],
            ]
        );

        if (isset($requestData['custom']) && $requestData['custom'] == 'true') {
            $this->validateCustomSubParams($requestData);
        }

        $user = $this->getUserService($sessionId)->getUser();

        return $this->format(
            $this->getTJBlockService($user)->create($siteId, $requestData)
        );
    }

    /**
     * @param Request $request
     * @param $sessionId
     * @param $blockId
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @internal param $tconfId
     */
    public function update(Request $request, $sessionId, $blockId)
    {
        $requestData = $request->request->all();

        $this->validateType(
            $requestData,
            [
                'name' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                ],
                'custom' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                    new Assert\Choice(['true', 'false'])
                ],
                'active' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                    new Assert\Choice(['true', 'false'])
                ],
            ]
        );

        if (isset($requestData['custom']) && $requestData['custom'] == 'true') {
            $requestData['ad_type'] = TJBlock::prepareById($blockId)->getAdType(); 
            $this->validateCustomSubParams($requestData);
        }

        $user = $this->getUserService($sessionId)->getUser();

        return $this->format(
            $this->getTJBlockService($user)->update($blockId, $requestData)
        );
    }

    /**
     * Валидация суб-параметров при создании / редактировании блока
     *
     * @param $requestData
     */
    private function validateCustomSubParams($requestData)
    {
        $this->validateRequired($requestData, [
            'params',
            'config',
        ]);

        $this->validateType(
            $requestData,
            [
                'params' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                ],
                'config' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                ],
            ]
        );

        $decodeConfig = json_decode($requestData['config'], true);
        Validator::validateValue(
            is_array($decodeConfig),
            new Assert\EqualTo(true),
            'unable to convert config into array'
        );

        $this->validateRequired($decodeConfig, [
            'data'
        ]);

        $this->validateType(
            $decodeConfig,
            [
                'data' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                ],
                TJBlock::PARAM_CLICK_INTERVAL => [
                    new Digit(),
                    new Assert\GreaterThan([
                        'value' => 0,
                    ]),
                ],
                TJBlock::PARAM_CLOSE_INTERVAL => [
                    new Digit(),
                    new Assert\GreaterThan([
                        'value' => 0,
                    ]),
                ],
                TJBlock::PARAM_COL => [
                    new Digit(),
                    new Assert\GreaterThan([
                        'value' => 0,
                    ]),
                ],
                TJBlock::PARAM_ROW => [
                    new Digit(),
                    new Assert\GreaterThan([
                        'value' => 0,
                    ]),
                ],
                TJBlock::PARAM_WIDTH => [
                    new Digit(),
                    new Assert\GreaterThan([
                        'value' => 0,
                    ]),
                ],
                TJBlock::PARAM_HEIGHT => [
                    new Digit(),
                    new Assert\GreaterThan([
                        'value' => 0,
                    ]),
                ],
            ]
        );

        if ($requestData['ad_type'] == Ad::ADTYPE_TEASER) {
            $decodeConfig = json_decode($requestData['config'], true);
            $this->validateRequired($decodeConfig, [
                TJBlock::PARAM_ROW,
                TJBlock::PARAM_COL,
                TJBlock::PARAM_CELL_WIDTH,
                TJBlock::PARAM_CELL_HEIGHT,
                TJBlock::PARAM_WIDTH,
                TJBlock::PARAM_HEIGHT,
            ]);
        } elseif ($requestData['ad_type'] == Ad::ADTYPE_BANNER) {
            $decodeConfig = json_decode($requestData['config'], true);
            $this->validateRequired($decodeConfig, [
                TJBlock::PARAM_WIDTH,
                TJBlock::PARAM_HEIGHT,
            ]);
        }
    }

    /**
     * Получить сервис для работы с TJBlock
     *
     * @param EUser $user
     * @return TJBlockService
     */
    protected function getTJBlockService(EUser $user)
    {
        return new TJBlockService($user);
    }
}
