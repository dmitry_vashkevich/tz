<?php

namespace Project\Controller\TJ;

use Project\Check\Validator;
use Project\Check\Validator\Digit;
use Project\Controller\Api;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Project\Service\Admin\User as AdminUserService;

/**
 * Контроллер работающий с tj-юзером
 *
 * - авторизация
 * - регистрация
 * - логаут
 * - смена пароля
 * - смена данных
 */
class User extends Api\User
{
    /**
     * Установка роутинга для конкретного контроллера
     *
     * @param ControllerCollection $controllers
     */
    protected function route(ControllerCollection $controllers)
    {
        parent::route($controllers);
        $controllers->put('/{sessionId}', $this->transaction('update'));
        $controllers->get('/{sessionId}', $this->transaction('info'));
        $controllers->get('/{sessionId}/search', $this->transaction('search'));
    }

    protected function validateRegister($requestData)
    {
        parent::validateRegister($requestData);

        $this->validateRequired($requestData, [
            'site_list'
        ]);

        $this->validateType(
            $requestData,
            [
                'ref_id' => [
                    new Digit(),
                    new Assert\NotBlank()
                ],
                'skype' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                ],
                'icq' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank()
                ],
                'jabber' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank()
                ],
                'site_list' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank()
                ],
            ]
        );

        Validator::validateValue(
            isset($requestData['skype']) || isset($requestData['icq']) || isset($requestData['jabber']),
            new Assert\EqualTo(true),
            'skype or icq or jabber required'
        );
    }

    /**
     * Обновление данных пользователя
     *
     * @param Request $request
     * @param string $sessionId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function update(Request $request, $sessionId)
    {
        $requestData = $request->request->all();

        $this->validateType(
            $requestData,
            [
                'first_name' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank()
                ],
                'second_name' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank()
                ],
                'skype' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank()
                ],
                'jabber' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank()
                ],
                'icq' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank()
                ],
                'email' => [
                    new Assert\Email(),
                    new Assert\NotBlank()
                ],
                'auto_pay' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                    new Assert\Choice(['true', 'false'])
                ],
            ]
        );

        return $this->format(
            $this->getUserService($sessionId)->update($requestData)
        );
    }

    /**
     * Получение данных пользователя
     *
     *
     * @param Request $request
     * @param string $sessionId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function info(Request $request, $sessionId)
    {
        return $this->format(
            $this->getUserService($sessionId)->info()
        );
    }

    /**
     * Получение списка пользователей
     *
     * @param Request $request
     * @param string $sessionId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function search(Request $request, $sessionId)
    {
        $requestData = $request->query->all();

        $this->validateType(
            $requestData,
            [
                'name' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                ],
                'offset' => [
                    new Digit(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                ],
                'limit' => [
                    new Digit(),
                    new Assert\GreaterThan([
                        'value' => 0,
                    ]),
                ],
            ]
        );

        //admin auth
        $this->getAdminUserService($sessionId)->getUser();

        return $this->format(
            $this->getUserService()->search($requestData)
        );
    }
    
    /**
     * Получить сервис для работы с пользователями
     *
     * @param string | null $sessionId
     * @return \Project\Service\TJ\User
     */
    protected function getUserService($sessionId = null)
    {
        return new \Project\Service\TJ\User($sessionId);
    }

    /**
     * Получить сервис для работы с пользователями-админами
     *
     * @param string $sessionId
     * @return AdminUserService
     */
    protected function getAdminUserService($sessionId)
    {
        return new AdminUserService($sessionId);
    }
}
