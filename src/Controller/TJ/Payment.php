<?php

namespace Project\Controller\TJ;

use Project\Check\Validator\Digit;
use Project\Check\Validator\Money;
use Project\Entity\TJUser;
use Project\Controller\TJUserController;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Контроллер работающий с выплатами от TJ
 */
class Payment extends TJUserController
{
    /**
     * Установка роутинга для конкретного контроллера
     *
     * @param ControllerCollection $controllers
     */
    protected function route(ControllerCollection $controllers)
    {
        $controllers->post('/{sessionId}', $this->transaction('create'));
        $controllers->get('/{sessionId}', $this->transaction('search'));
    }

    /**
     * Получить список выплат
     *
     * @param Request $request
     * @param $sessionId
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function search(Request $request, $sessionId)
    {
        $requestData = $request->query->all();

        $this->validateType(
            $requestData,
            [
                'date_from' => [
                    new Assert\Date(),
                ],
                'date_to' => [
                    new Assert\Date(),
                ],
                'offset' => [
                    new Digit(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                ],
                'limit' => [
                    new Digit(),
                    new Assert\GreaterThan([
                        'value' => 0,
                    ]),
                ],
            ]
        );

        /** @var TJUser $user */
        $user = $this->getUserService($sessionId)->getUser();

        return $this->format(
            $this->getPaymentService($user)->search($requestData)
        );
    }

    /**
     * Запрос выплаты
     *
     * @param Request $request
     * @param string $sessionId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(Request $request, $sessionId)
    {
        $requestData = $request->request->all();

        $this->validateRequired($requestData, [
            'wallet_id',
            'amount',
        ]);

        $this->validateType(
            $requestData,
            [
                'wallet_id' => [
                    new Digit(),
                    new Assert\NotEqualTo([
                        'value' => 0,
                    ]),
                ],

                'amount' => [
                    new Money([
                        'integer' => 7,
                        'decimal' => 4,
                    ]),
                    new Assert\NotEqualTo([
                        'value' => 0,
                    ]),
                    new Assert\LessThanOrEqual([
                        'value' => 10000000,
                    ]),
                ],

                'comment' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank()
                ],
            ]
        );
        
        /** @var TJUser $user */
        $user = $this->getUserService($sessionId)->getUser();

        return $this->format(
            $this->getPaymentService($user)->request($requestData)
        );
    }

    /**
     * Получить сервис для работы с выплатами
     *
     * @param TJUser $user
     * @return \Project\Service\Payment
     */
    protected function getPaymentService(TJUser $user)
    {
        return new \Project\Service\Payment($user);
    }
}
