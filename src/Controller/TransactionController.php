<?php

namespace Project\Controller;

use Project\Controller\Exception;
use Project\Proxy;
use Silex\ControllerProviderInterface;

/**
 * Обертка для контроллеров поддерживающих транзакции
 */
abstract class TransactionController implements ControllerProviderInterface
{

    /**
     * Конкретные обработчики логики контроллера
     */
    const TRANSACTION_PROCESS = 'transactionProcess';
    const RAW_PROCESS = 'rawProcess';

    /**
     * Оборачивает вызов метода контроллера в транзакции
     *
     * @param string $method
     * @param array $args
     *
     * @return mixed
     * @throws \Exception
     */
    protected function transactionProcess($method, $args)
    {
        $entityManager = $this->getEntityManager();

        try {
            //стартуем транзакцию
            $entityManager->beginTransaction();
            //получаем результат выполнения контроллера
            $controllerResult = call_user_func_array([$this,$method], $args);
            //коммитим транзакцию
            $entityManager->commit();
        } catch (\Exception $e) {
            //откатываем операции в случае выброса исключения
            $entityManager->rollback();
            throw $e;
        }

        return $controllerResult;
    }

    /**
     * Вызов метода как есть без оборачивания в транзакцию
     *
     * @param $method
     * @param $args
     * @throws \Exception
     * @return mixed
     */
    protected function rawProcess($method, $args)
    {
        return call_user_func_array([$this,$method], $args);
    }

    /**
     * Метод готовит список параметров для анонимной функции
     * в строковом представлении для передачи в Silex
     *
     * @param $method
     *
     * @return string
     */
    protected function prepareParams($method)
    {
        $params = (new \ReflectionMethod(get_class($this), $method))->getParameters();
        $strParams=[];
        foreach ($params as $param) {
            $strParam = '$'.$param->getName();

            if ($param->getClass() !== null) {
                $strParam = '\\'. $param->getClass()->getName() . ' ' . $strParam;
            }

            try {
                $strParam .= ' = '.$param->getDefaultValue();
            } catch (\Exception $e) {
            }

            $strParams[]=$strParam;
        }
        $pps = implode(', ', $strParams);
        return $pps;
    }

    /**
     * Метод готовит код анонимной функции
     * в строковом представлении для передачи в Silex
     *
     * @param $method
     * @param $process
     *
     * @return string
     */
    protected function prepareCode($method, $process)
    {
        return 'return $this->'.$process.'(\''.$method.'\', func_get_args());';
    }

    /**
     * Метод-прокси к методу-процессору transaction
     *
     * @param string $method
     *
     * @throws Exception\CustomCallableCreatingFailed
     * @return callable
     */
    public function transaction($method)
    {
        return $this->prepareCallable($method, self::TRANSACTION_PROCESS);
    }

    /**
     * Метод-прокси к методу-процессору raw
     *
     * @param string $method
     *
     * @throws Exception\CustomCallableCreatingFailed
     * @return callable
     */
    public function raw($method)
    {
        return $this->prepareCallable($method, self::RAW_PROCESS);
    }

    /**
     * Метод возвращает callable для передачи в роутинг Silex
     *
     * @param string $method
     * @param string $process
     *
     * @return callable
     * @throws Exception\CustomCallableCreatingFailed
     */
    protected function prepareCallable($method, $process)
    {
        $fParams=$this->prepareParams($method);
        $fCode=$this->prepareCode($method, $process);

        /**
         * @var \Closure $func
         */

        if (@eval('$func = function('.$fParams.') {'.$fCode.'};')===false || !is_callable($func)) {
            throw new Exception\CustomCallableCreatingFailed(
                "Callable creating failed with: code = "
                . print_r($fCode, true)
                . " params = "
                .print_r($fParams, true)
            );
        }

        return  $func;
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    protected function getEntityManager()
    {
        return Proxy::init()->getEntityManager();
    }
}
