<?php

namespace Project\Controller\Common;

use Project\Controller\Common;
use Project\Entity\Ad;
use Project\Service\ShowContent;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Контроллер отвечающий за отгрузку рекламного контента
 */
class Content extends Common
{
    /** @var  array */
    protected $requestData;

    /**
     * Дополнительные валидаторы
     *
     * @var array
     */
    protected static $validators = [
        Ad::ADTYPE_TEASER => Common\Content\Validator\Teaser::class,
    ];
    
    /**
     * Установка роутинга для конкретного контроллера
     *
     * @param ControllerCollection $controllers
     */
    protected function route(ControllerCollection $controllers)
    {
        $controllers->get('/{legacy}', $this->raw('show'));
    }

    /**
     * @param Request $request
     * @param $legacy
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function show(Request $request, $legacy)
    {
        $this->requestData = $request->query->all();

        $this->validateRequired($this->requestData, [
            'ad_type',
        ]);

        $this->getAdTypeValidator($this->requestData['ad_type'])->validate();

        $result = $this->getShowContentService()->show(
            $this->requestData
        );

        $headers = [];
        if ($this->requestData['ad_type'] == Ad::ADTYPE_SLIDER_BANNER) {
            $headers = [
                'Content-Type' => 'application/javascript'
            ];
        }

        return new Response($result, 200, $headers);
    }

    /**
     * @return ShowContent
     */
    protected function getShowContentService()
    {
        return new ShowContent();
    }

    /**
     * @param $adType
     * @return Content\Validator
     */
    protected function getAdTypeValidator($adType)
    {
        if (isset(self::$validators[$adType])) {
            return new self::$validators[$adType]($this);
        }
        
        return new Common\Content\Validator($this);
    }
    
    /**
     * @return array
     */
    public function getRequestData()
    {
        return $this->requestData;
    }
}
