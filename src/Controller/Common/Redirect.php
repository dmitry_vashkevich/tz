<?php

namespace Project\Controller\Common;

use Project\Ad\Action;
use Project\Cache\EntityCache\TJBlock;
use Project\Check\Validator;
use Project\Controller\Common;
use Project\Entity\Ad;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Контроллер отвечающий за редирект на урл кампании
 */
class Redirect extends Common
{
    /**
     * Установка роутинга для конкретного контроллера
     *
     * @param ControllerCollection $controllers
     */
    protected function route(ControllerCollection $controllers)
    {
        $controllers->get('/{redirectData}', $this->raw('redirect'));
    }

    /**
     * @param Request $request
     * @param $redirectData
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function redirect(Request $request, $redirectData)
    {
        $redirectData = json_decode(base64_decode($redirectData), true);

        Validator::validateValue(
            $redirectData,
            new Assert\NotNull(),
            'broken params'
        );

        $this->validateRequired($redirectData, [
            'url',
            'statData',
        ]);

        $this->validateType(
            $redirectData,
            [
                'url' => [
                    new Assert\Url(),
                ],
                'statData' => [
                    new Assert\NotNull(),
                    new Assert\Type('array')
                ]
            ]
        );

        $this->validateRequired($redirectData['statData'], [
            'adtype',
        ]);
        
        /** @var Action\Redirect $action */
        $action = Action::getAction(
            Action::TYPE_REDIRECT,
            $redirectData['statData']['adtype'],
            $redirectData['statData']
        );

        $blockData = null;
        if (isset($redirectData['statData']['tj_block_id'])) {
            $blockData = (new TJBlock())->get($redirectData['statData']['tj_block_id']);
        }
        
        $action->beforeRedirect($blockData);

        $cashback = '';

        if (isset($redirectData['referer']) && $redirectData['statData']['adtype'] == Ad::ADTYPE_CASHBACK) {
            $cashback = $redirectData['referer'];
        }

        header("Location: http://trafffmix.ru/#" . $redirectData['url'] . '?s1=' .
            base64_encode(json_encode($redirectData['statData'])) . '&s6='. urlencode($cashback)
        );

        fastcgi_finish_request();

        $action->afterRedirect($blockData);

        return new Response('');
    }
}
