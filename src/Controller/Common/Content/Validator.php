<?php

namespace Project\Controller\Common\Content;

use Project\Check\Validator\Digit;
use Project\Controller\Common\Content;
use Project\Entity\Ad;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Валидатор Content контроллера
 */
class Validator
{
    /** @var Content  */
    protected $controller;

    /**
     * Конструктор
     * @param Content $controller
     */
    public function __construct(Content $controller)
    {
        $this->controller = $controller;
    }

    /**
     * Валидация
     */
    public function validate()
    {
        $requestData = $this->controller->getRequestData(); 
        $this->controller->validateRequired($requestData, [
            'ad_type',
        ]);

        $this->controller->validateType(
            $requestData,
            [
                'ad_type' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                    new Assert\Choice(Ad::$adtypes)
                ],
                'block_id' => [
                    new Digit(),
                    new Assert\GreaterThan([
                        'value' => 0,
                    ]),
                ],
                'net_id' => [
                    new Digit(),
                ],
            ]
        );
    }
}