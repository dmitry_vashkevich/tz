<?php

namespace Project\Controller\Common\Content\Validator;

use Project\Check\Validator\Digit;
use Project\Controller\Common\Content\Validator;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Расширение валидации контроллера для тизеров
 */
class Teaser extends Validator
{
    /**
     * @inheritdoc
     */
    public function validate()
    {
        parent::validate();

        $requestData = $this->controller->getRequestData();
        if (isset($requestData['block_id'])) {
            return;
        }
        
        /** LEGACY SUPPORT */
        
        $this->controller->validateRequired($requestData, [
            'width',
            'height',
        ]);

        $this->controller->validateType(
            $requestData,
            [
                'width' => [
                    new Digit(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                ],
                'height' => [
                    new Digit(),
                    new Assert\GreaterThan([
                        'value' => 0,
                    ]),
                ],
            ]
        );
    }
}
