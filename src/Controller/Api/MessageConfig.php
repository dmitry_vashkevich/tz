<?php

namespace Project\Controller\Api;

use Project\Controller\Api;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Контроллер работающий с конфигами для месседжей
 */
class MessageConfig extends Api
{
    /**
     * Установка роутинга для конкретного контроллера
     *
     * @param ControllerCollection $controllers
     */
    protected function route(ControllerCollection $controllers)
    {
        $controllers->post('/', $this->transaction('create'));
        $controllers->get('/', $this->transaction('search'));;
        $controllers->put('/{configId}', $this->transaction('putActive'));
    }

    /**
     * @param Request $request
     * @param $configId
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @internal param $tconfId
     */
    public function putActive(Request $request, $configId)
    {
        $requestData = $request->request->all();

        $this->validateRequired($requestData, [
            'active'
        ]);

        $this->validateType(
            $requestData,
            [
                'active' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                    new Assert\Choice(['true', 'false'])
                ],
            ]
        );

        return $this->format(
            $this->getMessageConfigService()->update($configId, $requestData)
        );
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function search(Request $request)
    {
        $requestData = $request->query->all();

        return $this->format(
            $this->getMessageConfigService()->search($requestData)
        );
    }

    /**
     * Создание конфига
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(Request $request)
    {
        $requestData = $request->request->all();

        $this->validateRequired($requestData, [
            'data',
        ]);

        $this->validateType(
            $requestData,
            [
                'data' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank()
                ],
            ]
        );

        return $this->format(
            $this->getMessageConfigService()->create($requestData)
        );
    }


    /**
     * Получить сервис для работы с конфигами тизеров
     *
     * @return \Project\Service\MessageConfig
     */
    protected function getMessageConfigService()
    {
        return new \Project\Service\MessageConfig();
    }
}
