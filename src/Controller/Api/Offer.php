<?php

namespace Project\Controller\Api;

use Project\Controller\Api;
use Project\Entity\User as EUser;
use Project\Service\SiteUser\Offer as OfferService;
use Silex\ControllerCollection;
use Project\Service\SiteUser as SiteUserService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Контроллер работающий с офферами
 */
class Offer extends Api
{
    /**
     * Установка роутинга для конкретного контроллера
     *
     * @param ControllerCollection $controllers
     */
    protected function route(ControllerCollection $controllers)
    {
        $controllers->get('/{sessionId}', $this->transaction('search'));
    }

    /**
     * Получить список офферов
     *
     * @param Request $request
     * @param $sessionId
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function search(Request $request, $sessionId)
    {
        $requestData = $request->query->all();

        $this->validateType(
            $requestData,
            [
                'name' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                ],
            ]
        );
        
        $user = $this->getUserService($sessionId)->getUser();

        return $this->format(
            $this->getOfferService($user)->search($requestData)
        );
    }

    /**
     * Получить сервис для работы с офферами
     *
     * @param EUser $user
     * @return OfferService
     */
    protected function getOfferService(EUser $user)
    {
        return new OfferService($user);
    }

    /**
     * Получить сервис для работы с пользователями
     *
     * @param string $sessionId
     * @return SiteUserService
     */
    protected function getUserService($sessionId)
    {
        return new SiteUserService($sessionId);
    }
}
