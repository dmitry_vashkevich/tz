<?php

namespace Project\Controller\Api;

use Project\Controller\Api;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Контроллер работающий с админ-юзером
 *
 * - авторизация
 * - регистрация
 * - логаут
 * - смена пароля
 */
class AdminUser extends User
{
    /**
     * Получить сервис для работы с пользователями
     *
     * @param string | null $sessionId
     * @return \Project\Service\AdminUser
     */
    protected function getUserService($sessionId = null)
    {
        return new \Project\Service\AdminUser($sessionId);
    }
}
