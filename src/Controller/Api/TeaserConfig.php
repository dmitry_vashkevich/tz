<?php

namespace Project\Controller\Api;

use Project\Check\Validator\Digit;
use Project\Controller\Api;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Project\Service\SiteUser as SiteUserService;

/**
 * Контроллер работающий с конфигами для тизеров
 */
class TeaserConfig extends Api
{
    /**
     * Установка роутинга для конкретного контроллера
     *
     * @param ControllerCollection $controllers
     */
    protected function route(ControllerCollection $controllers)
    {
        $controllers->post('/{sessionId}', $this->transaction('create'));
        $controllers->get('/{sessionId}', $this->transaction('search'));
        $controllers->get('/{sessionId}/block', $this->transaction('searchBlock'));
        $controllers->post('/{sessionId}/{configId}', $this->transaction('putActive'));
    }

    /**
     * @param Request $request
     * @param $sessionId
     * @param $configId
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function putActive(Request $request, $sessionId, $configId)
    {
        $requestData = $request->request->all();

        $this->validateRequired($requestData, [
            'active'
        ]);

        $this->validateType(
            $requestData,
            [
                'active' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                    new Assert\Choice(['true', 'false'])
                ],
            ]
        );

        //user auth
        $this->getUserService($sessionId);
        
        $tConf = \Project\Entity\TeaserConfig::prepareById($configId);
        $tConf
            ->setActive($requestData['active'] == 'true' ? true : false)
            ->save();

        return $this->format(
            ['result' => true]
        );
    }

    /**
     * @param Request $request
     * @param $sessionId
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function search(Request $request, $sessionId)
    {
        $requestData = $request->query->all();

        $this->validateType(
            $requestData,
            [
                'b_width_from' => [
                    new Digit(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                ],
                'b_width_to' => [
                    new Digit(),
                    new Assert\GreaterThan([
                        'value' => 0,
                    ]),
                ],
                'b_height_from' => [
                    new Digit(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                ],
                'b_height_to' => [
                    new Digit(),
                    new Assert\GreaterThan([
                        'value' => 0,
                    ]),
                ],
            ]
        );

        //user auth
        $this->getUserService($sessionId);
        
        return $this->format(
            $this->getTeaserConfigService()->search($requestData)
        );
    }

    /**
     * Поиск конфигов под размер блока
     *
     * @param Request $request
     * @param $sessionId
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function searchBlock(Request $request, $sessionId)
    {
        $requestData = $request->query->all();

        $this->validateRequired($requestData, [
            'width',
            'height',
        ]);

        $this->validateType(
            $requestData,
            [
                'width' => [
                    new Digit(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                ],
                'height' => [
                    new Digit(),
                    new Assert\GreaterThan([
                        'value' => 0,
                    ]),
                ],
            ]
        );

        //user auth
        $this->getUserService($sessionId);
        
        return $this->format(
            $this->getTeaserConfigService()->searchByBlock($requestData)
        );
    }

    /**
     * Создание конфига
     *
     * @param Request $request
     * @param $sessionId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(Request $request, $sessionId)
    {
        $requestData = $request->request->all();

        $this->validateRequired($requestData, [
            'data',
            'b_width_from',
            'b_width_to',
            'b_height_from',
            'b_height_to',
        ]);

        $this->validateType(
            $requestData,
            [
                'data' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank()
                ],

                'b_width_from' => [
                    new Digit(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                ],
                'b_width_to' => [
                    new Digit(),
                    new Assert\GreaterThan([
                        'value' => 0,
                    ]),
                ],
                'b_height_from' => [
                    new Digit(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                ],
                'b_height_to' => [
                    new Digit(),
                    new Assert\GreaterThan([
                        'value' => 0,
                    ]),
                ],
            ]
        );

        //user auth
        $this->getUserService($sessionId);
        
        return $this->format(
            $this->getTeaserConfigService()->create($requestData)
        );
    }


    /**
     * Получить сервис для работы с конфигами тизеров
     *
     * @return \Project\Service\TeaserConfig
     */
    protected function getTeaserConfigService()
    {
        return new \Project\Service\TeaserConfig();
    }

    /**
     * Получить сервис для работы с пользователями
     *
     * @param string $sessionId
     * @return SiteUserService
     */
    protected function getUserService($sessionId)
    {
        return new SiteUserService($sessionId);
    }
}
