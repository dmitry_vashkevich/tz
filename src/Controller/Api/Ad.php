<?php

namespace Project\Controller\Api;

use Doctrine\Common\Collections\Criteria;
use Project\Ad\Search;
use Project\Check\Validator\Digit;
use Project\Controller\Api;
use Project\Service\SiteUser as SiteUserService;
use Project\Service\Ad as AdService;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Project\Entity\User as UserEntity;


/**
 * Контроллер работающий с рекламными материалам
 */
class Ad extends Api
{
    /**
     * Установка роутинга для конкретного контроллера
     *
     * @param ControllerCollection $controllers
     */
    protected function route(ControllerCollection $controllers)
    {
        $controllers->get('/{sessionId}/type', $this->raw('types'));
        $controllers->get('/{sessionId}', $this->raw('search'));
    }

    /**
     * Получить список кампаний
     *
     * @param Request $request
     * @param $sessionId
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function search(Request $request, $sessionId)
    {
        $requestData = $request->query->all();

        $this->validateRequired($requestData, [
            'campaign_id'
        ]);

        $this->validateType(
            $requestData,
            [
                'campaign_id' => [
                    new Digit(),
                    new Assert\GreaterThan([
                        'value' => 0,
                    ]),
                ],
                'order' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                    new Assert\Choice(Search::$orders)
                ],
                'direction' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                    new Assert\Choice([Criteria::ASC, Criteria::DESC])
                ],
            ]
        );

        $user = $this->getUserService($sessionId)->getUser();

        return $this->format(
            $this->getCampaignService($user)->search($requestData)
        );
    }

    /**
     * Получить список типов
     *
     * @param Request $request
     * @param $sessionId
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function types(Request $request, $sessionId)
    {
        $user = $this->getUserService($sessionId)->getUser();

        return $this->format(
            \Project\Entity\Ad::$adtypes
        );
    }

    /**
     * Получить сервис для работы с пользователями
     *
     * @param string $sessionId
     * @return SiteUserService
     */
    protected function getUserService($sessionId)
    {
        return new SiteUserService($sessionId);
    }

    /**
     * Получить сервис для работы с объявлениями
     *
     * @param UserEntity $user
     * @return AdService
     */
    protected function getCampaignService(UserEntity $user)
    {
        return new AdService($user);
    }
}
