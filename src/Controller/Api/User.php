<?php

namespace Project\Controller\Api;

use Project\Controller\Api;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Контроллер работающий с пользователем
 *
 * - авторизация
 * - регистрация
 * - логаут
 * - смена пароля 
 */
abstract class User extends Api
{
    /**
     * Установка роутинга для конкретного контроллера
     *
     * @param ControllerCollection $controllers
     */
    protected function route(ControllerCollection $controllers)
    {
        $controllers->post('/register', $this->transaction('register'));
        $controllers->post('/auth', $this->transaction('auth'));
        $controllers->put('/{sessionId}/logout', $this->transaction('logout'));
        $controllers->post('/{sessionId}/password', $this->transaction('password'));
    }

    /**
     * Регистрация
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function register(Request $request)
    {
        $requestData = $request->request->all();
        
        $this->validateRegister($requestData);

        return $this->format(
            $this->getUserService()->register($requestData)
        );
    }

    /**
     * @param $requestData
     */
    protected function validateRegister($requestData)
    {
        $this->validateRequired($requestData, [
            'login',
            'password',
            'confirm'
        ]);

        $this->validateType(
            $requestData,
            [
                'login' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank()
                ],
                'password' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank()
                ],
                'confirm' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                    new Assert\EqualTo($requestData['password'])
                ],
            ]
        );
    }
    
    /**
     * Авторизация
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function auth(Request $request)
    {
        $requestData = $request->request->all();

        $this->validateRequired($requestData, [
            'login',
            'password'
        ]);

        $this->validateType(
            $requestData,
            [
                'login' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank()
                ],
                'password' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank()
                ],
            ]
        );

        return $this->format(
            $this->getUserService()->auth($requestData)
        );
    }

    /**
     * Логаут
     *
     * @param Request $request
     * @param $sessionId
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function logout(Request $request, $sessionId)
    {
        return $this->format(
            $this->getUserService($sessionId)->logout()
        );
    }

    /**
     * Смена пароля
     *
     * @param Request $request
     * @param string $sessionId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function password(Request $request, $sessionId)
    {
        $requestData = $request->request->all();

        $this->validateRequired($requestData, [
            'old_password',
            'password',
            'confirm'
        ]);

        $this->validateType(
            $requestData,
            [
                'old_password' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank()
                ],
                'password' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank()
                ],
                'confirm' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                    new Assert\EqualTo($requestData['password'])
                ],
            ]
        );

        return $this->format(
            $this->getUserService($sessionId)->changePassword($requestData)
        );
    }

    /**
     * Получить сервис для работы с пользователями
     *
     * @param string | null $sessionId
     *
     * @return \Project\Service\User
     */
    abstract protected function getUserService($sessionId = null);
}
