<?php

namespace Project\Controller\Api;

use Project\Controller\Api;
use Project\Service\SiteUser as SiteUserService;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Контроллер работающий с прелендами
 */
class Preland extends Api
{
    /**
     * Установка роутинга для конкретного контроллера
     *
     * @param ControllerCollection $controllers
     */
    protected function route(ControllerCollection $controllers)
    {
        $controllers->get('/{sessionId}', $this->raw('search'));
    }


    /**
     * Получить список типов
     *
     * @param Request $request
     * @param $sessionId
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function search(Request $request, $sessionId)
    {
        $user = $this->getUserService($sessionId)->getUser();

        $prelands = \Project\Entity\Preland::prepareList();

        $result = [];

        /** @var \Project\Entity\Preland $land */
        foreach ($prelands as $preland) {
            if ($preland->isActive()) {
                $result[] = [
                    'id' => $preland->getId(),
                    'name' => $preland->getName(),
                    'url' => $preland->getUrl()
                ];
            }
        }

        return $this->format(
            $result
        );
    }

    /**
     * Получить сервис для работы с пользователями
     *
     * @param string $sessionId
     * @return SiteUserService
     */
    protected function getUserService($sessionId)
    {
        return new SiteUserService($sessionId);
    }
}
