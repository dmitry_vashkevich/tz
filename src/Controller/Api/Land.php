<?php

namespace Project\Controller\Api;

use Doctrine\Common\Collections\Criteria;
use Project\Check\Validator\Digit;
use Project\Check\Validator\JsonIdList;
use Project\Controller\Api;
use Project\Land\Search;
use Project\Service\SiteUser as SiteUserService;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Project\Service\SiteUser\Land as LandService;
use Project\Entity\User as UserEntity;

/**
 * Контроллер работающий с лендами
 */
class Land extends Api
{
    /**
     * Установка роутинга для конкретного контроллера
     *
     * @param ControllerCollection $controllers
     */
    protected function route(ControllerCollection $controllers)
    {
        $controllers->get('/{sessionId}', $this->raw('search'));
    }


    /**
     * Получить список типов
     *
     * @param Request $request
     * @param $sessionId
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function search(Request $request, $sessionId)
    {
        $requestData = $request->query->all();

        $this->validateType(
            $requestData,
            [
                'land_ids' => [
                    new JsonIdList()
                ],
                'offer_id' => [
                    new Digit(),
                    new Assert\GreaterThan([
                        'value' => 0,
                    ]),
                ],
                'order' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                    new Assert\Choice(Search::$orders)
                ],
                'direction' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                    new Assert\Choice([Criteria::ASC, Criteria::DESC])
                ],
            ]
        );

        $user = $this->getUserService($sessionId)->getUser();

        return $this->format(
            $this->getLandService($user)->search($requestData)
        );
    }

    /**
     * Получить сервис для работы с пользователями
     *
     * @param string $sessionId
     * @return SiteUserService
     */
    protected function getUserService($sessionId)
    {
        return new SiteUserService($sessionId);
    }
    
    /**
     * Получить сервис для работы с кампаниями
     *
     * @param UserEntity $user
     * @return LandService
     */
    protected function getLandService(UserEntity $user)
    {
        return new LandService($user);
    }
}
