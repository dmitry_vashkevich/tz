<?php

namespace Project\Controller\Api;

use Project\Controller\Api;
use Project\Service\Admin\User as AdminUserService;
use Silex\ControllerCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\Request;
use Project\Check\Validator\Digit;

/**
 * Контроллер работающий с сайт-юзером
 */
class SiteUser extends User
{
    /**
     * Установка роутинга для конкретного контроллера
     *
     * @param ControllerCollection $controllers
     */
    protected function route(ControllerCollection $controllers)
    {
        parent::route($controllers);
        $controllers->get('/{sessionId}', $this->transaction('info'));
        $controllers->get('/{sessionId}/search', $this->transaction('search'));
    }

    /**
     * Получение данных пользователя
     *
     *
     * @param Request $request
     * @param string $sessionId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function info(Request $request, $sessionId)
    {
        return $this->format(
            $this->getUserService($sessionId)->info()
        );
    }

    /**
     * Получение списка пользователей
     *
     * @param Request $request
     * @param string $sessionId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function search(Request $request, $sessionId)
    {
        $requestData = $request->query->all();

        $this->validateType(
            $requestData,
            [
                'name' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                ],
                'offset' => [
                    new Digit(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                ],
                'limit' => [
                    new Digit(),
                    new Assert\GreaterThan([
                        'value' => 0,
                    ]),
                ],
            ]
        );
        
        //admin auth
        $this->getAdminUserService($sessionId)->getUser();
        
        return $this->format(
            $this->getUserService()->search($requestData)
        );
    }

    /**
     * Получить сервис для работы с пользователями
     *
     * @param string | null $sessionId
     * @return \Project\Service\SiteUser
     */
    protected function getUserService($sessionId = null)
    {
        return new \Project\Service\SiteUser($sessionId);
    }

    /**
     * Получить сервис для работы с пользователями-админами
     *
     * @param string $sessionId
     * @return AdminUserService
     */
    protected function getAdminUserService($sessionId)
    {
        return new AdminUserService($sessionId);
    }
}
