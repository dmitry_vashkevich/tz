<?php

namespace Project\Controller\Api;

use Project\Check\Validator\Digit;
use Project\Controller\Api;
use Project\Entity\Ad;
use Project\Entity\User as EntityUser;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Project\Service\SiteUser as SiteUserService;
use Project\Service\Stats as StatsService;

/**
 * Контроллер пользовательской статистики
 */
class Stats extends Api
{
    /**
     * Установка роутинга для конкретного контроллера
     *
     * @param ControllerCollection $controllers
     */
    protected function route(ControllerCollection $controllers)
    {
        $controllers->get('/{sessionId}', $this->transaction('search'));
    }

    /**
     * Получить статистику
     *
     * @param Request $request
     * @param $sessionId
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function search(Request $request, $sessionId)
    {
        $requestData = $request->query->all();

        $this->validateRequired($requestData, [
            'key',
        ]);

        $orders = [
            "rs",
            "rbq",
            "rbqu",
            "usd",
            "scv",
            "tcv",
            "prelv",
            "lead",
            "lead_amount",
            "accept",
            "accept_amount",
            "decline",
            "decline_amount",
            "invalid",
            "invalid_amount",
            "cpm",
            "ctr",
        ];

        if ($requestData['key'] == 'date') {
            $orders[] = 'date';
        }

        if (isset($requestData['order']) && !in_array($requestData['order'], $orders)) {
            unset($requestData['order']);
        }

        $this->validateType(
            $requestData,
            [
                'key' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank()
                ],
                'offer_id' => [
                    new Digit(),
                    new Assert\NotEqualTo([
                        'value' => 0,
                    ]),
                ],
                'campaign_id' => [
                    new Digit(),
                    new Assert\NotEqualTo([
                        'value' => 0,
                    ]),
                ],
                'date_from' => [
                    new Assert\Date(),
                ],
                'date_to' => [
                    new Assert\Date(),
                ],
                'order' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                    new Assert\Choice($orders)
                ],
                'ad_type' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                    new Assert\Choice(Ad::$adtypes)
                ],
                'direction' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                    new Assert\Choice(['asc', 'desc'])
                ],
                'offset' => [
                    new Digit(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                ],
                'limit' => [
                    new Digit(),
                    new Assert\GreaterThan([
                        'value' => 0,
                    ]),
                ],
                'width_from' => [
                    new Digit(),
                    new Assert\GreaterThan([
                        'value' => 0,
                    ]),
                ],
                'height_from' => [
                    new Digit(),
                    new Assert\GreaterThan([
                        'value' => 0,
                    ]),
                ],
                'width_to' => [
                    new Digit(),
                    new Assert\GreaterThan([
                        'value' => 0,
                    ]),
                ],
                'height_to' => [
                    new Digit(),
                    new Assert\GreaterThan([
                        'value' => 0,
                    ]),
                ],
                'domain' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank()
                ],
                'tj_block_id' => [
                    new Digit(),
                    new Assert\NotEqualTo([
                        'value' => 0,
                    ]),
                ],
                'tj_site_id' => [
                    new Digit(),
                    new Assert\NotEqualTo([
                        'value' => 0,
                    ]),
                ],
                'tj_user_id' => [
                    new Digit(),
                    new Assert\NotEqualTo([
                        'value' => 0,
                    ]),
                ],
                'site_user_id' => [
                    new Assert\NotEqualTo([
                        'value' => 0,
                    ]),
                ],
                'refs' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                ],
                'browser' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank()
                ],
                'platform' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank()
                ],
            ]
        );

        $user = $this->getUserService($sessionId)->getUser();

        return $this->format(
            $this->getStatsService($user)->search($requestData)
        );
    }

    /**
     * Получить сервис для работы со статистикой
     *
     * @param EntityUser $user
     * @return StatsService
     */
    protected function getStatsService(EntityUser $user)
    {
        return new StatsService($user);
    }

    /**
     * Получить сервис для работы с пользователями
     *
     * @param string $sessionId
     * @return SiteUserService
     */
    protected function getUserService($sessionId)
    {
        return new SiteUserService($sessionId);
    }
}
