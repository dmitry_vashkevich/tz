<?php

namespace Project\Controller\Api;

use Doctrine\Common\Collections\Criteria;
use Project\Campaign\Search;
use Project\Check\Validator\Digit;
use Project\Controller\Api;
use Project\Entity\Ad;
use Project\Entity\User as UserEntity;
use Project\Service\SiteUser as SiteUserService;
use Project\Service\Campaign as CampaignService;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Контроллер работающий с кампаниями
 */
class Campaign extends Api
{
    /**
     * Установка роутинга для конкретного контроллера
     *
     * @param ControllerCollection $controllers
     */
    protected function route(ControllerCollection $controllers)
    {
        $controllers->post('/{sessionId}', $this->transaction('create'));
        $controllers->get('/{sessionId}', $this->raw('search'));
        $controllers->put('/{sessionId}/{campaignId}/flow', $this->transaction('putFlow'));
        $controllers->put('/{sessionId}/{campaignId}/targeting', $this->transaction('putTargeting'));
        $controllers->put('/{sessionId}/{campaignId}/active', $this->transaction('putActive'));
        $controllers->post('/{sessionId}/{campaignId}/content', $this->transaction('putContent'));
        $controllers->put('/{sessionId}/{campaignId}', $this->transaction('update'));
    }

    /**
     * Добавление контента в кампанию
     *
     * @param Request $request
     * @param $sessionId
     * @param $campaignId
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function putContent(Request $request, $sessionId, $campaignId)
    {
        $requestData = $request->request->all();

        $files = $request->files->all();

        /** @var UploadedFile $file */
        foreach ($files as $file) {
            $this->validateType(
                ['mimeType' => $file->getMimeType()],
                [
                    'mimeType' => [
                        new Assert\Type([
                            'type' => 'string'
                        ]),
                        new Assert\NotBlank(),
                        new Assert\Choice(['image/jpeg', 'image/gif', 'image/png'])
                    ],
                ]
            );
        }

        $this->validateRequired($requestData, [
            'data'
        ]);

        $this->validateType(
            $requestData,
            [
                'data' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                ],
            ]
        );

        $user = $this->getUserService($sessionId)->getUser();

        return $this->format(
            $this->getCampaignService($user)->putContent($campaignId, $requestData['data'], $files)
        );
    }

    /**
     * Вкл / выкл кампании
     *
     * @param Request $request
     * @param $sessionId
     * @param $campaignId
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function putActive(Request $request, $sessionId, $campaignId)
    {
        $requestData = $request->request->all();

        $this->validateRequired($requestData, [
            'active'
        ]);

        $this->validateType(
            $requestData,
            [
                'active' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                    new Assert\Choice(['true', 'false'])
                ],
            ]
        );

        $user = $this->getUserService($sessionId)->getUser();

        return $this->format(
            $this->getCampaignService($user)->putActive($campaignId, $requestData['active'])
        );
    }

    /**
     * Задать таргетинг
     *
     * @param Request $request
     * @param $sessionId
     * @param $campaignId
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function putTargeting(Request $request, $sessionId, $campaignId)
    {
        $requestData = $request->request->all();

        $this->validateType(
            $requestData,
            [
                'countries' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                ],
                'browsers' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                ],
                'platforms' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                ],
            ]
        );

        $user = $this->getUserService($sessionId)->getUser();

        return $this->format(
            $this->getCampaignService($user)->putTargeting($campaignId, $requestData)
        );
    }

    /**
     * Задать ленды и преленды кампании
     *
     * @param Request $request
     * @param $sessionId
     * @param $campaignId
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function putFlow(Request $request, $sessionId, $campaignId)
    {
        $requestData = $request->request->all();

        $this->validateRequired($requestData, [
            'flow'
        ]);

        $user = $this->getUserService($sessionId)->getUser();

        return $this->format(
            $this->getCampaignService($user)->putFlow($campaignId, $requestData['flow'])
        );
    }

    /**
     * Получить список кампаний
     *
     * @param Request $request
     * @param $sessionId
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function search(Request $request, $sessionId)
    {
        $requestData = $request->query->all();

        $this->validateType(
            $requestData,
            [
                'campaign_id' => [
                    new Digit(),
                    new Assert\GreaterThan([
                        'value' => 0,
                    ]),
                ],
                'offer_id' => [
                    new Digit(),
                    new Assert\GreaterThan([
                        'value' => 0,
                    ]),
                ],
                'site_user_id' => [
                    new Assert\NotEqualTo([
                        'value' => 0,
                    ]),
                ],
                'ad_type' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                    new Assert\Choice(Ad::$adtypes)
                ],
                'name' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                ],
                'active' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                    new Assert\Choice(['true', 'false'])
                ],
                'archive' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                    new Assert\Choice(['true', 'false'])
                ],
                'external' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                    new Assert\Choice(['true', 'false'])
                ],
                'order' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                    new Assert\Choice(Search::$orders)
                ],
                'direction' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                    new Assert\Choice([Criteria::ASC, Criteria::DESC])
                ],
                'offset' => [
                    new Digit(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                ],
                'limit' => [
                    new Digit(),
                    new Assert\GreaterThan([
                        'value' => 0,
                    ]),
                ],
            ]
        );

        $user = $this->getUserService($sessionId)->getUser();

        return $this->format(
            $this->getCampaignService($user)->search($requestData)
        );
    }

    /**
     * Обновление кампании
     *
     * @param Request $request
     * @param string $sessionId
     * @param $campaignId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function update(Request $request, $sessionId, $campaignId)
    {
        $requestData = $request->request->all();

        $this->validateType(
            $requestData,
            [
                'name' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                ],
                'offer_id' => [
                    new Digit(),
                    new Assert\GreaterThan([
                        'value' => 0,
                    ]),
                ],
                'cpm' => [
                    new Digit(),
                    new Assert\GreaterThan([
                        'value' => 0,
                    ]),
                ],
                'url' => [
                    new Assert\Url(),
                ],
                'limit_user_show' => [
                    new Digit(),
                    new Assert\GreaterThan([
                        'value' => 0,
                    ]),
                ],
                'limit_amount' => [
                    new Digit(),
                    new Assert\GreaterThan([
                        'value' => 0,
                    ]),
                ],
                'script' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                ],
                'b_width_from' => [
                    new Digit(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                ],
                'b_width_to' => [
                    new Digit(),
                    new Assert\GreaterThan([
                        'value' => 0,
                    ]),
                ],
                'b_height_from' => [
                    new Digit(),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                ],
                'b_height_to' => [
                    new Digit(),
                    new Assert\GreaterThan([
                        'value' => 0,
                    ]),
                ],
                'archive' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                    new Assert\Choice(['true', 'false'])
                ],
            ]
        );

        $user = $this->getUserService($sessionId)->getUser();

        return $this->format(
            $this->getCampaignService($user)->update($requestData, $campaignId)
        );
    }
    
    /**
     * Создание кампании
     *
     * @param Request $request
     * @param string $sessionId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(Request $request, $sessionId)
    {
        $requestData = $request->request->all();

        $this->validateRequired($requestData, [
            'ad_type',
            'name'
        ]);

        $this->validateType(
            $requestData,
            [
                'name' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                ],
                'ad_type' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                    new Assert\Choice(Ad::$adtypes)
                ],
                'offer_id' => [
                    new Digit(),
                    new Assert\GreaterThan([
                        'value' => 0,
                    ]),
                ],
                'cpm' => [
                    new Digit(),
                    new Assert\GreaterThan([
                        'value' => 0,
                    ]),
                ],
                'url' => [
                    new Assert\Url(),
                ],
                'limit_user_show' => [
                    new Digit(),
                    new Assert\GreaterThan([
                        'value' => 0,
                    ]),
                ],
                'limit_amount' => [
                    new Digit(),
                    new Assert\GreaterThan([
                        'value' => 0,
                    ]),
                ],
                'script' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                ],
                'archive' => [
                    new Assert\Type([
                        'type' => 'string'
                    ]),
                    new Assert\NotBlank(),
                    new Assert\Choice(['true', 'false'])
                ],
            ]
        );

        $this->validateByAdType($requestData);

        $user = $this->getUserService($sessionId)->getUser();

        return $this->format(
            $this->getCampaignService($user)->create($requestData)
        );
    }

    /**
     * Получить сервис для работы с кампаниями
     *
     * @param UserEntity $user
     * @return CampaignService
     */
    protected function getCampaignService(UserEntity $user)
    {
        return new CampaignService($user);
    }

    /**
     * Получить сервис для работы с пользователями
     *
     * @param string $sessionId
     * @return SiteUserService
     */
    protected function getUserService($sessionId)
    {
        return new SiteUserService($sessionId);
    }

    /**
     * @param $requestData
     */
    protected function validateByAdType($requestData)
    {
        if ($requestData['ad_type'] == Ad::ADTYPE_BANNER) {
            $this->validateRequired($requestData, [
                'b_width_from',
                'b_width_to',
                'b_height_from',
                'b_height_to',
            ]);

            $this->validateType(
                $requestData,
                [
                    'b_width_from' => [
                        new Digit(),
                        new Assert\GreaterThanOrEqual([
                            'value' => 0,
                        ]),
                    ],
                    'b_width_to' => [
                        new Digit(),
                        new Assert\GreaterThan([
                            'value' => 0,
                        ]),
                    ],
                    'b_height_from' => [
                        new Digit(),
                        new Assert\GreaterThanOrEqual([
                            'value' => 0,
                        ]),
                    ],
                    'b_height_to' => [
                        new Digit(),
                        new Assert\GreaterThan([
                            'value' => 0,
                        ]),
                    ],
                ]
            );
        }
    }
}
