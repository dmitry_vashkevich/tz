<?php

namespace Project\Controller;

use Project\Proxy;
use Silex\Application;
use Silex\ControllerCollection;
use Project\Check\Exception\ValidationException;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Абстрактный контроллер
 */
abstract class Common extends TransactionController
{

    /**
     * Установка роутинга для конкретного контроллера
     *
     * @param ControllerCollection $controllers
     */
    abstract protected function route(ControllerCollection $controllers);

    /**
     * Инициализация контроллера
     *
     * @param Application $app
     *
     * @return ControllerCollection
     */
    public function connect(Application $app)
    {
        $controllers = Proxy::init()->getControllers();

        $this->route($controllers);

        return $controllers;
    }

    /**
     * Получить Application-объект
     *
     * @return Application
     */
    protected function getApp()
    {
        return Proxy::init()->getApp();
    }

    /**
     * Проверка обязательных для заполнения полей
     *
     * @param array $data
     * @param array $required
     *
     * @throws HttpException
     */
    public function validateRequired(array $data, array $required)
    {
        $fields = [];

        foreach ($required as $field) {
            $fields[$field] = new Assert\NotNull();
        }

        $validator = new Assert\Collection([
            'fields' => $fields,
            'allowExtraFields' => true,
        ]);

        $this->validate($data, $validator);
    }

    /**
     * Проверка что переданные параметры соответствуют определенному типу
     *
     * @param array $data
     * @param array $types
     *
     * @throws HttpException
     */
    public function validateType(array $data, array $types)
    {
        $validator = new Assert\Collection([
            'fields' => $types,
            'allowMissingFields' => true,
            'allowExtraFields' => true,
        ]);

        $this->validate($data, $validator);
    }

    /**
     * Валидация в соответствии с Symfony-валидаторами
     *
     * @param $data
     * @param $validator
     *
     * @throws \Project\Check\Exception\ValidationException
     */
    private function validate($data, $validator)
    {
        $errors = Proxy::init()->getValidator()->validateValue($data, $validator);

        if (count($errors) !== 0) {
            throw (new ValidationException(
                $errors[0]->getPropertyPath() . ': ' .
                $errors[0]->getMessage()
            ));
        }
    }
}