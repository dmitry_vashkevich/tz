<?php

namespace Project\Tracker;

use Project\Proxy;
use Project\Http;
use Project\Tracker\Client\Exception\TrackerClientException;

/**
 * Клиент трекера
 */
class Client
{
    /**
     * Хост
     *
     * @var string
     */
    protected $host;

    /**
     * Таймстамп
     *
     * @var int
     */
    protected $timestamp;

    /**
     * Логин
     *
     * @var string
     */
    protected $login;

    /**
     * Пароль
     *
     * @var string
     */
    protected $password;

    /**
     * Конструктор
     * @param null|array $config
     */
    public function __construct($config = null)
    {
        $config = is_array($config) ? $config : $this->getConfig();
        $this->host = $config['host'];
        $this->timeout = $config['timeout'];
        $this->login = $config['login'];
        $this->password = $config['password'];
        $this->timestamp = time();
    }

    /**
     * Выполнить запрос к трекеру
     *
     * @param $method
     * @param array $params
     *
     * @return array
     */
    public function request($method, array $params)
    {
        $requestData = $this->prepareRequestData($method, $params);

        $response = $this
            ->getHttpClient()
            ->setUrl($this->host)
            ->setHttpMethod(Http\Client::HTTP_METHOD_POST)
            ->setTimeout($this->timeout)
            ->send([], ['Content-Type' => 'application/json'], $requestData);

        return $this->prepareResponse($response->json());
    }

    /**
     * Подготовка ответа
     *
     * @param mixed $response
     * @return array
     *
     * @throws TrackerClientException
     */
    protected function prepareResponse($response)
    {
        if (!is_array($response)) {
            throw new TrackerClientException('json-decoded response has to be array');
        }

        if (isset($response['error'])) {
            throw new TrackerClientException(
                'code ['
                . $response['error']['code'] .
                '] and message ['
                . $response['error']['message'] .
                ']'
            );
        }

        if (!isset($response['result'])) {
            throw new TrackerClientException('undefined result');
        }

        return $response['result'];
    }

    /**
     * Подготавливает параметры любого запроса к API в соответствии с протоколом трекера
     *
     * @param string $method
     * @param array $params
     * @return array
     */
    protected function prepareRequestData($method, $params)
    {
        $params['auth'] = $this->prepareAuthParams();

        return json_encode([
            'jsonrpc' => '2.0',
            'id' => 665,
            'method' => $method,
            'params' => $params,
        ]);
    }

    /**
     * Генерация блока атворизации при запросе к сервису
     *
     * @return array
     */
    protected function prepareAuthParams()
    {
        return [
            'login' => $this->login,
            'timestamp' => $this->timestamp,
            'hash' => $this->getHash()
        ];
    }

    /**
     * Получить Http клиент
     *
     * @return Http\Client
     */
    protected function getHttpClient()
    {
        return Http\Client::init();
    }

    /**
     * Получить конфиг трекера
     *
     * @return array
     */
    protected function getConfig()
    {
        return Proxy::init()->getConfig()['tracker'];
    }

    /**
     * Получить хеш
     *
     * @return string
     */
    protected function getHash()
    {
        return hash_hmac('sha256', $this->login . $this->timestamp, $this->password);
    }
}
