<?php

namespace Project\Tracker\Command\AddCampaign\Flow\Exception;

use Project\Exception\RestException\ServerError\InternalServerErrorException;

/**
 * При попытке создать flow с некорректными данными
 */
class FlowCreateException extends InternalServerErrorException
{

}