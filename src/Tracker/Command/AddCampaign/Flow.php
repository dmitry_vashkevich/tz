<?php

namespace Project\Tracker\Command\AddCampaign;

use Project\Check\Validator;
use Project\Entity\Land;
use Project\Entity\Preland;
use Project\Tracker\Command\AddCampaign\Flow\Exception\FlowCreateException;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Класс для сборки параметра flow метода AddCampaign
 * в терминах трекера
 */
class Flow
{

    /** произвольная страна (код трекера) */
    const ANY_COUNTRY = '--';

    /** индексы группировки путей */
    const GROUP_INDEX_MOBILE = 'mobile';
    const GROUP_INDEX_COMMON = 'common';

    /** вес по умолчанию */
    const DEFAULT_WEIGHT = 1;

    /** @var array  */
    protected $condition = [
        self::GROUP_INDEX_MOBILE => [],
        self::GROUP_INDEX_COMMON => [],
    ];

    /** @var array  */
    protected $default = [
        self::GROUP_INDEX_MOBILE => [],
        self::GROUP_INDEX_COMMON => [],
    ];

    /**
     * Добавить путь
     *
     * @param Land $land
     * @param Preland $preland
     */
    public function addPath(Land $land, Preland $preland = null)
    {
        $currentLandCountries = $land->getCountryList();
        $groupIndex = $this->getGroupIndex($land->getPlatform());
        $path = [
            'prelandId' => $preland === null ? 0 : (int) $preland->getId(),
            'landId' => (int) $land->getId(),
            'weight' => self::DEFAULT_WEIGHT
        ];

        $this->default[$groupIndex][] = $path;

        foreach ($currentLandCountries as $currentLandCountry) {
            $this->condition[$groupIndex][$currentLandCountry][] = $path;
        }
    }

    /**
     * Получить параметр flow для трекера
     * @return array
     * @throws FlowCreateException
     */
    public function prepare()
    {
        /**
         *  делаем копию перед выгрузкой, чтобы не повредить рабочие данные в объекте
         */
        $defaultCommon = $this->default[self::GROUP_INDEX_COMMON];

        if (count($defaultCommon) == 0) {
            throw new FlowCreateException('unable to calculate default path');
        }

        $defaultMobile = $this->default[self::GROUP_INDEX_MOBILE];

        $conditionCommon = $this->condition[self::GROUP_INDEX_COMMON];
        $conditionMobile = $this->condition[self::GROUP_INDEX_MOBILE];

        /**
         * формируем конечный параметр flow согласно апи трекера
         */

        $resultFlow = [];

        /** задаем пути по умолчанию */
        $resultFlow[0] = [
            'path' => $defaultCommon
        ];

        if (count($conditionCommon) > 0) {
            foreach ($conditionCommon as $country => $conditionCountryPaths) {
                $resultFlow[] = [
                    'condition' => [
                        ['country', $country]
                    ],
                    'path' => $conditionCountryPaths,
                ];
            }
        }

        if (count($defaultMobile) >0) {
            $resultFlow[] = [
                'condition' => [
                    ['mobile', true]
                ],
                'path' => $defaultMobile,
            ];

            if (count($conditionMobile) > 0) {
                foreach ($conditionMobile as $country => $conditionCountryPaths) {
                    $resultFlow[] = [
                        'condition' => [
                            ['mobile', true],
                            ['country', $country]
                        ],
                        'path' => $conditionCountryPaths,
                    ];
                }
            }
        }

        return $resultFlow;
    }

    /**
     * @param $landPlatform
     * @return string
     */
    protected function getGroupIndex($landPlatform)
    {
        if ($landPlatform == Land::PLATFORM_MOBILE) {
            $defaultIndex = self::GROUP_INDEX_MOBILE;
        } else {
            $defaultIndex = self::GROUP_INDEX_COMMON;
        }

        return $defaultIndex;
    }
}
