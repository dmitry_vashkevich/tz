<?php

namespace Project\Tracker\Command;

use Project\Tracker\Command;

/**
 * Команда getPrelandList
 */
class GetPrelandList extends Command
{
    const COMMAND_NAME = 'getPrelandList';

    /**
     * @inheritdoc
     */
    protected function validate($params)
    {
        //метод не требует параметров
    }
}
