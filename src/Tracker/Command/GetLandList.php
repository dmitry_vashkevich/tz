<?php

namespace Project\Tracker\Command;

use Project\Tracker\Command;

/**
 * Команда getProductList
 */
class GetLandList extends Command
{
    const COMMAND_NAME = 'getLandList';

    /**
     * @inheritdoc
     */
    protected function validate($params)
    {
        //метод не требует параметров
    }
}
