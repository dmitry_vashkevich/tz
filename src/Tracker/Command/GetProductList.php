<?php

namespace Project\Tracker\Command;

use Project\Tracker\Command;

/**
 * Команда getProductList
 */
class GetProductList extends Command
{
    const COMMAND_NAME = 'getProductList';

    /**
     * @inheritdoc
     */
    protected function validate($params)
    {
        //метод не требует параметров
    }
}
