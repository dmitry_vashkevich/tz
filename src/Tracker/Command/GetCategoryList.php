<?php

namespace Project\Tracker\Command;

use Project\Tracker\Command;

/**
 * Команда getCategoryList
 */
class GetCategoryList extends Command
{
    const COMMAND_NAME = 'getCategoryList';

    /**
     * @inheritdoc
     */
    protected function validate($params)
    {
        //метод не требует параметров
    }
}
