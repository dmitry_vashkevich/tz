<?php

namespace Project\Tracker;

/**
 * Абстрактная команда трекера
 */
abstract class Command
{
    /**
     * Имя команды
     */
    const COMMAND_NAME = 'abstract command name';

    /** @var  array */
    protected $config = null;

    /**
     * Конструктор
     * @param null $config
     */
    public function __construct($config = null)
    {
        $this->config = $config;
    }
    
    /**
     * Запуск команды
     *
     * @param $params
     *
     * @return array
     */
    public function run($params = [])
    {
        //валидация параметров команды
        $this->validate($params);

        //выполняем запрос для конкретной команды
        $response = $this
            ->getTrackerClient($this->config)
            ->request(
                $this->getCommandName(),
                $params
            );

        return $response;
    }

    /**
     * Возвращает имя команды в терминах трекера
     *
     * @return string
     */
    protected function getCommandName()
    {
        return static::COMMAND_NAME;
    }

    /**
     * Получить клиента к трекеру
     *
     * @param null|array $config
     * @return Client
     */
    protected function getTrackerClient($config = null)
    {
        return  new Client($config);
    }

    /**
     * Валидатор
     *
     * @param array $params
     *
     * @throws \Project\Check\Exception\ValidationException
     */
    abstract protected function validate($params);
}
