<?php

namespace Project\Tracker;

/**
 * Интерфейс в терминах трекера и фабрика команд
 */
class Tracker
{
    /** Команды рекламного сервера */
    const COMMAND_GET_CATEGORY_LIST = 'getCategoryList';
    const COMMAND_GET_PRODUCT_LIST = 'getProductList';
    const COMMAND_GET_LAND_LIST = 'getLandList';
    const COMMAND_GET_PRELAND_LIST = 'getPrelandList';
    const COMMAND_ADD_CAMPAIGN = 'addCampaign';
    const COMMAND_GET_EVENT_LIST = 'getEventList';
    const COMMAND_CONFIRM_EVENT_LIST = 'confirmEventList';

    /** @var  array */
    protected $config = null;

    /**
     * Конструктор
     * @param null $config
     */
    public function __construct($config = null)
    {
        $this->config = $config;
    }

    /**
     * Получить список категорий продуктов
     *
     * @return array
     */
    public function getCategoryList()
    {
        return $this->getCommand(self::COMMAND_GET_CATEGORY_LIST)->run();
    }

    /**
     * Получить список продуктов
     *
     * @return array
     */
    public function getProductList()
    {
        return $this->getCommand(self::COMMAND_GET_PRODUCT_LIST)->run();
    }

    /**
     * Получить список лендов
     *
     * @return array
     */
    public function getLandList()
    {
        return $this->getCommand(self::COMMAND_GET_LAND_LIST)->run();
    }

    /**
     * Получить список прелендов
     *
     * @return array
     */
    public function getPreLandList()
    {
        return $this->getCommand(self::COMMAND_GET_PRELAND_LIST)->run();
    }

    /**
     * Добавить компанию
     * @see Flow
     * @param array $flow
     * @return array
     */
    public function addCampaign($flow)
    {
        return $this->getCommand(self::COMMAND_ADD_CAMPAIGN)->run([
            'flow' => $flow,
        ]);
    }

    /**
     * Получить список событий
     *
     * @param int $lastId
     * @param int $limit
     * @return array
     */
    public function getEventList($lastId, $limit)
    {
        return $this->getCommand(self::COMMAND_GET_EVENT_LIST)->run([
            'lastId' => $lastId,
            'limit' => $limit,
        ]);
    }

    /**
     * Получить список прелендов
     *
     * @param int $lastId
     * @return array
     */
    public function confirmEventList($lastId)
    {
        return $this->getCommand(self::COMMAND_CONFIRM_EVENT_LIST)->run([
            'lastId' => $lastId
        ]);
    }

    /**
     * Получить команду
     *
     * @param $commandClassName
     * @return Command
     */
    protected function getCommand($commandClassName)
    {
        $commandClassName = '\\Project\\Tracker\\Command\\' . ucfirst($commandClassName);
        $obj = new $commandClassName($this->config);

        return $obj;
    }
}
