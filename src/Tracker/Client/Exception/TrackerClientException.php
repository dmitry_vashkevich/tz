<?php

namespace Project\Tracker\Client\Exception;

use Project\Exception\RestException\ServerError\InternalServerErrorException;

/**
 * Исключение для валидатора ответа трекер-клиента
 */
class TrackerClientException extends InternalServerErrorException
{

}
