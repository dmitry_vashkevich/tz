<?php

namespace Project\MessageConfig;

use Doctrine\Common\Collections\Criteria;
use Project\Entity\MessageConfig;

/**
 * Поиск конфигов
 */
class Search
{
    /**
     * Поиск
     *
     * @return \Doctrine\Common\Collections\Collection|static[]
     */
    public function process()
    {
        $criteria = new Criteria();
        $criteria->where(
            Criteria::expr()->eq('active', true)
        );

        $criteria->orderBy(['added' => 'desc']);

        return MessageConfig::prepareList(
            $criteria
        );
    }
}
