<?php

namespace Project\TeaserConfig;

use Doctrine\Common\Collections\Criteria;
use Project\Entity\TeaserConfig;

/**
 * Поиск конфигов под размер блока
 */
class Search
{
    /**
     * Поиск
     *
     * @param $blockWidthFrom
     * @param $blockWidthTo
     * @param $blockHeightFrom
     * @param $blockHeightTo
     * @return \Doctrine\Common\Collections\Collection|\Project\Entity\TeaserConfig[]
     */
    public function process($blockWidthFrom = null, $blockWidthTo = null, $blockHeightFrom = null, $blockHeightTo = null)
    {
        $criteria = new Criteria();
        $criteria->where(
            Criteria::expr()->eq('active', true)
        );

        if ($blockWidthFrom !== null) {
            $criteria->andWhere(
                Criteria::expr()->eq('blockWidthFrom', $blockWidthFrom)
            );
        }

        if ($blockWidthTo !== null) {
            $criteria->andWhere(
                Criteria::expr()->eq('blockWidthTo', $blockWidthTo)
            );
        }

        if ($blockHeightFrom !== null) {
            $criteria->andWhere(
                Criteria::expr()->eq('blockHeightFrom', $blockHeightFrom)
            );
        }

        if ($blockHeightTo !== null) {
            $criteria->andWhere(
                Criteria::expr()->eq('blockHeightTo', $blockHeightTo)
            );
        }

        $criteria->orderBy(['added' => 'desc']);

        return TeaserConfig::prepareList($criteria);
    }
}