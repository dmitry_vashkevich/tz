<?php

namespace Project\TeaserConfig;

/**
 * Поиск конфигов под размер блока
 */
abstract class SearchByBlock
{
    /**
     * Минимальный размер ячейки, меньше которого подбор прекращается
     */
    const MIN_WIDTH = 60;
    const MIN_HEIGHT = 60;
    const CELLSPACING = 4;
    const BORDER = 1;


    /** @var  int */
    protected $width;

    /** @var  int */
    protected $height;

    /**
     * Конструктор
     *
     * @param $width
     * @param $height
     */
    public function __construct($width, $height)
    {
        $this->width = $width;
        $this->height = $height;
    }

    /**
     * Поиск
     *
     * @return array
     */
    public function process()
    {
        return $this->prepareBlock(
            $this->height - 2 * self::BORDER,
            $this->width - 2 * self::BORDER,
            self::CELLSPACING
        );
    }

    /**
     * Поиск блоков
     *
     * @param $blockHeight
     * @param $blockWidth
     * @param $cellspacing
     * @return array
     */
    protected function prepareBlock($blockHeight, $blockWidth, $cellspacing)
    {
        $configs = $this->prepareConfigs();

        $maxRow = (int) ($blockWidth / 60);
        $maxCol = (int) ($blockHeight / 60);

        if ($maxRow <= 0 || $maxCol <= 0) {
            return [];
        }

        $res = [];
        for($col = 1; $col <= $maxCol; $col++) {
            for($row = 1; $row <= $maxRow; $row++) {
                $cellHeight = (int) (($blockHeight - ($col +1) * $cellspacing) / $col);
                $cellWidth = (int) (($blockWidth - ($row +1) * $cellspacing) / $row);
                $selectedConfigs = [];
                foreach ($configs as $config) {
                    if (
                        $config['b_width_from'] <= $cellWidth &&
                        $config['b_height_from'] <= $cellHeight &&
                        $config['b_width_to'] >= $cellWidth &&
                        $config['b_height_to'] >= $cellHeight
                    ) {
                        $selectedConfigs[] = $config;
                    }
                }

                if (count($selectedConfigs) <= 0) {
                    continue;
                }

                $res[] = [
                    'configs' => $selectedConfigs,
                    'row' => $row,
                    'col' => $col,
                    'cellHeight' => $cellHeight,
                    'cellWidth' => $cellWidth,
                ];
            }
        }
        
        return $res;
    }

    /**
     * Получить конфиги
     *
     * @return array
     */
    abstract protected function prepareConfigs();
}
