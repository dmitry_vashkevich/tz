<?php

namespace Project\TeaserConfig\SearchByBlock;

use Project\Cache\EntityCache;
use Project\TeaserConfig\SearchByBlock;

/**
 * Поиск конфигов под размер блока в кеше
 */
class Cache extends SearchByBlock
{
    /**
     * Получить конфиги
     *
     * @return array
     */
    protected function prepareConfigs()
    {
        return (new EntityCache\TeaserConfig())->getAll();
    }
}
