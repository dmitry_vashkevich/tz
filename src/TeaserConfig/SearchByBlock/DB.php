<?php

namespace Project\TeaserConfig\SearchByBlock;

use Project\Entity\TeaserConfig;
use Project\TeaserConfig\Search;
use Project\TeaserConfig\SearchByBlock;

/**
 * Поиск конфигов под размер блока в БД
 */
class DB extends SearchByBlock
{
    /**
     * Получить конфиги
     *
     * @return array
     */
    protected function prepareConfigs()
    {
        $configs = (new Search())->process();
        
        if ($configs->isEmpty()) {
            return [];
        }
        
        $data = [];
        foreach ($configs as $config) {
            $data[] = $this->format($config);
        }
        
        return $data;
    }

    /**
     * @inheritdoc
     */
    protected function format(TeaserConfig $entity)
    {
        return [
            'id' => $entity->getId(),
            'data' => $entity->getData(),
            'b_width_from' => $entity->getBlockWidthFrom(),
            'b_width_to' => $entity->getBlockWidthTo(),
            'b_height_from' => $entity->getBlockHeightFrom(),
            'b_height_to' => $entity->getBlockHeightTo(),
        ];
    }
}