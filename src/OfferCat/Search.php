<?php

namespace Project\OfferCat;

use Project\Entity\OfferCat;

/**
 * Поиск категорий офферов
 */
class Search
{
    /**
     * Поиск категорий офферов
     *
     * @return \Doctrine\Common\Collections\Collection | OfferCat[]
     */
    public function process()
    {
        return OfferCat::prepareList();
    }
}
