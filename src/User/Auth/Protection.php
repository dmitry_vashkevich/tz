<?php

namespace Project\User\Auth;

use Project\Proxy;
use Project\User\Auth\Exception\TimeInvalidAuthException;
use Project\User\Auth\Exception\TooManyInvalidAuthException;

/**
 * Класс защиты авторизации, принцип простой:
 * ATTEMPT_COUNT неудачных авторизаций с одного ip-адреса -
 * блокируем все попытки на MIN_COUNT минут
 */
class Protection
{
    /**
     * Префиксы ключей
     */
    const PREFIX_COUNT = 'ProtectionCount';
    const PREFIX_TIME = 'ProtectionTime';

    /**
     * Кол-во попыток прежде чем включается защита
     */
    const ATTEMPT_COUNT = 500000;

    /**
     * Кол-во минут ожидания прежде чем будет включена возможность авторизации
     */
    const MIN_COUNT = 1;

    /**
     * Метод проверки необходимости включения защиты
     *
     * @return $this
     * @throws TooManyInvalidAuthException
     */
    public function check()
    {
        return $this
            ->checkTime()
            ->checkAttempt();
    }

    /**
     * Добавить попытку в счетчик
     *
     * @return $this
     */
    public function addAttempt()
    {
        $this->getMemcache()->set(
            $this->prepareKey(self::PREFIX_COUNT),
            $this->getAttemptCount() + 1
        );

        return $this;
    }


    protected function checkTime()
    {
        if ($this->getTimer() >= new \DateTime()) {
            throw new TimeInvalidAuthException('Current-time is lower than control-time');
        }

        $this->dropCounter(self::PREFIX_TIME);

        return $this;
    }

    /**
     * Проверка счетчика попыток
     *
     * @return $this
     * @throws TooManyInvalidAuthException
     */
    protected function checkAttempt()
    {
        if ($this->getAttemptCount() < self::ATTEMPT_COUNT) {
            return $this;
        }

        $this->setTimer();
        $this->dropCounter(self::PREFIX_COUNT);

        throw new TooManyInvalidAuthException('Exceeded the maximum number of login attempts');
    }

    /**
     * Получить текущий счетчик попыток
     *
     * @return int
     */
    protected function getAttemptCount()
    {
        return (int) $this->getMemcache()->get(
            $this->prepareKey(self::PREFIX_COUNT)
        );
    }

    /**
     * Удалить счетчик попыток
     *
     * @param $const
     * @return mixed
     */
    protected function dropCounter($const)
    {
        return $this->getMemcache()->delete(
            $this->prepareKey($const)
        );
    }

    /**
     * Получить контрольное время
     *
     * @return \DateTime
     */
    protected function getTimer()
    {
        return (new \DateTime())->setTimestamp(
            (int) $this->getMemcache()->get(
                $this->prepareKey(self::PREFIX_TIME)
            )
        );
    }

    /**
     * Установить таймер
     *
     * @return $this
     */
    protected function setTimer()
    {
        $this->getMemcache()->set(
            $this->prepareKey(self::PREFIX_TIME),
            (new \DateTime())->add(
                new \DateInterval('PT' . self::MIN_COUNT . 'M')
            )->getTimestamp()
        );

        return $this;
    }

    /**
     * Получить ключ для получения / установки защиты
     *
     * @param string $const
     * @return string
     */
    protected function prepareKey($const)
    {
        return $const . $_SERVER['REMOTE_ADDR'];
    }

    /**
     * Получить объект для работы с мемкешем
     *
     * @return \Memcached
     */
    protected function getMemcache()
    {
        return Proxy::init()->getMemcache();
    }
}
