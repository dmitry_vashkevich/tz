<?php

namespace Project\User\Auth\Exception;

use Project\Exception\RestException\ServerError\InternalServerErrorException;

/**
 * При включенном таймере на попытки авторизации
 */
class TimeInvalidAuthException extends InternalServerErrorException
{

}
