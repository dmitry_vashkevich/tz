<?php

namespace Project\User\Auth\Exception;

use Project\Exception\RestException\ServerError\InternalServerErrorException;

/**
 * При многократных неудачных попытках авторизоваться
 */
class TooManyInvalidAuthException extends InternalServerErrorException
{

}
