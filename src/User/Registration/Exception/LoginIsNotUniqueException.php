<?php

namespace Project\User\Registration\Exception;

use Project\Exception\RestException\ServerError\InternalServerErrorException;

/**
 * При попытке зарегистрировать пользователя с уже существующим логином
 */
class LoginIsNotUniqueException extends InternalServerErrorException
{

}
