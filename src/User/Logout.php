<?php

namespace Project\User;

/**
 * Логаут
 */
class Logout
{
    /**
     * Сессия
     *
     * @var Session
     */
    protected $session;

    /**
     * Конструктор
     *
     * @param Session $session
     */
    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    /**
     * Логаут
     *
     * @return $this
     */
    public function process()
    {
        $this->session->drop();
        return $this;
    }
}
