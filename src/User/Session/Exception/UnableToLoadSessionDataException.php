<?php

namespace Project\User\Session\Exception;

use Project\Exception\RestException\ServerError\InternalServerErrorException;

/**
 * При невозможности получить данные по сессии
 */
class UnableToLoadSessionDataException extends InternalServerErrorException
{

}
