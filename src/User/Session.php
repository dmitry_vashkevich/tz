<?php

namespace Project\User;

use Project\Entity\User;
use Project\Proxy;
use Project\User\Session\Exception\IncorrectSessionIpException;
use Project\User\Session\Exception\UnableToLoadSessionDataException;

/**
 * Сессия пользователя
 */
abstract class Session
{
    /**
     * Сессионные параметры
     */
    const PARAM_USER_ID = 'user_id';
    const PARAM_IP = 'ip';

    /**
     * Пользователь
     *
     * @var  User
     */
    protected $user;

    /**
     * Строковый идентификатор сесии
     *
     * @var string
     */
    protected $id;

    /**
     * ip-адрес
     *
     * @var string
     */
    protected $ip;

    /**
     * Конструктор
     *
     * @param string | null $sessionId
     */
    public function __construct($sessionId = null)
    {
        if ($sessionId === null) {
            $this->id = $this->generateId();
            return;
        }

        $this->id = $sessionId;
        $this->loadData();
    }

    /**
     * Установить юзера
     *
     * @param User $user
     * @return $this
     */
    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * Сохраняет сессионные данные:
     * - юзера
     * - ip
     *
     * @return $this
     */
    public function save()
    {
        $this->getMemcache()->set(
            $this->prepareKey(),
            $this->prepareData()
        );

        return $this;
    }

    /**
     * Удаляет сессионный ключ пользователя
     *
     * @return $this
     */
    public function drop()
    {
        $this->getMemcache()->delete($this->prepareKey());
        return $this;
    }

    /**
     * Получить юзера
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Получить идентификатор сесии
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Получить ключ для получения / установки id юзера
     *
     * @return string
     */
    protected function prepareKey()
    {
        return $this->getPrefix() . $this->id;
    }

    /**
     * Подготовка сессионных данных
     *
     * @return string
     */
    protected function prepareData()
    {
        return json_encode([
            self::PARAM_USER_ID => $this->getUser()->getId(),
            self::PARAM_IP => $_SERVER['REMOTE_ADDR'],
        ]);
    }

    /**
     * Генератор сессионных идентификаторов
     *
     * @return string
     */
    protected function generateId()
    {
        return base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);
    }

    /**
     * Получить объект для работы с мемкешем
     *
     * @return \Memcached
     */
    protected function getMemcache()
    {
        return Proxy::init()->getMemcache();
    }

    /**
     * Подгружает юзера по сесиионному идентификатору
     *
     * @throws IncorrectSessionIpException
     */
    protected function loadData()
    {
        $data = json_decode($this->getMemcache()->get(
            $this->prepareKey(),
            function () {
                throw new UnableToLoadSessionDataException('Unable to load session data');
            }
        ), true);

        $this->user = $this->prepareUserById($data[self::PARAM_USER_ID]);
        $this->ip = $data[self::PARAM_IP];
        $this->checkIp();
    }

    /**
     * Проверка ip-адреса
     *
     * @throws IncorrectSessionIpException
     */
    protected function checkIp()
    {
        if ($this->ip != $_SERVER['REMOTE_ADDR']) {
            throw new IncorrectSessionIpException('IP validation failed');
        }
    }

    /**
     * Получить ip-адрес
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Установить ip-адрес
     *
     * @param string $ip
     * @return $this
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
        return $this;
    }

    /**
     * Получить префикс конкретной сессии
     *
     * @return string
     */
    abstract protected function getPrefix();

    /**
     * Получить конкретного юзера по id
     *
     * @param $id
     * @return User
     */
    abstract protected function prepareUserById($id);
}
