<?php

namespace Project\User;

use Project\Entity\User;
use Project\User\Password\Exception\CreateHashException;
use Project\User\Password\Exception\PasswordVerifyException;

/**
 * Пароль пользователя
 */
class Password
{
    /**
     * Пользователь
     *
     * @var User
     */
    protected $user;

    /**
     * Конструктор
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Генерация хранимого хеша пароля
     *
     * @param $password
     * @return string
     * @throws CreateHashException
     */
    public function createHash($password)
    {
        $hash = password_hash($password, PASSWORD_DEFAULT);

        if ($hash === false) {
            throw new CreateHashException('Unable to hash password');
        }

        return $hash;
    }

    /**
     * Верификация пароля
     *
     * @param string $password
     *
     * @throws PasswordVerifyException
     */
    public function verify($password)
    {
        if (!password_verify($password, $this->user->getPassword())) {
            throw new PasswordVerifyException('Incorrect password');
        }
    }
}
