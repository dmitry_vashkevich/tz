<?php

namespace Project\User;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Project\Entity\User;

/**
 * Поиск юзеров
 */
abstract class Search
{
    /**
     * Кол-во сущностей в выборке, до применения операции limit
     *
     * @var int
     */
    protected $count;

    /**
     * Поиск
     *
     * @param string $login
     * @param int $offset
     * @param int $limit
     * @return \Doctrine\Common\Collections\Collection|static[]
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function process($login = null, $offset = null, $limit = null)
    {
        $criteria = new Criteria();
        $criteria->where(
            Criteria::expr()->eq('active', true)
        );

        if ($login !== null) {
            $criteria->andWhere(
                Criteria::expr()->contains('login', $login)
            );
        }

        $this->count = $this->getRepo()->createQueryBuilder('base')
            ->select("count(base.id)")
            ->addCriteria($criteria)
            ->getQuery()
            ->getSingleScalarResult();

        $criteria->orderBy(['id' => 'asc']);

        if ($offset !== null) {
            $criteria->setFirstResult($offset);
        }

        if ($limit !== null) {
            $criteria->setMaxResults($limit);
        }

        return $this->getResult($criteria);
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return (int)$this->count;
    }

    /**
     * @return \Doctrine\ORM\EntityRepository
     */
    abstract protected function getRepo();

    /**
     * @param Criteria $criteria
     * @return Collection | User[]
     */
    abstract protected function getResult(Criteria $criteria);
}
