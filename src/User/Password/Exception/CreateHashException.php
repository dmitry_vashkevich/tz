<?php

namespace Project\User\Password\Exception;

use Project\Exception\RestException\ServerError\InternalServerErrorException;

/**
 * Исключение при создании хеща пароля
 */
class CreateHashException extends InternalServerErrorException
{

}
