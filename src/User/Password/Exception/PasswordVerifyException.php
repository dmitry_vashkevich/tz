<?php

namespace Project\User\Password\Exception;

use Project\Exception\RestException\ServerError\InternalServerErrorException;

/**
 * Исключение при проверке пароля
 */
class PasswordVerifyException extends InternalServerErrorException
{

}
