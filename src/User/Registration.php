<?php

namespace Project\User;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Project\Entity\User;
use Project\User\Registration\Exception\LoginIsNotUniqueException;

/**
 * Регистрация
 */
abstract class Registration
{
    /**
     * Логин
     *
     * @var string
     */
    protected $login;

    /**
     * Пароль
     *
     * @var string
     */
    protected $password;

    /**
     * Конструктор
     *
     * @param $login
     * @param $password
     */
    public function __construct($login, $password)
    {
        $this->login = $login;
        $this->password = $password;
    }

    /**
     * @throws LoginIsNotUniqueException
     * @throws Password\Exception\CreateHashException
     */
    public function process()
    {
        try {
            $user = $this->prepareUser();
            $user
                ->setLogin($this->login)
                ->setPassword((new Password($user))->createHash($this->password))
                ->save();
        } catch (UniqueConstraintViolationException $e) {
            throw new LoginIsNotUniqueException('Login [' . $this->login . '] is already in use');
        }
    }

    /**
     * @return User
     */
    abstract protected function prepareUser();
}
