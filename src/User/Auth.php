<?php

namespace Project\User;

use Project\Entity\User;

/**
 * Авторизация юзера
 */
abstract class Auth
{
    /**
     * Логин
     *
     * @var string
     */
    protected $login;

    /**
     * Пароль
     *
     * @var string
     */
    protected $password;

    /**
     * Конструктор
     *
     * @param $login
     * @param $password
     */
    public function __construct($login, $password)
    {
        $this->login = $login;
        $this->password = $password;
    }


    /**
     * Авторизация
     *
     * @return Session
     */
    public function process()
    {
        $user = $this->prepareUser();

        (new Password($user))->verify($this->password);

        return $this->getSession()
            ->setUser($user)
            ->save();
    }

    /**
     * @return User
     */
    abstract protected function prepareUser();

    /**
     * @return Session
     */
    abstract protected function getSession();
}
