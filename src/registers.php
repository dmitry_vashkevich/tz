<?php

$env = 'prod';
if (file_exists(__DIR__ . '/../dev')){
    $env = 'dev';
}

$config = 'base';

$app->register(new \Project\Provider\Config(__DIR__ . '/../config/' . $env . '/' . $config . '.php'));

$app->register(new \Project\Provider\ErrorHandler());
$app->register(new \Project\Provider\Logger());
$app->register(new \Project\Provider\Validation());
$app->register(new \Project\Provider\Doctrine\Driver());
$app->register(new \Project\Provider\Doctrine\Orm());
$app->register(new \Project\Provider\HttpClient());
$app->register(new \Project\Provider\Memcached());
$app->register(new \Project\Provider\Console());
$app->register(new \Project\Provider\Gearman());
