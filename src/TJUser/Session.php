<?php

namespace Project\TJUser;

use Project\Entity\TJUser;

/**
 * Сессия
 */
class Session extends \Project\User\Session
{
    const PREFIX = 'tj_user';

    /**
     * @inheritdoc
     */
    protected function getPrefix()
    {
        return self::PREFIX;
    }

    /**
     * @inheritdoc
     */
    protected function prepareUserById($id)
    {
        return TJUser::prepareById($id);
    }
}
