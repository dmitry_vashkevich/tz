<?php

namespace Project\TJUser;

use Project\Entity\TJUser;

/**
 * Регистрация
 */
class Auth extends \Project\User\Auth
{
    /**
     * @inheritdoc
     */
    protected function prepareUser()
    {
        return TJUser::prepareByParams([
            'login' => $this->login
        ]);
    }

    /**
     * @inheritdoc
     */
    protected function getSession()
    {
        return new Session();
    }
}
