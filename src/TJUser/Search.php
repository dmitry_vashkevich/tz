<?php

namespace Project\TJUser;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Project\Entity\TJUser;

/**
 * Поиск сайт-юзеров
 */
class Search extends \Project\User\Search 
{
    /**
     * @return \Doctrine\ORM\EntityRepository
     */
    protected function getRepo()
    {
        return TJUser::repo();
    }

    /**
     * @param $criteria
     * @return Collection | TJUser[]
     */
    protected function getResult(Criteria $criteria)
    {
        return TJUser::prepareList($criteria);
    }
}
