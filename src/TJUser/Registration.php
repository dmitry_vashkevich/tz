<?php

namespace Project\TJUser;

use Project\Entity\TJUser;

/**
 * Регистрация
 */
class Registration extends \Project\User\Registration
{
    /**
     * @var int
     */
    protected $refId;

    /**
     * @var string
     */
    protected $skype;

    /**
     * @var string
     */
    protected $icq;

    /**
     * @var string
     */
    protected $jabber;

    /**
     * @var string
     */
    protected $siteList;    
    
    /**
     * @inheritdoc
     */
    protected function prepareUser()
    {
        $user = new TJUser();
        $user->setSkype($this->skype);
        $user->setIcq($this->icq);
        $user->setJabber($this->jabber);
        $user->setSiteList($this->siteList);
        if ($this->refId !== null) {
            $refUser = TJUser::prepareById($this->refId);
            $user->setRefUser($refUser);
        }

        return $user;
    }

    /**
     * @param int $refId
     * @return $this
     */
    public function setRefId($refId)
    {
        $this->refId = $refId;
        return $this;
    }

    /**
     * @param string $skype
     * @return $this
     */
    public function setSkype($skype)
    {
        $this->skype = $skype;
        return $this;
    }

    /**
     * @param string $siteList
     * @return $this
     */
    public function setSiteList($siteList)
    {
        $this->siteList = $siteList;
        return $this;
    }

    /**
     * @param string $icq
     * @return $this
     */
    public function setIcq($icq)
    {
        $this->icq = $icq;
        return $this;
    }

    /**
     * @param string $jabber
     * @return $this
     */
    public function setJabber($jabber)
    {
        $this->jabber = $jabber;
        return $this;
    }
}
