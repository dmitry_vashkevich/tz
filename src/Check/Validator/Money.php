<?php

namespace Project\Check\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * Правила для валидации денежных сумм
 */
class Money extends Constraint
{
    /**
     * Сообщение об ошибке
     *
     * @var string
     */
    public $message = '"%string%" is not valid money amount';

    /**
     * Количество целых знаков
     *
     * @var int
     */
    public $integer = 10;

    /**
     * Количество десятичных знаков
     *
     * @var int
     */
    public $decimal = 4;
}
