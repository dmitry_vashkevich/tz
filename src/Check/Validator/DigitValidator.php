<?php

namespace Project\Check\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Валидатор цифровых символов
 */
class DigitValidator extends ConstraintValidator
{
    /**
     * Отвалидировать переданное значение
     *
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        /**
         * @var Digit $constraint
         */
        if (!is_scalar($value) || !ctype_digit((string) $value)) {
            $this->getContext()->addViolation($constraint->message);
        }
    }

    /**
     * @return \Symfony\Component\Validator\ExecutionContextInterface
     */
    protected function getContext()
    {
        return $this->context;
    }
}
