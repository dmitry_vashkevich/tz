<?php

namespace Project\Check\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Валидатор списка идов
 */
class JsonIdlistValidator extends ConstraintValidator
{
    /**
     * Отвалидировать переданное значение
     *
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        /**
         * @var JsonIdList $constraint
         */
        $ids = @json_decode($value, true);
        if (!is_array($ids) || count($ids) <= 0) {
            $this->getContext()->addViolation($constraint->message);
        }

        foreach ($ids as $id) {
            if (!is_scalar($id) || !ctype_digit((string) $id) || (int) $id <= 0) {
                $this->getContext()->addViolation($constraint->message);
                break;
            }
        }
    }

    /**
     * @return \Symfony\Component\Validator\ExecutionContextInterface
     */
    protected function getContext()
    {
        return $this->context;
    }
}
