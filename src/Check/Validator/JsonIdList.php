<?php

namespace Project\Check\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * Правила для валидации цифровых символов в строке
 */
class JsonIdList extends Constraint
{
    /**
     * Сообщение об ошибке
     *
     * @var string
     */
    public $message = 'This value should be non-empty array of integer greater than 0 in json format';
}
