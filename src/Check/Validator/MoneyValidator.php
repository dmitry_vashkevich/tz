<?php

namespace Project\Check\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Валидатор денежных сумм
 */
class MoneyValidator extends ConstraintValidator
{
    /**
     * Отвалидировать сумму
     *
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        /**
         * @var Money $constraint
         */

        if (
            !preg_match(
                '/^[0-9]{0,' . $constraint->integer . '}(?:\.[0-9]{0,' . $constraint->decimal . '})?$/',
                $value
            )
        ) {
            $this->context->addViolation(
                $constraint->message,
                ['%string%' => $value]
            );
        }
    }
}
