<?php

namespace Project\Check\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * Правила для валидации цифровых символов в строке
 */
class Digit extends Constraint
{
    /**
     * Сообщение об ошибке
     *
     * @var string
     */
    public $message = 'This value should be of type digit';
}
