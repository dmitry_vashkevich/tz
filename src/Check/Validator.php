<?php

namespace Project\Check;

use Project\Check\Exception\ValidationException;
use Project\Proxy;

/**
 * Валидатор
 */
class Validator
{
    /**
     * Валидирует значение в соответствии с переданными правилами
     *
     * @param $value
     * @param $rules
     * @param null $message
     * @param $exceptionClass
     */
    public static function validateValue($value, $rules, $message = null, $exceptionClass = ValidationException::class)
    {
        /**
         * @var \Symfony\Component\Validator\ConstraintViolation[] $errors
         */
        $errors = Proxy::init()->getValidator()->validateValue($value, $rules);

        if (count($errors) !== 0) {
            throw (new $exceptionClass(
                $errors[0]->getPropertyPath() . ': ' .
                $message !== null ? $message : $errors[0]->getMessage()
            ));
        }
    }
}
