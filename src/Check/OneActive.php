<?php

namespace Project\Check;

use Project\Check\Exception\InactiveEntityException;
use Project\Entity\Schema\Base;

/**
 * Проверка на то что модель имеет только 1 активную строчку
 */
class OneActive
{
    /**
     * Проверка
     *
     * Сначала проверяем что выборка не пустая, затем проверяем на активность
     *
     * @param Base|object|null $entity
     * @param $entityName
     *
     * @throws Exception\EmptyEntityException
     * @throws Exception\WrongEntityException
     * @throws InactiveEntityException
     *
     * @return Base
     */
    public static function check($entity, $entityName)
    {
        /** @var Base $entity */
        Entity::check($entity, $entityName);

        if (!$entity->isActive()) {
            throw new InactiveEntityException(
                'Entity "'. $entityName .'" with id "' . $entity->getId() . '" must be active'
            );
        }
    }
}
