<?php

namespace Project\Check\Exception;

/**
 * Сущность неактивна
 */
class InactiveEntityException extends WrongEntityException
{

}
