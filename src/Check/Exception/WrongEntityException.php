<?php

namespace Project\Check\Exception;

use Project\Exception\RestException\ClientError\NotFoundException;

/**
 * Неправильная сущность модели БД
 */
class WrongEntityException extends NotFoundException
{

}
