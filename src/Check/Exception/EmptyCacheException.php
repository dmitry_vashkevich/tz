<?php

namespace Project\Check\Exception;

use Project\Exception\RestException\ClientError\NotFoundException;

/**
 * Не попали в кеш
 */
class EmptyCacheException extends NotFoundException
{

}
