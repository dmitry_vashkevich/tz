<?php

namespace Project\Check\Exception;

/**
 * Пустой список сущностей
 */
class EmptyEntityListException extends WrongEntityException
{

}
