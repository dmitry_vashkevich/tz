<?php

namespace Project\Check\Exception;

/**
 * В случае если модель БД пустая
 */
class EmptyEntityException extends WrongEntityException
{

}
