<?php

namespace Project\Check\Exception;

use Project\Exception\RestException\ClientError\BadRequestException;

/**
 * Ошибки валидации
 */
class ValidationException extends BadRequestException
{
}
