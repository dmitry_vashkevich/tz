<?php

namespace Project\Check;

use Doctrine\Common\Collections\Collection;
use Project\Check\Exception\EmptyEntityException;
use Project\Check\Exception\EmptyEntityListException;
use Project\Check\Exception\WrongEntityException;
use Project\Entity\Schema\Base;

/**
 * Осуществляет проверки на корректность выборок
 */
class Entity
{
    /**
     * Проверка, что сущность найдена
     *
     * @param Base|object|null $entity
     * @param $entityName
     *
     * @throws Exception\EmptyEntityException
     * @throws Exception\WrongEntityException
     *
     * @return Base
     */
    public static function check($entity, $entityName)
    {
        if ($entity === null) {
            throw new EmptyEntityException('Empty entity "' . $entityName . '"');
        }

        if (!is_string($entityName) || empty($entityName)) {
            throw new WrongEntityException(
                'Entity name should be not empty string, `' . gettype($entityName) . '` given'
            );
        }

        if (!($entity instanceof $entityName)) {
            throw new WrongEntityException(
                'Entity must be instance of `' . $entityName . '`, `' . get_class($entity) . '` given'
            );
        }

        return $entity;
    }

    /**
     * Проверка списка
     *
     * @param Collection $entityList
     * @param string $entityName
     * @return Collection | Base[]
     * @throws EmptyEntityListException
     */
    public static function checkList(Collection $entityList, $entityName)
    {
        if ($entityList->isEmpty()) {
            throw new EmptyEntityListException('Empty entity list of "' . $entityName . '"');
        }

        return $entityList;
    }
}
