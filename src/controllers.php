<?php
/**
 * Точки входа Project
 */

use \Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\HttpFoundation\Response;
use Project\Proxy;
use Silex\Application;

$app->before(function (Request $request) use ($app) {
    (new \Project\Listener\Application\Before($request))->run();
}, Application::EARLY_EVENT);


//TJ
$app->mount('/api/tj_user', new \Project\Controller\TJ\User());
$app->mount('/api/tj_site', new \Project\Controller\TJ\Site());
$app->mount('/api/tj_block', new \Project\Controller\TJ\Block());
$app->mount('/api/tj_stats', new \Project\Controller\TJ\Stats());
$app->mount('/api/tj_payment', new \Project\Controller\TJ\Payment());
$app->mount('/api/tj_wallet', new \Project\Controller\TJ\Wallet());
$app->mount('/api/tj_wallet_type', new \Project\Controller\TJ\WalletType());

//api
$app->mount('/api/site_user', new \Project\Controller\Api\SiteUser());
$app->mount('/api/teaser_config', new \Project\Controller\Api\TeaserConfig());
$app->mount('/api/message_config', new \Project\Controller\Api\MessageConfig());
$app->mount('/api/campaign', new \Project\Controller\Api\Campaign());
$app->mount('/api/ad', new \Project\Controller\Api\Ad());
$app->mount('/api/land', new \Project\Controller\Api\Land());
$app->mount('/api/preland', new \Project\Controller\Api\Preland());
$app->mount('/api/offer', new \Project\Controller\Api\Offer());
$app->mount('/api/stats', new \Project\Controller\Api\Stats());

//common
$app->mount('/content', new \Project\Controller\Common\Content());
$app->mount('/redirect', new \Project\Controller\Common\Redirect());
$app->mount('/close', new \Project\Controller\Common\Close());

//admin
$app->mount('/api/admin_user', new \Project\Controller\Admin\User());
$app->mount('/api/admin_campaign', new \Project\Controller\Admin\Campaign());
$app->mount('/api/admin_stats', new \Project\Controller\Admin\Stats());
$app->mount('/api/admin_offer', new \Project\Controller\Admin\Offer());
$app->mount('/api/admin_ad', new \Project\Controller\Admin\Ad());
$app->mount('/api/admin_land', new \Project\Controller\Admin\Land());
$app->mount('/api/admin_site', new \Project\Controller\Admin\Site());
$app->mount('/api/admin_block', new \Project\Controller\Admin\Block());

$app->after(function (Request $request, Response $response) use ($app) {
    (new \Project\Listener\Application\After($request, $response))->run();
});

$app->error(function (\Exception $e) use ($app) {
    return Proxy::init()
        ->getErrorHandler()
        ->buildResponse($e);
});
