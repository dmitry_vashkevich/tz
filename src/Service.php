<?php

namespace Project;

/**
 * Сервисный слой приложения
 *
 * Инкапсулирует в себе логику и доступ к памяти
 */
abstract class Service
{
    /**
     * Инициализация сервиса
     *
     * @return static
     */
    public static function init()
    {
        return new static();
    }
}
