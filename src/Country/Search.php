<?php

namespace Project\Country;

use Project\Entity\Country;

/**
 * Поиск стран
 */
class Search
{
    /**
     * Поиск стран
     *
     * @return \Doctrine\Common\Collections\Collection | Country[]
     */
    public function process()
    {
        return Country::prepareList();
    }
}
