<?php

namespace Project\Exception;

/**
 * Базовое исключение, для которого установлен свой Error Handler,
 * позволяющий отдать в ответ message со статусом ответа responseCode.
 */
interface CustomResponseExceptionInterface
{
    /**
     * Получить код http ответа
     *
     * @return int
     */
    public function getResponseCode();

    /**
     * Установить код http ответа
     *
     * @param int $code
     */
    public function setResponseCode($code);

    /**
     * Получить сообщение для http ответа
     *
     * @return string
     */
    public function getResponseMessage();

    /**
     * Установить сообщение для http ответа
     *
     * @param string $message
     */
    public function setResponseMessage($message);

    /**
     * Получить билдер генерации ответа
     *
     * @return \Project\Exception\Response
     */
    public function getResponseBuilder();
}
