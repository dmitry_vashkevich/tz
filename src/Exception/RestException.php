<?php

namespace Project\Exception;

use Project\Exception\Response\JsonResponse;

/**
 * Базовый класс для исключений, позволяющих гененрировать ответы с HTTP кодами ошибок
 */
abstract class RestException extends \Exception implements CustomResponseExceptionInterface
{
    /**
     * Код Http ответа
     *
     * @var int
     */
    protected $responseCode;

    /**
     * Сообщение об ошибке
     *
     * @var string
     */
    protected $responseMessage;

    /**
     * @param string $message
     * @param int $code
     * @param \Exception $previous
     */
    public function __construct($message = "", $code = 0, \Exception $previous = null)
    {
        $this->responseMessage = $message;
        parent::__construct($message, $code, $previous);
    }

    /**
     * Установить код http ответа
     *
     * @param int $code
     */
    public function setResponseCode($code)
    {
        return;
    }

    /**
     * Получить сообщение для http ответа
     *
     * @return string
     */
    public function getResponseMessage()
    {
        return $this->responseMessage;
    }

    /**
     * Установить сообщение для http ответа
     *
     * @param string $message
     *
     * @return $this
     */
    public function setResponseMessage($message)
    {
        $this->responseMessage = $message;

        return $this;
    }

    /**
     * Получить билдер генерации ответа
     *
     * @return Response
     */
    public function getResponseBuilder()
    {
        return new JsonResponse($this);
    }
}
