<?php

namespace Project\Exception\RestException\ServerError;

use Project\Exception\RestException\ServerErrorException;

/**
 * Внутреняя ошибка Project
 */
class InternalServerErrorException extends ServerErrorException
{
    /**
     * Получить код http ответа
     *
     * @return int
     */
    public function getResponseCode()
    {
        return self::INTERNAL_SERVER_ERROR;
    }
}
