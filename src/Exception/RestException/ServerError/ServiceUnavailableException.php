<?php

namespace Project\Exception\RestException\ServerError;

use Project\Exception\RestException\ServerErrorException;

/**
 * Сервис не доступен
 */
class ServiceUnavailableException extends ServerErrorException
{

    /**
     * Получить код http ответа
     *
     * @return int
     */
    public function getResponseCode()
    {
        return self::SERVICE_UNAVAILABLE;
    }
}
