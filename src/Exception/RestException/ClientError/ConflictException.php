<?php

namespace Project\Exception\RestException\ClientError;

use Project\Exception\RestException\ClientErrorException;

/**
 * Конфликт при выполнении запроса
 */
class ConflictException extends ClientErrorException
{
    /**
     * Получить код http ответа
     *
     * @return int
     */
    public function getResponseCode()
    {
        return self::CONFLICT;
    }
}
