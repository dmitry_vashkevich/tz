<?php

namespace Project\Exception\RestException\ClientError;

use Project\Exception\RestException\ClientErrorException;

/**
 * Неприемлимое действие
 */
class NotAcceptableException extends ClientErrorException
{
    /**
     * Получить код http ответа
     *
     * @return int
     */
    public function getResponseCode()
    {
        return self::NOT_ACCEPTABLE;
    }
}
