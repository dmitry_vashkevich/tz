<?php

namespace Project\Exception\RestException\ClientError;

use Project\Exception\RestException\ClientErrorException;

/**
 * Плохой, неверный запрос
 */
class BadRequestException extends ClientErrorException
{
    /**
     * Получить код http ответа
     *
     * @return int
     */
    public function getResponseCode()
    {
        return self::BAD_REQUEST;
    }
}
