<?php

namespace Project\Exception\RestException\ClientError;

use Project\Exception\RestException\ClientErrorException;

/**
 * Метод не поддерживается
 */
class MethodNotAllowedException extends ClientErrorException
{

    /**
     * Получить код http ответа
     *
     * @return int
     */
    public function getResponseCode()
    {
        return self::METHOD_NOT_ALLOWED;
    }
}
