<?php

namespace Project\Exception\RestException\ClientError;

use Project\Exception\RestException\ClientErrorException;

/**
 * The server understood the request, but is refusing to fulfill it.
 * Authorization will not help and the request SHOULD NOT be repeated
 */
class ForbiddenException extends ClientErrorException
{
    /**
     * Получить код http ответа
     *
     * @return int
     */
    public function getResponseCode()
    {
        return self::FORBIDDEN;
    }
}
