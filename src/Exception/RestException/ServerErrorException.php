<?php

namespace Project\Exception\RestException;

use Project\Exception\RestException;

/**
 * Ошибка сервера
 */
abstract class ServerErrorException extends RestException
{
    const INTERNAL_SERVER_ERROR = 500;
    const BAD_GATEWAY = 502;
    const SERVICE_UNAVAILABLE = 503;
}
