<?php

namespace Project\Exception\RestException;

use Project\Exception\RestException;

/**
 * Ошибки клиента
 */
abstract class ClientErrorException extends RestException
{
    const BAD_REQUEST = 400;
    const UNAUTHORIZED = 401;
    const FORBIDDEN = 403;
    const NOT_FOUND = 404;
    const METHOD_NOT_ALLOWED = 405;
    const NOT_ACCEPTABLE = 406;
    const CONFLICT = 409;
}
