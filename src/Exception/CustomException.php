<?php

namespace Project\Exception;

use Project\Exception\Response\JsonResponse;

class CustomException extends \Exception implements CustomResponseExceptionInterface
{

    /**
     * @var int
     */
    private $responseCode = 0;

    /**
     * @var string
     */
    protected $responseMessage;

    /**
     * Получить код http ответа
     *
     * @return int
     */
    public function getResponseCode()
    {
        if ($this->responseCode === 0) {
            $this->responseCode = ($this->code === 0) ?: 500;
        }

        return $this->responseCode;
    }

    /**
     * Установить код http ответа
     *
     * @param int $code
     * @return CustomException
     */
    public function setResponseCode($code)
    {
        $this->responseCode = (int) $code;

        return $this;
    }

    /**
     * Получить сообщение для http ответа
     *
     * @return string
     */
    public function getResponseMessage()
    {
        if (empty($this->responseMessage)) {
            $this->responseMessage = empty($this->message) ?: 'Unknown error';
        }

        return $this->responseMessage;
    }

    /**
     * Установить сообщение для http ответа
     *
     * @param string $message
     * @return CustomException
     */
    public function setResponseMessage($message)
    {
        $this->responseMessage = $message;

        return $this;
    }

    /**
     * Получить билдер генерации ответа
     *
     * @return \Project\Exception\Response
     */
    public function getResponseBuilder()
    {
        return new JsonResponse($this);
    }
}
