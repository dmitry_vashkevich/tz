<?php

namespace Project\Exception;

use Project\Exception\Response\JsonResponse;

/**
 * Фабрика формирования ответа при обработке исключения
 */
abstract class Response
{
    /**
     * @var \Exception
     */
    protected $exception;

    /**
     * Режим запрета вывода сообщений об ошибках
     *
     * @var bool
     */
    protected $silence;

    /**
     * Фабрика создания объекта
     *
     * @param \Exception $exception
     * @param bool $silence
     * @return Response
     */
    public static function fabric(\Exception $exception, $silence)
    {
        if ($exception instanceof CustomResponseExceptionInterface) {
            $response = $exception->getResponseBuilder();
        } else {
            $response =  new JsonResponse($exception);
        }

        $response->silence = $silence;

        return $response;
    }

    /**
     * @param \Exception $exception
     */
    public function __construct(\Exception $exception)
    {
        $this->exception = $exception;
    }

    /**
     * Формирование ответа
     *
     * @return mixed
     */
    abstract public function build();
}
