<?php

namespace Project\Exception\Response;

use Project\Exception\Response;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

/**
 * Формирование строкового ответа для исключений
 */
class StringResponse extends Response
{
    /**
     * Формирование ответа
     *
     * @return mixed
     */
    public function build()
    {
        $params = new Params($this->exception, $this->silence);

        $response = (new HttpResponse($params->getMessage()));
        $response->headers->set('X-Status-Code', $params->getCode());

        return $response;
    }
}
