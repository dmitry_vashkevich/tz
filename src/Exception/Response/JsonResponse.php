<?php

namespace Project\Exception\Response;

use Project\Exception\Response;
use Project\Proxy;

/**
 * Формирование Json ответа для исключений
 */
class JsonResponse extends Response
{
    /**
     * Формирование ответа
     *
     * @return mixed
     */
    public function build()
    {
        $params = new Params($this->exception, $this->silence);

        return Proxy::init()
            ->getApp()
            ->json([
                'error' => [
                    'type'      => get_class($this->exception),
                    'message'   => $params->getMessage(),
                ],
            ], $params->getCode());
    }
}
