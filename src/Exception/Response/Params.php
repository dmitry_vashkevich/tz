<?php

namespace Project\Exception\Response;

use Project\Exception\RestException\ServerErrorException;
use Project\Exception\CustomResponseExceptionInterface;

/**
 * Параметры ответа для выброса исключения
 */
class Params
{
    /**
     * Http код ответа
     *
     * @var int
     */
    private $code;

    /**
     * Сообщение об ошибке
     *
     * @var string
     */
    private $message;

    /**
     * @param \Exception $ex
     * @param bool $silence
     */
    public function __construct(\Exception $ex, $silence)
    {
        $this->code     = ServerErrorException::INTERNAL_SERVER_ERROR;
        $this->message  = $ex->getMessage();

        if ($ex instanceof CustomResponseExceptionInterface) {
            $this->code     = $ex->getResponseCode() ?: $this->code;
            $this->message  = $ex->getResponseMessage() ?: $this->message;
        }

        if ($silence) {
            $this->message = $this->buildSilenceMessage();
        }
    }

    /**
     * Получить Http код ответа
     *
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Получить сообщение об ошибке
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Создать обобщенное сообщение об ошибке для режима подавления обычного сообщения об ошибке
     *
     * @return string
     */
    protected function buildSilenceMessage()
    {
        if ($this->code >= 400 && $this->code < 500) {
            return 'Client side error';
        }

        if ($this->code >= 500 && $this->code < 600) {
            return 'Server side error';
        }

        return 'Unknown error';
    }
}
