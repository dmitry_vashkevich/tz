<?php

namespace Project\Exception;

use Project\Proxy;

/**
 * Обработчик ошибок
 */
class ErrorHandler
{
    /**
     * Карта исключений
     *
     * @var array
     */
    private static $map = [
        'Symfony\Component\HttpKernel\Exception\HttpException' => '\Project\Exception\RestException\ClientError\BadRequestException',
        'Symfony\Component\HttpKernel\Exception\NotFoundHttpException' => '\Project\Exception\RestException\ClientError\NotFoundException',
        'Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException' => '\Project\Exception\RestException\ClientError\MethodNotAllowedException'
    ];

    /**
     * Режим запрета вывода сообщений об ошибках
     *
     * @var bool
     */
    private $silence = false;

    /**
     * Исключение
     *
     * @var \Exception
     */
    private $exception;

    /**
     * Построение ответа при исключении
     *
     * @param \Exception $ex
     * @return mixed
     */
    public function buildResponse(\Exception $ex)
    {
        $this->setException($ex);

        return Response::fabric($this->exception, $this->silence)->build();
    }

    /**
     * Установить режим запрета вывода сообщений об ошибках
     *
     * @param bool $silence
     */
    public function setSilence($silence = true)
    {
        if ($this->isSilenceEnabled()) {
            $this->silence = $silence;
        }
    }

    /**
     * Установить исключение
     *
     * @param \Exception $ex
     */
    private function setException(\Exception $ex)
    {
        $exceptionClass = get_class($ex);

        if (array_key_exists($exceptionClass, self::$map)) {
            $this->exception = new self::$map[$exceptionClass]($ex->getMessage());
        } else {
            $this->exception = $ex;
        }
    }

    /**
     * Флаг возможности режима запрета вывода сообщений об ошибках
     *
     * @return boolean
     */
    private function isSilenceEnabled()
    {
        $config = Proxy::init()->getConfig();

        return isset($config['exception']['silence_enabled']) ? (bool) $config['exception']['silence_enabled'] : false;
    }
}
