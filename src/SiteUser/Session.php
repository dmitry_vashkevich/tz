<?php

namespace Project\SiteUser;

use Project\Entity\SiteUser;

/**
 * Сессия
 */
class Session extends \Project\User\Session
{
    const PREFIX = 'site_user';

    /**
     * @inheritdoc
     */
    protected function getPrefix()
    {
        return self::PREFIX;
    }

    /**
     * @inheritdoc
     */
    protected function prepareUserById($id)
    {
        return SiteUser::prepareById($id);
    }
}
