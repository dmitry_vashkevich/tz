<?php

namespace Project\SiteUser;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Project\Entity\SiteUser;

/**
 * Поиск сайт-юзеров
 */
class Search extends \Project\User\Search 
{
    /**
     * @return \Doctrine\ORM\EntityRepository
     */
    protected function getRepo()
    {
        return SiteUser::repo();
    }

    /**
     * @param $criteria
     * @return Collection | SiteUser[]
     */
    protected function getResult(Criteria $criteria)
    {
        return SiteUser::prepareList($criteria);
    }
}
