<?php

namespace Project\SiteUser;

use Project\Entity\SiteUser;

/**
 * Регистрация
 */
class Registration extends \Project\User\Registration
{
    /**
     * @inheritdoc
     */
    protected function prepareUser()
    {
        return new SiteUser();
    }
}
