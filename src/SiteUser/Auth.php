<?php

namespace Project\SiteUser;

use Project\Entity\SiteUser;

/**
 * Регистрация
 */
class Auth extends \Project\User\Auth
{
    /**
     * @inheritdoc
     */
    protected function prepareUser()
    {
        return SiteUser::prepareByParams([
            'login' => $this->login
        ]);
    }

    /**
     * @inheritdoc
     */
    protected function getSession()
    {
        return new Session();
    }
}
