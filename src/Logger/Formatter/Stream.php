<?php

namespace Project\Logger\Formatter;

use Monolog\Formatter\LineFormatter;

/**
 * Класс форматирующий запись перед отправкой в поток
 */
class Stream extends LineFormatter
{
    /**
     * {@inheritdoc}
     */
    public function format(array $record)
    {
        return parent::format($record);
    }
}
