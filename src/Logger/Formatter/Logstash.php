<?php

namespace Project\Logger\Formatter;

use Monolog\Formatter\NormalizerFormatter;

/**
 * Класс форматирующий запись перед отправкой в logstash
 */
class Logstash extends NormalizerFormatter
{
    /**
     * Конструктор
     *
     * @param string $applicationName определяет поле type в формате logstash
     */
    public function __construct($applicationName = 'syslog')
    {
        //дата в формате, который требует logstash
        parent::__construct('Y-m-d\TH:i:s.uP');

        $this->applicationName = $applicationName;
    }

    /**
     * {@inheritdoc}
     */
    public function format(array $record)
    {
        $record = parent::format($record);

        $message = [
            '@timestamp' => $record['datetime'],
        ];

        if (isset($record['message'])) {
            $message['@message'] = $record['message'];
        }

        if (isset($record['level_name'])) {
            $message['level'] = $record['level_name'];
        }

        if (isset($record['channel'])) {
            $message['app'] = $record['channel'];
        } else {
            $message['app'] = 'Project';
        }

        if (isset($record['context'])) {
            if (isset($record['context']['X-Request-ID'])) {
                $message['X-Request-ID'] = $record['context']['X-Request-ID'];
                unset($record['context']['X-Request-ID']);
            }

            $message['context'] = $record['context'];
        }

        return $this->toJson($message) . "\n";
    }
}
