<?php

namespace Project\Logger\Formatter;

use Guzzle\Http\Message\EntityEnclosingRequestInterface;
use Guzzle\Log\MessageFormatter;
use Guzzle\Http\Curl\CurlHandle;
use Guzzle\Http\Message\RequestInterface;
use Guzzle\Http\Message\Response;

/**
 * Класс форматирующий запись в логи http запросов
 */
class Guzzle extends MessageFormatter
{
    /**
     * {@inheritdoc}
     */
    public function format(
        RequestInterface $request,
        Response $response = null,
        CurlHandle $handle = null,
        array $customData = array()
    ) {
        $cache = $customData;

        return preg_replace_callback(
            '/{\s*([A-Za-z_\-\.0-9]+)\s*}/',
            function (array $matches) use ($request, $response, $handle, &$cache) {

                if (array_key_exists($matches[1], $cache)) {
                    return $cache[$matches[1]];
                }

                $result = '';
                switch ($matches[1]) {
                    case 'request':
                        $result = (string) $request;
                        break;
                    case 'response':
                        $result = (string) $response;
                        break;
                    case 'req_body':
                        $result = $request instanceof EntityEnclosingRequestInterface
                            ? (string) $request->getBody() : '';
                        break;
                    case 'res_body':
                        if ($response) {
                            $res_body = $response->getBody(true);
                            $loglength = strlen($res_body);
                            if ($loglength > 60000) {
                                $result = 'response body > 60KB';
                            } else {
                                $result = $res_body;
                            }
                        } else {
                            $result = '';
                        }
                        break;
                    case 'ts':
                        $result = gmdate('c');
                        break;
                    case 'method':
                        $result = $request->getMethod();
                        break;
                    case 'url':
                        $result = (string) $request->getUrl();
                        break;
                    case 'resource':
                        $result = $request->getResource();
                        break;
                    case 'protocol':
                        $result = 'HTTP';
                        break;
                    case 'version':
                        $result = $request->getProtocolVersion();
                        break;
                    case 'host':
                        $result = $request->getHost();
                        break;
                    case 'hostname':
                        $result = gethostname();
                        break;
                    case 'port':
                        $result = $request->getPort();
                        break;
                    case 'code':
                        $result = $response ? $response->getStatusCode() : '';
                        break;
                    case 'phrase':
                        $result = $response ? $response->getReasonPhrase() : '';
                        break;
                    case 'connect_time':
                        $result = $handle && $handle->getInfo(CURLINFO_CONNECT_TIME)
                            ? $handle->getInfo(CURLINFO_CONNECT_TIME)
                            : ($response ? $response->getInfo('connect_time') : '');
                        break;
                    case 'total_time':
                        $result = $handle && $handle->getInfo(CURLINFO_TOTAL_TIME)
                            ? $handle->getInfo(CURLINFO_TOTAL_TIME)
                            : ($response ? $response->getInfo('total_time') : '');
                        break;
                    case 'curl_error':
                        $result = $handle ? $handle->getError() : '';
                        break;
                    case 'curl_code':
                        $result = $handle ? $handle->getErrorNo() : '';
                        break;
                    case 'curl_stderr':
                        $result =  $handle ? $handle->getStderr() : '';
                        break;
                    default:
                        if (strpos($matches[1], 'req_header_') === 0) {
                            $result = $request->getHeader(substr($matches[1], 11));
                        } elseif ($response && strpos($matches[1], 'res_header_') === 0) {
                            $result = $response->getHeader(substr($matches[1], 11));
                        }
                }

                $cache[$matches[1]] = $result;
                return $result;
            },
            $this->template
        );
    }
}
