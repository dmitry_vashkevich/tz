<?php

namespace Project\Logger;

use \Symfony\Component\Console\Event;

/**
 * Враппер логгера для события запуска консольной команды
 */
class ConsoleCommandEvent implements LoggerInterface
{
    /**
     * Тег группы
     */
    const TAG_GROUP = 'start';

    /**
     * @var \Symfony\Component\Console\Event\ConsoleCommandEvent
     */
    private $event;

    /**
     * Конструктор
     *
     * @param \Symfony\Component\Console\Event\ConsoleCommandEvent $event
     */
    public function __construct(Event\ConsoleCommandEvent $event)
    {
        $this->event = $event;
    }

    /**
     * @inheritdoc
     */
    public function getMessage($additionalInfo = '')
    {
        $message = sprintf(
            '$ %s',
            implode(' ', $_SERVER['argv'])
        );

        return $message;
    }

    /**
     * @inheritdoc
     */
    public function getContext()
    {
        return [
            'group'  => self::TAG_GROUP,
            'logger' => self::class,
        ];
    }
}
