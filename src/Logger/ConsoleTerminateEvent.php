<?php

namespace Project\Logger;

use \Symfony\Component\Console\Event;

/**
 * Враппер логгера для события запуска консольной команды
 */
class ConsoleTerminateEvent implements LoggerInterface
{
    /**
     * Тег группы
     */
    const TAG_GROUP = 'finish';

    /**
     * @var \Symfony\Component\Console\Event\ConsoleTerminateEvent
     */
    private $event;

    /**
     * Конструктор
     *
     * @param \Symfony\Component\Console\Event\ConsoleTerminateEvent $event
     */
    public function __construct(Event\ConsoleTerminateEvent $event)
    {
        $this->event = $event;
    }

    /**
     * @inheritdoc
     */
    public function getMessage($additionalInfo = '')
    {
        $message = sprintf(
            '$ %s | exited with status code %d',
            $this->event->getCommand()->getName(),
            $this->event->getExitCode()
        );

        return $message;
    }

    /**
     * @inheritdoc
     */
    public function getContext()
    {
        return [
            'group'  => self::TAG_GROUP,
            'logger' => self::class,
        ];
    }
}
