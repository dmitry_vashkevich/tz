<?php

namespace Project\Logger;

use Symfony\Component\HttpFoundation;

/**
 * Враппер над объектом Request для логгера
 */
class Request implements LoggerInterface
{
    /**
     * Тег группы при логировании запроса
     */
    const TAG_GROUP = 'start';

    /**
     * @var \Symfony\Component\HttpFoundation\Request
     */
    private $request;

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     */
    public function __construct(HttpFoundation\Request $request)
    {
        $this->request = $request;
    }

    /**
     * @inheritdoc
     */
    public function getContext()
    {
        $params = $this->request->request->all();

        return [
            'method' => $this->request->getMethod(),
            'body'   => $this->request->getContent(),
            'params' => print_r($params, true),
            'path'   => $this->request->getPathInfo(),
            'url'    => $this->request->getUri(),
            'ip'     => $this->request->getClientIp(),
            'host'   => $this->request->getHttpHost(),
            'group'  => self::TAG_GROUP,
            'logger' => self::class,
        ];
    }

    /**
     * @inheritdoc
     */
    public function getMessage($additionalInfo = '')
    {
        $message = '> ' . $this->request->getMethod() . ' ' . $this->request->getRequestUri();
        return $message;
    }
}
