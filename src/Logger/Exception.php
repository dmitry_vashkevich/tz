<?php

namespace Project\Logger;

/**
 * Враппер над объектом Exception для логгера
 */
class Exception implements LoggerInterface
{
    /**
     * Тег группы при логировании исключений
     */
    const TAG_GROUP = 'exception';

    /**
     * @var \Exception
     */
    private $exception;

    /**
     * Конструктор
     *
     * @param \Exception $exception
     */
    public function __construct(\Exception $exception)
    {
        $this->exception = $exception;
    }

    /**
     * @inheritdoc
     */
    public function getMessage($additionalInfo = '')
    {
        $message = sprintf(
            '%s: %s (%s) at %s line %s',
            get_class($this->exception),
            $this->exception->getMessage(),
            $additionalInfo,
            $this->exception->getFile(),
            $this->exception->getLine()
        );

        return $message;
    }

    /**
     * @inheritdoc
     */
    public function getContext()
    {
        return [
            'group'     => self::TAG_GROUP,
            'logger'    => self::class,
            'original_traceback'    => $this->exception->getTraceAsString(),
            'previous_traceback'    => $this->exception->getPrevious() ? $this->exception->getPrevious()->getTraceAsString() : '',
        ];
    }
}
