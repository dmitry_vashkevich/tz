<?php

namespace Project\Logger;

use Symfony\Component\HttpFoundation;

/**
 * Враппер над объектом Response для логгера
 */
class Response implements LoggerInterface
{
    /**
     * Тег группы при логировании ответа
     */
    const TAG_GROUP = 'finish';

    /**
     * @var \Symfony\Component\HttpFoundation\Response
     */
    private $response;


    /**
     * Конструктор
     *
     * @param HttpFoundation\Response $response
     */
    public function __construct(HttpFoundation\Response $response)
    {
        $this->response = $response;
    }

    /**
     * @inheritdoc
     */
    public function getContext()
    {
        return [
            'group'  => self::TAG_GROUP,
            'logger' => self::class,
        ];
    }

    /**
     * @inheritdoc
     */
    public function getMessage($additionalInfo = '')
    {
        if ($this->response instanceof HttpFoundation\RedirectResponse) {
            $message = '< ' . $this->response->getStatusCode() . ' ' . $this->response->getTargetUrl();
        } else {
            $message = '< ' . $this->response->getStatusCode() . ' ' . $this->response->getContent();
        }

        return $message;
    }
}
