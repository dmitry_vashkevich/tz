<?php

namespace Project\Logger;

/**
 * Интерфейс логирующих врапперов
 */
interface LoggerInterface
{
    /**
     * Получить отформатированное сообщение для записи в лог
     *
     * @param string $additionalInfo
     *
     * @return string
     */
    public function getMessage($additionalInfo = '');

    /**
     * Получить контекст логируемой сущности
     * @return string
     */
    public function getContext();
}
