<?php

namespace Project\Logger;

use \Symfony\Component\Console\Event;

/**
 * Враппер логгера для события ConsoleExceptionEvent
 */
class ConsoleExceptionEvent implements LoggerInterface
{
    /**
     * Тег группы
     */
    const TAG_GROUP = 'exception';

    /**
     * @var \Symfony\Component\Console\Event\ConsoleExceptionEvent
     */
    private $event;

    /**
     * Конструктор
     *
     * @param \Symfony\Component\Console\Event\ConsoleExceptionEvent $event
     */
    public function __construct(Event\ConsoleExceptionEvent $event)
    {
        $this->event = $event;
    }

    /**
     * @inheritdoc
     */
    public function getMessage($additionalInfo = '')
    {
        $command = $this->event->getCommand();
        $exception = $this->event->getException();

        $message = sprintf(
            '$ %s | exception %s: %s (uncaught exception) at %s line %s while running console command',
            $command->getName(),
            get_class($exception),
            $exception->getMessage(),
            $exception->getFile(),
            $exception->getLine()
        );

        return $message;
    }

    /**
     * @inheritdoc
     */
    public function getContext()
    {
        return [
            'group'     => self::TAG_GROUP,
            'logger'    => self::class,
            'traceback' => $this->event->getException()->getTraceAsString(),
        ];
    }
}
