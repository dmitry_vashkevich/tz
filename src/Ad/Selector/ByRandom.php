<?php

namespace Project\Ad\Selector;

use Project\Ad\Selector;

/**
 * Выбор рекламного материала случайным образом
 */
class ByRandom extends Selector
{
    /**
     * @inheritdoc
     */
    protected function getByAlg($count)
    {
        shuffle($this->cacheAds);
        return array_slice($this->cacheAds, 0, $count);
    }
}
