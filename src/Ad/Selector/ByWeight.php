<?php

namespace Project\Ad\Selector;

use Project\Ad\Selector;

/**
 * Выбор рекламного с учетом его веса
 */
class ByWeight extends Selector
{
    /** @var  array */
    protected $selectedIds = [];

    /** @var  int */
    protected $adWeightSum;

    /**
     * @inheritdoc
     */
    protected function getByAlg($count)
    {
        $result = [];
        $this->adWeightSum = $this->calcAdWeightSum();
        for ($i = 0; $i < $count; $i++) {
            $result[] = $this->getOneByWeight();
        }

        return $result;
    }

    /**
     * Вычисляет сумму весов
     *
     * @return int
     */
    protected function calcAdWeightSum()
    {
        $adWeightSum = 0;
        foreach ($this->cacheAds as $cacheAd) {
            $adWeightSum += $cacheAd['weight'];
        }

        return $adWeightSum;
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    protected function getOneByWeight()
    {
        $rand = rand(1, $this->adWeightSum);
        $localSum = 0;
        foreach ($this->cacheAds as $id => $cacheAd) {
            if (in_array($id, $this->selectedIds)) {
                continue;
            }
            
            $localSum += $cacheAd['weight'];
            if ($localSum >= $rand) {
                $this->adWeightSum -= $cacheAd['weight'];
                $this->selectedIds[] = $id;
                return $cacheAd;
            }
        }

        throw new \Exception("something wrong in ByWeight Algorithm");
    }
}
