<?php

namespace Project\Ad;

use Project\Ad\Limit\Exception\CheckLimitException;
use Project\Proxy;

/**
 * Управление лимитами для AdType
 */
abstract class Limit
{
    /**
     * интервалы по умолчанию при:
     * - отгрузке
     * - переходе
     * - закрытии
     */
    const DEFAULT_SHOW_INTERVAL = 10;
    const DEFAULT_CLICK_INTERVAL = 600;
    const DEFAULT_CLOSE_INTERVAL = 600;

    /** куки */
    const COOKIE_NAME_SHOW = 'abstract';
    const COOKIE_NAME_CLICK = 'abstract';
    const COOKIE_NAME_CLOSE = 'abstract';

    /** @var null | int */
    protected $showLimit;

    /** @var null | int */
    protected $clickLimit;

    /** @var null | int */
    protected $closeLimit;

    /** @var null | array */
    protected $blockData;
    
    /**
     * Конструктор
     *
     * @param array $blockData
     * @internal param $blockId
     */
    public function __construct(array $blockData = null)
    {
        $this->blockData = $blockData;
        $cookies = Proxy::init()->getRequest()->cookies->all();
        $this->showLimit = $this->getShowLimit($cookies);
        $this->clickLimit = $this->getClickLimit($cookies);
        $this->closeLimit = $this->getCloseLimit($cookies);
    }

    /**
     * Установка блока по факту перехода
     */
    public function setClickLimit()
    {
        $limitTime = time() + $this->getClickInterval();
        setcookie($this->getCookieName(static::COOKIE_NAME_CLICK), $limitTime, $limitTime, '/');
    }

    /**
     * Установка блока по факту отгрузки
     */
    public function setShowLimit()
    {
        $limitTime = time() + $this->getShowInterval();
        setcookie($this->getCookieName(static::COOKIE_NAME_SHOW), $limitTime, $limitTime, '/');
    }

    /**
     * Установка блока по факту закрытия
     */
    public function setCloseLimit()
    {
        $limitTime = time() + $this->getCloseInterval();
        setcookie($this->getCookieName(static::COOKIE_NAME_CLOSE), $limitTime, $limitTime, '/');
    }

    /**
     * Проверка на блок
     * @throws CheckLimitException
     */
    public function check()
    {
        if ($this->clickLimit || $this->showLimit || $this->closeLimit) {
            throw new CheckLimitException('ad show limit');
        }
    }

    /**
     * @return int
     */
    protected function getClickInterval()
    {
        if (isset($this->blockData['clickinterval'])) {
            return $this->blockData['clickinterval'];
        }
        
        return static::DEFAULT_CLICK_INTERVAL;
    }

    /**
     * @return int
     */
    protected function getCloseInterval()
    {
        if (isset($this->blockData['closeinterval'])) {
            return $this->blockData['closeinterval'];
        }
        
        return static::DEFAULT_CLOSE_INTERVAL;
    }

    /**
     * @return int
     */
    protected function getShowInterval()
    {
        return static::DEFAULT_SHOW_INTERVAL;
    }

    /**
     * @param $cookies
     * @return string | null
     */
    protected function getShowLimit($cookies)
    {
        $cookieName = $this->getCookieName(static::COOKIE_NAME_SHOW);
        return isset($cookies[$cookieName]) ? $cookies[$cookieName] : null;
    }
    
    /**
     * @param $cookies
     * @return string | null
     */
    protected function getClickLimit($cookies)
    {
        $cookieName = $this->getCookieName(static::COOKIE_NAME_CLICK);
        return isset($cookies[$cookieName]) ? $cookies[$cookieName] : null;
    }

    /**
     * @param $cookies
     * @return string | null
     */
    protected function getCloseLimit($cookies)
    {
        $cookieName = $this->getCookieName(static::COOKIE_NAME_CLOSE);
        return isset($cookies[$cookieName]) ? $cookies[$cookieName] : null;
    }
    
    /**
     * @param $prefix
     * @return string
     */
    protected function getCookieName($prefix)
    {
        if ($this->blockData !== null) {
            return $prefix . '|' . $this->blockData['id'];
        }
        
        return $prefix; 
    }
}
