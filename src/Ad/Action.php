<?php

namespace Project\Ad;

use Project\Ad\Action\Exception\UnableToLoadActionException;
use Project\Check\Validator;
use Project\Entity\Ad;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Действие объявления
 */
abstract class Action
{
    /** Типы */
    const TYPE_REDIRECT = 'Redirect';
    const TYPE_CONTENT = 'ShowContent';
    const TYPE_CLOSE = 'Close';

    const TYPES = [
        self::TYPE_REDIRECT,
        self::TYPE_CONTENT,
    ];

    const DEFAULT_TYPE_ACTION = 'Base';
    
    /** @var array */
    protected $statData;

    /**
     * Конструктор
     * @param $statData
     */
    public function __construct($statData)
    {
        $this->statData = $statData;
    }

    /**
     * Фабричный метод
     *
     * @param $action
     * @param $adtype
     * @param array $statData
     * @param string | null $uniq
     * @return static
     * @throws UnableToLoadActionException
     */
    public static function getAction($action, $adtype, array $statData, $uniq = null)
    {
        self::validate($action, $adtype);
        $overrideHandler = __NAMESPACE__ . '\\Action\\' . $action . '\\' . Ad::$typeHandlers[$adtype];
        $baseHandler = __NAMESPACE__ . '\\Action\\' . $action . '\\' . self::DEFAULT_TYPE_ACTION;
        
        if (class_exists($overrideHandler)) {
            return new $overrideHandler($statData, $uniq);
        } elseif (class_exists($baseHandler)) {
            return new $baseHandler($statData, $uniq);
        }
        
        throw new UnableToLoadActionException("action = $action, adtype = $adtype");
    }

    /**
     * Валидация
     *
     * @param $action
     * @param $adtype
     */
    private static function validate($action, $adtype)
    {
        Validator::validateValue(
            in_array($action, self::TYPES),
            new Assert\EqualTo(true),
            'unknown action'
        );

        Validator::validateValue(
            in_array($adtype, Ad::$adtypes),
            new Assert\EqualTo(true),
            'unknown adtype'
        );
    }

    /**
     * В некоторых случаях для получения дополненных данных статистики
     *
     * @return array
     */
    public function getStatData()
    {
        return $this->statData;
    }
}
