<?php

namespace Project\Ad\Action\Close;

use Project\Ad\Limit;

/**
 * @inheritdoc
 */
class Message extends Base
{
    /**
     * @inheritdoc
     */
    public function onClose(array $blockData = null)
    {
        (new Limit\Message($blockData))
            ->setCloseLimit();
    }
}
