<?php

namespace Project\Ad\Action\Close;

use Project\Ad\Limit;

/**
 * @inheritdoc
 */
class SliderBanner extends Base
{
    /**
     * @inheritdoc
     */
    public function onClose(array $blockData = null)
    {
        (new Limit\SliderBanner($blockData))
            ->setCloseLimit();
    }
}
