<?php

namespace Project\Ad\Action\Close;

use Project\Ad\Action\Close;
use Project\Stats\Queue;

/**
 * Базовый Экшен редиректа
 */
class Base extends Close
{
    /**
     * @inheritdoc
     */
    public function onClose(array $blockData = null)
    {
        //в общем случае ничего не делаем
    }
}
