<?php

namespace Project\Ad\Action;

use Project\Ad\Action;

/**
 * Экшен редиректа
 */
abstract class Redirect extends Action
{
    /**
     * Действие перед редиректом
     * @param array $blockData
     */
    abstract public function beforeRedirect(array $blockData = null);

    /**
     * Действие после редиректа
     * @param array $blockData
     */
    abstract public function afterRedirect(array $blockData = null);
}
