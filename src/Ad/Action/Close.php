<?php

namespace Project\Ad\Action;

use Project\Ad\Action;

/**
 * Экшен закрытия
 */
abstract class Close extends Action
{
    /**
     * Действие по закрытию
     * @param array $blockData
     */
    abstract public function onClose(array $blockData = null);
}
