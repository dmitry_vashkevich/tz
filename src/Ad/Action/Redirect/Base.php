<?php

namespace Project\Ad\Action\Redirect;

use Project\Ad\Action\Redirect;
use Project\Stats\Event\ClickSV;
use Project\Stats\Queue;

/**
 * Базовый Экшен редиректа
 */
class Base extends Redirect
{
    /**
     * @inheritdoc
     */
    public function beforeRedirect(array $blockData = null)
    {
    }

    /**
     * @inheritdoc
     */
    public function afterRedirect(array $blockData = null)
    {
        (new Queue())
            ->addEvent(
                new ClickSV(
                    $this->statData
                )
            );
    }
}
