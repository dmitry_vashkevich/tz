<?php

namespace Project\Ad\Action\Redirect;

use Project\Ad\Limit;

/**
 * @inheritdoc
 */
class SliderBanner extends Base
{
    /**
     * @inheritdoc
     */
    public function beforeRedirect(array $blockData = null)
    {
        (new Limit\SliderBanner($blockData))
            ->setClickLimit();
    }
}
