<?php

namespace Project\Ad\Action\Redirect;

use Project\Ad\Limit;

/**
 * @inheritdoc
 */
class Clickunder extends Base
{
    /**
     * @inheritdoc
     */
    public function beforeRedirect(array $blockData = null)
    {
        (new Limit\Clickunder($blockData))
            ->setClickLimit();
    }
}
