<?php

namespace Project\Ad\Action;

use Project\Ad\Action;
use Project\Ad\Selector;
use Project\Ad\View;
use Project\Campaign\Limit;
use Project\Proxy;
use Project\Stats\Event;
use Project\Stats\Queue;

/**
 * Экшен показа контента
 */
abstract class ShowContent extends Action
{
    /** урлы */
    const URL_REDIRECT_PATH = '/redirect/'; 
    const URL_CLOSE_PATH = '/close/';

    /** обычное кол-во выбираемого рекламного материала */
    const DEFAULT_AD_COUNT = 1;

    /**
     * Отобранные для показа рекламные материалы
     *
     * @var array
     */
    protected $ads;

    /**
     * Параметры блока
     *
     * @var array
     */
    protected $blockParams;

    /**
     * Конфиг блока
     *
     * @var array
     */
    protected $blockConfig;

    /**
     * Действия при запросе контента
     *
     * @param array $blockData
     * @param $eventUniq
     */
    public function requestBlockQueryStart(array $blockData = null, $eventUniq)
    {
        (new Queue())
            ->addEvent(
                new Event\RequestBlockQuery(
                    $this->statData,
                    null,
                    $eventUniq
                )
            );
    }

    /**
     * Действия при отгрузке контента
     *
     * @param array $blockData
     * @param $eventUniq
     * @param Limit $limit
     */
    public function requestBlockQueryFinish(array $blockData = null, $eventUniq, Limit $limit)
    {
        $limit->setUserShowLimit($this->ads);
        $this->sendStats($eventUniq);
    }

    /**
     * Отправка статистики по окончанию запроса
     *
     * @param $eventUniq
     */
    private function sendStats($eventUniq)
    {
        $queue = new Queue();

        $queue
            ->addEvent(
                new Event\RequestBlock(
                    $this->statData,
                    null,
                    $eventUniq
                )
            );

        foreach ($this->ads as $ad) {
            $queue
                ->addEvent(
                    new Event\Show(
                        $ad['statData']
                    )
                );
        }
    }
    
    /**
     * Действия по подготовке контента
     *
     * @param array $cacheAds
     * @param array $requestData
     * @param array $blockData
     * @return string
     */
    public function prepareContent(array $cacheAds, array $requestData, array $blockData = null)
    {
        $this->prepareAdParams($requestData, $blockData);
        $this->prepareAds($cacheAds);

        return $this->getView(
            $this->prepareViewParams()
        )->generate();
    }

    /**
     * @param array $cacheAds
     */
    protected function prepareAds(array $cacheAds)
    {
        //отобрали нужное кол-во рекламных материалов
        $ads = $this->getAdsByCount($cacheAds);
        
        //собираем урлы пеехода, и зашиваем в них стат-параметры
        foreach ($ads as &$ad) {
            $ad['statData'] += $this->statData;
            $urlData = base64_encode(
                json_encode([
                    'statData' => $ad['statData'],
                    'url' => $ad['campaignUrl'],
                    'referer' => isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER']: null,
                ])
            );
            
            $ad['url'] = Proxy::init()->getConfig()['selfUrl'] . self::URL_REDIRECT_PATH . $urlData;
            $ad['url_close'] = Proxy::init()->getConfig()['selfUrl'] . self::URL_CLOSE_PATH . $urlData;
        }

        $this->ads = $ads;
    }

    /**
     * @return array
     */
    protected function prepareViewParams()
    {
        return [
            'ads' => $this->ads,
            'params' => $this->blockParams,
        ];
    }

    /**
     * @return array
     */
    public function getAds()
    {
        return $this->ads;
    }

    /**
     * @param $requestData
     * @param $blockData
     * @return array
     */
    protected function prepareAdParams(array $requestData, array $blockData = null)
    {
        if ($blockData !== null && $blockData['custom'] === true && $blockData['params'] !== null) {
            $this->blockParams = json_decode($blockData['params'], true);
        }
    }

    /**
     * Получить View
     * @param $viewParams
     * @return View
     */
    abstract protected function getView(array $viewParams);

    /**
     * Получить нужное кол-во рекламных материалов
     *
     * @param array $cacheAds
     * @return array
     */
    protected function getAdsByCount(array $cacheAds)
    {
        return (new Selector\ByWeight($cacheAds))->get(
            $this->prepareRequiredNumber($cacheAds)
        );
    }

    /**
     * @param array $cacheAds
     * @return int
     */
    protected function prepareRequiredNumber(array $cacheAds)
    {
        return self::DEFAULT_AD_COUNT;
    }
}
