<?php

namespace Project\Ad\Action\ShowContent;

use Project\Ad\Action;
use Project\Ad\Limit;
use Project\Ad\View;
use Project\Campaign\Limit as CampaignLimit;

/**
 * @inheritdoc
 */
class Cashback extends Action\ShowContent
{
    /**
     * @inheritdoc
     */
    public function requestBlockQueryStart(array $blockData = null, $eventUniq)
    {
        parent::requestBlockQueryStart($blockData, $eventUniq);
        (new Limit\Cashback($blockData))
            ->check();
    }

    /**
     * @inheritdoc
     */
    public function requestBlockQueryFinish(array $blockData = null, $eventUniq, CampaignLimit $limit)
    {
        parent::requestBlockQueryFinish($blockData, $eventUniq, $limit);
        (new Limit\Cashback($blockData))
            ->setShowLimit();
    }
    
    /**
     * @inheritdoc
     */
    protected function getView(array $viewParams)
    {
        return new View\Cashback($viewParams);
    }
}
