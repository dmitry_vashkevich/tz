<?php

namespace Project\Ad\Action\ShowContent;

use Project\Ad\Action;
use Project\Ad\Limit;
use Project\Campaign\Limit as CampaignLimit;
use Project\Ad\View;

/**
 * @inheritdoc
 */
class Clickunder extends Action\ShowContent
{
    /**
     * @inheritdoc
     */
    public function requestBlockQueryStart(array $blockData = null, $eventUniq)
    {
        parent::requestBlockQueryStart($blockData, $eventUniq);
        (new Limit\Clickunder($blockData))
            ->check();
    }

    /**
     * @inheritdoc
     */
    protected function getView(array $viewParams)
    {
        return new View\Clickunder($viewParams);
    }

    /**
     * @inheritdoc
     */
    public function requestBlockQueryFinish(array $blockData = null, $eventUniq, CampaignLimit $limit)
    {
        parent::requestBlockQueryFinish($blockData, $eventUniq, $limit);
        (new Limit\Clickunder($blockData))
            ->setShowLimit();
    }
}
