<?php

namespace Project\Ad\Action\ShowContent\Teaser;

use Project\Check\Validator;
use Project\TeaserConfig\SearchByBlock;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Выбрать конфиг для тизера в размер блока
 */
class ConfigSelector
{
    /** @var  int */
    protected $width;

    /** @var  int */
    protected $height;

    /**
     * Конструктор
     *
     * @param $width
     * @param $height
     */
    public function __construct($width, $height)
    {
        $this->width = $width;
        $this->height = $height;
    }

    /**
     * @return string
     */
    public function process()
    {
        $configs = (new SearchByBlock\Cache($this->width, $this->height))->process();

        Validator::validateValue(
            count($configs),
            new Assert\GreaterThan(0),
            'unable to auto-find configs'
        );

        //выбираем случайный размер
        $size = $configs[rand(0, count($configs) - 1)];

        //и возвращаем для размера случайный конфиг
        //с параметрами ячейки
        $config = $size['configs'][rand(0, count($size['configs']) - 1)];
        $config['col'] = $size['col'];
        $config['row'] = $size['row'];
        $config['cell_width'] = $size['cellWidth'];
        $config['cell_height'] = $size['cellHeight'];

        return $config;
    }
}
