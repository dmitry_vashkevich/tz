<?php

namespace Project\Ad\Action\ShowContent\Teaser;

use Project\Exception\RestException\ClientError\BadRequestException;

/**
 * Когда неизвестны размеры области для отдачи контента тизеров
 */
class UnknownSizeException extends BadRequestException
{

}
