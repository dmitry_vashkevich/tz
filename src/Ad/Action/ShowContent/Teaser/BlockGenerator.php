<?php

namespace Project\Ad\Action\ShowContent\Teaser;

/**
 * Генератор случайных тизерных блоков
 */
class BlockGenerator
{
    protected $blockBorderColors = [
        'transparent',
        '#F1C40F',
        '#7F8C8D',
        '#000000',
        '#2980B9',
    ];

    protected $cellBorderColors = [
        'transparent',
        '#F1C40F',
        '#7F8C8D',
        '#000000',
        '#2980B9',
    ];

    protected $btnColors = [
        '#E74C3C',
        '#27AE60',
        '#2980B9',
        '#F39C12',
        '#7F8C8D',
    ];

    protected $headerColors = [
        '#E74C3C',
        '#27AE60',
        '#2980B9',
        '#F39C12',
        '#000000',
    ];

    protected $txtColors = [
        '#000000',
        '#2C3E50',
        '#7F8C8D',
        '#2980B9',
    ];

    protected $headerStyles = [
        ['bold'],
        ['bold', 'underlined'],
        ['bold', 'italic'],
        ['bold', 'italic', 'underlined'],
    ];

    protected $txtStyles = [
        ['common'],
        ['italic'],
    ];

    protected $blockBgColors = [
        '#ffffff',
        '#e5f4ff',
        '#e5ffed',
        '#fffdda',
        '#fceeff',
    ];

    protected $cellBgColors = [
        '#ffffff',
        '#e5f4ff',
        '#e5ffed',
        '#fffdda',
        '#fceeff',
    ];

    /**
     * Получить параметры случайныс образом
     *
     * @return array
     */
    public function process()
    {
        return [
            'blockBorderColor' => $this->blockBorderColors[rand(0, count($this->blockBorderColors) - 1)],
            'cellBorderColor' => $this->cellBorderColors[rand(0, count($this->cellBorderColors) - 1)],
            'imgBorderColor' => '#BDC3C7',
            'btnColor' => $this->btnColors[rand(0, count($this->btnColors) - 1)],
            'headerColor' => $this->headerColors[rand(0, count($this->headerColors) - 1)],
            'txtColor' => $this->txtColors[rand(0, count($this->txtColors) - 1)],
            'headerStyle' => $this->headerStyles[rand(0, count($this->headerStyles) - 1)],
            'txtStyle' => $this->txtStyles[rand(0, count($this->txtStyles) - 1)],
            'blockBgColor' => $this->blockBgColors[rand(0, count($this->blockBgColors) - 1)],
            'cellBgColor' => $this->cellBgColors[rand(0, count($this->cellBgColors) - 1)],
        ];
    }

    /**
     * @param array $randBlockParams
     * @return array
     */
    public function getRBPStatData(array $randBlockParams)
    {
        $headerStyle = '';
        if (in_array('bold', $randBlockParams['headerStyle'])) {
            $headerStyle .= 'b';
        }

        if (in_array('italic', $randBlockParams['headerStyle'])) {
            $headerStyle .= 'i';
        }

        if (in_array('underlined', $randBlockParams['headerStyle'])) {
            $headerStyle .= 'u';
        }

        $randBlockParams['headerStyle'] = $headerStyle;

        $txtStyle = 'c';
        if (in_array('italic', $randBlockParams['txtStyle'])) {
            $txtStyle = 'i';
        }

        $randBlockParams['txtStyle'] = $txtStyle;

        return array_change_key_case($randBlockParams, CASE_LOWER);
    }
}
