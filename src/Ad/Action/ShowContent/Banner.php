<?php

namespace Project\Ad\Action\ShowContent;

use Project\Ad\Action;
use Project\Ad\Limit;
use Project\Ad\View;
use Project\Check\Validator;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @inheritdoc
 */
class Banner extends Action\ShowContent
{
    /** @var int */
    protected $width;

    /** @var int */
    protected $height;

    /**
     * @inheritdoc
     */
    public function prepareAdParams(array $requestData, array $blockData = null)
    {
        $this->width = isset($blockData['width']) ? $blockData['width'] : $requestData['width'];
        $this->height = isset($blockData['height']) ? $blockData['height'] : $requestData['height'];
    }

    /**
     * @inheritdoc
     */
    protected function prepareAds(array $cacheAds)
    {
        $filtered = [];
        foreach ($cacheAds as $cacheAd) {
            if (
                $cacheAd['blockWidthFrom'] <= $this->width &&
                $cacheAd['blockWidthTo'] >= $this->width &&
                $cacheAd['blockHeightFrom'] <= $this->height &&
                $cacheAd['blockHeightTo'] >= $this->height
            ) {
                $filtered[] = $cacheAd;
            }
        }

        Validator::validateValue(
            count($filtered),
            new Assert\GreaterThan(0),
            'unable to find cacheAds by size'
        );

        parent::prepareAds($filtered);
    }

    /**
     * @inheritdoc
     */
    protected function getView(array $viewParams)
    {
        return new View\Banner($viewParams);
    }
}
