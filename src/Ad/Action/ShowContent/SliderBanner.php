<?php

namespace Project\Ad\Action\ShowContent;

use Project\Ad\Action;
use Project\Ad\Limit;
use Project\Campaign\Limit as CampaignLimit;
use Project\Ad\View;

/**
 * @inheritdoc
 */
class SliderBanner extends Action\ShowContent
{
    /**
     * @inheritdoc
     */
    public function requestBlockQueryStart(array $blockData = null, $eventUniq)
    {
        parent::requestBlockQueryStart($blockData, $eventUniq);
        (new Limit\SliderBanner($blockData))
            ->check();
    }

    /**
     * @inheritdoc
     */
    protected function getView(array $viewParams)
    {
        return new View\SliderBanner($viewParams);
    }

    /**
     * @inheritdoc
     */
    public function requestBlockQueryFinish(array $blockData = null, $eventUniq, CampaignLimit $limit)
    {
        parent::requestBlockQueryFinish($blockData, $eventUniq, $limit);
        (new Limit\SliderBanner($blockData))
            ->setShowLimit();
    }
}
