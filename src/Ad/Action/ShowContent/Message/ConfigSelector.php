<?php

namespace Project\Ad\Action\ShowContent\Message;

use Project\Cache\EntityCache;

/**
 * Выбрать конфиг для месседжа
 */
class ConfigSelector
{
    /**
     * @return string
     */
    public function process()
    {
        $configs = (new EntityCache\MessageConfig())->getAll();

        //выбираем случайный
        return $configs[rand(0, count($configs) - 1)];
    }
}
