<?php

namespace Project\Ad\Action\ShowContent;

use Project\Ad\Action;
use Project\Ad\Limit;
use Project\Ad\View;
use Project\Ad\Action\ShowContent\Teaser\BlockGenerator;
use Project\Ad\Action\ShowContent\Teaser\ConfigSelector;
use Project\Check\Validator;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @inheritdoc
 */
class Teaser extends Action\ShowContent
{
    /** @var int */
    protected $width;

    /** @var int */
    protected $height;

    /**
     * @inheritdoc
     */
    protected function prepareAdParams(array $requestData, array $blockData = null)
    {
        $this->validateAdParams($requestData, $blockData);
        $this->width = isset($blockData['width']) ? $blockData['width'] : $requestData['width'];
        $this->height = isset($blockData['height']) ? $blockData['height'] : $requestData['height'];
        $this->prepareBlockParamsAndConfig($blockData);
    }

    /**
     * @param array $requestData
     * @param array|null $blockData
     * @throws Teaser\UnknownSizeException
     */
    private function validateAdParams(array $requestData, array $blockData = null)
    {
        if (
            (!isset($blockData['width']) && !isset($requestData['width']))
            ||
            (!isset($blockData['height']) && !isset($requestData['height']))
        ) {
            throw new Action\ShowContent\Teaser\UnknownSizeException(
                "unable to get size from block or request data"
            );
        }
    }

    /**
     * @param $blockData
     * @return array
     */
    protected function prepareBlockParamsAndConfig(array $blockData = null)
    {
        $blockGenerator = new BlockGenerator();
        if ($blockData !== null && $blockData['custom'] === true && $blockData['params'] !== null) {
            $this->statData['config_id'] = 0;
            $this->blockParams = json_decode($blockData['params'], true);
            $this->blockConfig = json_decode($blockData['config'], true);
        } else {
            $this->blockParams = $blockGenerator->process();
            $configSelector = new ConfigSelector($this->width, $this->height);
            $this->blockConfig = $configSelector->process();
            $this->statData['config_id'] = $this->blockConfig['id'];
        }

        $RBPStatData = $blockGenerator->getRBPStatData($this->blockParams);
        $this->statData += $RBPStatData;
    }

    /**
     * @inheritdoc
     */
    protected function prepareRequiredNumber(array $cacheAds)
    {
        return $this->blockConfig['row'] * $this->blockConfig['col'];
    }

    /**
     * @inheritdoc
     */
    protected function prepareViewParams()
    {
        return [
            'width' => $this->width,
            'height' => $this->height,
            'row' => $this->blockConfig['row'],
            'col' => $this->blockConfig['col'],
            'cell_width' => $this->blockConfig['cell_width'],
            'cell_height' => $this->blockConfig['cell_height'],
            'config' => $this->blockConfig,
            'ads' => $this->ads,
            'params' => $this->blockParams,
        ];
    }

    /**
     * @inheritdoc
     */
    protected function getView(array $viewParams)
    {
        return new View\Teaser($viewParams);
    }
}
