<?php

namespace Project\Ad\Action\ShowContent;

use Project\Ad\Action;
use Project\Ad\Limit;
use Project\Campaign\Limit as CampaignLimit;
use Project\Ad\View;

/**
 * @inheritdoc
 */
class Popunder extends Action\ShowContent
{
    /**
     * @inheritdoc
     */
    public function requestBlockQueryStart(array $blockData = null, $eventUniq)
    {
        parent::requestBlockQueryStart($blockData, $eventUniq);
        (new Limit\Popunder($blockData))
            ->check();
    }

    /**
     * @inheritdoc
     */
    protected function getView(array $viewParams)
    {
        throw new \Exception('undefined popunder view');
    }

    /**
     * @inheritdoc
     */
    public function requestBlockQueryFinish(array $blockData = null, $eventUniq, CampaignLimit $limit)
    {
        parent::requestBlockQueryFinish($blockData, $eventUniq, $limit);
        (new Limit\Popunder($blockData))
            ->setShowLimit();
    }
}
