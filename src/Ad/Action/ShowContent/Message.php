<?php

namespace Project\Ad\Action\ShowContent;

use Project\Ad\Action;
use Project\Ad\Limit;
use Project\Campaign\Limit as CampaignLimit;
use Project\Ad\View;
use Project\Ad\Action\ShowContent\Message\ConfigSelector;

/**
 * @inheritdoc
 */
class Message extends Action\ShowContent
{
    /**
     * @return array
     */
    protected function prepareViewParams()
    {
        return [
            'config' => (new ConfigSelector())->process(),
            'ads' => $this->ads,
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function requestBlockQueryStart(array $blockData = null, $eventUniq)
    {
        parent::requestBlockQueryStart($blockData, $eventUniq);
        (new Limit\Message($blockData))
            ->check();
    }

    /**
     * @inheritdoc
     */
    protected function getView(array $viewParams)
    {
        return new View\Message($viewParams);
    }


    /**
     * @inheritdoc
     */
    public function requestBlockQueryFinish(array $blockData = null, $eventUniq, CampaignLimit $limit)
    {
        parent::requestBlockQueryFinish($blockData, $eventUniq, $limit);
        (new Limit\Message($blockData))
            ->setShowLimit();
    }
}
