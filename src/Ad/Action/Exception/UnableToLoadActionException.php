<?php

namespace Project\Ad\Action\Exception;

use Project\Exception\RestException\ServerError\InternalServerErrorException;

/**
 * При невозможности загрузить экшен
 */
class UnableToLoadActionException extends InternalServerErrorException
{

}
