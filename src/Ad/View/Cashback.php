<?php

namespace Project\Ad\View;

use Project\Ad\View;
use Project\Proxy;

/**
 * HTML слайдер-баннера
 */
class Cashback extends View
{
    /**
     * @inheritdoc
     */
    public function generate()
    {
        return $this->_generate(
            $this->params['ads'][0]
        );
    }

    /**
     * @param $ad
     * @return string
     */
    private function _generate($ad)
    {
        return '(function(){window.location=\''.$ad['url'].'\';})();';
    }
}