<?php

namespace Project\Ad\View;

use Project\Ad\View;

/**
 * HTML баннера
 */
class Banner extends View
{
    /**
     * @inheritdoc
     */
    public function generate()
    {
        return $this->_generate(
            $this->params['ads'][0]
        );
    }
    
    /**
     * @param $ad
     * @return string
     */
    private function _generate($ad)
    {
        ob_start();
        var_dump($ad);
        $str = ob_get_clean();
        return $str;
    }
}
