<?php

namespace Project\Ad\View;

use Project\Ad\View;
use Project\Proxy;

/**
 * HTML слайдер-баннера
 */
class SliderBanner extends View
{
    /**
     * @inheritdoc
     */
    public function generate()
    {
        return $this->_generate(
            $this->params['ads'][0],
            $this->params['params']
        );
    }

    /**
     * @param $ad
     * @param $params
     * @return string
     */
    private function _generate($ad, $params)
    {
        $str = '';
        if ($ad['campaignScript'] !== null) {
            $str .= '
            (function() {
                function e(data) {
                    var head = document.getElementsByTagName("head")[0] || document.documentElement,
                        script = document.createElement("script");
                    script.type = "text/javascript";
                    script.appendChild(document.createTextNode(data));
                    head.insertBefore(script, head.firstChild);
                }
                var div = document.createElement(\'div\');
                div.innerHTML = ' . json_encode($ad['campaignScript']) . ';
                var elements = div.childNodes;
                for (var i=0;i<elements.length;i++) {
                    var element = elements[i];
                    if (element.nodeName.toLowerCase() === \'script\' && element.getAttribute(\'src\') === null) {
                        e(element.innerHTML);
                    } else {
                        document.body.appendChild(element);
                    }
                }
            })();
            ';
        } else {
            $content_images = json_decode($ad['images'], true);
            $image = Proxy::init()->getConfig()['selfUrl'] . '/' . $content_images[0];

            $data = json_decode($params['data'], true);

            $str .= '(function () {
            var block = document.createElement(\'div\');
            block.innerHTML = \'<div \' +
                \'style="margin:0px !important;top: auto !important;left:auto !important;position:fixed !important;\' +
                \'' . ($data['place_block'] === 'bottom_left' ? 'left' : 'right') . ':20px !important;\' +
                \'bottom:20px !important;height:270px !important;width:320px !important;border:1px solid #fff !important;border-radius:5px !important;-webkit-box-shadow: 0px 0px 5px 1px rgba(0,0,0,0.5) !important;-moz-box-shadow: 0px 0px 5px 1px rgba(0,0,0,0.5) !important;box-shadow: 0px 0px 5px 1px rgba(0,0,0,0.5) !important;';
            if ($data['show_animate']) {
                $str .= '-webkit-transition: all 1s ease-out 0.5s !important; -moz-transition: all 1s ease-out 0.5s !important;-o-transition: all 1s ease-out 0.5s !important;transition: all 1s ease-out 0.5s !important;';
            }
            $str .= 'opacity:0 !important;">\' +
                \'<div \' +
                \'style="text-shadow: 0 1px #fff !important;position: absolute !important;right:4px !important;top:-30px !important;color:#969696;font-family: Verdana, Arial, \\\'Helvetica CY\\\', \\\'Nimbus Sans L\\\', sans-serif !important;font-size: 18px !important;opacity:' . ($data['close_disable'] ? 0 : 1) . ' !important;">\' +
                \'' . intval($data['close_delay']) . '\' +
                \'</div>\' +
                \'<div \' +
                \'onclick="this.parentNode.parentNode.removeChild(this.parentNode)" \' +
                \'style="cursor:pointer;display:none;text-shadow: 0 1px #fff !important;position: absolute !important;right:4px !important;top:-25px !important;color:#969696 !important;font-family: Verdana, Arial, \\\'Helvetica CY\\\', \\\'Nimbus Sans L\\\', sans-serif !important;font-size: 18px !important;">\' +
                \'<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"\' +
                \'preserveAspectRatio="xMidYMid" width="14" height="15" viewBox="0 0 16 17">\' +
                \'<image \' +
                \'xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAARCAMAAADjcdz2AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAA0lBMVEUAAACWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpacnJyXl5eXl5ednZ3q6uqWlpbt7e3////r6+uYmJjt7e3////q6uqWlpb///+Xl5eWlpaXl5ft7e3q6urHx8fMzMyWlpaZmZmampqWlpaWlpaYmJiWlpbt7e3////q6urt7e3////////p6emWlpaWlpbs7Oz///+WlpaYmJj///+ZmZn////Ly8v////Pz8////////////+Wlpafn5+hoaGgoKCenp7Nzc2dnZ2kpKSioqIAAAC6vts1AAAAPHRSTlMABAOG7TM67njz8fTr6zLgLu7x6SfuMS/whPPq7/XxOfj0LzjuN+t68OwpMfEuNuwyAe8q5QH1M+3e4m1rInWVAAAAAWJLR0QAiAUdSAAAAAlwSFlzAAALEgAACxIB0t1+/AAAAKNJREFUGNNNzYcOgkAQRdFnQ0XFBqLYK4rYC5bB/v/f5ICwuMkmc262AIghWnHeiWRKWEpngCzJYZFylAcKLslK6FsRKJVdqiiBqyoPJY1LzfPdN6BrD6o3DLo31eAxvfUg+jOXNodOZHR7HPoD4SHff75oFJaxQe7EnIrieWZhbnNZsJfslcXD+lc2W/bOP+qVPQ7Hd2DAsT8nwDzvxHfO5foFwyEYD1dXYLsAAAAASUVORK5CYII=" \' +
                \'width="16" height="17"/>\' +
                \'</svg>\' +
                \'</div>\' +
                \'<div \' +
                \'style="overflow:hidden !important;width:320px !important;height:270px !important;border-radius:5px !important">\' +
                \'<a href="' . $ad['url'] . '" target="_blank"><img src="' . $image . '" alt=""/></a>\' +
                \'</div>\' +
                \'</div>\';
            document.body.appendChild(block);';

            $str .= 'setTimeout(function () {
                block.getElementsByTagName(\'div\')[0].style.setProperty("opacity", "1", "important");';
            if (!$data['close_disable']) {

                $str .= 'var countdown = block.getElementsByTagName(\'div\')[1];
                var countdown_value = parseInt(countdown.innerHTML);
                setTimeout(function interval() {
                    countdown_value--;
                    if (!countdown_value) {
                        countdown.style.display = \'none\';
                        block.getElementsByTagName(\'div\')[2].style.display = \'block\';
                    } else {
                        countdown.innerHTML = countdown_value;
                        setTimeout(interval, 1000);
                    }
                }, 1000);';
            }
            $str .= '}, ' . (intval($data['show_delay']) * 1000 + 50) . ');
        })();';

        }
        return $str;
    }
}