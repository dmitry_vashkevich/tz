<?php

namespace Project\Ad\View;

use Project\Ad\View;
use Project\Proxy;

/**
 * HTML тизерного блока 
 */
class Teaser extends View
{

    /**
     * @inheritdoc
     */
    public function generate()
    {
        return $this->_generate(
            $this->params['width'],
            $this->params['height'],
            $this->params['row'],
            $this->params['col'],
            $this->params['cell_width'],
            $this->params['cell_height'],
            $this->params['config'],
            $this->params['ads'],
            $this->params['params']
        );
    }
    
    /**
     * @param $width
     * @param $height
     * @param $row
     * @param $col
     * @param $cell_width
     * @param $cell_height
     * @param $config
     * @param $ads
     * @param $params
     * @return string
     */
    private function _generate($width, $height, $row, $col, $cell_width, $cell_height, $config, $ads, $params)
    {
        $padding_between_cell = 4;
        $border_block = 1;
        $data = json_decode($config['data']);

        $captionTopParams = '
    onmouseover="this.style.color=\'' . $params['headerColor'] . '\'"
    onmouseout="this.style.color=\'' . $params['headerColor'] . '\'"
    style="line-height: ' . $data->lineheight_title . 'em;
    padding-bottom:' . $data->padding_title . 'px;
    color:' . $params['headerColor'] . ';
    font-family: ' . $data->font_title->id . ';
    font-weight: ' . (in_array('bold', $params['headerStyle']) ? 'bold' : 'normal') . ';
    font-style: ' . (in_array('italic', $params['headerStyle']) ? 'italic' : 'normal') . ';
    text-decoration: ' . (in_array('underlined', $params['headerStyle']) ? 'underline' : 'none') . ';
    font-size:' . $data->size_title . 'px;
    ';

        $captionBottomParams = '
    onmouseover="this.style.color=\'' . $params['txtColor'] . '\'"
    onmouseout="this.style.color=\'' . $params['txtColor'] . '\'"
    style="line-height: ' . $data->lineheight_text . 'em;
    padding-bottom:' . $data->padding_text . 'px;
    color:' . $params['txtColor'] . ';
    font-family: ' . $data->font_text->id . ';
    font-weight: ' . (in_array('bold', $params['txtStyle']) ? 'bold' : 'normal') . ';
    font-style: ' . (in_array('italic', $params['txtStyle']) ? 'italic' : 'normal') . ';
    text-decoration: ' . (in_array('underlined', $params['txtStyle']) ? 'underline' : 'none') . ';
    font-size:' . $data->size_text . 'px;
    ';

        $moreParams = '
    onmouseover="this.style.color=\'' . $data->color_button_hover . '\'"
    onmouseout="this.style.color=\'' . $data->color_button . '\'"
    style="
                    cursor:pointer;
                    border-top-left-radius:' . $data->button_border_radius_top . 'px;
                    border-top-right-radius:' . $data->button_border_radius_top . 'px;
                    border-bottom-left-radius:' . $data->button_border_radius_bottom . 'px;
                    border-bottom-right-radius:' . $data->button_border_radius_bottom . 'px;
                    text-align:center;
                    color:' . $data->color_button . ';
                    background-color:' . $params['btnColor'] . ';
                    width:' . $data->button_width . 'px;
                    height:' . $data->button_height . 'px;
                    line-height: ' . $data->lineheight_button . 'em;
                    font-family: ' . $data->font_button->id . ';
                    font-weight: ' . ($data->face_button->bold ? 'bold' : 'normal') . ';
                    font-style: ' . ($data->face_button->italic ? 'italic' : 'normal') . ';
                    text-decoration: ' . ($data->face_button->underline ? 'underline' : 'none') . ';
                    font-size:' . $data->size_button . 'px;
                    border-style:' . $data->border_button->border->id . ';
                    border-width:' . $data->border_button->size . 'px;
                    border-color:' . $data->border_button->color . ';';

        if ($data->place_text->id === 'right' || $data->place_title->id === 'right') {
            if ($data->place_button->id === 'top_title') {
                $moreParams .= '
            display:block;
            position:relative;
            margin-bottom:' . $data->padding_button . 'px;
            left: ' . (($data->align_button->id === 'center' || $data->align_button->id === 'center_stick') ? '50%' : 'auto') . ';
            float: ' . (($data->align_button->id === 'right' || $data->align_button->id === 'right_stick') ? 'right' : 'none') . ';
            margin-left:' . (($data->align_button->id === 'center' || $data->align_button->id === 'center_stick') ? '-' . round($data->button_width / 2) . 'px' : '0') . ';
            ';
            }

            if ($data->place_button->id === 'bottom_text') {
                $moreParams .= '
            display:block;
            position:absolute;
            margin-top:' . $data->padding_button . 'px;
            margin-left:' . (($data->align_button->id === 'center' || $data->align_button->id === 'center_stick') ? '-' . round($data->button_width / 2) . 'px' : '0') . ';
            left: ' . (($data->align_button->id === 'center' || $data->align_button->id === 'center_stick') ? '50%' : 'auto') . ';
            right: ' . (($data->align_button->id === 'right' || $data->align_button->id === 'right_stick') ? $data->padding_inside_cell . 'px' : 'auto') . ';
            bottom: ' . (($data->align_button->id === 'left_stick' || $data->align_button->id === 'right_stick' || $data->align_button->id === 'center_stick') ? $data->padding_inside_cell . 'px' : 'auto') . ';
            ';
            }
        }
        if ($data->place_button->id === 'top_title' && ($data->place_title->id === 'top' && $data->place_text->id !== 'right')) {
            $moreParams .= '
            display:inline-block;
            margin-top:' . $data->padding_button . 'px;
            margin-bottom:' . $data->padding_button . 'px;
            ';

            $captionTopParams .= 'padding-bottom:' . $data->padding_title . 'px;text-align:center;';
        }
        if ($data->place_title->id === 'bottom' && $data->place_text->id !== 'right') {
            $captionTopParams .= 'padding-bottom:' . $data->padding_title . 'px;padding-top:' . $data->padding_title . 'px;text-align:center;';
        }
        if ($data->place_text->id === 'top') {
            $captionBottomParams .= 'padding-bottom:' . $data->padding_text . 'px;text-align:center;';
        }
        if ($data->place_text->id === 'bottom') {
            $captionBottomParams .= 'padding-top:' . $data->padding_text . 'px;text-align:center;';
        }
        if ($data->place_button->id === 'top_image' || ($data->place_button->id === 'bottom_text' && $data->place_text->id === 'top')) {
            $moreParams .= '
            display:inline-block;
            margin-top:' . $data->padding_button . 'px;
            margin-bottom:' . $data->padding_button . 'px;
            ';
        }
        if ($data->place_button->id === 'bottom_image' || ($data->place_button->id === 'top_title' && ($data->place_title->id === 'bottom' && $data->place_text->id !== 'right'))) {
            $moreParams .= '
            display:inline-block;
            margin-top:' . $data->padding_button . 'px;
            ';
        }
        if ($data->place_button->id === 'bottom_text' && $data->place_text->id === 'bottom') {
            $moreParams .= '
            position:absolute;
              margin-top:' . $data->padding_button . 'px;
            margin-left:' . (($data->align_button->id === 'center' || $data->align_button->id === 'center_stick') ? '-' . round($data->button_width / 2) . 'px' : '0') . ';
            left: ' . (($data->align_button->id === 'center' || $data->align_button->id === 'center_stick') ? '50%' : 'auto') . ';
            right: ' . (($data->align_button->id === 'right' || $data->align_button->id === 'right_stick') ? $data->padding_inside_cell . 'px' : 'auto') . ';
            bottom: ' . (($data->align_button->id === 'left_stick' || $data->align_button->id === 'right_stick' || $data->align_button->id === 'center_stick') ? $data->padding_inside_cell . 'px' : 'auto') . ';
            ';
        }

        $moreParams .= '"';
        $captionTopParams .= '"';
        $captionBottomParams .= '"';

        $html = '
    <div style="
    position:relative;
    cursor:pointer;
    background: ' . $params['blockBgColor'] . ';
    height:' . $height . 'px;
    width:' . $width . 'px;
    overflow:hidden;
    border-style:solid;
    box-sizing: border-box;
    border-width:' . $border_block . 'px;
    border-color:' . $params['blockBorderColor'] . '">
    ';

        $top = $padding_between_cell;
        $left = $padding_between_cell;
        for ($i = 0; $i < $col; $i++) {
            if (!$i) {
                $top = $padding_between_cell + $border_block;
            } else {
                $top = $top + $cell_height + $padding_between_cell;
            }
            for ($j = 0; $j < $row; $j++) {
                $content = array_shift($ads);
                $content_data = json_decode($content['data']);
                $content_images = json_decode($content['images']);
                $url = $content['url'];
                $image = Proxy::init()->getConfig()['selfUrl'] . '/' . $content_images[0];

                if (!$j) {
                    $left = $padding_between_cell + $border_block;
                } else {
                    $left = $left + $cell_width + $padding_between_cell;
                }

                $html .= '
            <a href="' . $url . '" target="_blank">
            <div style="
            position: absolute;
            top: ' . $top . 'px;
            left: ' . $left . 'px;
            height:' . ($cell_height) . 'px;width:' . $cell_width . 'px;
            padding:' . $data->padding_inside_cell . 'px;
            border-color:' . $params['cellBorderColor'] . ';
            border-style:' . $data->border_teaser->border->id . ';
            border-width:' . $data->border_teaser->size . 'px;
            box-sizing: border-box;
            overflow:hidden;
            background:' . $params['cellBgColor'] . ';
    ">
';


                if ($data->place_title->id === 'top' && $data->place_text->id === 'right') {
                    $html .= '<div
                    onmouseover="this.style.color=\'' . $params['headerColor'] . '\'"
                    onmouseout="this.style.color=\'' . $params['headerColor'] . '\'"
                    style="
                    color: ' . $params['headerColor'] . '
                    line-height: ' . $data->lineheight_title . 'em;
                    padding-bottom:' . $data->padding_title . 'px;
                    text-align:left;
                    font-family: ' . $data->font_title->id . ';
                    font-weight: ' . $data->face_title->bold ? 'bold' : 'normal' . ';
                    font-style: ' . $data->face_title->italic ? 'italic' : 'none' . ';
                    text-decoration: ' . $data->face_title->underline ? 'underline' : 'none' . ';
                    font-size:' . $data->size_title . 'px"
                 >' . $content_data->captionTop . '</div>';
                }

                $html .= '<table style="table-layout:fixed;width:100%;border-spacing:0">';
                $html .= '<tr>';
                $html .= '<td style="padding:0;vertical-align:top;width:' . $data->teaser_image . 'px;text-align:center;">';

                if ($data->place_button->id === 'top_title' && ($data->place_title->id === 'top' && $data->place_text->id !== 'right')) {
                    $html .= '<div style="text-align:' . ($data->align_button->id == 'center' ? 'center' : ($data->align_button->id == 'right' ? 'right' : 'left')) . ';">';
                    $html .= '<button ' . $moreParams . '>Подробнее</button>';
                    $html .= '</div>';
                }

                if ($data->place_title->id === 'top' && $data->place_text->id !== 'right') {
                    $html .= '<div ' . $captionTopParams . '>' . $content_data->captionTop . '</div>';
                }
                if ($data->place_text->id === 'top' && $data->place_title->id !== 'right') {
                    $html .= '<div ' . $captionBottomParams . '>' . $content_data->captionBottom . '</div>';
                }
                if ($data->place_button->id === 'top_image' || ($data->place_button->id === 'bottom_text' && $data->place_text->id === 'top')) {
                    $html .= '<div style="text-align:' . ($data->align_button->id == 'center' ? 'center' : ($data->align_button->id == 'right' ? 'right' : 'left')) . ';">';
                    $html .= '<button ' . $moreParams . '>Подробнее</button>';
                    $html .= '</div>';
                }

                $html .= '<img src="' . $image . '"
                 width="' . $data->teaser_image . '"
                 height="' . $data->teaser_image . '"
                 style="
                 border-style:' . $data->border_image->border->id . ';
                 border-width:' . $data->border_image->size . 'px;
                 border-color:' . $params['imgBorderColor'] . '" alt="" />';

                if ($data->place_button->id === 'bottom_image' || ($data->place_button->id === 'top_title' && ($data->place_title->id === 'bottom' && $data->place_text->id !== 'right'))) {
                    $html .= '<div style="text-align:' . ($data->align_button->id == 'center' ? 'center' : ($data->align_button->id == 'right' ? 'right' : 'left')) . ';">';
                    $html .= '<button ' . $moreParams . '>Подробнее</button>';
                    $html .= '</div>';
                }

                if ($data->place_title->id === 'bottom' && $data->place_text->id !== 'right') {
                    $html .= '<div ' . $captionTopParams . '>' . $content_data->captionTop . '</div>';
                }
                if ($data->place_text->id === 'bottom') {
                    $html .= '<div ' . $captionBottomParams . '>' . $content_data->captionBottom . '</div>';
                }
                if ($data->place_button->id === 'bottom_text' && $data->place_text->id === 'bottom') {
                    $html .= '<button ' . $moreParams . '>Подробнее</button>';
                }

                $html .= '</td>';

                if ($data->place_text->id === 'right' || $data->place_title->id === 'right') {
                    $html .= '<td style="vertical-align:top;padding-left:' . $data->padding_inside_cell . 'px">';

                    if ($data->place_button->id === 'top_title') {
                        $html .= '<button ' . $moreParams . '>Подробнее</button>';
                    }
                    $html .= '<div style="clear:both;height:0"></div>';
                    if ($data->place_title->id === 'right') {
                        $html .= '<div ' . $captionTopParams . '>' . $content_data->captionTop . '</div>';
                    }
                    if ($data->place_text->id === 'right') {
                        $html .= '<div ' . $captionBottomParams . '>' . $content_data->captionBottom . '</div>';
                    }
                    if ($data->place_button->id === 'bottom_text') {
                        $html .= '<button ' . $moreParams . '>Подробнее</button>';
                    }

                    $html .= '</td>';
                }


                $html .= '</tr>';
                $html .= '</table>';

                $html .= '</div></a>';
            }
        }
        $html .= '</div>';
        return $html;
    }
}
