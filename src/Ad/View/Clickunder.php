<?php

namespace Project\Ad\View;

use Project\Ad\View;

/**
 * HTML кликандера
 */
class Clickunder extends View
{
    /**
     * @inheritdoc
     */
    public function generate()
    {
        return $this->_generate(
            $this->params['ads'][0]
        );
    }

    /**
     * @param $ad
     * @return string
     */
    private function _generate($ad)
    {
        $str = '(function(){
    var d = document.createElement(\'div\');
    d.style.background = \'transparent\';
    d.style.display = \'block\';
    d.style.width = \'100%\';
    d.style.zIndex = \'2147483647\';
    d.style.height = \'100%\';
    d.style.position = \'fixed\';
    d.style.top = 0;
    d.style.left = 0;
    d.onclick = function() {
        var win = window.open("", "hr_site");
        win.document.open();
        win.document.writeln(\'<sc\'+\'ript type="text/javascript">window.location = "'. $ad['url'] . '";<\/script>\');
        win.document.close();
        this.parentNode.removeChild(this);
    };
    document.body.appendChild(d);
    })();';
        return $str;
    }
}