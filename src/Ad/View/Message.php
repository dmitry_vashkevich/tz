<?php

namespace Project\Ad\View;

use Project\Ad\View;
use Project\Proxy;

/**
 * HTML месседжа
 */
class Message extends View
{
    /**
     * @inheritdoc
     */
    public function generate()
    {
        return $this->_generate(
            $this->params['config'],
            $this->params['ads'][0]
        );
    }
    /**
     * @param $config
     * @param $ad
     * @return string
     */
    private function _generate($config, $ad)
    {
        $content_data = json_decode($ad['data']);
        $content_images = json_decode($ad['images'], true);
        $image = Proxy::init()->getConfig()['selfUrl'] . '/' . $content_images[0];
        $data = json_decode($config['data'], true);
        $html = $data['template'];

        $html = str_replace('{{captionTop}}', $content_data->captionTop, $html);
        $html = str_replace('{{captionBottom}}', $content_data->captionBottom, $html);
        $html = str_replace('{{image}}', $image, $html);
        $html = str_replace('{{url}}', $ad['url'], $html);
        $str = '(function(){
            var content = ' . json_encode($html) . ';
            var div = document.createElement(\'div\');
            div.innerHTML = content;
            document.body.appendChild(div);
            setTimeout(function(){
            div.getElementsByTagName(\'div\')[0].style.bottom = \'20px\';
            }, 1000);
            })();';

        return $str;
    }
}
