<?php

namespace Project\Ad;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Query\Expr;
use Project\Entity\Campaign;
use Project\Proxy;

/**
 * Поиск рекламных материалов c подгрузкой статистики
 */
class Search
{
    public static $orders = [
        'id',
        'type',
        'rs',
        'rb',
        'rbq',
        'rbqu',
        'scv',
        'tcv',
        'prelv',
        'lead',
        'lead_amount',
        'accept',
        'accept_amount',
        'decline',
        'decline_amount',
        'invalid',
        'invalid_amount',
        'ctr',
        'cpmb',
        'ctrb'
    ];

    /**
     * Кол-во кампаний в выборке, до применения операции limit
     *
     * @var int
     */
    protected $count;

    protected $roundCalcFields = [
        'cpm',
        'ctr',
        'cpmb',
        'ctrb',
    ];

    /**
     * Поиск
     *
     * @param Campaign $campaign
     * @param string $order
     * @param string $direction
     * @return array
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function process(
        Campaign $campaign = null,
        $order = 'id',
        $direction = Criteria::DESC
    )
    {
        $criteria = new Criteria();
        
        if ($campaign !== null) {
            $criteria->where(
                Criteria::expr()->eq('campaign', $campaign)
            );
        }

        $campaigns = Proxy::init()->getEntityManager()
            ->createQueryBuilder()
            ->select(
                'ad.id as id',
                'ad.type as type',
                'ad.data as data',
                'ad.images as images',
                'COALESCE(SUM(s.rs), 0) as rs',
                'COALESCE(SUM(s.rb), 0) as rb',
                'COALESCE(SUM(s.rbq), 0) as rbq',
                'COALESCE(SUM(s.rbqu), 0) as rbqu',
                'COALESCE(SUM(s.scv), 0) as scv',
                'COALESCE(SUM(s.tcv), 0) as tcv',
                'COALESCE(SUM(s.prelv), 0) as prelv',
                'COALESCE(SUM(s.lead), 0) as lead',
                'COALESCE(SUM(s.lead_amount), 0) as lead_amount',
                'COALESCE(SUM(s.accept), 0) as accept',
                'COALESCE(SUM(s.accept_amount), 0) as accept_amount',
                'COALESCE(SUM(s.decline), 0) as decline',
                'COALESCE(SUM(s.decline_amount), 0) as decline_amount',
                'COALESCE(SUM(s.invalid), 0) as invalid',
                'COALESCE(SUM(s.invalid_amount), 0) as invalid_amount',
                'COALESCE(SUM(s.accept_amount) / NULLIF(SUM(s.rs), 0) * 1000, 0) as cpm',
                'COALESCE(SUM(s.scv) / NULLIF(SUM(s.rs), 0) * 100, 0) as ctr',
                'COALESCE(SUM(s.accept_amount) / NULLIF(SUM(s.rb), 0) * 1000, 0) as cpmb',
                'COALESCE(SUM(s.scv) / NULLIF(SUM(s.rb), 0) * 100, 0) as ctrb'
            )
            ->from('\Project\Entity\Ad', 'ad')
            ->leftJoin('\Project\Entity\StatsBase', 's', Expr\Join::WITH, 'ad.id = s.ad_id')
            ->groupBy('ad.id')
            ->addCriteria($criteria)
            ->orderBy($order, $direction)
            ->getQuery()
            ->getArrayResult();

        return $this->prepareSearchResult($campaigns);
    }

    protected function prepareSearchResult(array $searchResult)
    {
        if (count($searchResult) <= 0) {
            return $searchResult;
        }

        foreach ($searchResult as &$result) {
            foreach ($this->roundCalcFields as $roundCalcField) {
                $result[$roundCalcField] = round($result[$roundCalcField], 2);
            }
        }

        return $searchResult;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return (int)$this->count;
    }
}
