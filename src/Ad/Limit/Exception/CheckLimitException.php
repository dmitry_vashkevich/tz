<?php

namespace Project\Ad\Limit\Exception;

use Project\Exception\RestException\ClientError\BadRequestException;

/**
 * При выполнении проверки на лимиты при отгрузке контента
 */
class CheckLimitException extends BadRequestException
{

}
