<?php

namespace Project\Ad\Limit;

use Project\Ad\Limit;

/**
 * Лимиты слайдер баннера
 */
class SliderBanner extends Limit
{
    /** @inheritdoc */
    const COOKIE_NAME_SHOW = 'groupls';
    const COOKIE_NAME_CLICK = 'sbulclc';
    const COOKIE_NAME_CLOSE = 'sbulcls';
    const DEFAULT_SHOW_INTERVAL = 20 * 60;//20 минут
}
