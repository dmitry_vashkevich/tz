<?php

namespace Project\Ad\Limit;

use Project\Ad\Limit;

/**
 * Лимиты кликандера
 */
class Clickunder extends Limit
{
    /** @inheritdoc */
    const COOKIE_NAME_SHOW = 'groupls';
    const COOKIE_NAME_CLICK = 'culclc';
    const COOKIE_NAME_CLOSE = 'culcls';
}
