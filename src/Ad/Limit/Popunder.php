<?php

namespace Project\Ad\Limit;

use Project\Ad\Limit;

/**
 * Лимиты попандера
 */
class Popunder extends Limit
{
    /** @inheritdoc */
    const COOKIE_NAME_SHOW = 'groupls';
    const COOKIE_NAME_CLICK = 'pulclc';
    const COOKIE_NAME_CLOSE = 'pulcls';
}
