<?php

namespace Project\Ad\Limit;

use Project\Ad\Limit;

/**
 * Лимиты месседжа
 */
class Message extends Limit
{
    /** @inheritdoc */
    const COOKIE_NAME_SHOW = 'groupls';
    const COOKIE_NAME_CLICK = 'mulclc';
    const COOKIE_NAME_CLOSE = 'mulcls';
}
