<?php

namespace Project\Ad\Limit;

use Project\Ad\Limit;

/**
 * Лимиты кешбека
 */
class Cashback extends Limit
{
    /** @inheritdoc */
    const COOKIE_NAME_SHOW = 'cbls';
    const COOKIE_NAME_CLICK = 'cbclc';
    const COOKIE_NAME_CLOSE = 'cbcls';
    const DEFAULT_SHOW_INTERVAL = 60 * 60;//60 минут
}
