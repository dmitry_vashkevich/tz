<?php

namespace Project\Ad;

/**
 * Абстрактное отображение рекламного материала
 */
abstract class View
{
    /** @var  array */
    protected $params;

    /**
     * Конструктор
     *
     * @param array $params
     */
    public function __construct(array $params)
    {
        $this->params = $params;
    }

    /**
     * Генерация HTML
     *
     * @return string
     */
    abstract public function generate();
}
