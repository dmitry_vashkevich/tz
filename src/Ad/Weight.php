<?php

namespace Project\Ad;

use Project\Campaign\Search;
use Project\Entity\Campaign;

/**
 * Модуль получения веса рекламного материала
 */
class Weight
{
    /** базовый вес */
    const BASE_WEIGHT = 10;

    /** cpm rate */
    const CPM_RATE = 10;

    /** @var  array */
    protected $stats;

    /**
     * Конструктор
     * @param array $stats
     */
    public function __construct(array $stats)
    {
        $this->stats = $stats;
    }

    /**
     * @param Campaign $campaign
     * @return int
     */
    public function prepare(Campaign $campaign)
    {
        /**
         * CPM расчитывается при сборе статистики
         * @see Search
         */
        $cpm = $this->stats[$campaign->getId()]['cpm'];

        $weight = (int) (self::BASE_WEIGHT + self::CPM_RATE * $cpm);

        return $weight;
    }
}
