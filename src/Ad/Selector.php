<?php

namespace Project\Ad;

use Project\Check\Validator;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Абстрактный селектор рекламных материалов
 */
abstract class Selector
{
    /** @var  array */
    protected $cacheAds;

    /**
     * Конструктор
     * @param array $cacheAds
     */
    public function __construct(array $cacheAds)
    {
        $this->cacheAds = $cacheAds;
    }

    /**
     * Получить нужное кол-во метириалов
     * согласно алгоритму селекции
     *
     * @param $count
     * @return array
     */
    public function get($count)
    {
        $this->validate($count);
        return $this->getByAlg($count);
    }

    /**
     * @param $count
     */
    protected function validate($count)
    {
        Validator::validateValue(
            $count,
            new Assert\GreaterThan(0),
            'ad selector count must be > 0'
        );

        Validator::validateValue(
            count($this->cacheAds),
            new Assert\GreaterThanOrEqual($count),
            'requested adCount > cacheAds size'
        );
    }

    /**
     * @param $count
     * @return array
     */
    abstract protected function getByAlg($count);
}
