<?php

namespace Project\Ad;

use Project\Campaign\Limit as CampaignLimit;
use Project\Campaign\Limit\Exception\CheckLimitException;
use Project\Check\Validator;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Индекс объявлений
 */
class Index
{
    /** для не заданного таргетинга */
    const ANY_INDEX = 'any';

    /**
     * Данные в индексе
     * @var array
     */
    protected $data;

    /**
     * Конструктор
     * @param $data
     */
    public function __construct($data = [])
    {
        $this->data = $data;
    }

    /**
     * Добавить данные в индекс
     *
     * @param string $adType
     * @param string $country
     * @param string $platform
     * @param string $browser
     * @param string $domain
     * @param array $data
     */
    public function addData($adType, $country, $platform, $browser, $domain, $data)
    {
        $this->data[$adType][$country][$platform][$browser][$domain][] = $data;
    }

    /**
     * Получить данные индекса
     *
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Поиск рекламы в индексе
     *
     * @param string $country
     * @param string $platform
     * @param string $browser
     * @param string $domain
     * @param CampaignLimit $limit
     * @return array
     */
    public function search($country, $platform, $browser, $domain, CampaignLimit $limit)
    {
        $ads = [];

        foreach ([self::ANY_INDEX, $country] as $countryIndex) {
            foreach ([self::ANY_INDEX, $platform] as $platformIndex) {
                foreach ([self::ANY_INDEX, $browser] as $browserIndex) {
                    foreach ([self::ANY_INDEX, $domain] as $domainIndex) {
                        if (isset($this->data[$countryIndex][$platformIndex][$browserIndex][$domainIndex])) {
                            $ads += $this->data[$countryIndex][$platformIndex][$browserIndex][$domainIndex];
                        }
                    }
                }
            }
        }

        $ads = $this->filter($ads, $limit);

        $this->validateSearch($ads);

        return $ads;
    }

    /**
     * @param array $ads
     * @param CampaignLimit $limit
     * @return array
     */
    protected function filter(array $ads, CampaignLimit $limit)
    {
        $res = [];
        if (count($ads) <= 0) {
            return $res;
        }

        foreach ($ads as $ad) {
            try {
                $limit->checkUserShowLimit(
                    $ad['campaignId'],
                    $ad['limitUserShow']
                );
                $res[] = $ad;
            } catch (CheckLimitException $e) {
            }
        }

        return $res;
    }

    /**
     * Валидация результатов поиска
     * @param array $ads
     */
    protected function validateSearch(array $ads)
    {
        Validator::validateValue(
            count($ads),
            new Assert\GreaterThan(0),
            'unable to find cacheAds for rules'
        );
    }
}