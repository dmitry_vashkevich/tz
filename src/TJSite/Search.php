<?php

namespace Project\TJSite;

use Project\Entity\TJSite;
use Doctrine\Common\Collections\Criteria;
use Project\Entity\TJUser;

/**
 * Поиск TJSite
 */
class Search
{
    /**
     * Кол-во записей в выборке, до применения операции limit
     *
     * @var int
     */
    protected $count;

    /**
     * Поиск
     *
     * @param TJUser $user
     * @param null | bool $active
     * @param int  | null $offset
     * @param int  | null $limit
     * @return \Doctrine\Common\Collections\Collection|\Project\Entity\TJSite[]
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function process(
        $user = null,
        $active = null,
        $offset = null,
        $limit = null
    )
    {
        $criteria = (new Criteria())
            ->where(
                Criteria::expr()->eq('deleted', false)
            );

        if ($user !== null) {
            $criteria->andWhere(
                Criteria::expr()->eq('user', $user)
            );
        }
        
        if ($active !== null) {
            $criteria->andWhere(
                Criteria::expr()->eq('active', $active)
            );
        }

        $this->count = TJSite::repo()->createQueryBuilder('base')
            ->select("count(base.id)")
            ->addCriteria($criteria)
            ->getQuery()
            ->getSingleScalarResult();

        if ($offset !== null) {
            $criteria->setFirstResult($offset);
        }

        if ($limit !== null) {
            $criteria->setMaxResults($limit);
        }

        return TJSite::prepareList($criteria);
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }
}
