<?php

namespace Project\Offer;

use Project\Entity\Offer;
use Doctrine\Common\Collections\Criteria;

/**
 * Поиск офферов
 */
class Search
{
    /**
     * Поиск групп
     *
     * @param null $name
     * @return \Doctrine\Common\Collections\Collection|\Project\Entity\Offer[]
     */
    public function process($name = null)
    {
        $criteria = (new Criteria());

        $criteria->andWhere(
            Criteria::expr()->eq('active', true)
        );

        if ($name !== null) {
            $criteria->andWhere(
                Criteria::expr()->contains('name', $name)
            );
        }

        return Offer::prepareList($criteria);
    }
}
