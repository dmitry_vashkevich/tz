<?php

namespace Project\Payment;

use Doctrine\Common\Collections\Criteria;
use Project\Entity\Payment;
use Project\Entity\Search\SearchInUser;

/**
 * Поиск выплат
 */
class Search extends SearchInUser
{
    /**
     * Кол-во в выборке, до применения операции limit
     *
     * @var int
     */
    protected $count;

    /**
     * Поиск выплат
     *
     * @param \DateTime $dateFrom
     * @param \DateTime $dateTo
     * @param int $offset
     * @param int $limit
     * @return \Doctrine\Common\Collections\Collection|\Project\Entity\Payment[]
     */
    public function process(
        \DateTime $dateFrom,
        \DateTime $dateTo,
        $offset,
        $limit
    )
    {
        $criteria = (new Criteria())->where(
            Criteria::expr()->andX(
                Criteria::expr()->gte('changed', $dateFrom),
                Criteria::expr()->lte('changed', $dateTo),
                Criteria::expr()->eq('user', $this->user)
            )
        );

        $this->count = Payment::repo()->createQueryBuilder('base')
            ->select("count(base.id)")
            ->addCriteria($criteria)
            ->getQuery()
            ->getSingleScalarResult();

        $criteria->orderBy(['changed' => 'desc']);

        $criteria->setFirstResult($offset);
        $criteria->setMaxResults($limit);
        
        return Payment::prepareList($criteria);
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }
}
