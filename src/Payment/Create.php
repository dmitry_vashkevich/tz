<?php

namespace Project\Payment;

use Project\Check\Validator;
use Project\Entity\Payment;
use Project\Entity\TJUser;
use Project\Entity\Wallet;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Создание выплат
 */
class Create
{
    /**
     * Пользователь
     *
     * @var TJUser
     */
    protected $user;

    /**
     * Кошелек
     *
     * @var Wallet
     */
    protected $wallet;

    /**
     * Конструктор
     *
     * @param TJUser $user
     * @param Wallet $wallet
     */
    public function __construct(TJUser $user, Wallet $wallet)
    {
        $this->user = $user;
        $this->wallet = $wallet;
    }

    /**
     * Валидация
     *
     * @param $amount
     * @throws \Project\Check\Exception\ValidationException
     */
    protected function validate($amount)
    {
        //заказывать выплаты можно только на свои кошельки
        Validator::validateValue(
            $this->user->getId(),
            new Assert\EqualTo(
                $this->wallet->getUser()->getId()
            )
        );

        //сумма выплаты должна укладыавться в баланс
        Validator::validateValue(
            $this->user->getBalance(),
            new Assert\GreaterThanOrEqual(
                $amount
            ),
            'balance < payment amount'
        );
    }

    /**
     * Создание выплаты
     *
     * @param $amount
     * @param string | null $comment
     * @param string $status
     * @return Payment
     */
    public function process($amount, $comment = null, $status = Payment::STATUS_REQUEST)
    {
        $this->validate($amount);

        return (new Payment())
            ->setAmount($amount)
            ->setWallet($this->wallet)
            ->setComment($comment)
            ->setUser($this->user)
            ->setStatus($status)
            ->save();
    }
}
