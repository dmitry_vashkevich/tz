<?php

namespace Project\Provider;

use Bankiru\MonologLogstash\ZMQHandler;
use Monolog\Formatter\LogstashFormatter;
use Monolog\Handler\StreamHandler;
use Project\Listener\Application\Console\Log;
use Project\Listener\Application\Kernel;
use Project\Logger\Formatter\Stream;
use Project\Proxy;
use Monolog\Handler\NullHandler;
use Monolog\Handler\SocketHandler;
use Silex\Application;
use Silex\ServiceProviderInterface;
use Project\Logger\Formatter\Logstash;

/**
 * Провайдер логгера для Silex
 */
class Logger implements ServiceProviderInterface
{
    /**
     * Регистрация системы логгирования
     *
     * @param Application $app
     */
    public function registerApplication(Application $app)
    {
        $configs = Proxy::init()->getConfig()['logger'];
        $app['custom_logger'] = $app->share(function ($app) use ($configs) {
            $log = new \Project\Logger($configs['name']);

            // Хэндлер для logstash_udp
            $lsudpHandler = function ($configs) {
                $handler = new SocketHandler(
                    $configs['logstash_udp']['connection_string'],
                    $configs['logstash_udp']['level']
                );

                $handler->setFormatter(
                    new Logstash()
                );

                return $handler;
            };

            // Хэндлер для logstash_udp
            $lsTCPlocalHandler = function ($configs) {
                $handler = new SocketHandler(
                    $configs['logstash_tcp_local']['connection_string'],
                    $configs['logstash_tcp_local']['level']
                );

                $handler->setFormatter(
                    new LogstashFormatter(
                        'tizerka', null, null, 'ctxt_', LogstashFormatter::V1
                    )
                );

                return $handler;
            };

            $lsZmqHandler = function ($configs) {
                $handler = new ZMQHandler(
                    $configs['logstash_zmq']['connection_string']
                );

                $handler->setFormatter(
                    new LogstashFormatter(
                        'tizerka', null, null, 'ctxt_', LogstashFormatter::V1
                    )
                );

                return $handler;
            };

            // Хэндлер для stream
            $streamHandler = function ($configs) {
                $handler = new StreamHandler(
                    $configs['stream']['file'],
                    $configs['stream']['level']
                );

                $handler->setFormatter(
                    new Stream()
                );

                return $handler;
            };

            switch ($configs['type']) {
                case 'null':
                    $handlers = [new NullHandler()];
                    break;
                case 'logstash_udp':
                    $handlers = [$lsudpHandler($configs)];
                    break;
                case 'logstash_tcp_local':
                    $handlers = [$lsTCPlocalHandler($configs)];
                    break;
                case 'logstash_zmq':
                    $handlers = [$lsZmqHandler($configs)];
                    break;
                case 'stream':
                    $handlers = [$streamHandler($configs)];
                    break;
                default:
                    throw new \Exception('Broken logger configuration');
            }

            foreach ($handlers as $handler) {
                $log->pushHandler($handler);
            }

            return $log;
        });
    }

    /**
     * Регистрация логгера
     *
     * @param \Silex\Application $app
     */
    public function register(Application $app)
    {
        $this->registerApplication($app);
    }

    /**
     * @inheritdoc
     */
    public function boot(Application $app)
    {
        $app['dispatcher']->addSubscriber(new Kernel($app['custom_logger']));
        $app['dispatcher']->addSubscriber(new Log($app['custom_logger']));
    }
}
