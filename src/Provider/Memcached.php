<?php

namespace Project\Provider;

use Silex\Application;
use Silex\ServiceProviderInterface;

/**
 * Провайдер memcached
 */
class Memcached implements ServiceProviderInterface
{
    /**
     * @param Application $app
     */
    public function register(Application $app)
    {
        $app['memcached'] = $app->share(function () use ($app) {
            $memcached = new \Memcached();
            $memcached->addServers($app['memcached.servers']);
            return $memcached;
        });
    }

    /**
     * @param Application $app
     */
    public function boot(Application $app)
    {
    }
}
