<?php

namespace Project\Provider;

use Guzzle\GuzzleServiceProvider;
use Guzzle\Plugin\Log\LogPlugin;
use Project\Http\LogAdapter;
use Project\Logger\Formatter;
use Project\Proxy;
use Silex\Application;

/**
 * Провайдер Http клиента
 */
class HttpClient extends GuzzleServiceProvider
{
    /**
     * @inheritdoc
     */
    public function boot(Application $app)
    {
        $format = new Formatter\Guzzle('>>>{request}<<<{code} {res_body} [{total_time} sec]');

        $logAdapter = new LogAdapter(Proxy::init()->getLogger());
        $logPlugin  = new LogPlugin($logAdapter, $format);

        Proxy::init()->getHttpClient()->addSubscriber($logPlugin);
    }
}
