<?php

namespace Project\Provider;

use Silex\Application;
use Silex\ServiceProviderInterface;

/**
 * Провайдер gearman, сетапит на уровне фреймворка и клиента и воркера
 */
class Gearman implements ServiceProviderInterface
{
    /**
     * @param Application $app
     */
    public function register(Application $app)
    {
        $app['gearman.client'] = $app->share(function () use ($app) {
            $gc = new \GearmanClient();
            $gc->addServers($app['gearman.servers']);
            return $gc;
        });

        $app['gearman.worker'] = $app->share(function () use ($app) {
            $gc = new \GearmanWorker();
            $gc->addServers($app['gearman.servers']);
            return $gc;
        });
    }

    /**
     * @param Application $app
     */
    public function boot(Application $app)
    {
    }
}
