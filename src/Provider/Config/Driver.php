<?php

namespace Project\Provider\Config;

/**
 * Базовый драйвер конфигов для работы с php - конфигами
 */
class Driver
{
    /**
     * Загрузка конфигов из файла
     *
     * @param string $filename
     * @return array
     */
    public function load($filename)
    {
        $config = require $filename;
        $config = (1 === $config) ? [] : $config;

        return $config ?: [];
    }
}
