<?php
namespace Project\Provider\Config\Exception;

use Project\Exception\RestException\ServerError\InternalServerErrorException;

/**
 * Исключение выбрасываемое при ошибке загрузки файла конфигурации
 */
class IllegalFileException extends InternalServerErrorException
{

}
