<?php

namespace Project\Provider\Doctrine;

use Silex\Application;
use Silex\Provider\DoctrineServiceProvider;

/**
 * Провайдер для работы с Doctrine через Silex
 */
class Driver extends DoctrineServiceProvider
{
    /**
     * @param Application $app
     */
    public function register(Application $app)
    {
        parent::register($app);
    }
}
