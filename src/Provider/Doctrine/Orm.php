<?php

namespace Project\Provider\Doctrine;

use Dflydev\Silex\Provider\DoctrineOrm\DoctrineOrmServiceProvider;
use Doctrine\DBAL\Types\Type;
use Project\Listener\Entity;
use Silex\Application;

/**
 * Провайдер для работы с Doctrine ORM через Silex
 */

class Orm extends DoctrineOrmServiceProvider
{
    /**
     * @param Application $app
     * @throws \Doctrine\DBAL\DBALException
     */
    public function register(Application $app)
    {
        parent::register($app);
        Type::overrideType(Type::DATE, 'Project\Entity\Type\DateKeyType');
    }
}
