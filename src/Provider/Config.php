<?php

/**
 * Провайдер для конфигурирования фреймворка
 */

namespace Project\Provider;

use Project\Provider\Config\Driver;
use Project\Provider\Config\Exception\IllegalFileException;
use Silex\Application;
use Silex\ServiceProviderInterface;

/**
 * Провайдер для конфигураций
 */
class Config implements ServiceProviderInterface
{
    /**
     * Файл конфигурации
     *
     * @var string
     */
    private $filename;

    /**
     * Массив заменяемых значений
     *
     * @var array
     */
    private $replacements = [];
    /**
     * Драйвер
     *
     * @var \Project\Provider\Config\Driver
     */
    private $driver;

    /**
     * Конструктор
     *
     * @param string $filename
     * @param array $replacements
     */
    public function __construct($filename, array $replacements = [])
    {
        $this->filename = $filename;

        if ($replacements) {
            foreach ($replacements as $key => $value) {
                $this->replacements['%'.$key.'%'] = $value;
            }
        }

        $this->driver = new Driver();
    }

    /**
     * Регистрация системы конфигурирования
     *
     * @param Application $app
     */
    public function registerApplication(Application $app)
    {
        $config = $this->readConfig();

        foreach ($config as $name => $value) {
            if ('%' === substr($name, 0, 1)) {
                $this->replacements[$name] = (string) $value;
            }
        }

        $this->merge($app, $config);
    }

    /**
     * Чтение конфигурационного файла
     *
     * @return array
     */
    private function readConfig()
    {
        $this->validateFileName();
        return $this->driver->load($this->filename);
    }

    /**
     * Валидация имени файла конфигурации
     *
     * @throws IllegalFileException
     * @throws \InvalidArgumentException
     */
    private function validateFileName()
    {
        if (empty($this->filename)) {
            throw new \InvalidArgumentException("Configuration file value is empty");
        }

        if (!file_exists($this->filename)) {
            throw new IllegalFileException("Configuration file {$this->filename} not exists");
        }
    }


    /**
     * Мерж массива конфигурационных параметров
     *
     * @param Application $app
     * @param array $config
     */
    private function merge(Application $app, array $config)
    {
        foreach ($config as $name => $value) {
            if (isset($app[$name]) && is_array($value)) {
                $app[$name] = $this->mergeRecursively($app[$name], $value);
            } else {
                $app[$name] = $this->doReplacements($value);
            }
        }
    }

    /**
     * Рекурсивный мерж
     *
     * @param array $currentValue
     * @param array $newValue
     * @return array
     */
    private function mergeRecursively(array $currentValue, array $newValue)
    {
        foreach ($newValue as $name => $value) {
            if (is_array($value) && isset($currentValue[$name])) {
                $currentValue[$name] = $this->mergeRecursively($currentValue[$name], $value);
            } else {
                $currentValue[$name] = $this->doReplacements($value);
            }
        }

        return $currentValue;
    }

    /**
     * Замена параметра массива конфига
     *
     * @param mixed $value
     * @return mixed
     */
    private function doReplacements($value)
    {
        if (!$this->replacements) {
            return $value;
        }

        if (is_array($value)) {
            foreach ($value as $k => $v) {
                $value[$k] = $this->doReplacements($v);
            }

            return $value;
        }

        if (is_string($value)) {
            return strtr($value, $this->replacements);
        }

        return $value;
    }

    /**
     * Регистрация системы конфигурирования
     *
     * @param \Silex\Application $app
     */
    public function register(Application $app)
    {
        $this->registerApplication($app);
    }

    /**
     * @inheritdoc
     *
     * @param Application $app
     */
    public function boot(Application $app)
    {

    }
}
