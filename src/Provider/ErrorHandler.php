<?php

namespace Project\Provider;

use Silex\Application;
use Silex\ServiceProviderInterface;

/**
 * Провайдер обработчика ошибок
 */
class ErrorHandler implements ServiceProviderInterface
{

    /**
     * Регистрация обработчика ошибок
     */
    public function register(Application $app)
    {
        $app['error_handler'] = $app->share(function () {
            return new \Project\Exception\ErrorHandler();
        });
    }

    /**
     *
     * @param Application $app
     */
    public function boot(Application $app)
    {

    }
}
