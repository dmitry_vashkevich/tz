<?php

namespace Project\Provider;

use Silex\Application;
use Silex\ServiceProviderInterface;
use Symfony\Component\Console\Application as ConsoleApplication;

/**
 * Провайдер для работы с консолью
 */
class Console implements ServiceProviderInterface
{
    /**
     * Устанавливает Silex-диспетчер для слежения за событиями консоли
     *
     * @param Application $app
     */
    public function register(Application $app)
    {
        $app['console'] = $app->share(function () use ($app) {
            $application = new ConsoleApplication('Project');

            $app->boot();

            $application->setDispatcher($app['dispatcher']);

            return $application;
        });
    }

    /**
     * {@inheritdoc}
     */
    public function boot(Application $app)
    {

    }
}
