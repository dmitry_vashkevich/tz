<?php

namespace Project\Entity;

use Project\Entity\Schema\EntityWithDeterminedId;

/**
 * События трекера
 *
 * @Entity
 * @HasLifecycleCallbacks
 * @Table(
 *  name="tracker_events"
 * )
 */
class TrackerEvent extends EntityWithDeterminedId
{
    /**
     * @Column(type="text")
     * @var string
     */
    protected $data;

    /**
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param string $data
     * @return $this
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }
}
