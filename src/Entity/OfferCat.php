<?php

namespace Project\Entity;

use Project\Entity\Schema\EntityWithDeterminedId;

/**
 * Категории оферов
 *
 * @Entity
 * @HasLifecycleCallbacks
 * @Table(
 *  name="offer_cats"
 * )
 */
class OfferCat extends EntityWithDeterminedId
{
    /**
     * Название
     *
     * @Column(type="string", unique=true)
     */
    protected $name;

    /**
     * Офферы
     *
     * @ManyToMany(targetEntity="Offer", mappedBy="offer_cats")
     **/
    protected $offers;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }
}
