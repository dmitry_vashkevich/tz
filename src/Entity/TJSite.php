<?php

namespace Project\Entity;

use Project\Entity\Exception\IllegalValue;
use Project\Entity\Schema\Base;

/**
 * Площадки
 *
 * @Entity
 * @HasLifecycleCallbacks
 * @Table(
 *  name="tj_sites"
 * )
 */
class TJSite extends Base
{
    /** статусы */
    const STATUS_OK = 'approved';
    const STATUS_CHECK = 'checking';
    const STATUS_BLOCKED = 'blocked';
    const STATUSES = [
        self::STATUS_OK,
        self::STATUS_CHECK,
        self::STATUS_BLOCKED,
    ];

    /**
     * @ManyToOne(targetEntity="\Project\Entity\TJUser")
     * @JoinColumn(name="user_id", referencedColumnName="id", nullable = true)
     * @var TJUser
     */
    protected $user;

    /**
     * @Column(type="text")
     * @var string
     */
    protected $name;
    
    /**
     * @Column(type="text")
     * @var string
     */
    protected $url;

    /**
     * @Column(type="text", nullable = true)
     * @var string
     */
    protected $alias;

    /**
     * @Column(type="text", nullable = true)
     * @var string
     */
    protected $themes;

    /**
     * @Column(type="text", nullable = true)
     * @var string
     */
    protected $statlink;

    /**
     * @Column(type="text", nullable = true)
     * @var string
     */
    protected $params;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $status = self::STATUS_CHECK;

    /**
     * Флаг "удаленной" сущности
     *
     * @Column(type="boolean", options={"default":false})
     */
    protected $deleted = false;

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     * @return $this
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
        return $this;
    }
    
    /**
     * @return TJUser
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param TJUser $user
     * @return $this
     */
    public function setUser(TJUser $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * @param string $alias
     * @return $this
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;
        return $this;
    }

    /**
     * @return string
     */
    public function getThemes()
    {
        return $this->themes;
    }

    /**
     * @param string $themes
     * @return $this
     */
    public function setThemes($themes)
    {
        $this->themes = $themes;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatlink()
    {
        return $this->statlink;
    }

    /**
     * @param string $statlink
     * @return $this
     */
    public function setStatlink($statlink)
    {
        $this->statlink = $statlink;
        return $this;
    }
    
    /**
     * @return string
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param string $params
     * @return $this
     */
    public function setParams($params)
    {
        $this->params = $params;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return $this
     * @throws IllegalValue
     */
    public function setStatus($status)
    {
        if (!in_array($status, self::STATUSES)) {
            throw new IllegalValue("Illegal status: '{$status}'");
        }
        
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }
}
