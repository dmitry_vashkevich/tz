<?php

namespace Project\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Project\Entity\Schema\Base;

/**
 * Кампания
 *
 * @Entity
 * @HasLifecycleCallbacks
 * @Table(
 *  name="campaigns"
 * )
 */
class Campaign extends Base
{
    /**
     * Юзер
     *
     * @ManyToOne(targetEntity="\Project\Entity\SiteUser")
     * @JoinColumn(name="site_user_id", referencedColumnName="id", nullable = false)
     */
    protected $site_user;

    /**
     * @Column(type="integer")
     */
    protected $site_user_id;

    /**
     * Тип рекламы в кампании
     *
     * @Column(type="string")
     */
    protected $adType;

    /**
     * @Column(type="text", nullable=true)
     */
    protected $targetDomains;

    /**
     * Название кампании
     *
     * @Column(type="string")
     */
    protected $name;

    /**
     * @Column(type="boolean")
     */
    protected $external;

    /**
     * @Column(type="boolean", options={"default":false})
     */
    protected $archive;

    /**
     * @Column(type="integer", nullable=true)
     */
    protected $cpm;

    /**
     * Продукт
     *
     * @ManyToOne(targetEntity="\Project\Entity\Offer")
     * @JoinColumn(name="offer_id", referencedColumnName="id", nullable = true)
     */
    protected $offer;

    /**
     * @Column(type="integer", nullable = true)
     */
    protected $offer_id;

    /**
     * @Column(type="text", nullable=true)
     */
    protected $flow;

    /**
     * @Column(type="integer", nullable=true)
     */
    protected $ext_campaign_id;

    /**
     * @Column(type="string", nullable=true)
     */
    protected $url;

    /**
     * @Column(type="integer", nullable=true)
     */
    protected $limitUserShow;

    /**
     * @Column(type="integer", nullable=true)
     */
    protected $limitAmount;

    /**
     * @Column(type="text", nullable=true)
     */
    protected $targetCountries;

    /**
     * @Column(type="text", nullable=true)
     */
    protected $targetPlatforms;

    /**
     * @Column(type="text", nullable=true)
     */
    protected $targetBrowsers;

    /**
     * @Column(type="text", nullable=true)
     */
    protected $script;

    /** диапазон */

    /**
     * @Column(type="integer", nullable = true)
     * @var int
     */
    protected $blockWidthFrom;

    /**
     * @Column(type="integer", nullable = true)
     * @var int
     */
    protected $blockWidthTo;

    /**
     * @Column(type="integer", nullable = true)
     * @var int
     */
    protected $blockHeightFrom;

    /**
     * @Column(type="integer", nullable = true)
     * @var int
     */
    protected $blockHeightTo;

    /**
     * Объявления
     *
     * @OneToMany(targetEntity="\Project\Entity\Ad", mappedBy="campaign")
     * @var Ad[] | ArrayCollection
     **/
    protected $ads;

    /**
     * Конструктор
     */
    public function __construct()
    {
        $this->ads = new ArrayCollection();
    }

    /**
     * @return Ad[] | ArrayCollection
     */
    public function getAds()
    {
        return $this->ads;
    }


    /**
     * Установки перед вставкой в БД
     *
     * @PrePersist
     */
    public function prePersist()
    {
        //кампания по умолчанию создается не активной
        if ($this->active === null) {
            $this->active = false;
        }

        parent::prePersist();
    }

    /**
     * @return SiteUser
     */
    public function getSiteUser()
    {
        return $this->site_user;
    }

    /**
     * @param SiteUser $site_user
     * @return $this
     */
    public function setSiteUser(SiteUser $site_user)
    {
        $this->site_user = $site_user;
        return $this;
    }

    /**
     * @return Offer
     */
    public function getOffer()
    {
        return $this->offer;
    }

    /**
     * @param Offer $offer
     * @return $this
     */
    public function setOffer(Offer $offer = null)
    {
        $this->offer = $offer;
        return $this;
    }

    /**
     * @return int
     */
    public function getSiteUserId()
    {
        return $this->site_user_id;
    }

    /**
     * @return int | null
     */
    public function getOfferId()
    {
        return $this->offer_id;
    }

    /**
     * @return int
     */
    public function getExtCampaignId()
    {
        return $this->ext_campaign_id;
    }

    /**
     * @param int $ext_campaign_id
     * @return $this
     */
    public function setExtCampaignId($ext_campaign_id)
    {
        $this->ext_campaign_id = $ext_campaign_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return bool
     */
    public function isExternal()
    {
        return (bool) $this->external;
    }

    /**
     * @param bool $external
     * @return $this
     */
    public function setExternal($external)
    {
        $this->external = $external;
        return $this;
    }

    /**
     * @return int
     */
    public function getCpm()
    {
        return $this->cpm;
    }

    /**
     * @param int $cpm
     * @return $this
     */
    public function setCpm($cpm)
    {
        $this->cpm = $cpm;
        return $this;
    }

    /**
     * @return int
     */
    public function getLimitUserShow()
    {
        return $this->limitUserShow;
    }

    /**
     * @param int $limitUserShow
     * @return $this
     */
    public function setLimitUserShow($limitUserShow)
    {
        $this->limitUserShow = $limitUserShow;
        return $this;
    }

    /**
     * @return int
     */
    public function getLimitAmount()
    {
        return $this->limitAmount;
    }

    /**
     * @param int $limitAmount
     * @return $this
     */
    public function setLimitAmount($limitAmount)
    {
        $this->limitAmount = $limitAmount;
        return $this;
    }

    /**
     * @return string
     */
    public function getFlow()
    {
        return $this->flow;
    }

    /**
     * @param string $flow
     * @return $this
     */
    public function setFlow($flow)
    {
        $this->flow = $flow;
        return $this;
    }

    /**
     * @return string
     */
    public function getAdType()
    {
        return $this->adType;
    }

    /**
     * @param string $adType
     * @return $this
     */
    public function setAdType($adType)
    {
        $this->adType = $adType;
        return $this;
    }

    /**
     * @return string
     */
    public function getTargetCountries()
    {
        return $this->targetCountries;
    }

    /**
     * @param string $targetCountries
     * @return $this
     */
    public function setTargetCountries($targetCountries)
    {
        $this->targetCountries = $targetCountries;
        return $this;
    }

    /**
     * @return string
     */
    public function getTargetPlatforms()
    {
        return $this->targetPlatforms;
    }

    /**
     * @param string $targetPlatforms
     * @return $this
     */
    public function setTargetPlatforms($targetPlatforms)
    {
        $this->targetPlatforms = $targetPlatforms;
        return $this;
    }

    /**
     * @return string
     */
    public function getTargetBrowsers()
    {
        return $this->targetBrowsers;
    }

    /**
     * @param string $targetBrowsers
     * @return $this
     */
    public function setTargetBrowsers($targetBrowsers)
    {
        $this->targetBrowsers = $targetBrowsers;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getScript()
    {
        return $this->script;
    }

    /**
     * @param string $script
     * @return $this
     */
    public function setScript($script)
    {
        $this->script = $script;
        return $this;
    }

    /**
     * @return int
     */
    public function getBlockWidthFrom()
    {
        return $this->blockWidthFrom;
    }

    /**
     * @param int $blockWidthFrom
     * @return $this
     */
    public function setBlockWidthFrom($blockWidthFrom)
    {
        $this->blockWidthFrom = $blockWidthFrom;
        return $this;
    }

    /**
     * @return int
     */
    public function getBlockWidthTo()
    {
        return $this->blockWidthTo;
    }

    /**
     * @param int $blockWidthTo
     * @return $this
     */
    public function setBlockWidthTo($blockWidthTo)
    {
        $this->blockWidthTo = $blockWidthTo;
        return $this;
    }

    /**
     * @return int
     */
    public function getBlockHeightFrom()
    {
        return $this->blockHeightFrom;
    }

    /**
     * @param int $blockHeightFrom
     * @return $this
     */
    public function setBlockHeightFrom($blockHeightFrom)
    {
        $this->blockHeightFrom = $blockHeightFrom;
        return $this;
    }

    /**
     * @return int
     */
    public function getBlockHeightTo()
    {
        return $this->blockHeightTo;
    }

    /**
     * @param int $blockHeightTo
     * @return $this
     */
    public function setBlockHeightTo($blockHeightTo)
    {
        $this->blockHeightTo = $blockHeightTo;
        return $this;
    }

    /**
     * @return bool
     */
    public function isArchive()
    {
        return $this->archive;
    }

    /**
     * @param bool $archive
     * @return $this
     */
    public function setArchive($archive)
    {
        $this->archive = $archive;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTargetDomains()
    {
        return $this->targetDomains;
    }

    /**
     * @param mixed $targetDomains
     * @return $this
     */
    public function setTargetDomains($targetDomains)
    {
        $this->targetDomains = $targetDomains;
        return $this;
    }
}
