<?php

namespace Project\Entity;

use Project\Entity\Exception\IllegalValue;
use Project\Entity\Schema\Base;

/**
 * Пользователи
 *
 * @Entity
 * @HasLifecycleCallbacks
 * @Table(
 *  name="wallets"
 * )
 */
class Wallet extends Base
{
    /** статусы */
    const STATUS_OK = 'approved';
    const STATUS_CHECK = 'checking';
    const STATUS_BLOCKED = 'blocked';
    const STATUSES = [
        self::STATUS_OK,
        self::STATUS_CHECK,
        self::STATUS_BLOCKED,
    ];

    /**
     * Тип кошелька
     *
     * @ManyToOne(targetEntity="\Project\Entity\WalletType")
     * @JoinColumn(name="type_id", referencedColumnName="id", nullable = false)
     */
    protected $type;

    /**
     * Юзер
     *
     * @ManyToOne(targetEntity="\Project\Entity\TJUser")
     * @JoinColumn(name="user_id", referencedColumnName="id", nullable = false)
     */
    protected $user;

    /**
     * Номер
     *
     * @Column(type="string")
     */
    protected $number;

    /**
     * Комментарий
     *
     * @Column(type="string", nullable=true)
     */
    protected $comment;

    /**
     * Флаг "удаленной" сущности
     *
     * @Column(type="boolean", options={"default":false})
     */
    protected $deleted = false;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $status = self::STATUS_CHECK;
    
    /**
     * Установки перед вставкой в БД
     *
     * @PrePersist
     */
    public function prePersist()
    {
        if ($this->active === null) {
            $this->active = false;
        }

        parent::prePersist();
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return $this
     * @throws IllegalValue
     */
    public function setStatus($status)
    {
        if (!in_array($status, self::STATUSES)) {
            throw new IllegalValue("Illegal status: '{$status}'");
        }

        $this->status = $status;
        return $this;
    }
    
    /**
     * @return bool
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     * @return $this
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
        return $this;
    }
    
    /**
     * @return WalletType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param WalletType $type
     * @return $this
     */
    public function setType(WalletType $type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return TJUser
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param TJUser $user
     * @return $this
     */
    public function setUser(TJUser $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param string $number
     * @return $this
     */
    public function setNumber($number)
    {
        $this->number = $number;
        return $this;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     * @return $this
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
        return $this;
    }
}
