<?php

namespace Project\Entity\Search;

use Project\Check\Validator;
use Project\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Поиск сущности у пользователя
 */
abstract class SearchInUser
{
    /**
     * Юзер
     *
     * @var User
     */
    protected $user;

    /**
     * Конструктор
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }
}
