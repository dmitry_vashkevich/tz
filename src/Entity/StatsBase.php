<?php

namespace Project\Entity;

/**
 * Статистика
 *
 * @Entity
 * @Table(
 *  name="stats_base",
 *  uniqueConstraints={
 *     @UniqueConstraint(name="stats_base_calc_uniq_idx", columns={"uniq"}),
 *  },
 *  indexes={
 *     @Index(name="stats_base_date_idx", columns={"date"}),
 *  }
 * )
 */
class StatsBase extends Stats
{

    /**
     * должна соотв-ть реальной
     */
    const TABLE = 'stats_base';

    /**
     * Ключи аггрегации событий
     */
    const AGGREGATION_KEYS = [
        'uniq',
        'date',
        'campaign_id',
        'country',
        'platform',
        'browser',
        'site_user_id',
        'ref_user_id',
        'offer_id',
        'land_id',
        'preland_id',
        'adtype',
        'ad_id',
        'tj_block_id',
        'tj_user_id',
        'tj_site_id',
    ];

    /**
     * Ключ группировки
     * @Id
     * @Column(type="string")
     */
    protected $uniq;

    /**
     * Дата создания
     *
     * @Column(type="date")
     * @var \DateTime
     */
    protected $date;

    /**
     * Идентификатор кампании
     *
     * @Column(type="integer", nullable = true)
     * @var int
     */
    protected $campaign_id;

    /**
     * Код страны
     *
     * @Column(type="string", nullable = true)
     */
    protected $country;

    /**
     * Платформа
     *
     * @Column(type="string", nullable = true)
     */
    protected $platform;

    /**
     * Браузер
     *
     * @Column(type="string", nullable = true)
     */
    protected $browser;

    /**
     * Набор полей из компании
     */

    /**
     * Создатель кампаний
     *
     * @Column(type="integer", nullable = true)
     * @var int
     */
    protected $site_user_id;

    /**
     * TJ User
     *
     * @Column(type="integer", nullable = true)
     * @var int
     */
    protected $tj_user_id;

    /**
     * TJ Site
     *
     * @Column(type="integer", nullable = true)
     * @var int
     */
    protected $tj_site_id;

    /**
     * Рефовод
     *
     * @Column(type="integer", nullable = true)
     * @var int
     */
    protected $ref_user_id;

    /**
     * Продукт
     *
     * @Column(type="integer", nullable = true)
     * @var int
     */
    protected $offer_id;

    /**
     * Лэнд
     *
     * @Column(type="integer", nullable = true)
     * @var int
     */
    protected $land_id;

    /**
     * ПреЛэнд
     *
     * @Column(type="integer", nullable = true)
     * @var int
     *
     */
    protected $preland_id;

    /**
     * Тип рекламы
     * @Column(type="string", nullable = false)
     * @var string
     */
    protected $adType;

    /**
     * Реклама
     *
     * @Column(type="integer", nullable = true)
     * @var int
     */
    protected $ad_id;

    /**
     * TJ Block
     *
     * @Column(type="integer", nullable = true)
     * @var int
     */
    protected $tj_block_id;
}
