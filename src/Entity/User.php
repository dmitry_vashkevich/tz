<?php

namespace Project\Entity;

use Project\Entity\Schema\Base;

/**
 * Абстрактный пользователь
 */
abstract class User extends Base
{
    /**
     * Логин
     *
     * @Column(type="string", unique=true)
     */
    protected $login;

    /**
     * Пароль
     *
     * @Column(type="string")
     */
    protected $password;

    /**
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param string $login
     * @return $this
     */
    public function setLogin($login)
    {
        $this->login = $login;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }
}
