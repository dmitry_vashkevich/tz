<?php

namespace Project\Entity;

use Project\Entity\Exception\IllegalValue;
use Project\Entity\Schema\EntityWithDeterminedId;

/**
 * Кампания
 *
 * @Entity
 * @HasLifecycleCallbacks
 * @Table(
 *  name="lands"
 * )
 */
class Land extends EntityWithDeterminedId
{
    /**
     * Платформы
     */
    const PLATFORM_MOBILE = 'mobile';
    const PLATFORM_DESKTOP = 'desktop';
    const PLATFORM_BOTH = 'both';

    protected static $platformList = [
        self::PLATFORM_MOBILE,
        self::PLATFORM_DESKTOP,
        self::PLATFORM_BOTH,
    ];

    /**
     * Продукт
     *
     * @ManyToOne(targetEntity="\Project\Entity\Offer")
     * @JoinColumn(name="offer_id", referencedColumnName="id", nullable = true)
     */
    protected $offer;

    /**
     * @Column(type="integer", nullable = true)
     */
    protected $offer_id;

    /**
     * Название
     *
     * @Column(type="string")
     */
    protected $name;

    /**
     * Урл
     *
     * @Column(type="string")
     */
    protected $url;

    /**
     * Настройки политики в отношение комиссий и стран в формате json
     *
     * @Column(type="text")
     */
    protected $countrySettings;

    /**
     * Содержит информацию о поддержке лендингом мобильных декстоп платформ
     *
     * @Column(type="string")
     */
    protected $platform;

    /**
     * Индикатор кешбека
     *
     * @Column(type="boolean", options={"default":false})
     */
    protected $cashback = false;

    /**
     * @return Offer
     */
    public function getOffer()
    {
        return $this->offer;
    }

    /**
     * @param Offer $offer
     * @return $this
     */
    public function setOffer(Offer $offer)
    {
        $this->offer = $offer;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return string
     */
    public function getCountrySettings()
    {
        return $this->countrySettings;
    }

    /**
     * @param string $countrySettings
     * @return $this
     */
    public function setCountrySettings($countrySettings)
    {
        $this->countrySettings = $countrySettings;
        return $this;
    }

    /**
     * @return string
     */
    public function getPlatform()
    {
        return $this->platform;
    }

    /**
     * @param string $platform
     * @return $this
     * @throws IllegalValue
     */
    public function setPlatform($platform)
    {
        if (!in_array($platform, self::$platformList)) {
            throw new IllegalValue("Illegal platform: '{$platform}'");
        }

        $this->platform = $platform;
        return $this;
    }

    /**
     * Извелечь из настроек лендинга поддерживаемые страны
     *
     * @return array
     */
    public function getCountryList()
    {
        $countryList = [];
        $landCountrySetting = json_decode($this->countrySettings, true);
        foreach ($landCountrySetting as $data) {
            if (isset($data['amount']) && $data['amount'] != 0) {
                $countryList[] = $data['country'];
            }
        }

        return array_unique($countryList);
    }

    /**
     * @return bool
     */
    public function getCashback()
    {
        return $this->cashback;
    }

    /**
     * @param bool $cashback
     * @return $this
     */
    public function setCashback($cashback)
    {
        $this->cashback = $cashback;
        return $this;
    }
}
