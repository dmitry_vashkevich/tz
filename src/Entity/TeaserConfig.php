<?php

namespace Project\Entity;

use Project\Entity\Schema\Base;

/**
 * Конфиги для отображения тизеров
 *
 * @Entity
 * @HasLifecycleCallbacks
 * @Table(
 *  name="teaser_configs",
 *    indexes={
 *      @Index(name="blockWidthFrom_idx", columns={"blockWidthFrom"}),
 *      @Index(name="blockWidthTo_idx", columns={"blockWidthTo"}),
 *      @Index(name="blockHeightFrom_idx", columns={"blockHeightFrom"}),
 *      @Index(name="blockHeightTo_idx", columns={"blockHeightTo"}),
 *      @Index(name="teaser_configs_active_idx", columns={"active"})
 *    }
 * )
 */
class TeaserConfig extends Base
{
    /**
     * Данные
     *
     * @Column(type="text")
     * @var string
     */
    protected $data;

    /** диапазон */
    
    /**
     * @Column(type="integer")
     * @var int
     */
    protected $blockWidthFrom;

    /**
     * @Column(type="integer")
     * @var int
     */
    protected $blockWidthTo;

    /**
     * @Column(type="integer")
     * @var int
     */
    protected $blockHeightFrom;

    /**
     * @Column(type="integer")
     * @var int
     */
    protected $blockHeightTo;

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     * @return $this
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBlockWidthFrom()
    {
        return $this->blockWidthFrom;
    }

    /**
     * @param mixed $blockWidthFrom
     * @return $this
     */
    public function setBlockWidthFrom($blockWidthFrom)
    {
        $this->blockWidthFrom = $blockWidthFrom;
        return $this;
    }

    /**
     * @return int
     */
    public function getBlockWidthTo()
    {
        return $this->blockWidthTo;
    }

    /**
     * @param int $blockWidthTo
     * @return $this
     */
    public function setBlockWidthTo($blockWidthTo)
    {
        $this->blockWidthTo = $blockWidthTo;
        return $this;
    }

    /**
     * @return int
     */
    public function getBlockHeightFrom()
    {
        return $this->blockHeightFrom;
    }

    /**
     * @param int $blockHeightFrom
     * @return $this
     */
    public function setBlockHeightFrom($blockHeightFrom)
    {
        $this->blockHeightFrom = $blockHeightFrom;
        return $this;
    }

    /**
     * @return int
     */
    public function getBlockHeightTo()
    {
        return $this->blockHeightTo;
    }

    /**
     * @param int $blockHeightTo
     * @return $this
     */
    public function setBlockHeightTo($blockHeightTo)
    {
        $this->blockHeightTo = $blockHeightTo;
        return $this;
    }
}
