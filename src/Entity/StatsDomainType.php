<?php

namespace Project\Entity;

/**
 * Статистика по доменам с привязкой к типам
 *
 * @Entity
 * @Table(
 *  name="stats_domain_type",
 *  uniqueConstraints={
 *     @UniqueConstraint(name="stats_domain_type_calc_uniq_idx", columns={"uniq"}),
 * },
 *    indexes={
 *      @Index(name="stats_domain_type_domain_idx", columns={"domain"}),
 *      @Index(name="stats_domain_type_date_idx", columns={"date"}),
 *      @Index(name="stats_domain_type_adtype_idx", columns={"adtype"})
 *    }
 * )
 */
class StatsDomainType extends Stats
{
    /**
     * должна соотв-ть реальной
     */
    const TABLE = 'stats_domain_type';

    /**
     * Ключи аггрегации событий
     */
    const AGGREGATION_KEYS = [
        'uniq',
        'date',
        'domain',
        'adtype',
    ];

    /**
     * Счетчики
     */
    const INCREMENTS = [
        'rs',
        'rbq',
        'rbqu',
        'rb',
        'scv',
        'tcv',
        'prelv',
        'lead',
        'lead_amount',
        'accept',
        'accept_amount',
        'decline',
        'decline_amount',
        'invalid',
        'invalid_amount',
        'usd'
    ];

    /**
     * Ключ группировки
     * @Id
     * @Column(type="string")
     */
    protected $uniq;

    /**
     * Дата создания
     *
     * @Column(type="date")
     * @var \DateTime
     */
    protected $date;

    /**
     * @Column(type="string", nullable = true)
     * @var string
     */
    protected $domain;

    /**
     * @Column(type="string", nullable = true)
     * @var string
     */
    protected $adType;

    /**
     * Уникальные показы доменов пользователям
     *
     * @Column(type="integer", options={"default":0})
     * @var int
     */
    protected $usd;
}
