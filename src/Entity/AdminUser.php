<?php

namespace Project\Entity;

/**
 * Пользователи админки
 *
 * @Entity
 * @HasLifecycleCallbacks
 * @Table(
 *  name="admin_users"
 * )
 */
class AdminUser extends User
{
}
