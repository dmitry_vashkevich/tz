<?php

namespace Project\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Project\Entity\Schema\Base;

/**
 * Страны
 *
 * @Entity
 * @HasLifecycleCallbacks
 * @Table(
 *  name="countries"
 * )
 */
class Country extends Base
{
    /**
     * Название
     *
     * @Column(type="string", unique=true)
     */
    protected $name;

    /**
     * Код
     *
     * @Column(type="string", unique=true)
     */
    protected $code;

    /**
     * Офферы
     *
     * @ManyToMany(targetEntity="Offer", mappedBy="offer_countries")
     **/
    protected $offers;

    /**
     * конструктор
     */
    public function __construct()
    {
        $this->offers = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return Offer[] | ArrayCollection
     */
    public function getOffers()
    {
        return $this->offers;
    }
}
