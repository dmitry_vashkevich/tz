<?php

namespace Project\Entity;

/**
 * Статистика по доменам
 *
 * @Entity
 * @Table(
 *  name="stats_domain",
 *  uniqueConstraints={
 *     @UniqueConstraint(name="calc_uniq_idx", columns={"uniq"}),
 * },
 *    indexes={
 *      @Index(name="stats_domain_domain_idx", columns={"domain"}),
 *      @Index(name="stats_domain_date_idx", columns={"date"}),
 *      @Index(name="stats_domain_width_idx", columns={"width"}),
 *      @Index(name="stats_domain_height_idx", columns={"height"}),
 *      @Index(name="stats_domain_height_idx", columns={"height"})
 *    }
 * )
 */
class StatsDomain extends Stats
{
    /**
     * должна соотв-ть реальной
     */
    const TABLE = 'stats_domain';

    /**
     * Ключи аггрегации событий
     */
    const AGGREGATION_KEYS = [
        'uniq',
        'net_id',
        'date',
        'domain',
        'height',
        'width',
    ];

    /**
     * Счетчики
     */
    const INCREMENTS = [
        'rs',
        'rbq',
        'rbqu',
        'rb',
        'scv',
        'tcv',
        'prelv',
        'lead',
        'lead_amount',
        'accept',
        'accept_amount',
        'decline',
        'decline_amount',
        'invalid',
        'invalid_amount',
        'usd'
    ];

    /**
     * Ключ группировки
     * @Id
     * @Column(type="string")
     */
    protected $uniq;

    /**
     * Тип рекламы
     * @Column(type="integer", nullable = true)
     * @var string
     */
    protected $net_id;

    /**
     * Дата создания
     *
     * @Column(type="date")
     * @var \DateTime
     */
    protected $date;

    /**
     * @Column(type="string", nullable = true)
     * @var string
     */
    protected $domain;

    /**
     * @Column(type="integer", nullable = true)
     * @var int
     */
    protected $height;

    /**
     * @Column(type="integer", nullable = true)
     * @var int
     */
    protected $width;

    /**
     * Уникальные показы доменов пользователям
     *
     * @Column(type="integer", options={"default":0})
     * @var int
     */
    protected $usd;
}
