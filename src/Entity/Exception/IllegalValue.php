<?php

namespace Project\Entity\Exception;

use Project\Exception\RestException\ServerError\InternalServerErrorException;

/**
 * Исключение выбрасываемое при неверном значении поля
 */
class IllegalValue extends InternalServerErrorException
{

}
