<?php

namespace Project\Entity;

use Project\Entity\Schema\EntityWithDeterminedId;

/**
 * Кампания
 *
 * @Entity
 * @HasLifecycleCallbacks
 * @Table(
 *  name="prelands"
 * )
 */
class Preland extends EntityWithDeterminedId
{
    /**
     * Продукт
     *
     * @ManyToOne(targetEntity="\Project\Entity\Offer")
     * @JoinColumn(name="offer_id", referencedColumnName="id", nullable = true)
     */
    protected $offer;

    /**
     * Название
     *
     * @Column(type="string")
     */
    protected $name;

    /**
     * Урл
     *
     * @Column(type="string")
     */
    protected $url;

    /**
     * @return Offer
     */
    public function getOffer()
    {
        return $this->offer;
    }

    /**
     * @param Offer $offer
     * @return $this
     */
    public function setOffer(Offer $offer)
    {
        $this->offer = $offer;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }
}
