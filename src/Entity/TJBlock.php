<?php

namespace Project\Entity;

use Project\Entity\Schema\Base;

/**
 * Блоки
 *
 * @Entity
 * @HasLifecycleCallbacks
 * @Table(
 *  name="tj_blocks"
 * )
 */
class TJBlock extends Base
{
    /** названия интервальных параметров */
    const PARAM_CLICK_INTERVAL = 'click_interval';
    const PARAM_CLOSE_INTERVAL = 'close_interval';
    const PARAM_COL = 'col';
    const PARAM_ROW = 'row';
    const PARAM_CELL_WIDTH = 'cell_width';
    const PARAM_CELL_HEIGHT = 'cell_height';
    const PARAM_WIDTH = 'width';
    const PARAM_HEIGHT = 'height';
    
    /**
     * @ManyToOne(targetEntity="\Project\Entity\TJUser")
     * @JoinColumn(name="tj_user_id", referencedColumnName="id", nullable = true)
     * @var TJUser
     */
    protected $user;

    /**
     * @ManyToOne(targetEntity="\Project\Entity\TJSite")
     * @JoinColumn(name="tj_site_id", referencedColumnName="id", nullable = true)
     * @var TJSite
     */
    protected $site;

    /**
     * @Column(type="text")
     * @var string
     */
    protected $name;

    /**
     * @Column(type="boolean")
     * @var boolean
     */
    protected $custom;

    /**
     * @Column(type="text", nullable = true)
     * @var string
     */
    protected $config;

    /**
     * @Column(type="text", nullable = true)
     * @var string
     */
    protected $params;

    /**
     * @Column(type="integer", nullable = true)
     * @var int
     */
    protected $clickInterval;

    /**
     * @Column(type="integer", nullable = true)
     * @var int
     */
    protected $closeInterval;

    /**
     * @Column(type="integer", nullable = true)
     * @var int
     */
    protected $col;

    /**
     * @Column(type="integer", nullable = true)
     * @var int
     */
    protected $row;

    /**
     * @Column(type="integer", nullable = true)
     * @var int
     */
    protected $cell_width;

    /**
     * @Column(type="integer", nullable = true)
     * @var int
     */
    protected $cell_height;

    /**
     * @Column(type="integer", nullable = true)
     * @var int
     */
    protected $width;

    /**
     * @Column(type="integer", nullable = true)
     * @var int
     */
    protected $height;
    
    /**
     * Флаг "удаленной" сущности
     *
     * @Column(type="boolean", options={"default":false})
     */
    protected $deleted = false;

    /**
     * Тип рекламы
     *
     * @Column(type="string")
     * @var string
     */
    protected $adType;
    
    /**
     * @return bool
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     * @return $this
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
        return $this;
    }

    /**
     * @return TJUser
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param TJUser $user
     * @return $this
     */
    public function setUser(TJUser $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return TJSite
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * @param TJSite $site
     * @return $this
     */
    public function setSite(TJSite $site)
    {
        $this->site = $site;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param mixed $config
     * @return $this
     */
    public function setConfig($config)
    {
        $this->config = $config;
        $this->updateConfigParams();
        return $this;
    }

    /**
     * @return string
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param string $params
     * @return $this
     */
    public function setParams($params)
    {
        $this->params = $params;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isCustom()
    {
        return $this->custom;
    }

    /**
     * @param boolean $custom
     * @return $this
     */
    public function setCustom($custom)
    {
        $this->custom = $custom;
        return $this;
    }

    /**
     * @return string
     */
    public function getAdType()
    {
        return $this->adType;
    }

    /**
     * @param string $adType
     * @return $this
     */
    public function setAdType($adType)
    {
        $this->adType = $adType;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRow()
    {
        return $this->row;
    }

    /**
     * @param mixed $row
     * @return $this
     */
    public function setRow($row)
    {
        $this->row = $row;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCol()
    {
        return $this->col;
    }

    /**
     * @param mixed $col
     * @return $this
     */
    public function setCol($col)
    {
        $this->col = $col;
        return $this;
    }

    /**
     * @return int
     */
    public function getCloseInterval()
    {
        return $this->closeInterval;
    }

    /**
     * @param int $closeInterval
     * @return $this
     */
    public function setCloseInterval($closeInterval)
    {
        $this->closeInterval = $closeInterval;
        return $this;
    }

    /**
     * @return int
     */
    public function getClickInterval()
    {
        return $this->clickInterval;
    }

    /**
     * @param int $clickInterval
     * @return $this
     */
    public function setClickInterval($clickInterval)
    {
        $this->clickInterval = $clickInterval;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCellWidth()
    {
        return $this->cell_width;
    }

    /**
     * @param mixed $cell_width
     * @return $this
     */
    public function setCellWidth($cell_width)
    {
        $this->cell_width = $cell_width;
        return $this;
    }

    /**
     * @return int
     */
    public function getCellHeight()
    {
        return $this->cell_height;
    }

    /**
     * @param int $cell_height
     * @return $this
     */
    public function setCellHeight($cell_height)
    {
        $this->cell_height = $cell_height;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param int $width
     * @return $this
     */
    public function setWidth($width)
    {
        $this->width = $width;
        return $this;
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param int $height
     * @return $this
     */
    public function setHeight($height)
    {
        $this->height = $height;
        return $this;
    }
    
    /**
     * обновление интервальных таймаутов
     */
    private function updateConfigParams()
    {
        $this->clickInterval = null;
        $this->closeInterval = null;
        $config = json_decode($this->config, true);
        if (isset($config[self::PARAM_CLICK_INTERVAL])) {
            $this->clickInterval = (int) $config[self::PARAM_CLICK_INTERVAL] * 60;
        }

        if (isset($config[self::PARAM_CLOSE_INTERVAL])) {
            $this->closeInterval = (int) $config[self::PARAM_CLOSE_INTERVAL] * 60;
        }

        if (isset($config[self::PARAM_COL])) {
            $this->col = (int) $config[self::PARAM_COL];
        }

        if (isset($config[self::PARAM_ROW])) {
            $this->row = (int) $config[self::PARAM_ROW];
        }

        if (isset($config[self::PARAM_CELL_HEIGHT])) {
            $this->cell_height = (int) $config[self::PARAM_CELL_HEIGHT];
        }

        if (isset($config[self::PARAM_CELL_WIDTH])) {
            $this->cell_width = (int) $config[self::PARAM_CELL_WIDTH];
        }

        if (isset($config[self::PARAM_HEIGHT])) {
            $this->height = (int) $config[self::PARAM_HEIGHT];
        }

        if (isset($config[self::PARAM_WIDTH])) {
            $this->width = (int) $config[self::PARAM_WIDTH];
        }
    }
}
