<?php

namespace Project\Entity;

use Project\Entity\Exception\IllegalValue;
use Project\Entity\Schema\Base;

/**
 * Выплаты
 *
 * @Entity
 * @HasLifecycleCallbacks
 * @Table(
 *  name="payments"
 * )
 */
class Payment extends Base
{
    /**
     * Статусы выплат
     */
    const STATUS_REQUEST = 'request'; //запрошена
    const STATUS_PAID    = 'paid';    //выплачена
    const STATUS_CANCEL  = 'cancel';  //отменена

    protected static $statuses = [
        self::STATUS_REQUEST,
        self::STATUS_PAID,
        self::STATUS_CANCEL,
    ];

    /**
     * Кошелек
     *
     * @ManyToOne(targetEntity="\Project\Entity\Wallet")
     * @JoinColumn(name="wallet_id", referencedColumnName="id", nullable = false)
     */
    protected $wallet;

    /**
     * Юзер
     *
     * @ManyToOne(targetEntity="\Project\Entity\TJUser")
     * @JoinColumn(name="user_id", referencedColumnName="id", nullable = false)
     */
    protected $user;

    /**
     * Сумма
     *
     * @Column(type="decimal", precision=12, scale=4)
     */
    protected $amount;

    /**
     * Статус
     *
     * @Column(type="string")
     */
    protected $status;

    /**
     * Комментарий
     *
     * @Column(type="string", nullable=true)
     */
    protected $comment;

    /**
     * @return Wallet
     */
    public function getWallet()
    {
        return $this->wallet;
    }

    /**
     * @param Wallet $wallet
     * @return $this
     */
    public function setWallet(Wallet $wallet)
    {
        $this->wallet = $wallet;
        return $this;
    }

    /**
     * @return SiteUser
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param TJUser $user
     * @return $this
     */
    public function setUser(TJUser $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     * @return $this
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param $status
     * @return $this
     * @throws IllegalValue
     */
    public function setStatus($status)
    {
        if (!in_array($status, self::$statuses)) {
            throw new IllegalValue("Illegal status: '{$status}'");
        }

        $this->status = $status;

        return $this;
    }


    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     * @return $this
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
        return $this;
    }
}
