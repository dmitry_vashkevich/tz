<?php

namespace Project\Entity;

use Project\Entity\Schema\EntityWithDeterminedId;

/**
 * Типы кошельков
 *
 * @Entity
 * @HasLifecycleCallbacks
 * @Table(
 *  name="wallet_types"
 * )
 */
class WalletType extends EntityWithDeterminedId
{
    /**
     * Название
     *
     * @Column(type="string", unique=true)
     */
    protected $name;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }
}
