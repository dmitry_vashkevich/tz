<?php

namespace Project\Entity\Type;

/**
 * Враппер над \DateTime для доктрины с приведением к string
 */
class DateKey extends \DateTime
{
    const KEY_FORMAT = 'Y-m-d';
    /**
     * @return string
     */
    public function __toString()
    {
        return $this->format(self::KEY_FORMAT);
    }

    /**
     * @param \DateTime $dateTime
     * @return static
     */
    public static function fromDateTime(\DateTime $dateTime)
    {
        return new static($dateTime->format(self::KEY_FORMAT));
    }
}
