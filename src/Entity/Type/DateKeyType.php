<?php

namespace Project\Entity\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\DateTimeType as DoctrineDateTimeType;

/**
 * Расширение Date для использования в качестве составного первичного ключа таблицы для доктрины
 */
class DateKeyType extends DoctrineDateTimeType
{
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        $value = parent::convertToPHPValue($value, $platform);
        if ($value !== null) {
            $value = DateKey::fromDateTime($value);
        }
        return $value;
    }
}
