<?php

namespace Project\Entity;

/**
 * Статистика
 *
 * @Entity
 * @Table(name="stats_block")
 */
class StatsBlock extends Stats
{
    /**
     * должна соотв-ть реальной
     */
    const TABLE = 'stats_block';

    /**
     * Ключи аггрегации событий
     */
    const AGGREGATION_KEYS = [
        'uniq',
        'config_id',
        'blockbordercolor',
        'cellbordercolor',
        'imgbordercolor',
        'btncolor',
        'headercolor',
        'txtcolor',
        'headerstyle',
        'txtstyle',
        'blockbgcolor',
        'cellbgcolor',
    ];

    /**
     * Ключ группировки
     * @Id
     * @Column(type="string")
     */
    protected $uniq;

    /**
     * Идентификатор конфига блока
     *
     * @Column(type="integer", nullable = false)
     * @var int
     */
    protected $config_id;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $blockBorderColor;


    /**
     * @Column(type="string")
     * @var string
     */
    protected $cellBorderColor;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $imgBorderColor;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $btnColor;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $headerColor;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $txtColor;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $headerStyle;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $txtStyle;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $blockBgColor;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $cellBgColor;
}
