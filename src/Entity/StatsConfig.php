<?php

namespace Project\Entity;

/**
 * Статистика по конфигам
 *
 * @Entity
 * @Table(name="stats_config")
 */
class StatsConfig extends Stats
{
    /**
     * должна соотв-ть реальной
     */
    const TABLE = 'stats_config';

    /**
     * Ключи аггрегации событий
     */
    const AGGREGATION_KEYS = [
        'uniq',
        'adtype',
        'date',
        'campaign_id',
        'config_id',
    ];

    /**
     * Ключ группировки
     * @Id
     * @Column(type="string")
     */
    protected $uniq;

    /**
     * Тип рекламы
     * @Column(type="string", nullable = false)
     * @var string
     */
    protected $adType;

    /**
     * Дата создания
     *
     * @Column(type="date")
     * @var \DateTime
     */
    protected $date;

    /**
     * Идентификатор кампании
     *
     * @Column(type="integer", nullable = false)
     * @var int
     */
    protected $campaign_id;

    /**
     * @Column(type="integer")
     * @var int
     */
    protected $config_id;
}
