<?php

namespace Project\Entity;

/**
 * Пользователи (рекламодатели)
 *
 * @Entity
 * @HasLifecycleCallbacks
 * @Table(
 *  name="site_users"
 * )
 */
class SiteUser extends User
{
    /**
     * @Column(type="string", unique=true)
     * @var string
     */
    protected $key;

    /**
     * Внешний ли рекалмодатель (доступен функционал внутренних(!) кампаний и ограничений)
     *
     * @Column(type="boolean", options={"default":true})
     */
    protected $external = true;

    /**
     * Установки перед вставкой в БД
     *
     * @PrePersist
     */
    public function prePersist()
    {
        parent::prePersist();
        $this->key = sha1(rand(1,1000) . '|' . uniqid('site_user'));
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @return bool
     */
    public function isExternal()
    {
        return $this->external;
    }

    /**
     * @param bool $external
     * @return $this
     */
    public function setExternal($external)
    {
        $this->external = $external;
        return $this;
    }
}
