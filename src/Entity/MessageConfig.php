<?php

namespace Project\Entity;

use Project\Entity\Schema\Base;

/**
 * Конфиги для отображения тизеров
 *
 * @Entity
 * @HasLifecycleCallbacks
 * @Table(
 *  name="message_configs",
 *    indexes={
 *      @Index(name="message_configs_active_idx", columns={"active"})
 *    }
 * )
 */
class MessageConfig extends Base
{
    /**
     * Данные
     *
     * @Column(type="text")
     * @var string
     */
    protected $data;

    /**
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param string $data
     * @return $this
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }
}
