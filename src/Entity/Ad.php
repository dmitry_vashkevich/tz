<?php

namespace Project\Entity;

use Project\Entity\Schema\Base;

/**
 * Рекламный материал
 *
 * @Entity
 * @HasLifecycleCallbacks
 * @Table(
 *  name="ads"
 * )
 */
class Ad extends Base
{
    /** Типы Рекламы */
    const ADTYPE_TEASER = 'teaser';
    const ADTYPE_BANNER = 'banner';
    const ADTYPE_MESSAGE = 'message';
    const ADTYPE_POPUNDER = 'popunder';
    const ADTYPE_CLICKUNDER = 'clickunder';
    const ADTYPE_SLIDER_TEASER = 'slider_teaser';
    const ADTYPE_SLIDER_BANNER = 'slider_banner';
    const ADTYPE_CASHBACK = 'cashback';

    /** @var array  */
    public static $adtypes = [
        self::ADTYPE_TEASER,
        self::ADTYPE_BANNER,
        self::ADTYPE_MESSAGE,
        self::ADTYPE_POPUNDER,
        self::ADTYPE_CLICKUNDER,
        self::ADTYPE_SLIDER_TEASER,
        self::ADTYPE_SLIDER_BANNER,
        self::ADTYPE_CASHBACK,
    ];
    
    /** @var array  */
    public static $typeHandlers = [
        self::ADTYPE_CLICKUNDER => 'Clickunder',
        self::ADTYPE_TEASER => 'Teaser',
        self::ADTYPE_BANNER => 'Banner',
        self::ADTYPE_MESSAGE => 'Message',
        self::ADTYPE_POPUNDER => 'Popunder',
        self::ADTYPE_SLIDER_BANNER => 'SliderBanner',
        self::ADTYPE_SLIDER_TEASER => 'SliderTeaser',
        self::ADTYPE_CASHBACK => 'Cashback',
    ];
    
    /**
     * @Column(type="string")
     */
    protected $type;

    /**
     * Кампания
     *
     * @ManyToOne(targetEntity="\Project\Entity\Campaign")
     * @JoinColumn(name="campaign_id", referencedColumnName="id", nullable = false)
     */
    protected $campaign;

    /**
     * @Column(type="integer", nullable=false)
     */
    protected $campaign_id;

    /**
     * JSON массив ссылок на изображения
     *
     * @Column(type="text", nullable=true)
     */
    protected $images;

    /**
     * JSON данные
     *
     * @Column(type="text")
     */
    protected $data;

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCampaign()
    {
        return $this->campaign;
    }

    /**
     * @param mixed $campaign
     * @return $this
     */
    public function setCampaign($campaign)
    {
        $this->campaign = $campaign;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param mixed $images
     * @return $this
     */
    public function setImages($images)
    {
        $this->images = $images;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     * @return $this
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }
}
