<?php

namespace Project\Entity;

/**
 * Пользователи
 *
 * @Entity
 * @HasLifecycleCallbacks
 * @Table(
 *  name="tj_users"
 * )
 */
class TJUser extends User
{
    /**
     * @Column(type="string", nullable=true)
     * @var string
     */
    protected $firstName;

    /**
     * @Column(type="string", nullable=true)
     * @var string
     */
    protected $secondName;

    /**
     * @Column(type="string", nullable=true)
     * @var string
     */
    protected $email;

    /**
     * @Column(type="string", nullable=true)
     * @var string
     */
    protected $skype;

    /**
     * @Column(type="string", nullable=true)
     * @var string
     */
    protected $icq;

    /**
     * @Column(type="string", nullable=true)
     * @var string
     */
    protected $jabber;

    /**
     * @Column(type="text", nullable=true)
     * @var string
     */
    protected $siteList;

    /**
     * Рефовод (кто пригласил)
     *
     * @ManyToOne(targetEntity="\Project\Entity\TJUser")
     * @JoinColumn(name="ref_user_id", referencedColumnName="id", nullable = true)
     * @var TJUser
     */
    protected $refUser;

    /**
     * Баланс
     *
     * @Column(type="decimal", precision=12, scale = 4)
     */
    protected $balance = 0;

    /**
     * Холд
     *
     * @Column(type="decimal", precision = 12, scale = 4)
     */
    protected $hold = 0;

    /**
     * Флаг активности сущности
     *
     * @Column(type="boolean", options={"default":true})
     */
    protected $autoPay = true;

    /**
     * @Column(type="integer", options={"default":100})
     */
    protected $rate = 100;

    /**
     * @return int
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * @param int $rate
     * @return $this
     */
    public function setRate($rate)
    {
        $this->rate = $rate;
        return $this;
    }
    
    /**
     * @return string
     */
    public function getIcq()
    {
        return $this->icq;
    }

    /**
     * @param string $icq
     * @return $this
     */
    public function setIcq($icq)
    {
        $this->icq = $icq;
        return $this;
    }

    /**
     * @return string
     */
    public function getJabber()
    {
        return $this->jabber;
    }

    /**
     * @param string $jabber
     * @return $this
     */
    public function setJabber($jabber)
    {
        $this->jabber = $jabber;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAutoPay()
    {
        return $this->autoPay;
    }

    /**
     * @param bool $autoPay
     * @return $this
     */
    public function setAutoPay($autoPay)
    {
        $this->autoPay = $autoPay;
        return $this;
    }

    /**
     * @return string
     */
    public function getSiteList()
    {
        return $this->siteList;
    }

    /**
     * @param string $siteList
     * @return $this
     */
    public function setSiteList($siteList)
    {
        $this->siteList = $siteList;
        return $this;
    }

    /**
     * @return TJUser
     */
    public function getRefUser()
    {
        return $this->refUser;
    }

    /**
     * @param TJUser $refUser
     * @return $this
     */
    public function setRefUser(TJUser $refUser)
    {
        $this->refUser = $refUser;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return $this
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getSecondName()
    {
        return $this->secondName;
    }

    /**
     * @param string $secondName
     * @return $this
     */
    public function setSecondName($secondName)
    {
        $this->secondName = $secondName;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getSkype()
    {
        return $this->skype;
    }

    /**
     * @param string $skype
     * @return $this
     */
    public function setSkype($skype)
    {
        $this->skype = $skype;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * @param mixed $balance
     * @return $this
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHold()
    {
        return $this->hold;
    }

    /**
     * @param mixed $hold
     * @return $this
     */
    public function setHold($hold)
    {
        $this->hold = $hold;
        return $this;
    }
}
