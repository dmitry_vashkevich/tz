<?php

namespace Project\Entity;

use Project\Entity\Schema\Model;

/**
 * Абстрактная статистика с набором индикаторов
 */
abstract class Stats extends Model
{
    /**
     * должна соотв-ть реальной
     */
    const TABLE = 'abstract';

    /**
     * Ключи аггрегации событий
     */
    const AGGREGATION_KEYS = ['abstract'];

    /**
     * Счетчики
     */
    const INCREMENTS = [
        'rs',
        'rbq',
        'rbqu',
        'rb',
        'scv',
        'tcv',
        'prelv',
        'lead',
        'lead_amount',
        'accept',
        'accept_amount',
        'decline',
        'decline_amount',
        'invalid',
        'invalid_amount',
    ];

    /**
     * Индикаторы
     */

    /**
     * Показы общие по запросу от скрипта
     *
     * @Column(type="integer", options={"default":0})
     * @var int
     */
    protected $rs;

    /**
     * Запросы блоков
     *
     * @Column(type="integer", options={"default":0})
     * @var int
     */
    protected $rbq;

    /**
     * Уникальные запросы блоков
     *
     * @Column(type="integer", options={"default":0})
     * @var int
     */
    protected $rbqu;

    /**
     * Отгрузки блоков
     *
     * @Column(type="integer", options={"default":0})
     * @var int
     */
    protected $rb;
    
    /**
     * Переходы по ссылке кампании по версии системы
     *
     * @Column(type="integer", options={"default":0})
     * @var int
     */
    protected $scv;

    /**
     * Переходы по ссылке кампании по версии трекера
     *
     * @Column(type="integer", options={"default":0})
     * @var int
     */
    protected $tcv;

    /**
     * Переходы с преленда на ленд, в случае если есть преленд
     *
     * @Column(type="integer", options={"default":0})
     * @var int
     */
    protected $prelv;

    /**
     * Заказы
     *
     * @Column(type="integer", options={"default":0})
     * @var int
     */
    protected $lead;

    /**
     * @Column(type="decimal",precision=12,scale=4, options={"default":0})
     * @var float
     */
    protected $lead_amount;

    /**
     * Счетчик оплат
     *
     * @Column(type="integer", options={"default":0})
     * @var int
     */
    protected $accept;

    /**
     * @Column(type="decimal",precision=12,scale=4, options={"default":0})
     * @var float
     */
    protected $accept_amount;

    /**
     * Счетчик отказов
     *
     * @Column(type="integer", options={"default":0})
     * @var int
     */
    protected $decline;

    /**
     * @Column(type="decimal",precision=12,scale=4, options={"default":0})
     * @var float
     */
    protected $decline_amount;

    /**
     * Счетчик невалидных заказов, например пользователь не заполнил все данные или повторно заказал
     *
     * @Column(type="integer", options={"default":0})
     * @var int
     */
    protected $invalid;

    /**
     * @Column(type="decimal",precision=12,scale=4, options={"default":0})
     * @var float
     */
    protected $invalid_amount;

    /**
     * @inheritdoc
     */
    public static function calcUniq(array $statData)
    {
        $concat = '';
        foreach (static::AGGREGATION_KEYS as $key) {
            if (isset ($statData[$key])) {
                $concat .= $statData[$key] . '|';
            }
        }
        return sha1($concat);
    }
}
