<?php

namespace Project\Entity\Schema;

/**
 * Класс определяет сущности с определенным явнозадаваемым id
 */
abstract class EntityWithDeterminedId extends Base
{
    /**
     * Идентификатор задается явно
     *
     * @Id
     * @Column(type="integer")
     */
    protected $id;

    /**
     * Установка идентификатора
     *
     * @param $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
}
