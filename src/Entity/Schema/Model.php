<?php

namespace Project\Entity\Schema;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Project\Check\Entity;
use Project\Check\Exception\EmptyEntityException;
use Project\Check\Exception\WrongEntityException;
use Project\Proxy;

/**
 * Базовый класс моделей
 */
abstract class Model
{
    /**
     * Получение репозитория для работы с объектом
     *
     * @return \Doctrine\ORM\EntityRepository
     */
    public static function repo()
    {
        return Proxy::init()->getEntityManager()->getRepository(static::class);
    }

    /**
     * Сохранить в БД новый объект
     *
     * @return static
     */
    public function save()
    {
        $manager = $this->getEntityManager();

        $manager->persist($this);
        $manager->flush();

        return $this;
    }

    /**
     * Удалить из БД объект
     *
     * @return static
     */
    public function remove()
    {
        $manager = Proxy::init()->getEntityManager();

        $manager->remove($this);
        $manager->flush();

        return $this;
    }

    /**
     * Сформировать непустую выборку одной строки по параметру
     *
     * @param string $key
     * @param mixed $value
     *
     * @throws EmptyEntityException
     * @throws WrongEntityException
     *
     * @return static
     */
    public static function prepareByParam($key, $value)
    {
        return Entity::check(
            static::repo()->findOneBy([$key => $value]),
            static::class
        );
    }

    /**
     * Сформировать непустую выборку одной строки по списку параметров
     *
     * @param array $criteria
     *
     * @throws EmptyEntityException
     * @throws WrongEntityException
     *
     * @return static
     */
    public static function prepareByParams(array $criteria)
    {
        return Entity::check(
            static::repo()->findOneBy($criteria),
            static::class
        );
    }

    /**
     * Сформировать выборку по списку параметров
     *
     * @param Criteria $criteria
     *
     * @return Collection | static[]
     */
    public static function prepareList(Criteria $criteria = null)
    {
        if ($criteria === null) {
            $entities = static::repo()->matching(new Criteria());
        } else {
            $entities = static::repo()->matching($criteria);
        }

        return $entities;
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    protected function getEntityManager()
    {
        return Proxy::init()->getEntityManager();
    }
}
