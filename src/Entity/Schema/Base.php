<?php

namespace Project\Entity\Schema;

use Project\Check\Entity;
use Project\Check\Exception\EmptyEntityException;

/**
 * Модель со стандартными полями и методами их обсулживающими
 */
abstract class Base extends Model
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue("IDENTITY")
     */
    protected $id;

    /**
     * Дата создания
     *
     * @Column(type="datetime")
     */
    protected $added;

    /**
     * Дата изменения
     *
     * @Column(type="datetime")
     */
    protected $changed;

    /**
     * Флаг активности сущности
     *
     * @Column(type="boolean")
     */
    protected $active;

    /**
     * Приведение к строке
     *
     * @return string
     */
    public function __toString()
    {
        return static::class . '('. $this->getId() .')';
    }

    /**
     * Получить id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Получить дату добавления
     *
     * @return \DateTime
     */
    public function getAdded()
    {
        return $this->added;
    }

    /**
     * Получить дата изменения
     *
     * @return \DateTime
     */
    public function getChanged()
    {
        return $this->changed;
    }

    /**
     * При создании записи добавляем время созания
     *
     * @param \DateTime $added
     *
     * @return $this
     */
    public function setAdded(\DateTime $added)
    {
        $this->added = $added;

        return $this;
    }

    /**
     * Установка даты обновления строки
     *
     * @param \DateTime $changed
     *
     * @return $this
     */
    public function setChanged(\DateTime $changed)
    {
        $this->changed = $changed;

        return $this;
    }

    /**
     * Активна ли строчка в БД?
     *
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * Установить флаг активности
     *
     * @param boolean $active
     *
     * @return $this
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Установки перед вставкой в БД
     *
     * @PrePersist
     */
    public function prePersist()
    {
        $now = new \DateTime("now");

        if (!$this->getAdded()) {
            $this->setAdded($now);
        }

        if (!$this->getChanged()) {
            $this->setChanged($now);
        }

        if ($this->active === null) {
            $this->active = true;
        }
    }

    /**
     * Установки перед вставкой обновлением в БД
     *
     * @PreUpdate
     */
    public function preUpdate()
    {
        $this->setChanged(new \DateTime("now"));
    }

    /**
     * Ищет и создает объект на исновании идентификатора в БД
     *
     * Осуществляет поиск по БД, если данные есть, то вернет наполненный данными объект
     * Если найти в БД ничего не удалось - бросит эксепшн
     *
     * @throws EmptyEntityException
     *
     * @param int $id
     * @return static
     */
    public static function prepareById($id)
    {
        return Entity::check(
            static::repo()->find((int) $id),
            static::class
        );
    }
}
