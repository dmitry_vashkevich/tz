<?php

namespace Project\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Project\Entity\Schema\EntityWithDeterminedId;

/**
 * Продукты
 *
 * @Entity
 * @HasLifecycleCallbacks
 * @Table(
 *  name="offers"
 * )
 */
class Offer extends EntityWithDeterminedId
{
    /**
     * Название
     *
     * @Column(type="string")
     */
    protected $name;

    /**
     * Название
     *
     * @Column(type="text", nullable = true)
     */
    protected $description;

    /**
     * Модель ценообразования
     *
     * @Column(type="string", nullable = true)
     */
    protected $model;

    /**
     * @ManyToMany(targetEntity="\Project\Entity\OfferCat", inversedBy="offers")
     * @JoinTable(name="offer_offercats")
     */
    protected $offerCats;

    /**
     * Изображение
     *
     * @Column(type="string", nullable = true)
     */
    protected $imageUrl;

    /**
     * Уменьшенное изображение
     *
     * @Column(type="string", nullable = true)
     */
    protected $imageSmallUrl;
    
    /**
     * Конструктор
     */
    public function __construct()
    {
        $this->offerCats = new ArrayCollection();
    }
    
    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param string $imageUrl
     *
     * @return $this
     */
    public function setImageUrl($imageUrl)
    {
        $this->imageUrl = $imageUrl;

        return $this;
    }

    /**
     * @param string $imageSmallUrl
     *
     * @return $this
     */
    public function setImageSmallUrl($imageSmallUrl)
    {
        $this->imageSmallUrl = $imageSmallUrl;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param string $model
     *
     * @return $this
     */
    public function setPriceModel($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * @param OfferCat $offerCat
     *
     * @return $this
     */
    public function addOfferCat(OfferCat $offerCat)
    {
        $this->offerCats[] = $offerCat;

        return $this;
    }

    /**
     * @return OfferCat[] | ArrayCollection
     */
    public function getOfferCats()
    {
        return $this->offerCats;
    }

    /**
     * @return mixed
     */
    public function getImageUrl()
    {
        return $this->imageUrl;
    }

    /**
     * @return mixed
     */
    public function getImageSmallUrl()
    {
        return $this->imageSmallUrl;
    }
}
