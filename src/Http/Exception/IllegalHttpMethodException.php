<?php

namespace Project\Http\Exception;

use Project\Exception\RestException\ServerError\InternalServerErrorException;

/**
 * Недопустимый http метод
 */
class IllegalHttpMethodException extends InternalServerErrorException implements HttpExceptionInterface
{

}
