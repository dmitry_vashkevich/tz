<?php

namespace Project\Http\Exception;

use Project\Exception\RestException\ServerError\ServiceUnavailableException;

/**
 * У http ответа статус - ошибка
 */
class ErrorStatusCodeException extends ServiceUnavailableException implements HttpExceptionInterface
{

}
