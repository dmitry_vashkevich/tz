<?php

namespace Project\Http\Exception;

use Project\Exception\RestException\ServerError\InternalServerErrorException;

/**
 * Пустой url
 */
class EmptyUrlException extends InternalServerErrorException implements HttpExceptionInterface
{

}
