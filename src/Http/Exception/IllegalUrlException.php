<?php

namespace Project\Http\Exception;

use Project\Exception\RestException\ServerError\InternalServerErrorException;

/**
 * Недопустимый url для http запроса
 */
class IllegalUrlException extends InternalServerErrorException implements HttpExceptionInterface
{

}
