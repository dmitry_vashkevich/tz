<?php

namespace Project\Http;

use Guzzle\Log\LogAdapterInterface;
use Project\Logger;
use Psr\Log\LoggerInterface;

/**
 * Адаптер для логгера с которым работает HTTP клиент
 */
class LogAdapter implements LogAdapterInterface
{
    /**
     * Маппер для монолога
     *
     * @var array
     */
    private static $mapping = [
        LOG_DEBUG   => \Monolog\Logger::DEBUG,
        LOG_INFO    => \Monolog\Logger::INFO,
        LOG_WARNING => \Monolog\Logger::WARNING,
        LOG_ERR     => \Monolog\Logger::ERROR,
        LOG_CRIT    => \Monolog\Logger::CRITICAL,
        LOG_ALERT   => \Monolog\Logger::ALERT
    ];

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Конструктор
     *
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Логгирование в соответствии с интерфейсом
     * Специально в лог все сообщения пишутся как info, чтобы исключить
     * ситуацию, когда ядро бытается записать DEBUG.
     *
     * @param string $message
     * @param int $priority
     * @param array $extras
     *
     * @return null
     */
    public function log($message, $priority = LOG_INFO, $extras = [])
    {
        return $this->logger->log(self::$mapping[LOG_INFO], $message, $this->buildTags());
    }

    /**
     * Создать тэги
     *
     * @return array
     */
    private function buildTags()
    {
        return ['group' => 'request'];
    }
}
