<?php

namespace Project\Http\Request;

/**
 * Формирует put-запрос через Guzzle
 */
class Put extends \Project\Http\Request
{
    /**
     * Построить запрос
     *
     * @param \Guzzle\Http\Client $client
     * @return \Guzzle\Http\Message\RequestInterface
     */
    public function build(\Guzzle\Http\Client $client)
    {
        return $client->put(
            $this->uri,
            $this->headers,
            $this->body,
            $this->options
        );
    }
}
