<?php

namespace Project\Http\Request;

class Post  extends \Project\Http\Request
{
    /**
     * Построить запрос
     *
     * @param \Guzzle\Http\Client $client
     * @return \Guzzle\Http\Message\RequestInterface
     */
    public function build(\Guzzle\Http\Client $client)
    {
        return $client->post(
            $this->uri,
            $this->headers,
            $this->body,
            $this->options
        );
    }
}
