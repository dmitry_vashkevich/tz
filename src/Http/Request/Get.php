<?php

namespace Project\Http\Request;

class Get extends \Project\Http\Request
{
    /**
     * Построить запрос
     *
     * @param \Guzzle\Http\Client $client
     * @return \Guzzle\Http\Message\RequestInterface
     */
    public function build(\Guzzle\Http\Client $client)
    {
        $request = $client->get(
            $this->uri,
            $this->headers, 
            $this->options
        );

        $this->setGetParams($request);

        return $request;
    }

    /**
     * Установить GET параметры для HTTP запроса
     *
     * @param \Guzzle\Http\Message\RequestInterface $request
     */
    private function setGetParams(\Guzzle\Http\Message\RequestInterface $request)
    {
       if(is_array($this->body) && !empty($this->body)) {
            $query = $request->getQuery();
            foreach($this->body as $key=> $val) {
                $query->set($key, $val);
            }
        }
    }
}
