<?php

namespace Project\Http;

use Project\Http\Exception\IllegalHttpMethodException;

/**
 * Http запрос
 */
abstract class Request
{
    /**
     * URI запроса
     *
     * @var string
     */
    protected $uri;

    /**
     * Заголовки
     *
     * @var null|array|\Guzzle\Common\Collection
     */
    protected $headers = null;

    /**
     * Тело запроса для POST запроса
     *
     * @var null|string|resource|array|\Guzzle\Http\EntityBodyInterface
     */
    protected $body = null;

    /**
     * Массив параметров запроса
     *
     * @var array
     */
    protected $options = [];

    /**
     * Создание запроса
     *
     * @param string $httpMethod [POST, GET]
     * @return \Project\Http\Request
     * @throws \Project\Http\Exception\IllegalHttpMethodException
     */
    public static function factory($httpMethod)
    {
        $class = '\\Project\\Http\\Request\\'.ucfirst(strtolower($httpMethod));

        if (!class_exists($class)) {
            throw new IllegalHttpMethodException("Http Method '{$httpMethod}' not allowed");
        }

        return new $class();
    }

    /**
     * Установить URI запроса
     *
     * @param string $uri
     * @return \Project\Http\Request
     */
    public function setUri($uri)
    {
        $this->uri = $uri;

        return $this;
    }

    /**
     * Установить заголовки
     *
     * @param null|array|\Guzzle\Common\Collection $headers
     * @return \Project\Http\Request
     */
    public function setHeaders($headers = null)
    {
        $this->headers = $headers;

        return $this;
    }

    /**
     * Установить тело запроса для POST запроса
     *
     * @param null|string|resource|array|\Guzzle\Http\EntityBodyInterface $body
     * @return \Project\Http\Request
     */
    public function setBody($body = null)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Установить параметры
     *
     * @param array $options
     * @return \Project\Http\Request
     */
    public function setOptions(array $options = [])
    {
        $this->options = $options;

        return $this;
    }

    /**
     * Построить запрос
     *
     * @param \Guzzle\Http\Client $client клиент через который будет осуществляться запрос
     * @return \Guzzle\Http\Message\RequestInterface
     */
    abstract public function build(\Guzzle\Http\Client $client);
}
