<?php

namespace Project\Http;

use Project\Proxy;
use Project\Http\Exception\EmptyUrlException;
use Project\Http\Exception\ErrorStatusCodeException;
use Guzzle\Http\Message\Response;

/**
 * Клиент Http. Обертка над Guzzle с логированием запросов
 */
class Client
{
    /**
     * Доступные методы запросов
     */
    const HTTP_METHOD_POST  = 'POST';
    const HTTP_METHOD_GET   = 'GET';
    const HTTP_METHOD_PUT   = 'PUT';

    /**
     * HTTP(S) порты
     */
    const HTTP_PORT = 80;
    const HTTPS_PORT = 443;

    /**
     * Url запроса
     *
     * @var string
     */
    private $url;

    /**
     * Порт
     *
     * @var int
     */
    private $port = null;

    /**
     * Метод запроса
     *
     * @var string
     */
    private $httpMethod;

    /**
     * Таймаут
     *
     * @var int
     */
    private $timeout = 5;

    /**
     * Таймаут на соединение
     *
     * @var int
     */
    private $connectionTimeout = 0;

    /**
     * @var \Guzzle\Http\Client
     */
    public $client;

    /**
     * Конструктор
     * инициализация конкретного клиента
     */
    public function __construct()
    {
        $this->client = Proxy::init()->getHttpClient();
    }

    /**
     * Инициализация
     *
     * @return \Project\Http\Client
     */
    public static function init()
    {
        return new self();
    }

    /**
     * Установка метода запроса
     *
     * @param string $method
     *
     * @return \Project\Http\Client
     */
    public function setHttpMethod($method)
    {
        $this->httpMethod = strtoupper($method);

        return $this;
    }

    /**
     * Установка таймаута
     *
     * @param int $timeout
     *
     * @return \Project\Http\Client
     */
    public function setTimeout($timeout)
    {
        $this->timeout = (int) $timeout;

        return $this;
    }

    /**
     * Установка таймаута соединения
     *
     * @param int $connectionTimeout
     * @return \Project\Http\Client
     */
    public function setConnectionTimeout($connectionTimeout)
    {
        $this->connectionTimeout = $connectionTimeout;

        return $this;
    }

    /**
     * Установка URL запроса
     *
     * @param string $url
     * @throws \Project\Http\Exception\IllegalUrlException
     * @return \Project\Http\Client
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Получение URL запроса
     *
     * @throws \Project\Http\Exception\EmptyUrlException
     * @return string
     */
    public function getUrl()
    {
        if (!$this->url) {
            throw new EmptyUrlException('Url not set');
        }

        return $this->url;
    }

    /**
     * Установка порта
     *
     * @param integer $port
     * @return \Project\Http\Client
     */
    public function setPort($port)
    {
        $this->port = (int) $port;

        return $this;
    }

    /**
     * Получение порта
     *
     * @return integer
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * Проверка статуса ответа
     *
     * @param \Guzzle\Http\Message\Response $response
     * @throws \Project\Http\Exception\ErrorStatusCodeException
     * @return \Guzzle\Http\Message\Response
     */
    private static function validateResponseStatus(Response $response)
    {
        if ($response->isError()) {
            throw new ErrorStatusCodeException(
                "Error status code: ". $response->getStatusCode() .
                " with body: ". $response->getBody(true)
            );
        }

        return $response;
    }

    /**
     *
     * @param array $options
     * @param null|array|\Guzzle\Common\Collection $headers
     * @param null|string|resource|array|\Guzzle\Http\EntityBodyInterface $body
     * @return \Guzzle\Http\Message\RequestInterface
     */
    private function buildRequest(array $options = [], $headers = null, $body = null)
    {
        $request = Request::factory($this->httpMethod)
            ->setUri($this->getUrl())
            ->setHeaders($headers)
            ->setBody($body)
            ->setOptions($options)
            ->build($this->client);

        if ($this->getPort()) {
            $request->setPort($this->getPort());
        }

        return $request;
    }

    /**
     * Отправка Http запроса
     *
     * @param array $options
     * @param null|array|\Guzzle\Common\Collection $headers
     * @param null|string|resource|array|\Guzzle\Http\EntityBodyInterface $body
     *
     * @return \Guzzle\Http\Message\Response
     */
    public function send(array $options = [], $headers = null, $body = null)
    {
        $request = $this->buildRequest($options, $headers, $body);

        $response = $this->client->send($request);

        $response = self::validateResponseStatus($response);

        return $response;
    }

    /**
     * Отключить проверку SSL сертификата
     *
     * @param bool $value
     * @return \Project\Http\Client
     */
    public function setSslVerify($value)
    {
        $this->client->setDefaultOption('verify', (bool) $value);

        return $this;
    }

    /**
     * @param $subscriber
     * @return $this
     */
    public function addSubscriber($subscriber)
    {
        $this->client->addSubscriber($subscriber);
        return $this;
    }
}
