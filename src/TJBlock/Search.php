<?php

namespace Project\TJBlock;

use Project\Entity\TJBlock;
use Doctrine\Common\Collections\Criteria;
use Project\Entity\TJSite;
use Project\Entity\TJUser;

/**
 * Поиск TJBlock
 */
class Search
{
    /**
     * Поиск
     *
     * @param TJUser $user
     * @param TJSite $site
     * @return \Doctrine\Common\Collections\Collection|\Project\Entity\TJSite[]
     */
    public function process(TJUser $user = null, TJSite $site = null)
    {
        $criteria = (new Criteria());

        $criteria
            ->where(
                Criteria::expr()->eq('deleted', false)
            );

        if ($site !== null) {
            $criteria->andWhere(
                Criteria::expr()->eq('site', $site)
            );
        }

        if ($user !== null) {
            $criteria->andWhere(
                Criteria::expr()->eq('user', $user)
            );
        }

        return TJBlock::prepareList($criteria);
    }
}
