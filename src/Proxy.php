<?php

namespace Project;

use Doctrine\ORM\EntityManager;
use Monolog\Logger;
use Silex\Application as SilexApplication;
use Silex\ControllerCollection;
use Symfony\Component\Console\Application as ConsoleApplication;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator;

/**
 * Провайдер продоставляет доступ к разным объектам
 *
 * Посредством него решается задача избавлнения от зависимостей, IDE понимают с объектом какого класса идет работа
 * и это единственный способ получить инициированый в самом начале объект Silex\Application
 */
class Proxy
{

    /**
     * @var SilexApplication
     */
    private $app;

    /**
     * Синглтон
     *
     * @var Proxy
     */
    private static $instance;

    /**
     * Конструктор синглтона
     *
     * @param SilexApplication $app
     */
    private function __construct(SilexApplication $app)
    {
        $this->app = $app;
    }

    /**
     * Инициализация синглтона
     *
     * @param SilexApplication $app
     * @return Proxy
     */
    public static function init(SilexApplication $app = null)
    {
        if (self::$instance === null) {
            self::$instance = new self($app);
        }

        return self::$instance;
    }

    /**
     * @return SilexApplication
     */
    public function getApp()
    {
        return $this->app;
    }

    /**
     * @return Logger
     */
    public function getLogger()
    {
        return $this->app['custom_logger'];
    }

    /**
     * @return Validator
     */
    public function getValidator()
    {
        return $this->app['validator'];
    }

    /**
     * @return ControllerCollection
     */
    public function getControllers()
    {
        return $this->app['controllers_factory'];
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return $this->app['orm.em'];
    }

    /**
     * @return \Guzzle\Service\Client
     */
    public function getHttpClient()
    {
        return $this->app['guzzle.client'];
    }

    /**
     * Получение массива с конфигами проекта
     *
     * @return array
     */
    public function getConfig()
    {
        return $this->app['config'];
    }

    /**
     * Объект с данными запроса
     *
     * @return Request
     */
    public function getRequest()
    {
        return $this->app['request'];
    }

    /**
     * Получить объект memcache
     * для общих задач
     *
     * @return \Memcached
     */
    public function getMemcache()
    {
        return $this->app['memcached'];
    }

    /**
     * Получить клиента gearman
     *
     * @return \GearmanClient
     */
    public function getGearmanClient()
    {
        return $this->app['gearman.client'];
    }

    /**
     * Получить воркера gearman
     *
     * @return \GearmanWorker
     */
    public function getGearmanWorker()
    {
        return $this->app['gearman.worker'];
    }

    /**
     * Получить объект для работы с консолью
     *
     * @return ConsoleApplication
     */
    public function getConsole()
    {
        return $this->app['console'];
    }


    /**
     * Получить объект обработчика ошибок
     *
     * @return \Project\Exception\ErrorHandler
     */
    public function getErrorHandler()
    {
        return $this->app['error_handler'];
    }
}
